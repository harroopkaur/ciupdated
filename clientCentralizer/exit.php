<?php
session_start();                 // Comienza la sesion
$urlInicio = "../clientCentralizer/";

session_unset();                 // Vacia las variables de sesion
session_destroy();               // Destruye la sesion

header("location: " . $urlInicio);
?>
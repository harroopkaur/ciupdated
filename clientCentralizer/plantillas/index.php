    <div class="col-sm-3">
        <div class="row margenTop">
            <center><img src="<?= $GLOBALS["domain_root"] ?>/img/Logo.png" class="imgAjusteHorizontal" style="width:8vw;"></center>
        </div>
        
        <div class="row imagen">
            <center><img src="<?= $GLOBALS["domain_root"] ?>/img/cloudDownload.png" class="imgAjusteHorizontal pointer" style="width:9vw;"></center>
            <!--onclick='window.open("<?= $GLOBALS["domain_root1"] ?>/instaladores/setupCIM.msi");'-->
        </div>
    </div>

    <div class="col-sm-7">
        <div class="row margenTop">
            <div class="col-sm-10 col-sm-offset-1 divLogin">
                <form class="form-horizontal" role="form" method="post" action="authenticate.php">
                    <input type="hidden" id="entrar" name="entrar" value="1">
                    <div class="form-group contIndex">
                        <label for="inputEmail3" class="col-sm-4 control-label"><span class="glyphicon glyphicon-user"></span>&nbsp;User</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="login" name="login">
                        </div>
                    </div>
                    <div class="form-group contIndex">
                        <label for="inputPassword3" class="col-sm-4 control-label"><span class="glyphicon glyphicon-lock"></span>&nbsp;Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                    </div>
                    <!--<div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Company</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="cliente" name="cliente">
                                <option value="">--Select--</option>
                            </select>
                        </div>
                    </div>-->

                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-4 text-center">
                            <button type="submit" class="btn btn-default">Login</button>
                        </div>
                    </div>
                </form>

                <?php if (isset($_GET['error']) == 1) { ?>
                    <div align="center" class="error_prog">Invalid Login or password</div>
                <?php } ?>
            </div>
        </div>
    </div>
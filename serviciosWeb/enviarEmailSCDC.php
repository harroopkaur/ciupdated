<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validador_App.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_email.php");

$general = new General();
$validador = new validadorApp();
$objEmail = new email();

$result = 0;

$email = "";
if (isset($_POST["email"]) && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) !== false){
    $email = $general->get_escape($_POST["email"]);
}

$serial = $general->get_escape($_POST["serial"]);
$serialHHD = $general->get_escape($_POST["serialHHD"]);
$tipo = $general->get_escape($_POST["tipo"]);
$emailEnv = $general->get_escape($_POST["emailEnv"]);
$nombre = $general->get_escape($_POST["nombre"]);
$comentario = $general->get_escape($_POST["comentario"]);
$asunto = $general->get_escape($_POST["asunto"]);


$emialEnv = "m_acero_n@hotmail.com";
if ($validador->serialActivadoSmartControlDC($email, $serial) > 0){
    $result = 1;
    
    $row = $validador->verificarSerial2SmartControlDC($email, $serial, $serialHHD);
    if ($row["id"] > 0){
       if ($tipo == "Notification"){
           $objEmail->enviar_notificacion_SCDC($emailEnv, $asunto, $comentario);
       } else if ($tipo == "Upgrade"){
           $comentario1 = "Good Day Dear User,<br>
           We have successfully received your service extension request. We are evaluating that request and will soon receive response to it.<br>
           You can also contact us via telephone, Monday to Friday from 8am to 5pm, to the number(305) 851-3545.";
           $objEmail->enviar_notificacion_SCDC($emailEnv, "Notify Upgrade", $comentario);
           $objEmail->enviar_notificacion_SCDC($emailEnv, "Notify Upgrade", $comentario1);
       } else if ($tipo == "Notify Problem"){
           $comentario1 = "Good Day Dear User,<br>
           We have successfully received your request for technical support. We are evaluating that request and will soon receive response to it.<br>
           You can also contact us via telephone, Monday to Friday from 8am to 5pm, to the number(305) 851-3545.";
           $objEmail->enviar_notificacion_SCDC($emailEnv, "Notify Problem", $comentario);
           $objEmail->enviar_notificacion_SCDC($emailEnv, "Notify Problem", $comentario1);
       }
    } 
} 

$list[] = array('result'=>$result, "fechaIni"=>"", "fechaFin"=>"");

echo json_encode($list);
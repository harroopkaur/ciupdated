<?php
require_once("../clases/clase_general.php");
require_once("../results/clases/clase_modulo_servidores.php");

$general = new General();
$servidores = new moduloServidores();

$serial = stripslashes($_POST["var1"]);
$servidoresString = stripslashes($_POST["var2"]);
$cliente = stripslashes($_POST["var3"]);
$sqlString = stripslashes($_POST["var4"]);
$alineacionWindows = stripslashes($_POST["var5"]);
$alineacionSQL = stripslashes($_POST["var6"]);

$result = 0;
if($servidores->eliminarWindowServer1($cliente, $serial)){
    if($servidoresString == "()" || $servidores->agregarWindowServer1($servidoresString)){
        $result = 3;
        if($servidores->eliminarSqlServer1($cliente, $serial)){
            if($sqlString == "()" || $servidores->agregarSQLServer1($sqlString)){
                $result = 5;
                if($servidores->eliminarAlineaionWindowServer($cliente, $serial)){
                    if($alineacionWindows == "()" || $servidores->agregarAlineacionWindowServer1($alineacionWindows)){
                        $result = 7;
                        if($servidores->eliminarAlineaionSqlServer($cliente, $serial)){
                            if($alineacionSQL == "()" || $servidores->agregarAlineacionSQLServer1($alineacionSQL)){
                                $result = 1;
                            }else {
                                $result = 8;
                            }
                        }
                    }else {
                        $result = 6;
                    }
                }
            }else {
                $result = 4;
            }
        }
    } else{
        $result = 2;
    }
}

$array = array('var1'=>$result);

echo json_encode($array);
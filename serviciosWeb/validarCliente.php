<?php
require_once("../clases/clase_general.php");
require_once("../adminControl/clases/clase_clientes.php");

$general = new General();
$clientes = new Clientes();

$serial = "";
if(isset($_POST["valor1"])){
    $serial = $general->get_escape($_POST["valor1"]);
}

$clientes = $clientes->idCliente($serial);
if ($clientes == null){
    $clientes = 0;
}

$array = array('var1'=>$clientes);

echo json_encode($array);
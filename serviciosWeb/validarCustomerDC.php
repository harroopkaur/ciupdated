<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_clientesReseller.php");

$general = new General();
$validador = new clase_clientesReseller();

$result = 0;

$providerEmail = "";
if(isset($_POST["providerEmail"]) && filter_var($_POST["providerEmail"], FILTER_VALIDATE_EMAIL) !== false){
    $providerEmail = $_POST["providerEmail"];
}

$companyName = "";
if (isset($_POST["companyName"])){
    $companyName = $general->get_escape($_POST["companyName"]);
}

$serial = "";
if (isset($_POST["serial"])){
    $serial = $general->get_escape($_POST["serial"]);
}

/*if ($validador->existeCustomerDC($providerEmail, $companyName) > 0){
    $result = 1;
}*/

if ($validador->existeCustomerDCLicencia($providerEmail, $companyName, $serial) > 0){
    $result = 1;
}

$array = array('var1'=>$result);

echo json_encode($array);
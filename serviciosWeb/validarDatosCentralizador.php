<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
//require_once($GLOBALS["app_root1"] . "/clases/clase_clientesReseller.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validador_App.php");

$general = new General();
//$validador = new clase_clientesReseller();
$validador = new validadorApp();

$result = 0;
$id = 0;
$fecha = "1981-01-01";
//$nivelServicio = 0;
//$permisos = array();

$email = "";
if (isset($_POST["email"]) && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) !== false){
    $email = $general->get_escape($_POST["email"]);
}

$serial = "";
if (isset($_POST["serial"])){
    $serial = $general->get_escape($_POST["serial"]);
}

//$serialHDD = $general->get_escape($_POST["serialHDD"]);

//$arraySerialHDD = explode("*", $serialHDD);

if ($validador->serialActivadoSmartControlDC($email, $serial) > 0){
    $result = 1;
    
    $row = $validador->verificarSerialActivadoCentralizadorDC($email, $serial);
    if ($row["id"] > 0){
        //$fechaIni = $row["fechaIniCentralizador"];
        //$fechaFin = $row["fechaFinCentralizador"];
        $id = $row["id"];
        $fecha = $row["fechaFinCentralizador"];
    } else{
        $result = 2;
    }
} 
$array = array('var1'=>$result, 'var2'=>$id, 'var3'=>$fecha);

echo json_encode($array);
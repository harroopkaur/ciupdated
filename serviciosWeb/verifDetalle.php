<?php
require_once("../clases/clase_general.php");
require_once("../results/clases/clase_detalles_equipo2.php");

$general = new General();
$detalle = new DetallesE_f();

$serial = stripslashes($_POST["var1"]);
$detalleString = stripslashes($_POST["var2"]);
$cliente = stripslashes($_POST["var3"]);

$result = 0;
if($detalle->eliminar($cliente, $serial)){
    if($detalleString == "()" || $detalle->insertar1($detalleString)){
        $result = 1;
    } else{
        $result = 2;
    }
}

$array = array('var1'=>$result);

echo json_encode($array);
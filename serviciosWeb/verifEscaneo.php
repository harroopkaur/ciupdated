<?php
require_once("../clases/clase_general.php");
require_once("../results/clases/clase_escaneo2.php");

$general = new General();
$escaneo = new Scaneo_f();

$serial = stripslashes($_POST["var1"]);
$escaneoString = stripslashes($_POST["var2"]);
$cliente = stripslashes($_POST["var3"]);

$result = 0;
if($escaneo->eliminar($cliente, $serial)){
    if($escaneoString == "()" || $escaneo->insertar1($escaneoString)){
        $result = 1;
    } else{
        $result = 2;
    }
}

$array = array('var1'=>$result);

echo json_encode($array);
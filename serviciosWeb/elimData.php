<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_centralizador.php");

$general = new General();
$centralizador = new clase_centralizador();

$idAgente = 0;
if(isset($_POST["params"]) && filter_var($_POST["params"], FILTER_VALIDATE_INT) !== false){
    $idAgente = $_POST["params"];
}

$result = 0;
if($centralizador->eliminarDatos($idAgente)){
    $result = 1;
}

$array = array("result"=>$result);
echo json_encode($array);
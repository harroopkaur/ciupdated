<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validador_App.php");

$general = new General();
$validador = new validadorApp();
$email = "";
if (isset($_POST["email"]) && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) !== false){
    $email = $general->get_escape($_POST["email"]);
}

$serial = $general->get_escape($_POST["serial"]);

$serialHHD = $general->get_escape($_POST["serialHHD"]);

$fechaIni = "1981-01-01";
$fechaFin = "1981-01-01";
//$nivelServicio = 0;

$row = $validador->verificarSerialSmartControlDC($email, $serial);
if ($row["id"] > 0){
    if ($validador->activarSerial1SmartControlDC($row["id"], $serialHHD)){
        $result = 1;
        $fechaIni = $row["fechaIniCentralizador"];
        $fechaFin = $row["fechaFinCentralizador"];
    }
} else{
    $row1 = $validador->verificarSerial2SmartControlDC($email, $serial, $serialHHD);
        
    if($row1["id"] > 0){
        $fechaIni = $row1["fechaIniCentralizador"];
        $fechaFin = $row1["fechaFinCentralizador"];
        
        $result = 1;
    } else{
        $result = 2;
    }
}

$list[] = array('result'=>$result, "fechaIni"=>$fechaIni, "fechaFin"=>$fechaFin);

echo json_encode($list);
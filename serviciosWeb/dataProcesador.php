<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_centralizador.php");

$general = new General();
$centralizador = new clase_centralizador();

$codigo = "";
if(isset($_POST["codigo"])){
    $codigo = $general->get_escape($_POST["codigo"]);
}

/*$reseller = $centralizador->obtenerReseller($codigo);

$listado = $centralizador->dataProcesador($reseller);*/  

$licencia = $centralizador->obtenerLicenciaReseller($codigo);

$listado = $centralizador->dataProcesadorLicencia($licencia);  

$list = array();
foreach($listado as $row){
    $list[] = array('id_agente'=>$row["id_agente"], "tx_dato_contrl"=>$row["tx_dato_contrl"], "tx_host_name"=>$row["tx_host_name"],
    "tx_tipo_cpu"=>$row["tx_tipo_cpu"], "tx_nu_cpu"=>$row["tx_nu_cpu"], "tx_nu_cores"=>$row["tx_nu_cores"], 
    "tx_procsd_logico"=>$row["tx_procsd_logico"], "tx_tipo_escano"=>$row["tx_tipo_escano"]);
}

echo json_encode($list);

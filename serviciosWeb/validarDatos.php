<?php
//require_once("../clases/clase_general_licenciamiento.php");
require_once("../clases/clase_general.php");
require_once("../clases/clase_validador_App.php");

//$general = new clase_general_licenciamiento();
$general = new General();
$validador = new validadorApp();

$result = 0;
$id = 0;
$fecha = "1981-01-01";
$nivelServicio = 0;
$permisos = array();

$email = "";
if (isset($_POST["email"]) && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) !== false){
    $email = $general->get_escape($_POST["email"]);
}

$app = 1;
if (isset($_POST["app"]) && filter_var($_POST["app"], FILTER_VALIDATE_INT) !== false){
    $app = $general->get_escape($_POST["app"]);
}

$serial = $general->get_escape($_POST["serial"]);

$serialHHD = $general->get_escape($_POST["serialHHD"]);

$arraySerialHHD = explode("*", $serialHHD);

if ($validador->serialActivado($email, $serial) > 0){
    $result = 1;
    
    //$id = $validador->verificarSerial1($email, $serial);
    for ($index = 0; $index < count($arraySerialHHD); $index++){
        $row = $validador->verificarSerial2($email, $serial, $arraySerialHHD[$index]);
        if ($row["id"] > 0){
            $id = $row["id"];
            $fecha = $row["fechaFin"];
            $nivelServicio = $row["nivelServicio"];
            break;
        }
    }

    if ($row["id"] == 0){
        $result = 2;
    }
} 
$array = array('var1'=>$result, 'var2'=>$id, 'var3'=>$fecha, 'var4'=>$nivelServicio);

echo json_encode($array);
<?php
//require_once("../clases/clase_general_licenciamiento.php");
require_once("../clases/clase_general.php");
require_once("../clases/clase_validador_App.php");

//$general = new clase_general_licenciamiento();
$general = new General();
$validador = new validadorApp();

$result = 0;
$permisos = array();

$email = "";
if (isset($_POST["email"]) && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) !== false){
    $email = $general->get_escape($_POST["email"]);
}

$serial = $general->get_escape($_POST["serial"]);

$serialHHD = $general->get_escape($_POST["serialHHD"]);

$arraySerialHHD = explode("*", $serialHHD);

$id = 0;
$fecha = "1981-01-01";
$nivelServicio = 0;

$row = $validador->verificarSerial($email, $serial);
if ($row["id"] > 0){
    if ($validador->activarSerial1($row["id"], $arraySerialHHD[0])){
        $result = 1;
        $id = $row["id"];
        $fecha = $row["fechaFin"];
        $nivelServicio = $row["nivelServicio"];
    }
} else{
    for ($index = 0; $index < count($arraySerialHHD); $index++){
        $row1 = $validador->verificarSerial2($email, $serial, $arraySerialHHD[$index]);
        if ($row1["id"] > 0){
            break;
        }
    }
    
    if($row1["id"] > 0){
        $id = $row1["id"];
        $fecha = $row1["fechaFin"];
        $nivelServicio = $row1["nivelServicio"];
        $result = 1;
    } else{
        $result = 2;
    }
}

$array = array('var1'=>$result, 'var2'=>$id, 'var3'=>$fecha, 'var4'=>$nivelServicio);

echo json_encode($array);
<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_centralizador.php");

$general = new General();
$centralizador = new clase_centralizador();

$datos = json_decode($_POST["data"], true);
$datosParam = json_decode($_POST["param"], true);

$centralizador->procesar($datos, $datosParam["idAgente"], $datosParam["tabla"]);
//$centralizador->insertarHistorico($datosParam["idAgente"]);

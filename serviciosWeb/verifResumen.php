<?php
require_once("../clases/clase_general.php");
require_once("../results/clases/clase_resumen_offices.php");

$general = new General();
$resumen = new Resumen_Of();

$serial = stripslashes($_POST["var1"]);
$resumenString = stripslashes($_POST["var2"]);
$cliente = stripslashes($_POST["var3"]);

$result = 0;
if($resumenString == "()" || $resumen->eliminar($cliente, $serial)){
    if($resumen->insertar1($resumenString)){
        $result = 1;
    } else{
        $result = 2;
    }
}

$array = array('var1'=>$result);

echo json_encode($array);
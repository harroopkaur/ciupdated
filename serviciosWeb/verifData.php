<?php
require_once("../clases/clase_general.php");
require_once("../results/clases/clase_resumen_offices.php");
require_once("../results/clases/clase_detalles_equipo2.php");
require_once("../results/clases/clase_escaneo2.php");
require_once("../results/clases/clase_balance2.php");
require_once("../results/clases/clase_modulo_servidores.php");
require_once("../results/clases/clase_compras2.php");
require_once("../results/clases/clase_balance2.php");

$general = new General();
$resumen = new Resumen_Of();
$detalle = new DetallesE_f();
$escaneo = new Scaneo_f();
$servidores = new moduloServidores();
$compras = new Compras_f();
$balanza = new Balance_f();

$serial = stripslashes($_POST["var1"]);
$cliente = stripslashes($_POST["var2"]);

$result = 0;
if($resumen->eliminar($cliente, $serial)){
    $result = 2;
    if($detalle->eliminar($cliente, $serial)){
        $result = 3;
        if($escaneo->eliminar($cliente, $serial)){
            $result = 4;
            if($servidores->eliminarWindowServer1($cliente, $serial)){
                $result = 5;
                if($servidores->eliminarSqlServer1($cliente, $serial)){
                    $result = 6;
                    if($servidores->eliminarAlineaionWindowServer($cliente, $serial)){
                        $result = 7;
                        if($servidores->eliminarAlineaionSqlServer($cliente, $serial)){
                            $result = 8;
                            if($compras->eliminar($cliente, $serial)){
                                $result = 9;
                                if($balanza->eliminar1($cliente, $serial)){
                                    $result = 1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

$array = array('var1'=>$result);

echo json_encode($array);
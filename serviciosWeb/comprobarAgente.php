<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_clientesReseller.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_centralizador.php");

$general = new General();
$validador = new clase_clientesReseller();
$centralizador = new clase_centralizador();

$result = 0;

$hostName = "";
if(isset($_POST["hostName"])){
    $hostName = $general->get_escape($_POST["hostName"]);
}

$serialDisk = "";
if(isset($_POST["serialDisk"])){
    $serialDisk = $general->get_escape($_POST["serialDisk"]);
}

$ip = "";
if(isset($_POST["ip"])){
    $ip = $general->get_escape($_POST["ip"]);
}

$providerEmail = "";
if(isset($_POST["providerEmail"])){
    $providerEmail = $general->get_escape($_POST["providerEmail"]);
}

$companyName = "";
if(isset($_POST["companyName"])){
    $companyName = $general->get_escape($_POST["companyName"]);
}

$serial = "";
if(isset($_POST["serial"])){
    $serial = $general->get_escape($_POST["serial"]);
}

$row = $validador->customerDCWeb($providerEmail, $companyName);

$licencia = $centralizador->obtenerLicenciaReseller($serial);

$idAgente = 0;
$result = 0;
if(count($row["id"]) > 0){
    //$row1 = $validador->datosAgente($hostName, $serialDisk);
    $row1 = $validador->datosAgenteLicencia($hostName, $serialDisk, $licencia);
    
    if(count($row1["id"]) == 0){
        if($validador->insertarAgenteWebLicencia($row["id"], $hostName, $serialDisk, $ip, $licencia)){
            $idAgente = $validador->ultIdAgenteWeb();
            $result = 1;
            
            
        } 
    } else{
        $idAgente = $row1["id"];
        $result = 3;
    }
} else{
    $result = 2;
}

$array = array('var1'=>$result, 'var2'=>$idAgente);

echo json_encode($array);
<?php
require_once("../clases/clase_general.php");
require_once("../results/clases/clase_balance2.php");

$general = new General();
$balanza = new Balance_f();

$serial = stripslashes($_POST["var1"]);
$balanzaString = stripslashes($_POST["var2"]);
$cliente = stripslashes($_POST["var3"]);

$result = 0;
if($balanza->eliminar1($cliente, $serial)){
    if($balanzaString == "()" || $balanza->insertar1($balanzaString)){
        $result = 1;
    } else{
        $result = 2;
    }
}

$array = array('var1'=>$result);

echo json_encode($array);
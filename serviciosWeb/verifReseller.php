<?php
require_once("../clases/clase_general.php");
require_once("../adminControl/clases/clase_empresa_llaves.php");
require_once("../adminControl/clases/clase_empresas.php");

$general = new General();
$llaves = new clase_empresa_llaves();
$empresas = new empresas();

$serialHHD = stripslashes($_POST["var1"]);
$codigo = stripslashes($_POST["var2"]);

$id = $llaves->idCliente($serialHHD);
$idReseller = $empresas->idReseller($codigo);

$result = 0;
if($id > 0 && $idReseller > 0){
    $empresas->eliminarRelacionReseller($id);
    
    if($empresas->agregarRelacionReseller($id, $idReseller)){
        $result = 1;
    }
}

$array = array('var1'=>$result);

echo json_encode($array);
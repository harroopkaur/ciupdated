<?php
require_once("../clases/clase_general.php");
require_once("../results/clases/clase_compras2.php");

$general = new General();
$compras = new Compras_f();

$serial = stripslashes($_POST["var1"]);
$comprasString = stripslashes($_POST["var2"]);
$cliente = stripslashes($_POST["var3"]);

$result = 0;
if($compras->eliminar($cliente, $serial)){
    if($comprasString == "()" || $compras->insertar1($comprasString)){
        $result = 1;
    } else{
        $result = 2;
    }
}

$array = array('var1'=>$result);

echo json_encode($array);
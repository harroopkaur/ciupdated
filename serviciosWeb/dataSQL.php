<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_centralizador.php");

$general = new General();
$centralizador = new clase_centralizador();

$codigo = "";
if(isset($_POST["codigo"])){
    $codigo = $general->get_escape($_POST["codigo"]);
}

/*$reseller = $centralizador->obtenerReseller($codigo);

$listado = $centralizador->dataSQL($reseller);*/   

$licencia = $centralizador->obtenerLicenciaReseller($codigo);

$listado = $centralizador->dataResultadoEscaneoLicencia($licencia); 

$list = array();
foreach($listado as $row){
    $list[] = array('id_agente'=>$row["id_agente"], "tx_dato_contrl"=>$row["tx_dato_contrl"],
    "tx_host_name"=>$row["tx_host_name"], "tx_llave_regist"=>$row["tx_llave_regist"], "tx_edicon"=>$row["tx_edicon"],
    "tx_verson"=>$row["tx_verson"], "tx_ruta_instlc"=>$row["tx_ruta_instlc"]);
}

echo json_encode($list);

<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_centralizador.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_clientesReseller.php");

$general = new General();
$centralizador = new clase_centralizador();

$codigo = "";
if(isset($_POST["codigo"])){
    $codigo = $general->get_escape($_POST["codigo"]);
}

/*$reseller = $centralizador->obtenerReseller($codigo);

$agenteCliente = $centralizador->agenteClienteReseller($reseller);*/

$licencia = $centralizador->obtenerLicenciaReseller($codigo);

$reseller = $centralizador->obtenerIdReseller($codigo);

$agenteCliente = $centralizador->agenteClienteResellerLicencia($reseller, $licencia);

$array = array();
foreach($agenteCliente as $row){
    $array[] = array('agente'=>$row["agente"], 'cliente'=>$row["cliente"]);
}

echo json_encode($array);

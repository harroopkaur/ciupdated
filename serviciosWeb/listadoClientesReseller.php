<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_centralizador.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_clientesReseller.php");

$general = new General();
$centralizador = new clase_centralizador();
$clientes = new clase_clientesReseller();

$codigo = "";
if(isset($_POST["codigo"])){
    $codigo = $general->get_escape($_POST["codigo"]);
}

//$reseller = $centralizador->obtenerReseller($codigo);

$reseller = $centralizador->obtenerIdReseller($codigo);

$listado = $clientes->clientesReseller($reseller);

$list = array();
foreach($listado as $row){
    $list[] = array('id'=>$row["id"], 'cliente'=>$row["nombreEmpresa"]);
}

echo json_encode($list);

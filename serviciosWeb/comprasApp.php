<?php
require_once("../clases/clase_general.php");
require_once("../clases/clase_validador_App.php");
require_once("../results/clases/clase_compras2.php");

$general = new General();
$validador = new validadorApp();
$compras = new Compras_f();

$serial = $general->get_escape($_POST["serial"]);
$comprasString = stripslashes($_POST["compras"]);

$result = 0;
if($compras->insertar1($comprasString)){
    $result = 1;
}

$array = array('result'=>$result);

echo json_encode($array);
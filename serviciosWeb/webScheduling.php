<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validador_App.php");

$general = new General();
$validador = new validadorApp();

$nombScheduling = "";
if(isset($_POST["var1"])){
    $nombScheduling = $general->get_escape($_POST["var1"]);
}

$emailProvider = "";
if(isset($_POST["var2"])){
    $emailProvider = $general->get_escape($_POST["var2"]);
}

$companyCustomerName = "";
if(isset($_POST["var3"])){
    $companyCustomerName = $general->get_escape($_POST["var3"]);
}

$row = $validador->verificarAgenteDC($emailProvider, $companyCustomerName);

$array = array();
if(count($row["id"]) > 0){
    $row1 = $validador->verifNombScheduling($nombScheduling, $row["cliente"]);
    if($row1["cantidad"] > 0){
        $tabla = $validador->dataScheduling($nombScheduling, $row["cliente"]);
       
        foreach($tabla as $row){
            $array[] = array('tx_descrip'=>$row["tx_descrip"], 'fe_fecha_inicio'=>$row["fe_fecha_inicio"], 'tx_schedule'=>$row["tx_schedule"],
            'int_repeat_days'=>$row["int_repeat_days"], 'int_repeat_weeks'=>$row["int_repeat_weeks"], 'tx_days'=>$row["tx_days"],
            'tx_months'=>$row["tx_months"], 'num_days'=>$row["num_days"]);
        }
    } else{
        $array[] = array('tx_descrip'=>"");
    }
} else{
    $array[] = array('tx_descrip'=>"");
}

echo json_encode($array);
<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validador_App.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_centralizador.php");

$general = new General();
$validador = new validadorApp();
$centralizador = new clase_centralizador();

$email = "";
if (isset($_POST["email"]) && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) !== false){
    $email = $general->get_escape($_POST["email"]);
}

$serial = $general->get_escape($_POST["serial"]);

$serialHHD = $general->get_escape($_POST["serialHHD"]);

$fechaIni = "1981-01-01";
$fechaFin = "1981-01-01";
//$nivelServicio = 0;

$result = 0;
$row = $validador->verificarSerial2SmartControlDC($email, $serial, $serialHHD);
        
if($row["id"] > 0){
    $row1 = $centralizador->agentesWebActivos($serial, $serialHHD);
    $cantidad = $row1[0]["cantidad"];
    $cantidadActiva = $row1[0]["cantidadActiva"];
    $result = 1;
}

$list[] = array('result'=>$result, "cantidad"=>$cantidad, "cantidadActiva"=>$cantidadActiva);

echo json_encode($list);
<?php
require_once("../centralizer/configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validador_App.php");

$validador = new validadorApp();
$general = new General();

$result = 0;

$providerEmail = "";
if(isset($_POST["providerEmail"])){
    $providerEmail = $general->get_escape($_POST["providerEmail"]);
}

$companyCustomerName = "";
if(isset($_POST["companyCustomerName"])){
    $companyCustomerName = $general->get_escape($_POST["companyCustomerName"]);
}

$scheduling = "";
if(isset($_POST["schedulingName"])){
    $scheduling = $general->get_escape($_POST["schedulingName"]);
}

//$nivelServicio = 0;

if ($validador->verificarSchedulingName($providerEmail, $companyCustomerName, $scheduling) > 0){
    $result = 1;
} 

$array = array('var1'=>$result);

echo json_encode($array);
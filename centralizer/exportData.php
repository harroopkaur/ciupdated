<?php
require_once("configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/procesos/exportarData.php");
require_once($GLOBALS["app_root"] . "/plantillas/head.php");
require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="contenedorCuerpoAdmin">
        <div class="contenedorCentralInterno">
            <div class="contenedorMenuInterno">
                <?php 
                $menuCentralizador = 5;
                include_once($GLOBALS["app_root1"] . "/centralizer/plantillas/menu.php");
                ?>
            </div>

            <div class="bordeContenedor">
                <div class="contenido">
                    <?php include_once($GLOBALS["app_root1"] . "/centralizer/plantillas/exportarData.php"); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
include_once($GLOBALS["app_root"] . "/plantillas/foot.php");
?>
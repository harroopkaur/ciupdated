<!--<div class="containerPersonal" style="margin-top:20px; border: 1px solid; ">-->
<div class="containerP" style="margin-top:20px;">
    <div class="row">
        <div class="col-sm-3">
            <div class="row">
                <img src="<?= $GLOBALS["domain_web_root"] ?>/img/circuloSmartControl.png" class='img-circulo'>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-6">
            <div class="row">
                <form role="form" id="form1" name="form1" method="post" action="results/authenticate.php">
                    <input type="hidden" class="form-control" name="entrar" id="entrar" value="1">
                    <div class="form-group">
                        <input type="text" class="form-control" name="login" id="login" placeholder="User">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="contrasena" id="contrasena" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <select name="company" id="company" class="form-control"> 
                            <option value="">Company</option>
                        </select>
                    </div>
                    <button type="submit" class="btn botonGris form-control">Log On</button>
                </form>
                
                <?php if (isset($_GET['error']) == 1) { ?>
                    <div align="center" class="error_prog">Invalid Login or password</div>
                <?php } ?>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-2 col-sm-offset-3 circleTop">
            <div class="circulo1 backColor0000">
                <div class="centrado">
                    <p class="color0000 p1">1</p>
                    <p class="color0000">Deployment & Licenses</p>
                </div>
            </div>
        </div>
        
        <div class="col-sm-2 circleTop">
            <div class="circulo1 backColor0001">
                <div class="centrado">
                    <p class="color0001 p1">2</p>
                    <p class="color0001">Manage Software</p>
                </div>
            </div>
        </div>
        
        <div class="col-sm-2 circleTop">
            <div class="circulo1 backColor002">
                <div class="centrado">
                    <p class="color0001 p1">3</p>
                    <p class="color0001">Remote Assistance</p>
                </div>
            </div>
        </div>        
    </div>
</div>

<div class="containerP1">         
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <br>
                    <h2 class="bold text-center color0000">All in One Optimization</h2>
                </div>
            </div>
        </div>

        <div class='row content-principal'>
            <div class='col-sm-12 imgDivFondo'>
                <img src="img/fondoAzul.png" style="width:100%; height:100%;">
            </div>
    
            <div class='col-sm-12 imgDivFondo1'>
                <div class="row" style="position:relative;">
                    <div class='col-sm-10 col-sm-offset-1 centrado'>
                        <div class="row">
                            <!--inicio contenido central-->
                            <div class='col-sm-3'>
                                <div class="col-sm-12 content-info">
                                    <div class="row">
                                        <div class='interno'>
                                            <div class='interno-head'>
                                                <img src="<?= $GLOBALS["domain_web_root"] ?>/img/discovery.png" align="right">
                                                <h3> <span class='bold'>Discovery</span></h3> 
                                            </div>

                                            <div class='col-sm-12 interno-content'>
                                                <p class="text-center tituloSAMDiagnostic2">Detects aplications &<br>
                                                Software unused</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class='col-sm-3'>
                                <div class="col-sm-12 content-info">
                                    <div class="row">
                                        <div class='interno'>
                                            <div class='interno-head'>
                                                <img src="<?= $GLOBALS["domain_web_root"] ?>/img/contract.png" align="right">
                                                <h3> <span class='bold'>Licenses</span></h3> 
                                            </div>

                                            <div class='col-sm-12 interno-content'>
                                                <p class="text-center tituloSAMDiagnostic2">Admin, plans &<br>
                                                organizes licensing<br>
                                                contracts</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class='col-sm-3'>
                                <div class="col-sm-12 content-info">
                                    <div class="row">
                                        <div class='interno'>
                                            <div class='interno-head'>
                                                <img src="<?= $GLOBALS["domain_web_root"] ?>/img/reports.png" class="horizontal" align="right">
                                                <h3> <span class='bold'>Reports</span></h3> 
                                            </div>

                                            <div class='col-sm-12 interno-content'>
                                                <p class="text-center tituloSAMDiagnostic2">Generates reports  usability,<br>
                                                optimization, duplicate applications, unused software</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class='col-sm-3'>
                                <div class="col-sm-12 content-info">
                                    <div class="row">
                                        <div class='interno'>
                                            <div class='interno-head'>
                                                <img src="<?= $GLOBALS["domain_web_root"] ?>/img/imgSPLA.png" class="horizontal" align="right">
                                                <h3> <span class='bold'>KPI´s</span></h3> 
                                            </div>

                                            <div class='col-sm-12 interno-content'>
                                                <p class="text-center tituloSAMDiagnostic2">Performance indicators that ensures software optimization and control</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-1 arrowRight">
                        <div class="centrado">
                            <span class="glyphicon glyphicon-chevron-right pointer btnArrow" id='siguiente'></span>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>

        <div class="col-sm-2 col-sm-offset-10">
            <div class="row">
                <br>
                <img src="<?= $GLOBALS["domain_web_root"] ?>/img/freeTrial.png" class='img-download' style="width:100%; height:auto;">
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#siguiente").click(function(){
            location.href = "discovery.php";
        });
        
        $("#login, #contrasena").blur(function(){
           $.post("results/ajax/verificarEmpresa.php", { login : $("#login").val(), contrasena : $("#contrasena").val() }, function(data){
                $("#company").empty();
                $("#company").append(data[0].combo);
            }, "json")
            .fail(function(jqXHR){
                $.alert.open('error', "Error: " + jqXHR.status);
            }); 
        });
    });
</script>
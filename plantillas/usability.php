<div class="containerPersonal">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <span class="glyphicon glyphicon-chevron-left desplazamiento btnArrow" id='atras'></span>
                    <div class="text-center">
                        <h2 class="bold text-center" style="display:inline-block;">Usability Report</h2>
                        <img src="<?= $GLOBALS["domain_web_root"] ?>/img/reports.png" style="display:inline-block;width: 8vw; height:auto;" align="l">
                    </div>
                    <span class="glyphicon glyphicon-chevron-right desplazamientoRight btnArrow" id='siguiente'></span>
                </div>
            </div>
        </div>

        <div class='row'>
            <div class='col-sm-12'>
                <div class='col-sm-12 text-center'>
                    <img src="<?= $GLOBALS["domain_web_root"] ?>/img/usabilityScreen.png" class="img img-responsive" style="width:55vw; margin:0 auto;">
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#siguiente").click(function(){
            location.href = "balance.php"
        });
        
        $("#atras").click(function(){
            location.href = "coverage.php"; 
        });
    })
</script>
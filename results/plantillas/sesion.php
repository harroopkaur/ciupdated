<?php
// Verificar inicio sesion
if(!$_SESSION['client_autorizado']) {
    $urlInicio = $GLOBALS['domain_root1'].'/index.php';
    if($_SESSION["idioma"] == 2){
        $urlInicio = $GLOBALS['domain_root1'].'/index.php?idioma=2';
    }
	echo '<script language="javascript" type="text/javascript">';
        
        if ($_SESSION["idioma"] == 1){
            echo '	alert("¡Usted debe Iniciar Sesión!");';
        } else if($_SESSION["idioma"] == 2){
            echo '	alert("You must Log In!");';
        }
	//echo '	alert("' . $general->getDebeIniSesion($_SESSION["idioma"]) . '");';
	echo '	location.href="' . $urlInicio . '";';
	echo '</script>';
}

// Verificar tiempo de sesion
$tiempo_sesion = time() - $_SESSION['client_tiempo'];
if($tiempo_sesion > $TIEMPO_MAXIMO_SESION) {
	echo '<script language="javascript" type="text/javascript">';
        
        if ($_SESSION["idioma"] == 1){
            echo '	alert("¡Usted pasó mucho tiempo inactivo!");';
        } else if($_SESSION["idioma"] == 2){
            echo '	alert("You spent a lot of time inactive!");';
        }
	//echo '	alert("' . $general->getInactivo($_SESSION["idioma"]) . '");';
	echo '	location.href="'.$GLOBALS['domain_root'].'/plantillas/salida.php";';
	echo '</script>';
} else {
	$_SESSION['client_tiempo'] = time();
}
?>

<?php
session_start();                 // Comienza la sesion
$urlInicio = "../../index.php";
if ($_SESSION["idioma"] == 2){
    $urlInicio = "../../index.php?idioma=2";
} 

session_unset();                 // Vacia las variables de sesion
session_destroy();               // Destruye la sesion

header("location: " . $urlInicio);
//header("location: ../index.php");
?>
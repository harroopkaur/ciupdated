<!DOCTYPE html>
<html>
    <head>
        <title>.:Smart Control Results:.</title>
        <link rel="shortcut icon" href="<?= $GLOBALS["domain_root1"] ?>/img/Logo.ico">
        
        <!-- Custom Theme files -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Custom Theme files -->
        <link href="<?=$GLOBALS['domain_root1']?>/plugin-alert/css/alert.min.css" rel="stylesheet" />
        <link href="<?=$GLOBALS['domain_root1']?>/plugin-alert/themes/default/theme.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
        <link href="<?=$GLOBALS['domain_root1']?>/css/estilos.css" rel="stylesheet" />
        <link href="<?=$GLOBALS['domain_root']?>/css/estilos.css" rel="stylesheet" />
        
        <!--Google Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!--Google Fonts-->

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?=$GLOBALS["domain_root1"]?>/font-awesome/css/font-awesome.min.css" type="text/css" />

        <link href="<?=$GLOBALS['domain_root']?>/css/dark-unica.css" rel="stylesheet" />

        <!--<link rel="stylesheet" href="<?= $GLOBALS['domain_root'] ?>/css/example.css" type="text/css" />-->
        <link rel="stylesheet" href="<?= $GLOBALS['domain_root1'] ?>/css/jquery.countdown.timer.css" type="text/css" />

        <script language="javascript" type="text/javascript" src="<?=$GLOBALS['domain_root1']?>/js/funciones_generales.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        
        <?php if($_SESSION["idioma"] == 1){ 
        ?>
            <script src="<?=$GLOBALS['domain_root']?>/js/datepicker-es.js"></script>
        <?php
        }
        ?>

        <script src="<?=$GLOBALS['domain_root1']?>/js/jquery.numeric.js"></script>

        <script type="text/javascript" src="<?=$GLOBALS['domain_root1']?>/js/jquery.tablesorter.js"></script>
        <script type="text/javascript" src="<?=$GLOBALS['domain_root1']?>/js/tableHeadFixer.js"></script>
        <script src="<?=$GLOBALS['domain_root1']?>/plugin-alert/js/alert.js"></script>
        <script src="<?=$GLOBALS['domain_root']?>/js/cabecera.js"></script>
        
        <script src="https://code.highcharts.com/js/highcharts.js"></script>
        <script src="https://code.highcharts.com/js/modules/exporting.js"></script>
        <script src="<?=$GLOBALS['domain_root1']?>/js/jquery.pluginCountdown.min.js"></script>
        <script src="<?=$GLOBALS['domain_root1']?>/js/jquery.countdown.min.js"></script>
    </head>
    <body>
        <div class="fondo" id="fondo" style="display:none;">
            <img src="<?= $GLOBALS["domain_root1"] ?>/img/loading.gif" style="width:50px; height:50px; top:50%; margin-top:-25px; left:50%; margin-left:-25px; position:absolute;">
        </div>
        
        <div class="fondo1" id="fondo1"></div>
        
        <audio id="player" src="<?= $GLOBALS['domain_root1'] ?>/sonidos/ping.mp3"></audio>
        <div id="clock" class="hide"></div>
        
        <script>
            $(function () {
                var shortly = new Date(); 
                shortly.setSeconds(shortly.getSeconds() + <?= $general->tiempoLimite ?>); 
                $('#clock').countdown({until: shortly, onExpiry: liftOff}); 
            });
            
            function liftOff() { 
                $.alert.open('confirm', 'Confirmar', '<?= $lib->lg0004 ?>', {'<?= $lib->lg0002 ?>' : '<?= $lib->lg0002 ?>', '<?= $lib->lg0003 ?>' : '<?= $lib->lg0003 ?>'}, function(button){
                    if(button === 'No'){
                        $("#fondo").show();
                        $.post("<?= $GLOBALS["domain_root"] ?>/ajax/finalizarSesion.php", "", function(){
                            location.href="<?= $GLOBALS["domain_root1"] ?>";
                        }, 'json')
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'}, function(){
                                location.href="<?= $GLOBALS["domain_root1"] ?>";
                            });
                        });
                    } else{
                        $("#fondo").show();
                        $.post("<?= $GLOBALS["domain_root"] ?>/ajax/renovarSesion.php", "", function(){
                            $("#fondo").hide();
                            $.alert.open('info', 'Renewed session', {'Ok' : 'Ok'});
                            iniciarClock();
                        }, 'json')
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'});
                        });
                    }
                });
                document.getElementById('player').play();
            }
            
            function iniciarClock(){
                shortly = new Date(); 
                shortly.setSeconds(shortly.getSeconds() + <?= $general->tiempoLimite ?>); 
                $('#clock').countdown('option', {until: shortly}); 
            }
        </script>
<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root"] . "/librerias/idioma_en.php");

$lib = new idioma_web_adminControl_en();

require_once($GLOBALS["app_root"] . "/plantillas/sesion.php");
require_once($GLOBALS["app_root"] . "/microsoft/procesos/detalle.php");
require_once($GLOBALS["app_root"] . "/plantillas/head.php");
require_once($GLOBALS['app_root'] . "/plantillas/cabecera2.php");
?>

<section class="contenedor">
    <div class="divContenedor">
        <div class="divMenuContenido">
            <?php
            $menuMicrosoft = 4;
            include_once($GLOBALS['app_root'] . "/microsoft/plantillas/menu.php");
            ?>
        </div>

        <div class="bordeContenedor">
            <div class="contenido">
                <?php 
                    include_once($GLOBALS["app_root"] . "/microsoft/graficos/detalle.php"); 
                    include_once($GLOBALS["app_root"] . "/microsoft/plantillas/detalle.php");  
                ?>  
            </div>
        </div>
    </div>
</section>
<?php
require_once($GLOBALS['app_root'] . "/plantillas/foot.php");
?>
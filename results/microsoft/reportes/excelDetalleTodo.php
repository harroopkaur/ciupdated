<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
//error_reporting(E_ALL);
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resultados_general.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = "¡Usted debe Iniciar Sesión!";
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}
if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli') die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../../assets/lib/Excel/PHPExcel.php';
    //require_once $_SERVER['DOCUMENT_ROOT']."/licensingassurance.com/assets/lib/Excel/PHPExcel.php"

    $detalles = new  DetallesE_f();
    $resumen   = new Resumen_f();
    $claseResultadosGeneral = new clase_resultados_general();
    
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Smart Control")
                                 ->setTitle("Clientes");
    
    $asignaciones = array(); //$general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
    $asig = "";
    if(isset($_GET["asig"])){
        $asig = $general->get_escape($_GET["asig"]);
    }
    
    $dup = "Si";
    if(isset($_GET["dup"]) && $_GET["dup"] == "No"){
        $dup = "No";
    }
    
    if($_SESSION["idioma"] == 2 && $dup == "Si"){
        $dup = "Yes";
    }
    
    $fab = "microsoft";

    //inicio Windows OS Activos
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Activos');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device Name')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Operating System')
                ->setCellValue('E1', 'Active in AD')
                ->setCellValue('F1', 'LA Tool')
                ->setCellValue('G1', 'Usability');

    $listar_equipos0 = $detalles->listar_todog0Asignacion($_SESSION['client_id'], $asig, $asignaciones);

    $tipo       = 1;
    $tipoNombre = "Client"; 

    $i = 2;
    $j = 1;
    foreach($listar_equipos0 as $reg_equipos0){
        /*$error = "No";
        if($reg_equipos0["errors"] == 'Ninguno'){
            $error = 'Si';
        }*/
        $error = $reg_equipos0["LaTool"];
		if($error == "Si"){
			$error = "Yes";
		}

        $usabilidad = "In Use";
        if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_equipos0["rango"] != 1){
            $usabilidad = "Obsolete";
        }



        if(($reg_equipos0["rango"] == 1 || $reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3) && $reg_equipos0["tipo"] == $tipo){

            $myWorkSheet->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                        ->setCellValue('C'.$i, $tipoNombre)
                        ->setCellValue('D'.$i, $reg_equipos0["os"])
                        ->setCellValue('E'.$i, "Yes")
                        ->setCellValue('F'.$i, $error)
                        ->setCellValue('G'.$i, $usabilidad);
            $i++;
            $j++;
        } 

    }

    $objPHPExcel->addSheet($myWorkSheet, 0);
    //fin Windows OS Activos


    //inicio Windows OS Inactivos
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Inactivos');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device Name')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Operating System')
                ->setCellValue('E1', 'Active in AD')
                ->setCellValue('F1', 'LA Tool')
                ->setCellValue('G1', 'Usability');

    $listar_equipos0 = $detalles->listar_todog0Asignacion($_SESSION['client_id'], $asig, $asignaciones);

    $tipo       = 1;
    $tipoNombre = "Client"; 

    $i = 2;
    $k = 1;
    foreach($listar_equipos0 as $reg_equipos0){

        /*$error = "No";
        if($reg_equipos0["errors"] == 'Ninguno'){
            $error = 'Si';
        }*/
        
        $error = $reg_equipos0["LaTool"];
		if($error == "Si"){
			$error = "Yes";
		}
		
        $usabilidad = "In Use";
        if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_equipos0["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        if($reg_equipos0["rango"]!=1 && $reg_equipos0["rango"]!=2 && $reg_equipos0["rango"]!=3 && $reg_equipos0["tipo"] == $tipo){

            $myWorkSheet->setCellValue('A'.$i, $k)
                        ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                        ->setCellValue('C'.$i, $tipoNombre)
                        ->setCellValue('D'.$i, $reg_equipos0["os"])
                        ->setCellValue('E'.$i, "No")
                        ->setCellValue('F'.$i, $error)
                        ->setCellValue('G'.$i, $usabilidad);
             $i++;
             $k++;
        }
    }
    $objPHPExcel->addSheet($myWorkSheet, 1);
    //fin Windows OS Inactivos


    //inicio Windows Enterprise
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Enterprise');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device Name')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Operating System')
                ->setCellValue('E1', 'Active in AD')
                ->setCellValue('F1', 'LA Tool')
                ->setCellValue('G1', 'Usability');

    $listar_equipos0 = $detalles->listar_todog1Asignacion($_SESSION['client_id'], 'Enterprise', $asig, $asignaciones);

    $tipo       = 1;
    $tipoNombre = "Client"; 

    $i = 2;
    $j = 1;
    foreach($listar_equipos0 as $reg_equipos0){
        /*$error = "No";
        if($reg_equipos0["errors"] == 'Ninguno'){
            $error = 'Si';
        }*/
        $error = $reg_equipos0["errors"];
        if($error == "Si"){
			$error = "Yes";
		}

        $usabilidad = "In Use";
        if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_equipos0["rango"] != 1){
            $usabilidad = "Obsolete";
        }



        if($reg_equipos0["tipo"] == $tipo){

            $myWorkSheet->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                        ->setCellValue('C'.$i, $tipoNombre)
                        ->setCellValue('D'.$i, $reg_equipos0["os"])
                        ->setCellValue('E'.$i, "Yes")
                        ->setCellValue('F'.$i, $error)
                        ->setCellValue('G'.$i, $usabilidad);
            $i++;
            $j++;
        } 

    }

    $objPHPExcel->addSheet($myWorkSheet, 2);
    //fin Windows Enterprise


    //inicio Windows Professional
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Professional');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device Name')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Operating System')
                ->setCellValue('E1', 'Active in AD')
                ->setCellValue('F1', 'LA Tool')
                ->setCellValue('G1', 'Usability');

    $listar_equipos0 = $detalles->listar_todog1Asignacion($_SESSION['client_id'], 'Professional', $asig, $asignaciones);

    $tipo       = 1;
    $tipoNombre = "Client"; 

    $i = 2;
    $j = 1;
    foreach($listar_equipos0 as $reg_equipos0){
        /*$error = "No";
        if($reg_equipos0["errors"] == 'Ninguno'){
            $error = 'Si';
        }*/
        
        $error = $reg_equipos0["errors"];
		if($error == "Si"){
			$error = "Yes";
		}
		
        $usabilidad = "In Use";
        if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_equipos0["rango"] != 1){
            $usabilidad = "Obsolete";
        }



        if($reg_equipos0["tipo"] == $tipo){

            $myWorkSheet->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                        ->setCellValue('C'.$i, $tipoNombre)
                        ->setCellValue('D'.$i, $reg_equipos0["os"])
                        ->setCellValue('E'.$i, "Yes")
                        ->setCellValue('F'.$i, $error)
                        ->setCellValue('G'.$i, $usabilidad);
            $i++;
            $j++;
        } 

    }

    $objPHPExcel->addSheet($myWorkSheet, 3);
    //fin Windows Professional


    //inicio Windows Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Otros');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device Name')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Operating System')
                ->setCellValue('E1', 'Active in AD')
                ->setCellValue('F1', 'LA Tool')
                ->setCellValue('G1', 'Usability');

    $listar_equipos0 = $detalles->listar_todog2Asignacion($_SESSION['client_id'], 'Enterprise', 'Professional', $asig, $asignaciones);

    $tipo       = 1;
    $tipoNombre = "Client"; 

    $i = 2;
    $j = 1;
    foreach($listar_equipos0 as $reg_equipos0){
        /*$error = "No";
        if($reg_equipos0["errors"] == 'Ninguno'){
            $error = 'Si';
        }*/
        
        $error = $reg_equipos0["errors"];
		if($error == "Si"){
			$error = "Yes";
		}
		
        $usabilidad = "In Use";
        if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_equipos0["rango"] != 1){
            $usabilidad = "Obsolete";
        }



        if($reg_equipos0["tipo"] == $tipo){

            $myWorkSheet->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                        ->setCellValue('C'.$i, $tipoNombre)
                        ->setCellValue('D'.$i, $reg_equipos0["os"])
                        ->setCellValue('E'.$i, "Yes")
                        ->setCellValue('F'.$i, $error)
                        ->setCellValue('G'.$i, $usabilidad);
            $i++;
            $j++;
        } 
    }

    $objPHPExcel->addSheet($myWorkSheet, 4);
    //fin Windows Otros

    //inicio Office Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Office Standard');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $claseResultadosGeneral->listar_datos6Asignacion($fab, $_SESSION['client_id'], 'Office', 'Standard', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 5);
    //fin Office Standard


    //inicio Office Professional
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Office Professional');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $claseResultadosGeneral->listar_datos6Asignacion($fab, $_SESSION['client_id'], 'Office', 'Professional', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 6);
    //fin Office Professional


    //inicio Office Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Office Otros');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION['client_id'], 'Office', 'Standard', 'Professional', $asig, $asignaciones, $dup);    

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 7);
    //fin Office Otros


    //inicio Project Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Project Standard');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Project', 'Standard', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 8);
    //fin Project Standard


    //inicio Project Professional
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Project Professional');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Project', 'Professional', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 9);
    //fin Project Professional


    //inicio Project Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Project Otros');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION['client_id'], 'Project', 'Standard', 'Professional', $asig, $asignaciones, $dup);    

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 10);
    //fin Project Otros


    //inicio Visio Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visio Standard');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Visio', 'Standard', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 11);
    //fin Visio Standard


    //inicio Visio Professional
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visio Professional');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Visio', 'Professional', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 12);
    //fin Visio Professional


    //inicio Visio Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visio Otros');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION['client_id'], 'Visio', 'Standard', 'Professional', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 13);
    //fin Visio Otros


    //inicio SQL Server Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Standard');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'SQL Server', 'Standard', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 14);
    //fin SQL Server Standard


    //inicio SQL Server Datacenter
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Datacenter');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'SQL Server', 'Datacenter', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 15);
    //fin SQL Server Datacenter


    //inicio SQL Server Enterprise
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Enterprise');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'SQL Server', 'Enterprise', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 16);
    //fin SQL Server Enterprise


    //inicio SQL Server Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'SQL Server Otros');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos8Asignacion($_SESSION['client_id'], 'SQL Server', 'Standard', 'Datacenter', 'Enterprise', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 17);
    //fin SQL Server Otros

    //inicio Windows Server Activos
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Activos');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device Name')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Operating System')
                ->setCellValue('E1', 'Active in AD')
                ->setCellValue('F1', 'LA Tool')
                ->setCellValue('G1', 'Usability');

    $listar_equipos0 = $detalles->listar_todog0Asignacion($_SESSION['client_id'], $asig, $asignaciones);

    $tipo       = 2;
    $tipoNombre = "Server"; 

    $i = 2;
    $j = 1;
    foreach($listar_equipos0 as $reg_equipos0){
        /*$error = "No";
        if($reg_equipos0["errors"] == 'Ninguno'){
            $error = 'Si';
        }*/
        $error = $reg_equipos0["LaTool"];
		if($error == "Si"){
			$error = "Yes";
		}

        $usabilidad = "In Use";
        if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_equipos0["rango"] != 1){
            $usabilidad = "Obsolete";
        }



        if(($reg_equipos0["rango"] == 1 || $reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3) && $reg_equipos0["tipo"] == $tipo){

            $myWorkSheet->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                        ->setCellValue('C'.$i, $tipoNombre)
                        ->setCellValue('D'.$i, $reg_equipos0["os"])
                        ->setCellValue('E'.$i, "Yes")
                        ->setCellValue('F'.$i, $error)
                        ->setCellValue('G'.$i, $usabilidad);
            $i++;
            $j++;
        } 

    }

    $objPHPExcel->addSheet($myWorkSheet, 18);
    //fin Windows OS Activos

    //inicio Windows Server Inactivos
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Inactivos');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device Name')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Operating System')
                ->setCellValue('E1', 'Active in AD')
                ->setCellValue('F1', 'LA Tool')
                ->setCellValue('G1', 'Usability');

    $listar_equipos0 = $detalles->listar_todog0Asignacion($_SESSION['client_id'], $asig, $asignaciones);

    $tipo       = 2;
    $tipoNombre = "Server"; 

    $i = 2;
    $k = 1;
    foreach($listar_equipos0 as $reg_equipos0){

        /*$error = "No";
        if($reg_equipos0["errors"] == 'Ninguno'){
            $error = 'Si';
        }*/
        
        $error = $reg_equipos0["LaTool"];
		if($error == "Si"){
			$error = "Yes";
		}

        $usabilidad = "In Use";
        if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_equipos0["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        if($reg_equipos0["rango"]!=1 && $reg_equipos0["rango"]!=2 && $reg_equipos0["rango"]!=3 && $reg_equipos0["tipo"] == $tipo){

            $myWorkSheet->setCellValue('A'.$i, $k)
                        ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                        ->setCellValue('C'.$i, $tipoNombre)
                        ->setCellValue('D'.$i, $reg_equipos0["os"])
                        ->setCellValue('E'.$i, "No")
                        ->setCellValue('F'.$i, $error)
                        ->setCellValue('G'.$i, $usabilidad);
             $i++;
             $k++;
        }
    }
    $objPHPExcel->addSheet($myWorkSheet, 19);
    //fin Windows OS Inactivos

    //inicio Windows Server Standard
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Standard');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device Name')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Operating System')
                ->setCellValue('E1', 'Active in AD')
                ->setCellValue('F1', 'LA Tool')
                ->setCellValue('G1', 'Usability');

    $listar_equipos0 = $detalles->listar_todog1Asignacion($_SESSION['client_id'], 'Standard', $asig, $asignaciones);

    $tipo       = 2;
    $tipoNombre = "Server"; 

    $i = 2;
    $j = 1;
    foreach($listar_equipos0 as $reg_equipos0){
        /*$error = "No";
        if($reg_equipos0["errors"] == 'Ninguno'){
            $error = 'Si';
        }*/
        $error = $reg_equipos0["errors"];
        if($error == "Si"){
			$error = "Yes";
		}

        $usabilidad = "In Use";
        if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_equipos0["rango"] != 1){
            $usabilidad = "Obsolete";
        }



        if($reg_equipos0["tipo"] == $tipo){

            $myWorkSheet->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                        ->setCellValue('C'.$i, $tipoNombre)
                        ->setCellValue('D'.$i, $reg_equipos0["os"])
                        ->setCellValue('E'.$i, "Yes")
                        ->setCellValue('F'.$i, $error)
                        ->setCellValue('G'.$i, $usabilidad);
            $i++;
            $j++;
        } 

    }

    $objPHPExcel->addSheet($myWorkSheet, 20);
    //fin Windows Server Standard


    //inicio Windows Server Datacenter
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Datacenter');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device Name')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Operating System')
                ->setCellValue('E1', 'Active in AD')
                ->setCellValue('F1', 'LA Tool')
                ->setCellValue('G1', 'Usability');

    $listar_equipos0 = $detalles->listar_todog1Asignacion($_SESSION['client_id'], 'Datacenter', $asig, $asignaciones);

    $tipo       = 2;
    $tipoNombre = "Server"; 

    $i = 2;
    $j = 1;
    foreach($listar_equipos0 as $reg_equipos0){
        /*$error = "No";
        if($reg_equipos0["errors"] == 'Ninguno'){
            $error = 'Si';
        }*/
        
        $error = $reg_equipos0["errors"];
		if($error == "Si"){
			$error = "Yes";
		}
		
        $usabilidad = "In Use";
        if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_equipos0["rango"] != 1){
            $usabilidad = "Obsolete";
        }



        if($reg_equipos0["tipo"] == $tipo){

            $myWorkSheet->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                        ->setCellValue('C'.$i, $tipoNombre)
                        ->setCellValue('D'.$i, $reg_equipos0["os"])
                        ->setCellValue('E'.$i, "Yes")
                        ->setCellValue('F'.$i, $error)
                        ->setCellValue('G'.$i, $usabilidad);
            $i++;
            $j++;
        } 

    }

    $objPHPExcel->addSheet($myWorkSheet, 21);
    //fin Windows Server Datacenter
    
    //inicio Windows Server Enterprise
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Enterprise');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device Name')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Operating System')
                ->setCellValue('E1', 'Active in AD')
                ->setCellValue('F1', 'LA Tool')
                ->setCellValue('G1', 'Usability');

    $listar_equipos0 = $detalles->listar_todog1Asignacion($_SESSION['client_id'], 'Enterprise', $asig, $asignaciones);

    $tipo       = 2;
    $tipoNombre = "Server"; 

    $i = 2;
    $j = 1;
    foreach($listar_equipos0 as $reg_equipos0){
        /*$error = "No";
        if($reg_equipos0["errors"] == 'Ninguno'){
            $error = 'Si';
        }*/
        
        $error = $reg_equipos0["errors"];
		if($error == "Si"){
			$error = "Yes";
		}

        $usabilidad = "In Use";
        if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_equipos0["rango"] != 1){
            $usabilidad = "Obsolete";
        }



        if($reg_equipos0["tipo"] == $tipo){

            $myWorkSheet->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                        ->setCellValue('C'.$i, $tipoNombre)
                        ->setCellValue('D'.$i, $reg_equipos0["os"])
                        ->setCellValue('E'.$i, "Yes")
                        ->setCellValue('F'.$i, $error)
                        ->setCellValue('G'.$i, $usabilidad);
            $i++;
            $j++;
        } 

    }

    $objPHPExcel->addSheet($myWorkSheet, 22);
    //fin Windows Server Enterprise

    //inicio Windows Server Otros
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Windows Server Otros');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device Name')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Operating System')
                ->setCellValue('E1', 'Active in AD')
                ->setCellValue('F1', 'LA Tool')
                ->setCellValue('G1', 'Usability');

    $listar_equipos0 = $detalles->listar_todog3Asignacion($_SESSION['client_id'], 'Standard', 'Datacenter', 'Enterprise', $asig, $asignaciones);

    $tipo       = 2;
    $tipoNombre = "Server"; 

    $i = 2;
    $j = 1;
    foreach($listar_equipos0 as $reg_equipos0){
        /*$error = "No";
        if($reg_equipos0["errors"] == 'Ninguno'){
            $error = 'Si';
        }*/
        
        $error = $reg_equipos0["errors"];
		if($error == "Si"){
			$error = "Yes";
		}

        $usabilidad = "In Use";
        if($reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_equipos0["rango"] != 1){
            $usabilidad = "Obsolete";
        }



        if($reg_equipos0["tipo"] == $tipo){

            $myWorkSheet->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $reg_equipos0["equipo"])
                        ->setCellValue('C'.$i, $tipoNombre)
                        ->setCellValue('D'.$i, $reg_equipos0["os"])
                        ->setCellValue('E'.$i, "Yes")
                        ->setCellValue('F'.$i, $error)
                        ->setCellValue('G'.$i, $usabilidad);
            $i++;
            $j++;
        } 
    }

    $objPHPExcel->addSheet($myWorkSheet, 23);
    //fin Windows Server Otros
    
    //inicio Visual Studio
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Visual Studio');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Visual Studio', '', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 24);
    //fin Visual Studio


    //inicio Exchange Server
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Exchange Server');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Exchange Server', '', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 25);
    //fin Exchange Server


    //inicio Sharepoint Server
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Sharepoint Server');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Sharepoint Server', '', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 26);
    //fin Sharepoint Server


    //inicio Skype for Business
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Skype for Business');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'Skype for Business', '', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 27);
    //fin Skype for Business


    //inicio System Center
    // Create a new worksheet called "My Data"
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'System Center');
    // Add some data

    $myWorkSheet->setCellValue('A1', '')
                ->setCellValue('B1', 'Device')
                ->setCellValue('C1', 'Type')
                ->setCellValue('D1', 'Family')
                ->setCellValue('E1', 'Edition')
                ->setCellValue('F1', 'Version')
                ->setCellValue('G1', 'Instalation Date')
                ->setCellValue('H1', 'Usability')
                ->setCellValue('I1', 'Observation');

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 'System Center', '', $asig, $asignaciones, $dup);

    $i = 2;
    $j = 1;
    foreach($lista_calculo as $reg_calculo){
        $usabilidad = "In Use";
        if($reg_calculo["rango"] == 2 || $reg_calculo["rango"] == 3){
            $usabilidad = "Probably in Use";
        }
        else if($reg_calculo["rango"] != 1){
            $usabilidad = "Obsolete";
        }

        $duplicado = "";
        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
            $duplicado = "Duplicate";
        }
        $myWorkSheet->setCellValue('A'.$i, $j)
                    ->setCellValue('B'.$i, $reg_calculo["equipo"])
                    ->setCellValue('C'.$i, $reg_calculo["tipo"])
                    ->setCellValue('D'.$i, $reg_calculo["familia"])
                    ->setCellValue('E'.$i, $reg_calculo["edicion"])
                    ->setCellValue('F'.$i, $reg_calculo["version"])
                    ->setCellValue('G'.$i, $reg_calculo["fecha_instalacion"])
                    ->setCellValue('H'.$i, $usabilidad)
                    ->setCellValue('I'.$i, $duplicado);
        $i++;
        $j++;
    }	

    $objPHPExcel->addSheet($myWorkSheet, 28);
    //fin System Center    
    
    $objPHPExcel->setActiveSheetIndex(0);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Reporte Detalle Microsoft.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
    $general->salirPorSesionReporte($mensaje);
}
?>
<div style="width:95%; overflow:hidden; margin:0 auto; margin-top:20px; margin-bottom:20px;">
    <div style="width:20%; margin:0; padding:0px; overflow:hidden; float:left;">
        <img src="<?= $GLOBALS["domain_root"] ?>/img/imgMicrosoft.png" style="width:70%; height:auto;">
        <br><br>
        
        <div class="form-group">
            <select name="company" id="company" class="form-control"> 
                <option value="">Company</option>
                <?php 
                foreach($listadoEmpresas as $rowEmpresa){
                ?>
                <option value="<?= $rowEmpresa["id"] ?>" <?php if($rowEmpresa["id"] == $_SESSION["client_id"]){ echo "selected='selected'"; } ?>><?= $rowEmpresa["empresa"] ?></option>
                <?php 
                }
                ?>
            </select>
        </div>
       
        <br>

        <span style="color:#0070c0; font-weight:bold; font-size:1vw;">Select the report</span><br>
        
        <ul class="nav nav-tabs" id="myTab">
            <li class="<?php if($vert == 0 || $vert == 1 || $vert == 7 || $vert == 8 || $vert == 9){ 
                echo 'active';
            }?>"><a href="#kpi" data-toggle="tab" onclick="location.href='summary.php';" style="font-size:1vw;">KPI´s</a></li>
            <li class="<?php if($vert == 2){ 
                echo 'active';
            }?>"><a href="#balance" data-toggle="tab" onclick="location.href='summary.php?vert=2';" style="font-size:1vw;">Balance</a></li>
            <li class="<?php if($vert == 6 || $vert == 3 || $vert == 4){ 
                echo 'active';
            }?>"><a href="#optimization" data-toggle="tab" onclick="location.href='summary.php?vert=6';" style="font-size:1vw;">Opt.</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane <?php if($vert == 0 || $vert == 1 || $vert == 7 || $vert == 8 || $vert == 9){ 
                echo 'active';
            }?>" id="kpi" style="background:#ffffff; overflow:hidden;">
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgAlcance ?>" style="width:100%; height:auto;" onclick="location.href='summary.php';">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgUsabilidad ?>" style="width:100%; height:auto;" onclick="location.href='summary.php?vert=1';">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgDuplicado ?>" style="width:100%; height:auto;" onclick="location.href='summary.php?vert=7';">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgErroneous ?>" style="width:100%; height:auto;" onclick="location.href='summary.php?vert=8';">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgUnsupported ?>" style="width:100%; height:auto;" onclick="location.href='summary.php?vert=9';">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="tab-pane <?php if($vert == 2){ 
                echo 'active';
            }?>" id="balance" style="background:#ffffff; overflow:hidden;">
                <!--<div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgClientes ?>" style="width:100%; height:auto;">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgServidores ?>" style="width:100%; height:auto;">
                            </div>
                        </div>
                    </div>
                </div>-->
                
                <ul class="nav nav-tabs" id="myTabFamilia">
                    <li class="active"><a href="#tabClient" data-toggle="tab" style="font-size:1vw;">Client</a></li>
                    <li><a href="#tabServer" data-toggle="tab" style="font-size:1vw;">Server</a></li>
                    <li><a href="#tabHibrid" data-toggle="tab" style="font-size:1vw;">Hibrid</a></li>
                </ul>
                
                <input type="hidden" id="familia" name="familia">
                <input type="hidden" id="edicion" name="edicion">
                
                <div class="tab-content">
                    <div class="tab-pane active" id="tabClient" style="background:#ffffff; overflow:hidden;">
                        <div class="row-personal">
                            <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                                <div class="col-sm-12 btnSombreado">
                                    <div class="row">
                                        <img src="<?= $imgWindows ?>" style="width:100%; height:auto;" id="balWindows">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                                <div class="col-sm-12 btnSombreado">
                                    <div class="row">
                                        <img src="<?= $imgOffice ?>" style="width:100%; height:auto;" id="balOffice">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row-personal">
                            <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                                <div class="col-sm-12 btnSombreado">
                                    <div class="row">
                                        <img src="<?= $imgVisio ?>" style="width:100%; height:auto;" id="balVisio">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                                <div class="col-sm-12 btnSombreado">
                                    <div class="row">
                                        <img src="<?= $imgProject ?>" style="width:100%; height:auto;" id="balProject">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tabServer" style="background:#ffffff; overflow:hidden;">
                        <div class="row-personal">
                            <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                                <div class="col-sm-12 btnSombreado">
                                    <div class="row">
                                        <img src="<?= $imgWindowsServer ?>" style="width:100%; height:auto;" id="balWindowsServer">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                                <div class="col-sm-12 btnSombreado">
                                    <div class="row">
                                        <img src="<?= $imgSQLServer ?>" style="width:100%; height:auto;" id="balSQLServer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tabHibrid" style="background:#ffffff; overflow:hidden;">
                        <div class="row-personal">
                            <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                                <div class="col-sm-12 btnSombreado">
                                    <div class="row">
                                        <img src="<?= $imgVisual ?>" style="width:100%; height:auto;" id="balVisual">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                                <div class="col-sm-12 btnSombreado">
                                    <div class="row">
                                        <img src="<?= $imgExchange ?>" style="width:100%; height:auto;" id="balExchange">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row-personal">
                            <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                                <div class="col-sm-12 btnSombreado">
                                    <div class="row">
                                        <img src="<?= $imgSharepoint ?>" style="width:100%; height:auto;" id="balSharepoint">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                                <div class="col-sm-12 btnSombreado">
                                    <div class="row">
                                        <img src="<?= $imgSkype ?>" style="width:100%; height:auto;" id="balSkype">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row-personal">
                            <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                                <div class="col-sm-12 btnSombreado">
                                    <div class="row">
                                        <img src="<?= $imgSystem ?>" style="width:100%; height:auto;" id="balSystem">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="tab-pane <?php if($vert == 6 || $vert == 3 || $vert == 4){ 
                echo 'active';
            }?>" id="optimization" style="background:#ffffff; overflow:hidden;">
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgUndiscovered ?>" style="width:100%; height:auto;" onclick="location.href='summary.php?vert=6';">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgOptimization ?>" style="width:100%; height:auto;" onclick="location.href='summary.php?vert=3';">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgUnused ?>" style="width:100%; height:auto;" onclick="location.href='summary.php?vert=4';">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        
        <br>
        
        <ul class="nav nav-tabs" id="myTabEdition" style="display:none;">
            <li class="active"><a href="#edition" data-toggle="tab" style="font-size:1vw;">Edition</a></li>
        </ul>

        <div class="tab-content" id="contentTabEdition" style="display:none;">
            <div class="tab-pane active" id="edition" style="background:#ffffff; overflow:hidden;">
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <input type="hidden" id="valEdition1" name="valEdition1">
                                <img src="<?= $imgEdition1 ?>" id="imgEdition1" style="width:100%; height:auto;">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <input type="hidden" id="valEdition2" name="valEdition2">
                                <img src="<?= $imgEdition2 ?>" id="imgEdition2" style="width:100%; height:auto;">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <input type="hidden" id="valEdition3" name="valEdition3">
                                <img src="<?= $imgEdition3 ?>" id="imgEdition3" style="width:100%; height:auto;">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" id="divImgEdition4" style="margin-top:1vw; margin-bottom:1vw; display:none;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <input type="hidden" id="valEdition4" name="valEdition4">
                                <img src="<?= $imgEdition4 ?>" id="imgEdition4" style="width:100%; height:auto;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--<br>
            
        <div>Allocation:<strong style="color:#000; font-weight:bold;">
            <?//= $asig ?></strong>
            
            <br>
            <select onchange="MM_jumpMenu('self',this,0);" style="width:100px;">
                <option value="detail.php" selected="selected">Select..</option>
                <option value="detail.php?vert=<?//= $vert ?>">All</option>
                <?php 
                /*foreach($asignaciones as $row){
                ?>
                    <option value="detalle.php?vert=<?= $vert ?>&asig=<?= $row["asignacion"] ?>"><?= $row["asignacion"] ?></option>
                <?php
                }*/
                ?>
            </select>
        </div>-->         
        <br>
        
        <ul class="nav nav-tabs" id="myTabReport">
            <li class="active"><a href="#tabExport" data-toggle="tab" style="font-size: 1vw;">Export</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tabExport" style="background:#ffffff; overflow:hidden;">
                <div class="row-personal">
                     <form id="formExportar" name="formExportar" method="post" action="<?= $dirExport ?>">
                        <div class="col-sm-6" id="divExport" style="margin-top:1vw; margin-bottom:1vw; display:none;">
                            <div class="col-sm-12 btnSombreado">
                                <div class="row">
                                    <input type="hidden" id="familiaExcel" name="familiaExcel">
                                    <input type="hidden" id="edicionExcel" name="edicionExcel">
                                    <input type="hidden" id="vertExportar" name="vert" value = "<?= $vert ?>">
                                    <input type="hidden" id="asig" name="asig" value = "<?= $asig ?>">
                                    <input type="hidden" id="opcion" name="opcion"><!-- Usado para saber que exportar en optimizacion o en Equipos No Descubiertos-->
                                    <input type="submit" id="exportar" name="exportar" style="display:none">
                                    <img src="<?= $imgExport ?>" style="width:100%; height:auto;" id="export">
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgExportAll ?>" style="width:100%; height:auto;" id="exportTodo" onclick="window.open('reportes/excelDetalleTodo.php?asig=<?= $asig ?>&dup=<?= $dup ?>')">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div style="width:80%; float:left; margin:0px; padding:0px; min-height:450px; overflow:hidden;">
        <?php
        if ($vert == 0  || $vert == 4 || $vert == 7 || $vert == 8 || $vert == 9) {
        ?>
            <div id="container1" style="height:30vw; width:47%; margin:10px; float:left;"></div>
            <div id="container2" style="height:30vw; width:47%; margin:10px; float:right;"></div>
            <div  style="width: 47%; margin:10px; float:left; text-align:center; font-size:1vw;"><a onClick="showhide('ttabla1');" style="cursor:pointer;" ><img src="<?= $GLOBALS["domain_root"] ?>/img/verDetalle.png" style="height:2vw; width:auto;"></a></div>
            <div  style="width: 47%; margin:10px; float:right; text-align:center; font-size:1vw;"><a onClick="showhide('ttabla1');" style="cursor:pointer;" ><img src="<?= $GLOBALS["domain_root"] ?>/img/verDetalle.png" style="height:2vw; width:auto;"></a></div>
        <?php
        }

        if ($vert == 1 || $vert == 3 || $vert == 6) {
        ?>
            <div id="container1" style="height:30vw; width: 47%; margin:10px; float:left;"></div>
            <div id="container2" style="height:30vw; width: 47%; margin:10px; float:right;"></div>
            <div id="container3" class="hide" style="height:30vw; width: 47%; margin:10px; float:left;"></div>
            <div id="container4" class="hide" style="height:30vw; width: 47%; margin:10px; float:right;"></div>
            <div id="container5" class="hide" style="height:30vw; width: 47%; margin:10px; float:left;"></div>
            <div id="container6" class="hide" style="height:30vw; width: 47%; margin:10px; float:right;"></div>
            <div  style="width: 47%; margin:10px; float:left; text-align:center; font-size:1vw;"><a <?php if ($vert == 1) { echo 'onclick="showhide(\'ttabla1\')";'; } 
            else if ($vert == 3){ echo 'id="verOptimizacionCliente"'; } else{ echo 'id="verDetalleCliente"'; }?> style="cursor:pointer;" ><img src="<?= $GLOBALS["domain_root"] ?>/img/verDetalle.png" style="height:2vw; width:auto;"></a></div>
            <div  style="width: 47%; margin:10px; float:right; text-align:center; font-size:1vw;"><a <?php if ($vert == 1) { echo 'onclick="showhide(\'ttabla1\')";'; } 
            else if ($vert == 3){ echo 'id="verOptimizacionServidor"'; } else{ echo 'id="verDetalleServidor"'; }?> style="cursor:pointer;" ><img src="<?= $GLOBALS["domain_root"] ?>/img/verDetalle.png" style="height:2vw; width:auto;"></a></div>
        <?php
        }
        if($vert == 5){
        ?>
            <div id="container1" style="height:30vw; width: 47%; margin:10px; float:left;"></div>
            <div id="container2" style="height:30vw; width: 47%; margin:10px; float:right;"></div>
            <div  style="width: 47%; margin:10px; float:left; text-align:center; font-size:1vw;"><a onclick="mostrarListado('cliente');" class="pointer"><img src="<?= $GLOBALS["domain_root"] ?>/img/verDetalle.png" style="height:2vw; width:auto;"></a></div>
            <div  style="width: 47%; margin:10px; float:right; text-align:center; font-size:1vw;"><a onclick="mostrarListado('servidor');" class="pointer" ><img src="<?= $GLOBALS["domain_root"] ?>/img/verDetalle.png" style="height:2vw; width:auto;"></a></div>
        <?php
        }

        if ($vert == 2) {
        ?>
            <div style="width:99%; overflow-x:auto; overflow-y:hidden;">
                <div id="btnServidores" class="pointer" style="width:45%; text-align:center; padding:10px; float:left">
                    <p id="server" style="width:100%; height:50px; font-size:20px; font-weight:bold; line-height:50px;  background-color:#06B6FF; color:#FFFFFF">Servers</p>
                </div>
                <div id="btnClientes" class="pointer" style="width:45%; text-align:center; padding: 10px 0px 10px 10px; float:right">
                    <p id="client" style="width:100%; height:50px; font-size:20px; font-weight:bold;  line-height:50px;  background-color:#FFFFFF; color:#06B6FF">Clients</p>
                </div>
                <div id="contenedorGraficos" style="width:100%; overflow-x: auto; overflow-y:hidden;">
                    <div id="graficosServidores" style="width:650px; float:left; text-align:center;">

                        <div id="container3" style="height:250px; width:300px; margin:10px; float:left;"></div>
                        <div id="container4" style="height:250px; width:300px; margin:10px; float:right;"></div>

                        <br style="clear:both">
                        <div style="width:auto; text-align:center; padding:10px; float:left">
                            <p style="width:300px; height:50px; font-size:20px; font-weight:bold;  line-height:50px;  background-color:#FFFFFF;">SQL</p>
                        </div>
                        <div style="width:auto; text-align:center; padding:10px; float:right">
                            <p style="width:300px; height:50px; font-size:20px; font-weight:bold;  line-height:50px;  background-color:#FFFFFF;">OS</p>
                        </div>
                    </div>

                    <div id="graficosClientes" style="width:auto; float:left; text-align:center; display:none;">

                        <div id="container5" style="height:250px; width:300px; margin:10px; float:left;"></div>
                        <div id="container6" style="height:250px; width:300px; margin:10px; float:left;"></div>
                        <div id="container7" style="height:250px; width:300px; margin:10px; float:left;"></div>

                        <br style="clear:both">
                        <div style="width:auto; text-align:center; padding:10px; float:left">
                            <p style="width:300px; height:50px; font-size:20px; font-weight:bold;  line-height:50px;  background-color:#FFFFFF;">OS</p>
                        </div>
                        <div style="width:auto; text-align:center; padding:10px; float:left">
                            <p style="width:300px; height:50px; font-size:20px; font-weight:bold;  line-height:50px;  background-color:#FFFFFF;">Office</p>
                        </div>
                        <div style="width:auto; text-align:center; padding:10px; float:left">
                            <p style="width:300px; height:50px; font-size:20px; font-weight:bold;  line-height:50px;  background-color:#FFFFFF;">Products</p>
                        </div>
                    </div>
                </div>
            </div>	
            
            <!--<div style="width:98%; margin:10px; overflow:hidden; border: 1px solid;">-->
                <div id="containerDetalle" style="height:250px; width:calc(100% - 20px); margin:10px; margin-bottom:0; overflow:hidden;">

                </div>
            <!--</div>-->
        <?php
        }
        ?>
            
        <br>
        
        <?php
        if ($vert == 0) {
        ?>
            <div id="ttabla1"  style="display:none; width:99%; margin:10px; float:left;">
                <?php 
                    include_once("plantillas/alcance.php"); 
                ?>
            </div>
        <?php
        }//0

        if ($vert == 1) {
        ?>
            <div id="ttabla1"  style=" display:none; width:99%; margin:10px; float:left;">
                <?php 
                    include_once("plantillas/usabilidad.php"); 
                ?>
            </div>
        <?php
        }
        else if($vert == 2){
        ?>
            <div style="<?php if (!$balanza){ echo 'display:none;'; } ?> margin:10px;">
                <form id="formGAP" name="formGAP" method="post">
                    <input type="hidden" id="tokenGAP" name="token">
                    <input type="hidden" id="bandEditar" value="false">
                    <div id="divTabla" style="display:none; width:100%; max-height:400px; overflow:auto;">
                        <table class="tablap" id="tablaBalanza" style="margin-top:-5px;">
                            <thead>
                                <tr style="background:#333; color:#fff;">
                                    <th class="text-center">Product</th>
                                    <th class="text-center">Edition</th>
                                    <th class="text-center">Version</th>
                                    <th class="text-center">Allocation</th>
                                    <th class="text-center">Installations</th>
                                    <th class="text-center">Purchases</th>
                                    <th class="text-center">Available</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">GAP Amount</th>
                                    <th class="text-center">GAP Available</th>
                                </tr>
                            </thead>
                            <tbody id="tablaDetalle">

                            </tbody>
                        </table>
                    </div>

                    <!--<br>
                    <div class="botones_m2 boton1" style="display:none; float:right;" id="guardarGAP">Update GAP</div>
                    <div class="botones_m2 boton1" style="display:none; float:right;" id="editarGAP" onclick="editarGAP()">Edit GAP</div>-->
                </form>
            </div>
        <?php
        }
        else if ($vert == 3) {
        ?>
            <div id="ttabla1"  style=" display:none; width:99%; margin:10px; float:left;">

            </div>
        <?php
        } else if($vert == 5){
            include_once("plantillas/detalleEquipos.php");
        } else if ($vert == 4) {
            include_once("plantillas/softwareDesuso.php");
        } else if ($vert == 6){
        ?>
        <?php if($vert == 6){ ?>
            <!--<div style="width:99%; margin:10px; float:left;">
                <div style="width:680px; height: <?php //if($asig == ''){ echo '44px'; } else{ echo '70px'; } ?>; margin:0 auto;">
                    <fieldset class="fieldsetNoDescubiertoEquipo">
                        <input type="radio" id="equipoBoton" name="noDescubierto" checked="checked"><span class="bold pointer" style="margin-right:150px; line-height:44px;" id="EquipoNoDesc">Device</span>
                    </fieldset>

                    <fieldset class="fieldsetNoDescubiertoReto" style="width:380px;"> 
                        <input type="radio" id="retoBoton" name="noDescubierto"><span class="bold pointer" style="line-height:44px;" id="RetoNoDesc">Challenge</span>
                        <div style="float:right; overflow:hidden; margin-top:2px; margin-right:5px;">
                            <div id="btnTotal" class="botonesSAM boton1 hide">Total</div>
                            <div id="btnActivos" class="botonesSAM boton5 hide">Active</div>
                        </div>
                    </fieldset>
                </div>
            </div>-->
        <?php
             } 
            include_once("plantillas/equiposNoDescubiertos.php");
        } else if ($vert == 7){
            include_once("plantillas/equiposDuplicados.php");
        } else if ($vert == 8){
            include_once("plantillas/instalErroneas.php");
        } else if ($vert == 9){
            include_once("plantillas/softSinSoporte.php");
        }
        ?>
    </div>
</div>

<script>
    var vertAux = "";
    
    $(document).ready(function () {
        $("#tablaAlcance").tablesorter();
        $("#tablaAlcance").tableHeadFixer();
        $("#tablaDesusoCliente").tablesorter();
        $("#tablaDesusoServidor").tablesorter();
        $("#tablaBalanza").tableHeadFixer();
        
        $("#btnServidores").click(function () {
            $("#server").css("color", "#FFFFFF");
            $("#server").css("background-color", "#06B6FF");
            $("#client").css("color", "#06B6FF");
            $("#client").css("background-color", "#FFFFFF");
            $("#contenedorGraficos").css("width", "100%");
            $("#graficosClientes").hide();
            $("#graficosServidores").show();
        });

        $("#btnClientes").click(function () {
            $("#client").css("color", "#FFFFFF");
            $("#client").css("background-color", "#06B6FF");
            $("#server").css("color", "#06B6FF");
            $("#server").css("background-color", "#FFFFFF");
            $("#contenedorGraficos").css("width", "1000px");
            $("#graficosClientes").show();
            $("#graficosServidores").hide();
        });

        $("#export").click(function () {
            $("#exportar").click();
        });
        
        $("#verOptimizacionServidor").click(function(){
            mostrarTabla(3, "", "servidor");
        });
        
        $("#verOptimizacionCliente").click(function(){
             mostrarTabla(3, "", "cliente");
        });
        
        $("#verDetalleCliente").click(function(){
            <?php if($vert != 6 ){ ?>
                mostrarTabla(5, "", "cliente");
            <?php 
            } else{
            ?>
                if($("#container1").is(":visible") || $("#container5").is(":visible")){
                    if($('#ttabla1').is(":visible")){
                        $('#ttabla1').hide();
                        $('#ttabla2').hide();
                        $("#export").hide();
                        $("#opcion").val("Total");
                    } else{
                        $('#ttabla1').show();
                        $('#ttabla2').hide();
                        $("#export").show();
                        $("#opcion").val("Total");
                    }
                } else if($("#container3").is(":visible")){
                    if($('#ttabla2').is(":visible")){
                        $('#ttabla1').hide();
                        $('#ttabla2').hide();
                        $("#export").hide();
                        $("#opcion").val("Activo");
                    } else{
                        $('#ttabla1').hide();
                        $('#ttabla2').show();
                        $("#export").show();
                        $("#opcion").val("Activo");
                    }
                }
            <?php    
            }
            ?>
        });
        
        $("#verDetalleServidor").click(function(){
            <?php if($vert != 6 ){ ?>
                mostrarTabla(5, "", "cliente");
            <?php 
            } else{
            ?>
               $("#verDetalleCliente").click();
            <?php    
            }
            ?>
        });

        /*$("#familia").change(function () {
            $("#edicion").val("");
            realizarBusqueda();
        });
        
        $("#edicion").change(function () {
            realizarBusqueda();
        });*/

        $("#balWindows").click(function(){
            $("#familia").val("Windows");
            $("#edicion").val("");
            desactivarFamilia();
            
            $("#balWindows").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsSel.png");
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsEnterprise.png");
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsProfessional.png");
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOthers.png");
            $("#divImgEdition4").hide();
            $("#myTabEdition").show();
            $("#contentTabEdition").show();
            
            $("#valEdition1").val("Enterprise");
            $("#valEdition2").val("Professional");
            $("#valEdition3").val("Others");
            realizarBusqueda();
        });
        
        $("#balOffice").click(function(){
            $("#familia").val("Office");
            $("#edicion").val("");
            desactivarFamilia();
            
            $("#balOffice").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOfficeSel.png");
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOfficeStandard.png");
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOfficeProfessional.png");
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOthers.png");
            $("#divImgEdition4").hide();
            $("#myTabEdition").show();
            $("#contentTabEdition").show();
            
            $("#valEdition1").val("Standard");
            $("#valEdition2").val("Professional");
            $("#valEdition3").val("Others");
            realizarBusqueda();
        });
        
        $("#balProject").click(function(){
            $("#familia").val("Project");
            $("#edicion").val("");
            desactivarFamilia();
            
            $("#balProject").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnProjectSel.png");            
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnProjectStandard.png");
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnProjectProfessional.png");
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOthers.png");
            $("#divImgEdition4").hide();
            $("#myTabEdition").show();
            $("#contentTabEdition").show();
            
            $("#valEdition1").val("Standard");
            $("#valEdition2").val("Professional");
            $("#valEdition3").val("Others");
            realizarBusqueda();
        });
        
        $("#balVisio").click(function(){
            $("#familia").val("Visio");
            $("#edicion").val("");
            desactivarFamilia();
            
            $("#balVisio").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnVisioSel.png"); 
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnVisioStandard.png");
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnVisioProfessional.png");
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOthers.png");
            $("#divImgEdition4").hide();
            $("#myTabEdition").show();
            $("#contentTabEdition").show();
            
            $("#valEdition1").val("Standard");
            $("#valEdition2").val("Professional");
            $("#valEdition3").val("Others");
            realizarBusqueda();
        });
        
        $("#balWindowsServer").click(function(){
            $("#familia").val("Windows Server");
            $("#edicion").val("");
            desactivarFamilia();
            
            $("#balWindowsServer").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsServerSel.png"); 
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsStandard.png");
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsServerDatacenter.png");
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsEnterprise.png");
            $("#imgEdition4").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOthers.png");
            $("#divImgEdition4").show();
            $("#myTabEdition").show();
            $("#contentTabEdition").show();
            
            $("#valEdition1").val("Standard");
            $("#valEdition2").val("Datacenter");
            $("#valEdition3").val("Enterprise");
            $("#valEdition4").val("Others");
            realizarBusqueda();
        });

        $("#balSQLServer").click(function(){
            $("#familia").val("SQL Server");
            $("#edicion").val("");
            desactivarFamilia();
            
            $("#balSQLServer").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSQLServerSel.png"); 
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSQLServerStandard.png");
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSQLServerDatacenter.png");
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSQLServerEnterprise.png");
            $("#imgEdition4").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOthers.png");
            $("#divImgEdition4").show();
            $("#myTabEdition").show();
            $("#contentTabEdition").show();
            
            $("#valEdition1").val("Standard");
            $("#valEdition2").val("Datacenter");
            $("#valEdition3").val("Enterprise");
            $("#valEdition4").val("Others");
            realizarBusqueda();
        });
        
        $("#balVisual").click(function(){
            $("#familia").val("Others");
            $("#edicion").val("Visual Studio");
            desactivarFamilia();
            
            $("#balVisual").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnVisualStudioSel.png"); 
            realizarBusqueda();
        });
        
        $("#balExchange").click(function(){
            $("#familia").val("Others");
            $("#edicion").val("Exchange Server");
            desactivarFamilia();
            
            $("#balExchange").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnExchangeServerSel.png"); 
            realizarBusqueda();
        });
        
        $("#balSharepoint").click(function(){
            $("#familia").val("Others");
            $("#edicion").val("Sharepoint Server");
            desactivarFamilia();
            
            $("#balSharepoint").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSharepointServerSel.png"); 
            realizarBusqueda();
        });
        
        $("#balSkype").click(function(){
            $("#familia").val("Others");
            $("#edicion").val("Skype for Business");
            desactivarFamilia();
            
            $("#balSkype").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSkypeforBusinessSel.png"); 
            realizarBusqueda();
        });
        
        $("#balSystem").click(function(){
            $("#familia").val("Others");
            $("#edicion").val("System Center");
            desactivarFamilia();
            
            $("#balSystem").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSystemCenterSel.png"); 
            realizarBusqueda();
        });
        
        $("#imgEdition1").click(function(){
            $("#edicion").val($("#valEdition1").val());
            activarBtnEdition(1);
            realizarBusqueda();
        });
        
        $("#imgEdition2").click(function(){
            $("#edicion").val($("#valEdition2").val());
            activarBtnEdition(2);
            realizarBusqueda();
        });
        
        $("#imgEdition3").click(function(){
            $("#edicion").val($("#valEdition3").val());
            activarBtnEdition(3);
            realizarBusqueda();
        });
        
        $("#imgEdition4").click(function(){
            $("#edicion").val($("#valEdition4").val());
            activarBtnEdition(4);
            realizarBusqueda();
        });
        
        $("#exportarTodo").click(function(){
            window.open("<?= $exportBalanzaTodo ?>");
        });
        
        $("#EquipoNoDesc, #equipoBoton").click(function(){
            $("#equipoBoton").prop("checked", true);
            $("#container1").show();
            $("#container2").show();
            $("#container3").hide();
            $("#container4").hide();
            $("#container5").hide();
            $("#container6").hide();
            $("#btnTotal").hide();
            $("#btnActivos").hide();
            $("#ttabla1").hide();
            $("#ttabla2").hide();
            $("#divExport").hide();
        });
        
        $("#RetoNoDesc, #retoBoton").click(function(){
            $("#retoBoton").prop("checked", true);
            $("#container1").hide();
            $("#container2").hide();
            $("#container3").show();
            $("#container4").show();
            $("#btnTotal").show();
            $("#btnActivos").show();
            $("#btnActivos").click();
            $("#ttabla1").hide();
            $("#ttabla2").hide();
            $("#divExport").hide();
        });
        
        $("#btnActivos").click(function(){
            $("#container3").show();
            $("#container4").show();
            $("#container5").hide();
            $("#container6").hide();
            $("#btnActivos").removeClass("boton1").addClass("boton5");
            $("#btnTotal").removeClass("boton5").addClass("boton1");
            $("#ttabla1").hide();
            $("#ttabla2").hide();
            $("#divExport").hide();
            //$("#btnActivos").click();
        });
        
        $("#btnTotal").click(function(){
            $("#container3").hide();
            $("#container4").hide();
            $("#container5").show();
            $("#container6").show();
            $("#btnActivos").removeClass("boton5").addClass("boton1");
            $("#btnTotal").removeClass("boton1").addClass("boton5");
            $("#ttabla1").hide();
            $("#ttabla2").hide();
            $("#divExport").hide();
        });
        
        $("#company").change(function(){
            if($("#company").val() === ""){
                return false;
            }
            
            $.post("ajax/cambiarEmpresa.php", { company : $("#company").val(), token : localStorage.smartControlToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                
                if(data[0].result === true){
                    location.reload();
                }
                
                $('#fondo').hide();
            }, "json")
            .fail(function( jqXHR ){
                $('#fondo').hide();
                $.alert.open('error', "Error: " + jqXHR.status);
            });
        });
        
        $('#myTabFamilia a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
            desactivarFamilia();
            $("#myTabEdition").hide();
            $("#contentTabEdition").hide();
        });
    });
    
    function realizarBusqueda(){        
        if ($("#familia").val() === "") {
            $.alert.open('warning', "You must select a family");
            return false;
        }
        
        $("#fondo").show();
        $("#divExport").show();
        $("#exportarTodo").show();

        $.post("ajax/balanzaDetalle.php", {familia: $("#familia").val(), edicion: $("#edicion").val(), asignacion : '<?= $asig ?>', token : localStorage.smartControlToken }, function (data) {
            <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
            $("#containerDetalle").empty();
            $("#tablaDetalle").empty();
            $("#edicion").empty();
            $("#edicion").append(data[0].edicion);
            $("#familiaExcel").val($("#familia").val());
            $("#edicionExcel").val($("#edicion").val());

            familia = $("#familia").val();
            
            if($("#edicion").val() === "Otros"){
                edicion = "<?= $otrosIdioma ?>";
            }else{
                edicion = $("#edicion").val();
            }
            
            if(familia === "Others" && edicion === ""){
                familia = "<?= $otrosIdioma ?>"; 
                edicion = "";
            }
            else if(familia === "Others" && edicion !== ""){
                if($("#edicion").val() === "Visual"){
                    familia = "Visual Studio";
                }
                else if($("#edicion").val() === "Exchange"){
                    familia = "Exchange Server";
                }
                else if($("#edicion").val() === "Sharepoint"){
                    familia = "Sharepoint Server";
                }
                else if($("#edicion").val() === "Skype"){
                    familia = "Skype for Business";
                }
                else{
                    familia = $("#edicion").val();
                }
                edicion = "";
            }

            serie = [{
                name: 'Obsolete',
                data: [0, data[0].obsoleto, 0],
                color: '<?= $color4 ?>'
            }, {
                name: 'Net',
                data: [0, 0, data[0].neto],
                color: '<?= $color3 ?>'
            }, {
                name: 'Installations',
                data: [0, data[0].instalacion, 0],
                color: '<?= $color2 ?>'
            }, {
                name: 'Purchases',
                data: [parseInt(data[0].compra), 0, 0],
                color: '<?= $color1 ?>'
            }];

            $(function () {
                $('#containerDetalle').highcharts({
                    chart: {
                        type: 'bar'
                    },
                    title: {
                         text: familia + ' ' + edicion
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        categories: ['', '', ''],
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                            }
                        }
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                         stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    legend: {
                       reversed: true
                    },
                    plotOptions: {
                        dataLabels: {
                            enabled: true
                        },
                        series: {
                           stacking: 'normal'
                        }
                    },
                    tooltip: {
                        headerFormat: ''
                    },
                    series: serie
                });
            });

            $("#tablaDetalle").append(data[0].tabla);
            $("#divTabla").show();
            $("#containerDetalle").show();
            $("#fondo").hide();
        }, "json")
        .fail(function( jqXHR ){
            $("#fondo").hide();
            $.alert.open('error', "Error: " + jqXHR.status);
        });
    }
    
    function mostrarTabla(vert, ordenar, opcion){
        if(vertAux === vert && $("#opcion").val() === opcion){
            if($('#ttabla1').is(':visible')){
                $('#ttabla1').hide();
                //$("#export").hide();
                $("#divExport").hide();
            }
            else{
                $('#ttabla1').show();
                //$("#export").show();
                $("#divExport").show();
            }
        }
        else{
            $("#fondo").show();
            vertAux = vert;
            $("#opcion").val(opcion);
            
            if(vert === 3){
                url = "ajax/optimizacionDetalle.php";
            }
            else if(vert === 5){
                url = "ajax/detallesEquipoResumen.php";
            }    
            
            $.post(url, { ordenar : ordenar, direccion : $("#direccion").val(), opcion : opcion, asignacion : '<?= $asig ?>', token : localStorage.smartControlToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>   
                
                $('#ttabla1').empty();
                $('#ttabla1').append(data[0].tabla);
                $('#ttabla1').show();
                //$("#export").show();
                $("#divExport").show();
                $("#fondo").hide();
            }, "json")
            .fail(function( jqXHR ){
                $("#fondo").hide();
                $.alert.open('error', "Error: " + jqXHR.status);
            });
        }
    }
    
    function desactivarFamilia(){
        $("#divTabla").hide();
        $("#containerDetalle").hide();
        $("#balWindows").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindows.png");
        $("#balOffice").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOffice.png");
        $("#balVisio").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnVisio.png");
        $("#balProject").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnProject.png");
        $("#balWindowsServer").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsServer.png");
        $("#balSQLServer").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSQLServer.png");
        $("#balVisual").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnVisualStudio.png");
        $("#balExchange").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnExchangeServer.png");
        $("#balSharepoint").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSharepointServer.png");
        $("#balSkype").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSkypeforBusiness.png");
        $("#balSystem").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSystemCenter.png");
    }
    
    function activarBtnEdition(btn){
        familia = $("#familia").val();
        edicion = $("#edicion").val();
        
        if(familia === "Windows Server" && (edicion === "Standard" || edicion === "Enterprise")){
            familia = "Windows";
        } else if(familia === "Windows Server" && edicion === "Datacenter"){
            familia = "WindowsServer";
        }  
        
        if(familia === "SQL Server"){
            familia = "SQLServer";
        }
        
        if(edicion === "Others"){
            familia = "";
        }
        
        if($("#familia").val() === "Windows"){
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsEnterprise.png");
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsProfessional.png");
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOthers.png");  
        } else if($("#familia").val() === "Office"){
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOfficeStandard.png");
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOfficeProfessional.png");
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOthers.png"); 
        } else if($("#familia").val() === "Visio"){
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnVisioStandard.png");
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnVisioProfessional.png");
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOthers.png"); 
        } else if($("#familia").val() === "Project"){
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnProjectStandard.png");
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnProjectProfessional.png");
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOthers.png"); 
        } else if($("#familia").val() === "Windows Server"){
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsStandard.png");
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsServerDatacenter.png");
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnWindowsEnterprise.png"); 
            $("#imgEdition4").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOthers.png"); 
        } else if($("#familia").val() === "SQL Server"){
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSQLServerStandard.png");
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSQLServerDatacenter.png");
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnSQLServerEnterprise.png"); 
            $("#imgEdition4").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btnOthers.png"); 
        }
        
        if(btn === 1){
            $("#imgEdition1").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btn" + familia + edicion + "Sel.png");
        } else if(btn === 2){
            $("#imgEdition2").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btn" + familia + edicion + "Sel.png");
        } else if(btn === 3){
            $("#imgEdition3").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btn" + familia + edicion + "Sel.png");
        } else if(btn === 4){            
            $("#imgEdition4").attr("src", "<?= $GLOBALS["domain_root"] ?>/img/btn" + familia + edicion + "Sel.png");
        }
    }
</script>
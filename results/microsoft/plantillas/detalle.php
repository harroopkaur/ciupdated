<div style="width:95%; overflow:hidden; margin:0 auto; margin-top:20px; margin-bottom:20px;">
    <div style="width:20%; margin:0; padding:0px; overflow:hidden; float:left;">
        <img src="<?= $GLOBALS["domain_root"] ?>/img/imgMicrosoft.png" style="width:70%; height:auto;">
        <br><br>
        
        <div class="form-group">
            <select name="company" id="company" class="form-control"> 
                <option value="">Company</option>
                <?php 
                foreach($listadoEmpresas as $rowEmpresa){
                ?>
                <option value="<?= $rowEmpresa["id"] ?>" <?php if($rowEmpresa["id"] == $_SESSION["client_id"]){ echo "selected='selected'"; } ?>><?= $rowEmpresa["empresa"] ?></option>
                <?php 
                }
                ?>
            </select>
        </div>
       
        <br>

        <span style="color:#0070c0; font-weight:bold; font-size:1vw;">Select the product</span><br>
        
        <ul class="nav nav-tabs" id="myTab">
            <li class="<?php if($vert == 0 || $vert == 1 || $vert == 2 || $vert == 3 || $vert == 5 || $vert == 51 || $vert == 52 
            || $vert == 53 || $vert == 6 || $vert == 61 || $vert == 62 || $vert == 63 || $vert == 7 || $vert == 71 || $vert == 72 || $vert == 73){ 
                echo 'Active';
            }?>"><a href="#client" data-toggle="tab" onclick="location.href='detail.php';" style="font-size:1vw;">Client</a></li>
            <li class="<?php if($vert == 8 || $vert == 81 || $vert == 82 || $vert == 83 || $vert == 84 || $vert == 9 || $vert == 91 
            || $vert == 92 || $vert == 93 || $vert == 94){ 
                echo 'Active';
            }?>"><a href="#server" data-toggle="tab" onclick="location.href='detail.php?vert=8';" style="font-size:1vw;">Server</a></li>
            <li class="<?php if($vert == 10 || $vert == 11 || $vert == 12 || $vert == 13 || $vert == 14){ 
                echo 'Active';
            }?>"><a href="#hibrid" data-toggle="tab" onclick="location.href='detail.php?vert=10';" style="font-size:1vw;">Hibrid</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane <?php if($vert == 0 || $vert == 1 || $vert == 2 || $vert == 3 || $vert == 5 || $vert == 51 || $vert == 52 
            || $vert == 53 || $vert == 6 || $vert == 61 || $vert == 62 || $vert == 63 || $vert == 7 || $vert == 71 || $vert == 72 || $vert == 73){ 
                echo 'Active';
            }?>" id="client" style="background:#ffffff; overflow:hidden;">
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgWindows ?>" style="width:100%; height:auto;" onclick="location.href='detail.php';">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgOffice ?>" style="width:100%; height:auto;" onclick="location.href='detail.php?vert=5';">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgVisio ?>" style="width:100%; height:auto;" onclick="location.href='detail.php?vert=7';">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgProject ?>" style="width:100%; height:auto;" onclick="location.href='detail.php?vert=6';">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="tab-pane <?php if($vert == 8 || $vert == 81 || $vert == 82 || $vert == 83 || $vert == 84 || $vert == 9 || $vert == 91 
            || $vert == 92 || $vert == 93 || $vert == 94){ 
                echo 'Active';
            }?>" id="server" style="background:#ffffff; overflow:hidden;">
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgWindowsServer ?>" style="width:100%; height:auto;" onclick="location.href='detail.php?vert=8';">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgSQLServer ?>" style="width:100%; height:auto;" onclick="location.href='detail.php?vert=9';">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="tab-pane <?php if($vert == 10 || $vert == 11 || $vert == 12 || $vert == 13 || $vert == 14){ 
                echo 'Active';
            }?>" id="hibrid" style="background:#ffffff; overflow:hidden;">
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgVisual ?>" style="width:100%; height:auto;" onclick="location.href='detail.php?vert=10';">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgExchange ?>" style="width:100%; height:auto;" onclick="location.href='detail.php?vert=11';">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgSharepoint ?>" style="width:100%; height:auto;" onclick="location.href='detail.php?vert=12';">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgSkype ?>" style="width:100%; height:auto;" onclick="location.href='detail.php?vert=13';">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgSystem ?>" style="width:100%; height:auto;" onclick="location.href='detail.php?vert=14';">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        
        <br>
        
        <ul class="nav nav-tabs <?php if($vert == 10 || $vert == 11 || $vert == 12 || $vert == 13 || $vert == 14){ 
            echo 'hide';
        }?>" id="myTabEdition">
            <li class="active"><a href="#edition" data-toggle="tab" style="font-size:1vw;">Edition</a></li>
        </ul>

        <div class="tab-content <?php if($vert == 10 || $vert == 11 || $vert == 12 || $vert == 13 || $vert == 14){ 
            echo 'hide';
        }?>">
            <div class="tab-pane active" id="edition" style="background:#ffffff; overflow:hidden;">
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgEdition1 ?>" style="width:100%; height:auto;" onclick="location.href='<?= $rutaEdition1 ?>';">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgEdition2 ?>" style="width:100%; height:auto;" onclick="location.href='<?= $rutaEdition2 ?>';">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgEdition3 ?>" style="width:100%; height:auto;" onclick="location.href='<?= $rutaEdition3 ?>';">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 <?php if($vert != 8 && $vert != 81 && $vert != 82 && $vert != 83 && $vert != 84 
                        && $vert != 9 && $vert != 91 && $vert != 92 && $vert != 93 && $vert != 94){ echo 'hide'; } ?>" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgEdition4 ?>" style="width:100%; height:auto;" onclick="location.href='<?= $rutaEdition4 ?>';">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--<br>
            
        <div>Allocation:<strong style="color:#000; font-weight:bold;">
            <?//= $asig ?></strong>
            
            <br>
            <select onchange="MM_jumpMenu('self',this,0);" style="width:100px;">
                <option value="detail.php" selected="selected">Select..</option>
                <option value="detail.php?vert=<?//= $vert ?>">All</option>
                <?php 
                /*foreach($asignaciones as $row){
                ?>
                    <option value="detalle.php?vert=<?= $vert ?>&asig=<?= $row["asignacion"] ?>"><?= $row["asignacion"] ?></option>
                <?php
                }*/
                ?>
            </select>
        </div>--> 
        <br>
        
        <ul class="nav nav-tabs" id="myTabDuplicate">
            <li class="active"><a href="#duplicate" data-toggle="tab" style="font-size:1vw;">Duplicate</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="duplicate" style="background:#ffffff; overflow:hidden;">
                <div class="row-personal">
                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgDuplicateYes ?>" style="width:100%; height:auto;" onclick="location.href='detail.php?vert=<?= $vert ?>&asig=<?= $asig ?>';">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgDuplicateNo ?>" style="width:100%; height:auto;" onclick="location.href='detail.php?vert=<?= $vert ?>&asig=<?= $asig ?>&dup=No';">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <br>
        
        <ul class="nav nav-tabs" id="myTabReport">
            <li class="active"><a href="#tabExport" data-toggle="tab" style="font-size: 1vw;">Export</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tabExport" style="background:#ffffff; overflow:hidden;">
                <div class="row-personal">
                     <form id="formExportar" name="formExportar" method="post" action="reportes/excelMicrosoftDetalle.php">
                        <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                            <div class="col-sm-12 btnSombreado">
                                <div class="row">
                                    <input type="hidden" id="vert" name="vert" value="<?= $vert ?>">
                                    <input type="hidden" id="asig" name="asig" value="<?= $asig ?>">
                                    <input type="hidden" id="dup" name="dup" value="<?= $dup ?>">
                                    <input type="submit" id="exportar" name="exportar" style="display:none">
                                    <img src="<?= $imgExport ?>" style="width:100%; height:auto;" id="export">
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="col-sm-6" style="margin-top:1vw; margin-bottom:1vw;">
                        <div class="col-sm-12 btnSombreado">
                            <div class="row">
                                <img src="<?= $imgExportAll ?>" style="width:100%; height:auto;" id="exportTodo" onclick="window.open('reportes/excelDetalleTodo.php?asig=<?= $asig ?>&dup=<?= $dup ?>')">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="width:80%; float:left; margin:0px; padding:0px; min-height:450px; overflow:hidden;">
        <div id="container3" style=" <?php if($vert==0 || $vert==8){ echo 'display:none;';  }  ?> height:30vw; width:99%; margin:10px; float:left;"></div>
        <div id="container1" style=" <?php if($vert!=0 && $vert!=8){ echo 'display:none;';  }  ?> height:30vw; width:47%; margin:10px; float:left;"></div>
        <div id="container2" style=" <?php if($vert!=0 && $vert!=8){ echo 'display:none;';  }  ?> height:30vw; width:47%; margin:10px; float:right;"></div>
        <?php
        if($vert == 0 || $vert == 8){
        ?>
            <div  style=" width:47%; margin:10px; float:left; text-align:center; font-size:1vw;"><a onclick="mostrarTabla(<?=$vert?>, 1, '<?= $asig ?>', '<?= $dup ?>');" style="cursor:pointer;" ><img src="<?= $GLOBALS["domain_root"] ?>/img/verDetalle.png" style="height:2vw; width:auto;"></a></div>
            <div  style=" width:47%; margin:10px; float:right; text-align:center; font-size:1vw;"><a onclick="mostrarTabla(<?=$vert?>, 2, '<?= $asig ?>', '<?= $dup ?>');" style="cursor:pointer;" ><img src="<?= $GLOBALS["domain_root"] ?>/img/verDetalle.png" style="height:2vw; width:auto;"></a></div>
        <?php
        }
        else{
        ?>
            <div  style=" width:99%; margin:10px; float:left; text-align:center; font-size:1vw;"><a onclick="mostrarTabla(<?=$vert?>, 0, '<?= $asig ?>', '<?= $dup ?>');" style="cursor:pointer;" ><img src="<?= $GLOBALS["domain_root"] ?>/img/verDetalle.png" style="height:2vw; width:auto;"></a></div>
        <?php
        }
        ?>
            
        <div id="ttabla1"  style="display:none; width:99%; max-height: 40vw; overflow-y:hidden; overflow-x:auto; 
        clear:both; margin:10px; padding:0;">

        </div>
    </div>
</div>

<script>
    var vertAux = "";
    var opcAux  = "";
    $(document).ready(function(){
        $("#export").click(function(){
            $("#exportar").click();
            $("#export").attr('src','<?= $GLOBALS["domain_root"]?>/img/btnExportExcelSel.png');
            $("#exportTodo").attr('src', '<?= $GLOBALS["domain_root"]?>/img/btnExportAll.png');
        });
        
        $("#exportTodo").click(function(){
            $("#export").attr('src','<?= $GLOBALS["domain_root"]?>/img/btnExportExcel.png');
            $("#exportTodo").attr('src', '<?= $GLOBALS["domain_root"]?>/img/btnExportAllSel.png');
        });
        
        $("#company").change(function(){
            if($("#company").val() === ""){
                return false;
            }
            
            $.post("ajax/cambiarEmpresa.php", { company : $("#company").val(), token : localStorage.smartControlToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                
                if(data[0].result === true){
                    location.reload();
                }
                
                $('#fondo').hide();
            }, "json")
            .fail(function( jqXHR ){
                $('#fondo').hide();
                $.alert.open('error', "Error: " + jqXHR.status);
            });
        });
    });
    
    function mostrarTabla(vert, opcion, asig, dup){
        if(vertAux == vert && opcAux == opcion){
            if($('#ttabla1').is(':visible')){
                $('#ttabla1').hide();
            }
            else{
                $('#ttabla1').show();
            }
        }
        else{
            vertAux = vert;
            opcAux  = opcion;
            $("#fondo").show();
            $.post("ajax/tablasMicrosoftDetalle.php", { vert : vert, opcion : opcion, asig : asig, dup : dup, token : localStorage.smartControlToken }, function(data){
                <?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>
                        
                $('#ttabla1').empty();
                $('#ttabla1').append(data[0].div);
                $('#ttabla1').show();
                $('#fondo').hide();
            }, "json")
            .fail(function( jqXHR ){
                $('#fondo').hide();
                $.alert.open('error', "Error: " + jqXHR.status);
            });
        }
    }
</script>
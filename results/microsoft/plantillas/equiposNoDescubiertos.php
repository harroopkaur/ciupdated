<div id="ttabla1"  style="display:none; width:95%; margin:10px; float:left; height:400px;">
    <table width="95%" class="tablap" id="tablaEquipoNoDescubiertos" style="margin-top: -5px;">
        <thead>
        <tr style="background:#333; color:#fff;">
            <th  align="center" valign="middle"><span>&nbsp;</span></th>
            <th  align="center" valign="middle"><span>Device</span></th>
            <th  align="center" valign="middle"><span>Type</span></th>
            <th  align="center" valign="middle"><span>Challenge</span></th>
        </tr>
        </thead>
        <tbody>
            <?php 
            $i = 1;
            foreach($tablaEscaneo as $row){
                $tipo = "Client"; 
                if($row["tipo"] == "Servidor"){
                    $tipo = "Server";
                }
                
                $errores = $row["errors"];
                if($errores == "Ping Sin Respuesta"){
                    $errores = "Ping if answer";
                }
                
            ?>
                <tr>
                    <td class="text-left"><?= $i ?></td>
                    <td class="text-left"><?= $row["equipo"] ?></td>
                    <td class="text-left"><?= $tipo ?></td>
                    <td class="text-left"><?=  $errores ?></td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>

<div id="ttabla2"  style="display:none; width:95%; margin:10px; float:left; height:400px;">
    <table width="95%" class="tablap" id="tablaEquipoNoDescubiertosActivos" style="margin-top: -5px;">
        <thead>
        <tr style="background:#333; color:#fff;">
            <th  align="center" valign="middle"><span>&nbsp;</span></th>
            <th  align="center" valign="middle"><span>Device</span></th>
            <th  align="center" valign="middle"><span>Type</span></th>
            <th  align="center" valign="middle"><span>Challenge</span></th>
        </tr>
        </thead>
        <tbody>
            <?php 
            $i = 1;
            foreach($tablaEscaneoActivos as $row){
                $tipo = "Client"; 
                if($row["tipo"] == "Servidor"){
                    $tipo = "Server";
                }
                
                $errores = $row["errors"];
                if($errores == "Ping Sin Respuesta"){
                    $errores = "Ping if answer";
                }
            ?>
                <tr>
                    <td class="text-left"><?= $i ?></td>
                    <td class="text-left"><?= $row["equipo"] ?></td>
                    <td class="text-left"><?= $tipo ?></td>
                    <td class="text-left"><?= $errores ?></td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function(){
        $("#tablaEquipoNoDescubiertos").tableHeadFixer();
        $("#tablaEquipoNoDescubiertos").tablesorter();
        
        $("#tablaEquipoNoDescubiertosActivos").tableHeadFixer();
        $("#tablaEquipoNoDescubiertosActivos").tablesorter();
    });
</script>
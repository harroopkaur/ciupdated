<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
//require_once($GLOBALS["app_root"] . "/clases/clase_balance2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resultados_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresas.php");

$exito  = 0;
$error  = 0;
$exito2 = 0;
$error2 = 0;
$exito3 = 0;
$error3 = 0;

/*$balanceg  = new Balance_f();
/*$balance4g = new Balance_f();
$balance2g = new Balance_f();
$balance3g = new Balance_f();*/
$detalles1 = new DetallesE_f();
$detalles2 = new DetallesE_f();
$resumen   = new Resumen_f();
$general = new General();
$claseResultadosGeneral = new clase_resultados_general();
$empresas = new empresas();

$total_1LAc  = 0;
$total_2DVc  = 0;
$total_1LAs  = 0;
$total_2DVs  = 0;
$total_1     = 0;

$total_1LAc2 = 0;
$total_2DVc2 = 0;
$total_1LAs2 = 0;
$total_2DVs2 = 0;
$total_2     = 0;

$total_1LAc3 = 0;
$total_2DVc3 = 0;
$total_1LAs3 = 0;
$total_2DVs3 = 0;
$total_3     = 0;

$total_1LAc4 = 0;
$total_2DVc4 = 0;
$total_1LAs4 = 0;
$total_2DVs4 = 0;
$total_4     = 0;

$tclient     = 0;
$tclient2    = 0;
$tclient3    = 0;
$tclient4    = 0;

$tserver     = 0;
$tserver2    = 0;
$tserver3    = 0;
$tserver4    = 0;

$uso1        = 0;
$nouso1      = 0;
$uso2        = 0;
$nouso2      = 0;
$uso3        = 0;
$nouso3      = 0;
$uso4        = 0;
$nouso4      = 0;

$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#243C67';
$color4 = '#00AFF0';

$imgWindows = $GLOBALS["domain_root"] . "/img/btnWindows.png";
$imgOffice = $GLOBALS["domain_root"] . "/img/btnOffice.png";
$imgVisio = $GLOBALS["domain_root"] . "/img/btnVisio.png";
$imgProject = $GLOBALS["domain_root"] . "/img/btnProject.png";
$imgWindowsServer = $GLOBALS["domain_root"] . "/img/btnWindowsServer.png";
$imgSQLServer = $GLOBALS["domain_root"] . "/img/btnSQLServer.png";
$imgVisual = $GLOBALS["domain_root"] . "/img/btnVisualStudio.png";
$imgExchange = $GLOBALS["domain_root"] . "/img/btnExchangeServer.png";
$imgSharepoint = $GLOBALS["domain_root"] . "/img/btnSharepointServer.png";
$imgSkype = $GLOBALS["domain_root"] . "/img/btnSkypeforBusiness.png";
$imgSystem = $GLOBALS["domain_root"] . "/img/btnSystemCenter.png";
$imgDuplicateYes = $GLOBALS["domain_root"] . "/img/btnDuplicateYes.png";
$imgDuplicateNo = $GLOBALS["domain_root"] . "/img/btnDuplicateNo.png";

$imgEdition1 = $GLOBALS["domain_root"] . "/img/btnWindowsStandard.png";
$imgEdition2 = $GLOBALS["domain_root"] . "/img/btnWindowsProfessional.png";
$imgEdition3 = $GLOBALS["domain_root"] . "/img/btnWindowsEnterprise.png";
$imgEdition4 = $GLOBALS["domain_root"] . "/img/btnOthers.png";

$rutaEdition1 = $GLOBALS["domain_root"] . "/img/btnWindowsStandard.png";
$rutaEdition2 = $GLOBALS["domain_root"] . "/img/btnWindowsProfessional.png";
$rutaEdition3 = $GLOBALS["domain_root"] . "/img/btnWindowsEnterprise.png";
$rutaEdition4 = $GLOBALS["domain_root"] . "/img/btnOthers.png";

$imgExport = $GLOBALS["domain_root"] . "/img/btnExportExcel.png";
$imgExportAll = $GLOBALS["domain_root"] . "/img/btnExportAll.png";

$dup = "Si";
if(isset($_GET['dup']) && $_GET['dup'] == "No"){
    $dup = "No";
}

$dupCombo = $dup;

if(/*$_SESSION["idioma"] == 2 &&*/ $dup == "Si"){
    $dupCombo = "Yes";
    $imgDuplicateYes = $GLOBALS["domain_root"] . "/img/btnDuplicateYesSel.png";
} else{
    $imgDuplicateNo = $GLOBALS["domain_root"] . "/img/btnDuplicateNoSel.png";
}

if(isset($_GET['vert']) && filter_var($_GET['vert'], FILTER_VALIDATE_INT) !== false){
    $vert = $_GET['vert'];
}
else{
    $vert = 0;
}

$asig = "";
if(isset($_GET['asig'])){
    $asig = $general->get_escape($_GET["asig"]);
}

$asignaciones = array(); //$general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);

if($vert==0){	
    $imgWindows = $GLOBALS["domain_root"] . "/img/btnWindowsSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnWindowsEnterprise.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnWindowsProfessional.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=1";
    $rutaEdition2 = "detail.php?vert=2";
    $rutaEdition3 = "detail.php?vert=3";
    
    $listar_equipos=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
    'enterprise', $asig, $asignaciones);
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==1){//cliente
                $tclient=($tclient+1);
                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAc=($total_1LAc+1);
                }

                if($reg_equipos["rango"]==1){
                    $total_2DVc=($total_2DVc+1);

                    }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVc=($total_2DVc+1);

                }
                $total_1=($total_1+1);
            }
        }

        $uso1=$total_2DVc;
        $nouso1=$total_1-$uso1;
    }//1
		
    $listar_equipos2=$detalles1->listar_todog1Asignacion($_SESSION['client_id'],  
    'professional', $asig, $asignaciones);
    if($listar_equipos2){
        foreach($listar_equipos2 as $reg_equipos2){

            if($reg_equipos2["tipo"]==1){//cliente
                $tclient2=($tclient2+1);
                if($reg_equipos2["errors"]=='Ninguno'){
                    $total_1LAc2=($total_1LAc2+1);
                }

                if($reg_equipos2["rango"]==1){
                    $total_2DVc2=($total_2DVc2+1);

                }else if($reg_equipos2["rango"]==2 || $reg_equipos2["rango"]==3){
                    $total_2DVc2=($total_2DVc2+1);

                }
                $total_2=($total_2+1);
            }
        }
        
        $uso2=$total_2DVc2;
        $nouso2=$total_2-$uso2;
    }//2
		
    $listar_equipos3=$detalles1->listar_todog2Asignacion($_SESSION['client_id'], 
    'enterprise', 'professional', $asig, $asignaciones);
    if($listar_equipos3){
        foreach($listar_equipos3 as $reg_equipos3){

            if($reg_equipos3["tipo"]==1){//cliente
                    $tclient3=($tclient3+1);
                    if($reg_equipos3["errors"]=='Ninguno'){
                        $total_1LAc3=($total_1LAc3+1);
                    }

                    if($reg_equipos3["rango"]==1){
                        $total_2DVc3=($total_2DVc3+1);

                        }else if($reg_equipos3["rango"]==2 || $reg_equipos3["rango"]==3){
                        $total_2DVc3=($total_2DVc3+1);

                    }
                $total_3=($total_3+1);
            }
        }

        $uso3=$total_2DVc3;
        $nouso3=$total_3-$uso3;
    }//3
}//ver 0

//ver 1
if($vert==1){
    $imgWindows = $GLOBALS["domain_root"] . "/img/btnWindowsSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnWindowsEnterpriseSel.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnWindowsProfessional.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=1";
    $rutaEdition2 = "detail.php?vert=2";
    $rutaEdition3 = "detail.php?vert=3";
    
    $listar_equipos=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
    'enterprise', $asig, $asignaciones);
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==1){//cliente
                $tclient=($tclient+1);
                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAc=($total_1LAc+1);
                }

                if($reg_equipos["rango"]==1){
                    $total_2DVc=($total_2DVc+1);

                    }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVc=($total_2DVc+1);

                }
                $total_1=($total_1+1);
            }
            
        }

        $uso1=$total_2DVc;
        $nouso1=$total_1-$uso1;
    }//1
}//ver 1

//ver 2
if($vert==2){
    $imgWindows = $GLOBALS["domain_root"] . "/img/btnWindowsSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnWindowsEnterprise.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnWindowsProfessionalSel.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=1";
    $rutaEdition2 = "detail.php?vert=2";
    $rutaEdition3 = "detail.php?vert=3";
    
    $listar_equipos=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
    'professional', $asig, $asignaciones);
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==1){//cliente
                $tclient=($tclient+1);
                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAc=($total_1LAc+1);
                }

                if($reg_equipos["rango"]==1){
                    $total_2DVc=($total_2DVc+1);

                }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVc=($total_2DVc+1);

                }
                $total_1=($total_1+1);
            }
        }
        
        $uso1=$total_2DVc;
        $nouso1=$total_1-$uso1;
    }//1
}// ver 2

//ver 3
if($vert==3){
    $imgWindows = $GLOBALS["domain_root"] . "/img/btnWindowsSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnWindowsEnterprise.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnWindowsProfessional.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthersSel.png";
    
    $rutaEdition1 = "detail.php?vert=1";
    $rutaEdition2 = "detail.php?vert=2";
    $rutaEdition3 = "detail.php?vert=3";
    
    $listar_equipos3=$detalles1->listar_todog2Asignacion($_SESSION['client_id'], 
    'enterprise', 'professional', $asig, $asignaciones);
    if($listar_equipos3){
        foreach($listar_equipos3 as $reg_equipos3){

            if($reg_equipos3["tipo"]==1){//cliente
                $tclient3=($tclient3+1);
                if($reg_equipos3["errors"]=='Ninguno'){
                    $total_1LAc3=($total_1LAc3+1);
                }

                if($reg_equipos3["rango"]==1){
                    $total_2DVc3=($total_2DVc3+1);

                    }else if($reg_equipos3["rango"]==2 || $reg_equipos3["rango"]==3){
                    $total_2DVc3=($total_2DVc3+1);

                }
                $total_3=($total_3+1);
            }
        }
        $uso3=$total_2DVc3;
        $nouso3=$total_3-$uso3;
    }
}//ver 3

$total_i=0;
//ver 5
if($vert==5){
    $imgOffice = $GLOBALS["domain_root"] . "/img/btnOfficeSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnOfficeStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnOfficeProfessional.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=51";
    $rutaEdition2 = "detail.php?vert=52";
    $rutaEdition3 = "detail.php?vert=53";

    $lista_calculo = $claseResultadosGeneral->listar_datos6Asignacion("microsoft", $_SESSION["client_id"], 'Office', '', $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 5

//ver 51
if($vert==51){
    $imgOffice = $GLOBALS["domain_root"] . "/img/btnOfficeSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnOfficeStandardSel.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnOfficeProfessional.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=51";
    $rutaEdition2 = "detail.php?vert=52";
    $rutaEdition3 = "detail.php?vert=53";
    
    $lista_calculo = $claseResultadosGeneral->listar_datos6Asignacion("microsoft", $_SESSION["client_id"], 'Office', 'Standard', $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 51

//ver 52
if($vert==52){	
    $imgOffice = $GLOBALS["domain_root"] . "/img/btnOfficeSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnOfficeStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnOfficeProfessionalSel.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=51";
    $rutaEdition2 = "detail.php?vert=52";
    $rutaEdition3 = "detail.php?vert=53";
   
    $lista_calculo = $claseResultadosGeneral->listar_datos6Asignacion("microsoft", $_SESSION["client_id"], 'Office', 'Professional', $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 52

//ver 53
if($vert==53){	
    $imgOffice = $GLOBALS["domain_root"] . "/img/btnOfficeSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnOfficeStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnOfficeProfessional.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthersSel.png";
    
    $rutaEdition1 = "detail.php?vert=51";
    $rutaEdition2 = "detail.php?vert=52";
    $rutaEdition3 = "detail.php?vert=53";
    
    $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION["client_id"], 'Office', 'Standard', 'Professional', 
    $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 53

//ver 6
if($vert==6){	
    $imgProject = $GLOBALS["domain_root"] . "/img/btnProjectSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnProjectStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnProjectProfessional.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=61";
    $rutaEdition2 = "detail.php?vert=62";
    $rutaEdition3 = "detail.php?vert=63";

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'Project', '', $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 6

//ver 61
if($vert==61){	
    $imgProject = $GLOBALS["domain_root"] . "/img/btnProjectSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnProjectStandardSel.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnProjectProfessional.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=61";
    $rutaEdition2 = "detail.php?vert=62";
    $rutaEdition3 = "detail.php?vert=63";
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'Project', 'Standard', $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 61

//ver 62
if($vert==62){	
    $imgProject = $GLOBALS["domain_root"] . "/img/btnProjectSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnProjectStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnProjectProfessionalSel.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=61";
    $rutaEdition2 = "detail.php?vert=62";
    $rutaEdition3 = "detail.php?vert=63";
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'Project', 'Professional', 
    $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 62

//ver 63
if($vert==63){	
    $imgProject = $GLOBALS["domain_root"] . "/img/btnProjectSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnProjectStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnProjectProfessional.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthersSel.png";
    
    $rutaEdition1 = "detail.php?vert=61";
    $rutaEdition2 = "detail.php?vert=62";
    $rutaEdition3 = "detail.php?vert=63";
    
    $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION["client_id"], 'Project', 'Standard', 
    'Professional', $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 63

//ver 7
if($vert==7){	
    $imgVisio = $GLOBALS["domain_root"] . "/img/btnVisioSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnVisioStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnVisioProfessional.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=71";
    $rutaEdition2 = "detail.php?vert=72";
    $rutaEdition3 = "detail.php?vert=73";
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'Visio', '', $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 7

//ver 71
if($vert==71){	
    $imgVisio = $GLOBALS["domain_root"] . "/img/btnVisioSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnVisioStandardSel.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnVisioProfessional.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=71";
    $rutaEdition2 = "detail.php?vert=72";
    $rutaEdition3 = "detail.php?vert=73";
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'Visio', 'Standard', $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 71

//ver 72
if($vert==72){	
    $imgVisio = $GLOBALS["domain_root"] . "/img/btnVisioSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnVisioStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnVisioProfessionalSel.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=71";
    $rutaEdition2 = "detail.php?vert=72";
    $rutaEdition3 = "detail.php?vert=73";
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'Visio', 'Professional', 
    $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 72

//ver 73
if($vert==73){	
    $imgVisio = $GLOBALS["domain_root"] . "/img/btnVisioSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnVisioStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnVisioProfessional.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnOthersSel.png";
    
    $rutaEdition1 = "detail.php?vert=71";
    $rutaEdition2 = "detail.php?vert=72";
    $rutaEdition3 = "detail.php?vert=73";
    
    $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION["client_id"], 
    'Visio', 'Standard', 'Professional', $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 73

//ver 8
if($vert==8){	
    $imgWindowsServer = $GLOBALS["domain_root"] . "/img/btnWindowsServerSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnWindowsStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnWindowsServerDatacenter.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnWindowsEnterprise.png";
    $imgEdition4 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=81";
    $rutaEdition2 = "detail.php?vert=82";
    $rutaEdition3 = "detail.php?vert=83";
    $rutaEdition4 = "detail.php?vert=84";
    
    $listar_equipos=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
    'Standard', $asig, $asignaciones);
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==2){//cliente

                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAs=($total_1LAs+1);
                }

                $tserver=($tserver+1);

                if($reg_equipos["rango"]==1){
                    $total_2DVs=($total_2DVs+1);

                }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVs=($total_2DVs+1);
                }

                $total_1=($total_1+1);
            }

        }

        $uso1=$total_2DVs;
        $nouso1=$total_1-$uso1;	
    }//1
    
    $listar_equipos2=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
    'Datacenter', $asig, $asignaciones);
    if($listar_equipos2){
        foreach($listar_equipos2 as $reg_equipos2){

            if($reg_equipos2["tipo"]==2){//cliente					
                if($reg_equipos2["errors"]=='Ninguno'){
                    $total_1LAs2=($total_1LAs2+1);
                }

                $tserver2=($tserver2+1);

                if($reg_equipos2["rango"]==1){
                    $total_2DVs2=($total_2DVs2+1);

                }else if($reg_equipos2["rango"]==2 || $reg_equipos2["rango"]==3){
                    $total_2DVs2=($total_2DVs2  +1);
                }

                $total_2=($total_2+1);
            }
        }
        $uso2=$total_2DVs2;
        $nouso2=$total_2-$uso2;
    }//2
    
    $listar_equipos3=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
    'Enterprise', $asig, $asignaciones);
    if($listar_equipos3){
        foreach($listar_equipos3 as $reg_equipos3){

            if($reg_equipos3["tipo"]==2){//cliente					
                if($reg_equipos3["errors"]=='Ninguno'){
                    $total_1LAs3=($total_1LAs3+1);
                }

                $tserver3=($tserver3+1);

                if($reg_equipos3["rango"]==1){
                    $total_2DVs3=($total_2DVs3+1);

                }else if($reg_equipos3["rango"]==2 || $reg_equipos3["rango"]==3){
                    $total_2DVs3=($total_2DVs3  +1);
                }

                $total_3=($total_3+1);
            }
        }
        $uso3=$total_2DVs3;
        $nouso3=$total_3-$uso3;
    }//2

    /*$listar_equipos3=$detalles1->listar_todog2Asignacion($_SESSION['client_id'], 
    'enterprise', 'professional', $asig, $asignaciones);*/
    $listar_equipos4=$detalles1->listar_todog3Asignacion($_SESSION['client_id'], $asig, $asignaciones);
    if($listar_equipos4){
        foreach($listar_equipos4 as $reg_equipos4){

            if($reg_equipos4["tipo"]==2){//cliente					
                if($reg_equipos4["errors"]=='Ninguno'){
                    $total_1LAs4=($total_1LAs4+1);
                }

                $tserver4=($tserver4+1);

                if($reg_equipos4["rango"]==1){
                    $total_2DVs4=($total_2DVs4+1);

                }else if($reg_equipos4["rango"]==2 || $reg_equipos4["rango"]==3){
                    $total_2DVs4=($total_2DVs4+1);
                }

                $total_4=($total_4+1);	
            }
        }
        $uso4=$total_2DVs4;
        $nouso4=$total_4-$uso4;


    }//3
}//ver 8

//ver 81
if($vert==81){	
    $imgWindowsServer = $GLOBALS["domain_root"] . "/img/btnWindowsServerSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnWindowsStandardSel.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnWindowsServerDatacenter.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnWindowsEnterprise.png";
    $imgEdition4 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=81";
    $rutaEdition2 = "detail.php?vert=82";
    $rutaEdition3 = "detail.php?vert=83";
    $rutaEdition4 = "detail.php?vert=84";
    
    $listar_equipos=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
    'Standard', $asig, $asignaciones);
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==2){//cliente

                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAs=($total_1LAs+1);
                }

                $tserver=($tserver+1);

                if($reg_equipos["rango"]==1){
                    $total_2DVs=($total_2DVs+1);

                }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVs=($total_2DVs+1);
                }

                $total_1=($total_1+1);
            }


        }

        $uso1=$total_2DVs;
        $nouso1=$total_1-$uso1;


    }//1
}//ver 81

//ver 82
if($vert==82){		
    $imgWindowsServer = $GLOBALS["domain_root"] . "/img/btnWindowsServerSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnWindowsStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnWindowsServerDatacenterSel.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnWindowsEnterprise.png";
    $imgEdition4 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=81";
    $rutaEdition2 = "detail.php?vert=82";
    $rutaEdition3 = "detail.php?vert=83";
    $rutaEdition4 = "detail.php?vert=84";
    
    $listar_equipos=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
    'Datacenter', $asig, $asignaciones);
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==2){//cliente

                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAs=($total_1LAs+1);
                }

                $tserver=($tserver+1);

                if($reg_equipos["rango"]==1){
                    $total_2DVs=($total_2DVs+1);

                }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVs=($total_2DVs+1);
                }

                $total_1=($total_1+1);
            }


        }

        $uso1=$total_2DVs;
        $nouso1=$total_1-$uso1;


    }//1
}//ver 82

//ver 83
if($vert==83){		
    $imgWindowsServer = $GLOBALS["domain_root"] . "/img/btnWindowsServerSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnWindowsStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnWindowsServerDatacenter.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnWindowsEnterpriseSel.png";
    $imgEdition4 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=81";
    $rutaEdition2 = "detail.php?vert=82";
    $rutaEdition3 = "detail.php?vert=83";
    $rutaEdition4 = "detail.php?vert=84";
    
    $listar_equipos=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
    'Enterprise', $asig, $asignaciones);
    if($listar_equipos){
        foreach($listar_equipos as $reg_equipos){

            if($reg_equipos["tipo"]==2){//cliente

                if($reg_equipos["errors"]=='Ninguno'){
                    $total_1LAs=($total_1LAs+1);
                }

                $tserver=($tserver+1);

                if($reg_equipos["rango"]==1){
                    $total_2DVs=($total_2DVs+1);

                }else if($reg_equipos["rango"]==2 || $reg_equipos["rango"]==3){
                    $total_2DVs=($total_2DVs+1);
                }

                $total_1=($total_1+1);
            }


        }

        $uso1=$total_2DVs;
        $nouso1=$total_1-$uso1;


    }//1
}//ver 83

//ver 84
if($vert==84){ 
    $imgWindowsServer = $GLOBALS["domain_root"] . "/img/btnWindowsServerSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnWindowsStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnWindowsServerDatacenter.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnWindowsEnterprise.png";
    $imgEdition4 = $GLOBALS["domain_root"] . "/img/btnOthersSel.png";
    
    $rutaEdition1 = "detail.php?vert=81";
    $rutaEdition2 = "detail.php?vert=82";
    $rutaEdition3 = "detail.php?vert=83";
    $rutaEdition4 = "detail.php?vert=84";
    
    $listar_equipos3=$detalles1->listar_todog3Asignacion($_SESSION['client_id'], $asig, $asignaciones);
    if($listar_equipos3){
        foreach($listar_equipos3 as $reg_equipos3){

            if($reg_equipos3["tipo"]==2){//cliente

                if($reg_equipos3["errors"]=='Ninguno'){
                    $total_1LAs3=($total_1LAs3+1);
                }

                $tserver3=($tserver3+1);

                if($reg_equipos3["rango"]==1){
                    $total_2DVs3=($total_2DVs3+1);

                }else if($reg_equipos3["rango"]==2 || $reg_equipos3["rango"]==3){
                    $total_2DVs3=($total_2DVs3+1);
                }

                $total_3=($total_3+1);
            }


        }

        $uso3=$total_2DVs3;
        $nouso3=$total_3-$uso3;

    }
}//ver 84

//ver 9
if($vert==9){	
    $imgSQLServer = $GLOBALS["domain_root"] . "/img/btnSQLServerSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnSQLServerStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnSQLServerDatacenter.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnSQLServerEnterprise.png";
    $imgEdition4 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=91";
    $rutaEdition2 = "detail.php?vert=92";
    $rutaEdition3 = "detail.php?vert=93";
    $rutaEdition4 = "detail.php?vert=94";
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'SQL Server', '', 
    $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 9

//ver 91
if($vert==91){	
    $imgSQLServer = $GLOBALS["domain_root"] . "/img/btnSQLServerSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnSQLServerStandardSel.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnSQLServerDatacenter.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnSQLServerEnterprise.png";
    $imgEdition4 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=91";
    $rutaEdition2 = "detail.php?vert=92";
    $rutaEdition3 = "detail.php?vert=93";
    $rutaEdition4 = "detail.php?vert=94";
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'SQL Server', 'Standard', 
    $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 91

//ver 92
if($vert==92){	
    $imgSQLServer = $GLOBALS["domain_root"] . "/img/btnSQLServerSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnSQLServerStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnSQLServerDatacenterSel.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnSQLServerEnterprise.png";
    $imgEdition4 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=91";
    $rutaEdition2 = "detail.php?vert=92";
    $rutaEdition3 = "detail.php?vert=93";
    $rutaEdition4 = "detail.php?vert=94";
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'SQL Server', 'Datacenter', 
    $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 92

//ver 93
if($vert==93){	
    $imgSQLServer = $GLOBALS["domain_root"] . "/img/btnSQLServerSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnSQLServerStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnSQLServerDatacenter.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnSQLServerEnterpriseSel.png";
    $imgEdition4 = $GLOBALS["domain_root"] . "/img/btnOthers.png";
    
    $rutaEdition1 = "detail.php?vert=91";
    $rutaEdition2 = "detail.php?vert=92";
    $rutaEdition3 = "detail.php?vert=93";
    $rutaEdition4 = "detail.php?vert=94";
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'SQL Server', 'Enterprise', 
    $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 93

//ver 94
if($vert==94){	
    $imgSQLServer = $GLOBALS["domain_root"] . "/img/btnSQLServerSel.png";
    $imgEdition1 = $GLOBALS["domain_root"] . "/img/btnSQLServerStandard.png";
    $imgEdition2 = $GLOBALS["domain_root"] . "/img/btnSQLServerDatacenter.png";
    $imgEdition3 = $GLOBALS["domain_root"] . "/img/btnSQLServerEnterprise.png";
    $imgEdition4 = $GLOBALS["domain_root"] . "/img/btnOthersSel.png";
    
    $rutaEdition1 = "detail.php?vert=91";
    $rutaEdition2 = "detail.php?vert=92";
    $rutaEdition3 = "detail.php?vert=93";
    $rutaEdition4 = "detail.php?vert=94";
    
    $lista_calculo = $resumen->listar_datos8Asignacion($_SESSION["client_id"], 'SQL Server', 'Standard', 
    'Datacenter', 'Enterprise', $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 94

//ver 10
if($vert==10){	
    $imgVisual = $GLOBALS["domain_root"] . "/img/btnVisualStudioSel.png";

    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'Visual Studio', '', 
    $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 10

//ver 11
if($vert==11){	
    $imgExchange = $GLOBALS["domain_root"] . "/img/btnExchangeServerSel.png";
    
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'Exchange Server', '', 
    $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 11

//ver 12
if($vert==12){	
    $imgSharepoint = $GLOBALS["domain_root"] . "/img/btnSharepointServerSel.png";
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'Sharepoint Server', '', 
    $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 12

//ver 13
if($vert==13){	
    $imgSkype = $GLOBALS["domain_root"] . "/img/btnSkypeforBusinessSel.png";
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'Skype for Business', '', 
    $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 13

//ver 14
if($vert==14){	
    $imgSystem = $GLOBALS["domain_root"] . "/img/btnSystemCenterSel.png";
    
    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION["client_id"], 'System Center', '', 
    $asig, $asignaciones, $dup);
    $total_i = count($lista_calculo);
}//ver 14

$listadoEmpresas = $empresas->listadoEmp1($_SESSION["reseller"]);
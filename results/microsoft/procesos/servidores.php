<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
require_once($GLOBALS["app_root"] . "/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_modulo_servidores.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resultados_general.php");

$moduloServidores = new moduloServidores();
$claseResultadosGeneral = new clase_resultados_general();

$vert = 0;
if(isset($_GET["vert"]) && filter_var($_GET['vert'], FILTER_VALIDATE_INT) !== false){
    $vert = $_GET["vert"];
}

$moduloServer = new moduloServidores();
$general = new General();
$mostrarBalanza = false;
$tablaBalanza   = array();

if($vert == 0){
    $producto = 5;
    $ediciones = $moduloServidores->obtenerEdicProducto(3, $producto); //3 es el id de fabricante de Microsoft
    
    $existeAlineacionFisico = $moduloServer->existeWindowServerCliente($_SESSION["client_id"], 0, "Fisico");
    if($existeAlineacionFisico > 0){
        $tablaFisico  = $moduloServer->windowServerCliente($_SESSION["client_id"], 0, "Fisico");
        $mostrarBalanza = true;
    }
    else{
        //$tablaFisico  = $moduloServer->windowServer($_SESSION["client_id"], $_SESSION["client_empleado"], "Fisico");
        $tablaFisico = $claseResultadosGeneral->windowServer("microsoft", $_SESSION["client_id"]);
    }
    
    $existeAlineacionVirtual = $moduloServer->existeWindowServerAlineacion($_SESSION["client_id"], 0);
    if($existeAlineacionVirtual > 0){
        $tablaVirtual = $moduloServer->windowServerAlineacion($_SESSION["client_id"], 0);
    }
    else{
        $existe = $moduloServer->existeWindowServerCliente($_SESSION["client_id"], 0, "Virtual");
        if($existe > 0){
            $tablaVirtual = $moduloServer->windowServerCliente($_SESSION["client_id"], 0, "Virtual");
        }
        else{
            //$tablaVirtual = $moduloServer->windowServer($_SESSION["client_id"], $_SESSION["client_empleado"], "Virtual");
            $tablaVirtual = $claseResultadosGeneral->windowServer("microsoft", $_SESSION["client_id"], "Virtual");
        }
        $mostrarBalanza = false;
    }
    
    if($mostrarBalanza){
        $tablaBalanza = $moduloServidores->balanzaServer($_SESSION["client_id"], 0, "Windows Server");
    }
}
else{
    $producto = 13;
    $ediciones = $moduloServidores->obtenerEdicProducto(3, $producto); //3 es el id de fabricante de Microsoft 
    
    $existeAlineacionFisico = $moduloServer->existeSqlServerCliente($_SESSION["client_id"], 0, "Fisico");
    if($existeAlineacionFisico > 0){
        $tablaFisico  = $moduloServer->sqlServerCliente($_SESSION["client_id"], 0, "Fisico");
        $mostrarBalanza = true;
    }
    else{
        $tablaFisico  = $moduloServer->SQLServer($_SESSION["client_id"], 0, "Fisico");
    }
    
    $existeAlineacionVirtual = $moduloServer->existeSqlServerAlineacion($_SESSION["client_id"], 0);
    if($existeAlineacionVirtual > 0){
        $tablaVirtual = $moduloServer->sqlServerAlineacion($_SESSION["client_id"], 0);
    }
    else{
        $existe = $moduloServer->existeSqlServerCliente($_SESSION["client_id"], 0, "Virtual");
        if($existe > 0){
            $tablaVirtual  = $moduloServer->sqlServerCliente($_SESSION["client_id"], 0, "Virtual");
        }
        else{
            $tablaVirtual  = $moduloServer->SQLServer($_SESSION["client_id"], 0, "Virtual");
        }
        $mostrarBalanza = false;
    }
    if($mostrarBalanza){
        $tablaBalanza = $moduloServidores->balanzaServer($_SESSION["client_id"], 0, "SQL Server");
    }
}
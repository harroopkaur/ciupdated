<?php
require_once("../../../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"]);
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_detalles_equipo2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resumen2.php");
require_once($GLOBALS["app_root"] . "/clases/clase_resultados_general.php");

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

$array = array(0=>array('resultado'=>false));
if($nuevo_middleware->compararAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            
            $vert = 0;
            if(isset($_POST["vert"]) && filter_var($_POST['vert'], FILTER_VALIDATE_INT) !== false){
                $vert = $_POST['vert']; 
            }
            
            $opcion = "";
            if(isset($_POST['opcion']) AND filter_var($_POST["opcion"], FILTER_VALIDATE_INT) !== false){
                $opcion = $_POST['opcion'];
            }
            
            $asig = "";
            if(isset($_POST['asig'])){
                $asig = $general->get_escape($_POST['asig']);
            }
            
            $dup = "Si";
            if(isset($_POST['dup']) && $_POST["dup"] == "No"){
                $dup = "No";
            }
            
            
            /*if($dup == "Si"){
                $dup = "Yes";
            }*/

            $detalles1 = new DetallesE_f();
            $detalles2 = new DetallesE_f();
            $resumen   = new Resumen_f();
            $claseResultadosGeneral = new clase_resultados_general();
            
            $asignaciones = array(); //$general->obtenerAsignacionClienteEmpleado($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $tabla = '';
            if($vert == 0 || $vert == 1 || $vert == 2 || $vert == 3 || $vert == 8 || $vert == 81 || $vert == 82 || $vert == 83 || $vert == 84){
                $tabla .= '<table width="99%;" style="margin-top:-5px;" class="tablap" id="tablaMicrosoftDetalle">
                            <thead>
                                <tr style="background:#333; color:#fff;">
                                        <th valign="middle"><span>&nbsp;</span></th>
                                        <th valign="middle"><span>Device Name</span></th>
                                        <th valign="middle"><span>Type</span></th>
                                        <th valign="middle"><span>Operating System</span></th>
                                        <th valign="middle"><span>Active in AD</span></th>
                                        <th valign="middle"><span>LA Tool</span></th>
                                        <th valign="middle"><span>Usability</span></th>
                                </tr>
                            </thead>
                            <tbody>';

                if($vert == 0 || $vert == 8){
                    $listar_equipos0 = $detalles2->listar_todog0Asignacion($_SESSION['client_id'], $asig, $asignaciones);
                    $i = 1;
                    foreach($listar_equipos0 as $reg_equipos0){
                        if($vert == 0){
                            $tipo = 1;
                            $tipoMaquina = 'Client';
                        }
                        else{
                            $tipo = 2;
                            $tipoMaquina = 'Server';
                        }
						
						$ActivoAD = "No";
						if($reg_equipos0["ActivoAD"] == "Si"){
							$ActivoAD = "Yes";
						}
						
						$LaTool = "No";
						if($reg_equipos0["LaTool"] == "Si"){
							$LaTool = "Yes";
						}
						
						$usab = "In Use";
						if($reg_equipos0["usabilidad"] == "Uso Probable"){
							$usab = "Probably in Use";
						} elseif($reg_equipos0["usabilidad"] == "Obsoleto"){
							$usab = "Obsolete";
						}
								
                        if($opcion == 1){
                            if(($reg_equipos0["rango"] == 1 || $reg_equipos0["rango"] == 2 || $reg_equipos0["rango"] == 3) && $reg_equipos0["tipo"] == $tipo){    								
								$tabla .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $reg_equipos0["equipo"] . '</td>
                                    <td>' . $tipoMaquina . '</td>
                                    <td>' . $reg_equipos0["os"] . '</td>
                                    <td>' . $ActivoAD . '</td>
                                    <td>' . $LaTool . '</td>
                                    <td>' . $usab . '</td>
                                    </tr>';
                                 $i++;
                            }
                        }
                        else{
                            if($reg_equipos0["rango"] != 1 && $reg_equipos0["rango"] != 2 && $reg_equipos0["rango"] != 3 && $reg_equipos0["tipo"] == $tipo){
                                $tabla .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $reg_equipos0["equipo"] . '</td>
                                    <td>' . $tipoMaquina . '</td>
                                    <td>' . $reg_equipos0["os"] . '</td>
                                    <td>' . $ActivoAD . '</td>
                                    <td>' . $LaTool . '</td>
                                    <td>' . $usab . '</td>
                                </tr>';
                                 $i++;
                            }
                        }   

                    }
                }
                else{
                    if($vert == 1){
                        $listar_equipos0=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
                        'enterprise', $asig, $asignaciones);
                        $tipo = 1;
                        $tipoMaquina = 'Client';
                    }
                    if($vert == 2){
                        $listar_equipos0=$detalles1->listar_todog1Asignacion($_SESSION['client_id'], 
                        'professional', $asig, $asignaciones);
                        $tipo = 1;
                        $tipoMaquina = 'Client';
                    }
                    if($vert == 3){
                        $listar_equipos0=$detalles1->listar_todog2Asignacion($_SESSION['client_id'],
                        'enterprise','professional', $asig, $asignaciones);
                        $tipo = 1;
                        $tipoMaquina = 'Client';
                    }
                    if($vert == 81){
                        $listar_equipos0=$detalles1->listar_todog1Asignacion($_SESSION['client_id'],
                        'Standard', $asig, $asignaciones);
                        $tipo = 2;
                        $tipoMaquina = 'Server';
                    }
                    if($vert == 82){
                        $listar_equipos0=$detalles1->listar_todog1Asignacion($_SESSION['client_id'],
                        'Datacenter', $asig, $asignaciones);
                        $tipo = 2;
                        $tipoMaquina = 'Server';
                    }
                    if($vert == 83){
                        $listar_equipos0=$detalles1->listar_todog1Asignacion($_SESSION['client_id'],
                        'Enterprise', $asig, $asignaciones);
                        $tipo = 2;
                        $tipoMaquina = 'Server';
                    }
                    if($vert == 84){
                        $listar_equipos0=$detalles1->listar_todog3Asignacion($_SESSION['client_id'], $asig, $asignaciones);
                        $tipo = 2;
                        $tipoMaquina = 'Server';
                    }
                    $i = 1;
                    foreach($listar_equipos0 as $reg_equipos0){
                        if($reg_equipos0["tipo"] == $tipo){
							$ActivoAD = "No";
							if($reg_equipos0["ActivoAD"] == "Si"){
								$ActivoAD = "Yes";
							}
							
							$errors = "No";
							if($reg_equipos0["errors"] == "Si"){
								$errors = "Yes";
							}
							
							$usab = "In Use";
							if($reg_equipos0["usabilidad"] == "Uso Probable"){
								$usab = "Probably in Use";
							} elseif($reg_equipos0["usabilidad"] == "Obsoleto"){
								$usab = "Obsolete";
							}
							
                            $tabla .= '<tr>
                                <td>' . $i . '</td>
                                <td>' . $reg_equipos0["equipo"] . '</td>
                                <td>' . $tipoMaquina . '</td>
                                <td>' . $reg_equipos0["os"] . '</td>
                                <td>' . $ActivoAD . '</td>
                                <td>' . $errors . '</td>
                                <td>' . $usab . '</td>
                            </tr>';
                            $i++;
                        }
                    }
                }
            }
            else{
                $tabla .= '<table width="99%; style="margin-top:-5px;" class="tablap" id="tablaMicrosoftDetalle">
                    <thead>
                        <tr style="background:#333; color:#fff;">
                            <th valign="middle"><span>&nbsp;</span></th>
                            <th valign="middle"><span>Device</span></th>
                            <th valign="middle"><span>Type</span></th>
                            <th valign="middle"><span>Family</span></th>
                            <th valign="middle"><span>Edition</span></th>
                            <th valign="middle"><span>Version</span></th>
                            <th valign="middle"><span>Installation Date</span></th>
                            <th valign="middle"><span>Usability</span></th>
                            <th valign="middle"><span>Observation</span></th>
                        </tr>
                    </thead>
                    <tbody>';
                if($vert == 5){
                    /*$lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Office','', $asig, $asignaciones);*/
                    $lista_calculo = $claseResultadosGeneral->listar_datos6Asignacion("microsoft", $_SESSION["client_id"], 'Office', '', $asig, $asignaciones, $dup);
                }
                if($vert == 51){
                    /*$lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Office','Standard', $asig, $asignaciones);*/
                    $lista_calculo = $claseResultadosGeneral->listar_datos6Asignacion("microsoft", $_SESSION["client_id"], 'Office', 'Standard', $asig, $asignaciones, $dup);
                }
                if($vert == 52){
                    /*$lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'],
                    'Office','Professional', $asig, $asignaciones);*/
                    $lista_calculo = $claseResultadosGeneral->listar_datos6Asignacion("microsoft", $_SESSION["client_id"], 'Office', 'Professional', $asig, $asignaciones, $dup);
                }
                if($vert == 53){
                    $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION['client_id'], 
                    'Office', 'Standard', 'Professional', $asig, $asignaciones, $dup);
                } 
                if($vert == 6){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Project','', $asig, $asignaciones, $dup);
                }
                if($vert == 61){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Project','Standard', $asig, $asignaciones, $dup);
                }
                if($vert == 62){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Project','Professional', $asig, $asignaciones, $dup);
                }
                if($vert == 63){
                    $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION['client_id'], 
                    'Project','Professional', 'Standard', $asig, $asignaciones, $dup);
                }
                if($vert == 7){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'visio','', $asig, $asignaciones, $dup);
                }
                if($vert == 71){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'visio','Standard', $asig, $asignaciones, $dup);
                }
                if($vert == 72){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'visio','Professional', $asig, $asignaciones, $dup);
                }
                if($vert == 73){
                    $lista_calculo = $resumen->listar_datos7Asignacion($_SESSION['client_id'], 
                    'visio','Standard','Professional', $asig, $asignaciones, $dup);
                }
                if($vert == 9){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'SQL Server','', $asig, $asignaciones, $dup);
                }
                if($vert == 91){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'SQL Server','Standard', $asig, $asignaciones, $dup);
                }
                if($vert == 92){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'SQL Server','Datacenter', $asig, $asignaciones, $dup);
                }
                if($vert == 93){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'SQL Server','Enterprise', $asig, $asignaciones, $dup);
                }
                if($vert == 94){
                    $lista_calculo = $resumen->listar_datos8Asignacion($_SESSION['client_id'], 
                    'SQL Server','Standard','Datacenter','Enterprise', $asig, $asignaciones, $dup);
                }
                if($vert == 10){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Visual Studio','', $asig, $asignaciones, $dup);
                }
                if($vert == 11){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Exchange Server','', $asig, $asignaciones, $dup);
                }
                if($vert == 12){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Sharepoint Server','', $asig, $asignaciones, $dup);
                }
                if($vert == 13){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'Skype for Business','', $asig, $asignaciones, $dup);
                }
                if($vert == 14){
                    $lista_calculo = $resumen->listar_datos6Asignacion($_SESSION['client_id'], 
                    'System Center','', $asig, $asignaciones, $dup);
                }

                $i = 1;
                foreach($lista_calculo as $reg_calculo){
					$usab = "In Use";
					if($reg_calculo["rango"] == "Uso Probable"){
						$usab = "Probably in Use";
					} elseif($reg_calculo["rango"] == "Obsoleto"){
						$usab = "Obsolete";
					}
                                        
                                        $tipo = "Client";
                                        if($reg_calculo["tipo"] == "Servidor"){
                                            $tipo = "Server";
                                        }
						
                    $tabla .= '<tr>
                        <td>' . $i . '</td>
                        <td>' . $reg_calculo["equipo"] . '</td>
                        <td>' . $tipo . '</td>
                        <td>' . $reg_calculo["familia"] . '</td>
                        <td>' . $reg_calculo["edicion"] . '</td>
                        <td>' . $reg_calculo["version"] . '</td>
                        <td>' . $reg_calculo["fecha_instalacion"] . '</td>
                        <td>' . $usab . '</td>
                        <td>';
                        if($resumen->duplicado($_SESSION['client_id'], 0, $reg_calculo["equipo"], $reg_calculo["familia"]) > 1){
                            $tabla .= " Duplicate";
                        }
                        $tabla .= '</td>
                    </tr>';
                    $i++;
                }
            }
            $tabla .= '</tbody>
                </table>
            <script> 
                $(document).ready(function(){
                    $("#tablaMicrosoftDetalle").tablesorter();
                    $("#tablaMicrosoftDetalle").tableHeadFixer();
                });
            </script>';
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'div'=>$tabla, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);
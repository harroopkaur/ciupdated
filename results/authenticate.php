<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_clientes.php");
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");

// Objetos
$usuario = new Clientes();
// Inicializar error
$error = 0;

if (isset($_POST['entrar']) && $_POST['entrar'] == 1) {
    if ($usuario->autenticar($_POST['login'], $_POST['contrasena'], $_POST["company"])) {
        $nuevo_middleware = new Middleware($GLOBALS["domain_root1"]);
        ?>
        <script>
            localStorage.smartControlToken =  '<?= $nuevo_middleware->obtener_token(true) ?>';
            //location.href = "microsoft/";
            location.href = "microsoft/detail.php";
        </script>
        <?php
    } else {
        header('location: ../index.php?error=1');
    }
}
?>
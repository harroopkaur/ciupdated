<?php
require_once("configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general_conf.php");
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_clientes.php");

// Objetos
$usuario = new Clientes();
// Inicializar error
$error = 0;

if (isset($_POST['entrar']) && $_POST['entrar'] == 1) {
    if ($usuario->autenticarResellerCIM($_POST['login'], $_POST['password'])) {
        $nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/CIM");
        ?>
        <script>
            localStorage.smartControlCIMToken =  '<?= $nuevo_middleware->obtener_tokenCIM() ?>';
            location.href = "home.php";
        </script>
        <?php
    } else {
        header('location: index.php?error=1');
    }
}
?>
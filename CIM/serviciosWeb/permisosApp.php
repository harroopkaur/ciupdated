<?php
require_once("../webtool/clases/clase_general_licenciamiento.php");
require_once("../webtool/clases/clase_validador_App.php");

$general = new clase_general_licenciamiento();
$validador = new validadorApp();

$id = 1;
if (isset($_POST["buscarPermisos"]) && filter_var($_POST["buscarPermisos"], FILTER_VALIDATE_INT) !== false){
    $id = $_POST["buscarPermisos"];
}

$permisos = $validador->permisosSerial($id);
$array = array();
foreach ($permisos as $row){
    $array[] = array('permisos'=>$row["accesos"]);
}

echo json_encode($array);
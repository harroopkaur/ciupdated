<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_CIM.php");

$general = new General();
$cim = new clase_CIM();

$cliente = 0;
if(isset($_POST["cliente"]) && filter_var($_POST["cliente"], FILTER_VALIDATE_INT) !== false){
    $cliente = $_POST["cliente"];
}

$cimResults = array();
if(isset($_POST["cimResults"]) && $_POST["cimResults"] != ""){
    $cimResults = json_decode($_POST["cimResults"], true);
}

$cimErrors = array();
if(isset($_POST["cimErrors"]) && $_POST["cimErrors"] != ""){
    $cimErrors = json_decode($_POST["cimErrors"], true);
}

$cimSQL = array();
if(isset($_POST["cimSQL"]) && $_POST["cimSQL"] != ""){
    $cimSQL = json_decode($_POST["cimSQL"], true);
}

$cimLog = array();
if(isset($_POST["cimLog"]) && $_POST["cimLog"] != ""){
    $cimLog = json_decode($_POST["cimLog"], true);
}

$errors = 0;
if($cim->existeCIMResults($cliente) == 0){
    $errors = $cim->procesarCIMResults($cimResults);
    
    if($errors == 0){
        $errors = $cim->procesarCIMErrors($cimErrors);
    }
    
    if($errors == 0){
        $errors = $cim->procesarCIMSQL($cimSQL);
    }
}

if($errors == 0){
    $errors = $cim->procesarCIMLog($cimLog);
}

$array = array('var1'=>$errors);

echo json_encode($array);
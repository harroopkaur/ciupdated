<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientesCIM.php");

$general = new General();
$clientes = new clase_clientesCIM();

$emailProvider = "";
if (isset($_POST["var1"]) && filter_var($_POST["var1"], FILTER_VALIDATE_EMAIL) !== false){
    $emailProvider = $general->get_escape($_POST["var1"]);
}

$customerName = "";
if (isset($_POST["var2"])){
    $customerName = $general->get_escape($_POST["var2"]);
}

$result = 0;
$id = 0;
$correo = "";
     
$rowReseller = $clientes->existeEmailReseller($emailProvider);
if ($rowReseller["cantidad"] > 0){
    $idReseller = $rowReseller["id"];
    $row = $clientes->datosEmpresa($customerName);
    
    if ($row["id"] != ""){        
        if($clientes->existeResellerCustomer($idReseller, $row["id"]) > 0){
            $result = 1;
            $id = $row["id"];
            $correo = $row["correo"];
        }else {
            $result = 3;
        }
    }else {
        $result = 2;
    }
} 
$array = array('var1'=>$result, 'var2'=>$id, 'var3'=>$correo);

echo json_encode($array);
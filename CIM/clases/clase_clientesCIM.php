<?php
class clase_clientesCIM extends General{
    public $error;
    
    function insertar($nombre, $correo) {
        $this->conexion();
        $query = "INSERT INTO clientesCIM (nombreEmpresa, correo, fechaRegistro) ";
        $query .= "VALUES (:nombre, :correo, NOW())";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':nombre'=>$nombre, ':correo'=>$correo));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarClienteReseller($reseller, $empresa) {
        $this->conexion();
        $query = "INSERT INTO clientesResellerCIM (reseller, clienteCIM) ";
        $query .= "VALUES (:reseller, :empresa)";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':empresa'=>$empresa));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }    
    
    function actualizar($id, $nombre, $correo) {
        $this->conexion();
        $query = "UPDATE clientesCIM SET nombreEmpresa = :nombre, correo = :correo WHERE id = :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id, ':nombre'=>$nombre, ':correo'=>$correo));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarClienteReseller($reseller, $empresa) {
        $this->conexion();
        $query = "DELETE FROM clientesResellerCIM WHERE reseller = :reseller AND clienteCIM = :empresa";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':empresa'=>$empresa));
            return true;
        } catch (PDOException $e) {
            echo $this->error = $e->getMessage();
            return false;
        }
    }    
    
    function existeEmpresa($nombre, $id = 0) {
        $this->conexion();
        $query = "SELECT COUNT(id) AS cantidad "
            . "FROM clientesCIM "
            . "WHERE clientesCIM.nombreEmpresa = :nombre AND id != :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':nombre'=>$nombre, ':id'=>$id));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return -1;
        }
    }
    
    function datosEmpresa($nombre) {
        $this->conexion();
        $query = "SELECT id, correo "
            . "FROM clientesCIM "
            . "WHERE clientesCIM.nombreEmpresa = :nombre AND status = 1";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':nombre'=>$nombre));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array("id"=>"", "correo"=>"");
        }
    }
    
    function existeEmailReseller($email) {
        $this->conexion();
        $query = "SELECT id, COUNT(id) AS cantidad "
            . "FROM admin004 "
            . "WHERE correo = :email AND status = 1";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':email'=>$email));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array("id"=>0, "cantidad"=>"");
        }
    }
    
    function existeResellerCustomer($reseller, $customer) {
        $this->conexion();
        $query = "SELECT COUNT(reseller) AS cantidad "
            . "FROM clientesResellerCIM "
            . "WHERE reseller = :reseller AND clienteCIM = :customer";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':customer'=>$customer));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function ultIdEmpresa() {
        $this->conexion();
        $query = "SELECT MAX(id) AS id "
            . "FROM clientesCIM";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function idEmpresa($empresa) {
        $this->conexion();
        $query = "SELECT id "
            . "FROM clientesCIM "
            . "WHERE nombreEmpresa = :empresa";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":empresa"=>$empresa));
            $row = $sql->fetch();
            return $row["id"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function clientesReseller($reseller) {
        $this->conexion();
        $query = "SELECT clientesCIM.id, clientesCIM.nombreEmpresa, clientesCIM.correo, DATE_FORMAT(clientesCIM.fechaRegistro, '%m/%d/%Y') AS fechaRegistro "
            . "FROM clientesResellerCIM "
                . "INNER JOIN clientesCIM ON clientesResellerCIM.clienteCIM = clientesCIM.id AND clientesCIM.status = 1 "
                . "INNER JOIN admin004 ON clientesResellerCIM.reseller = admin004.id AND admin004.status = 1 "
            . "WHERE clientesResellerCIM.reseller = :reseller";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function datosCliente($id) {
        $this->conexion();
        $query = "SELECT clientesCIM.id, clientesCIM.nombreEmpresa, clientesCIM.correo, DATE_FORMAT(clientesCIM.fechaRegistro, '%m/%d/%Y') AS fechaRegistro "
            . "FROM clientesCIM "
            . "WHERE id = :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array("id"=>0, "nombreEmpresa"=>"", "correo"=>"", "fechaRegistro"=>"");
        }
    }
}
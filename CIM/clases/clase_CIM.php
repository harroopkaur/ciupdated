<?php
class clase_CIM extends General{
    public $error;
    
    function insertarCIMResults($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO cim_results (clienteCIM, equipo, operatingSystem, cpuModel, numbersCPU, cpuSpeed,
        cores, avgCPU, memorySize, memoryAvaliable, diskTotalSize, diskAvailable, fecha) ";
        $query .= "VALUES " . $bloque;
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarCIMErrors($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO cim_errors (clienteCIM, equipo, source, errors, description) ";
        $query .= "VALUES " . $bloque;
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarCIMSQL($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO cim_sql (clienteCIM, DatoControl, HostName, LlaveRegistro, Edicion, Version,
        RutaInstalacion) ";
        $query .= "VALUES " . $bloque;
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarCIMLog($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO cim_log (clienteCIM, MachineName, AvgCPUUsage, fecha, hora) ";
        $query .= "VALUES " . $bloque;
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function existeCIMResults($cliente){
        $this->conexion();
        $query = "SELECT COUNT(clienteCIM) AS cantidad "
            . "FROM cim_results "
            . "WHERE clienteCIM = :cliente";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":cliente"=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return -1;
        }
    }
    
    function procesarCIMResults($cimResults){
        $error = 0;
        $i = 0;
        $bloque = "";
        $bloqueValores = array();
        $insertarBloque = false;
        
        $fechaArray = getdate();
        $fecha = $fechaArray ["year"] . "-" . $fechaArray ["mon"] . "-" . $fechaArray ["mday"]; 

        foreach($cimResults as $row){
            if($i == 0){
                $insertarBloque = true;
                $bloque .= "(:clienteCIM" . $i . ", :equipo" . $i . ", :operatingSystem" . $i . ", :cpuModel" . $i . ", :numbersCPU" . $i . ", 
                :cpuSpeed" . $i . ", :cores" . $i . ", :avgCPU" . $i . ", :memorySize" . $i . ", :memoryAvaliable" . $i . ", 
                :diskTotalSize" . $i . ", :diskAvailable" . $i . ", :fecha" . $i . ")";
            } else {
                $bloque .= ", (:clienteCIM" . $i . ", :equipo" . $i . ", :operatingSystem" . $i . ", :cpuModel" . $i . ", :numbersCPU" . $i . ", 
                :cpuSpeed" . $i . ", :cores" . $i . ", :avgCPU" . $i . ", :memorySize" . $i . ", :memoryAvaliable" . $i . ", 
                :diskTotalSize" . $i . ", :diskAvailable" . $i . ", :fecha" . $i . ")";
            } 

            $bloqueValores[":clienteCIM" . $i] = $cimResults[$i]["cliente"];
            $bloqueValores[":equipo" . $i] = $cimResults[$i][0];
            $bloqueValores[":operatingSystem" . $i] = $cimResults[$i][1];
            $bloqueValores[":cpuModel" . $i] = $cimResults[$i][2];

            $bloqueValores[":numbersCPU" . $i] = 0;
            if(filter_var($bloqueValores[":numbersCPU" . $i], FILTER_VALIDATE_INT) !== false){
                $bloqueValores[":numbersCPU" . $i] = $cimResults[$i][3];
            }    

            $bloqueValores[":cpuSpeed" . $i] = 0;
            if(filter_var($bloqueValores[":cpuSpeed" . $i], FILTER_VALIDATE_INT) !== false){
                $bloqueValores[":cpuSpeed" . $i] = $cimResults[$i][4];
            }  

            $bloqueValores[":cores" . $i] = 0;
            if(filter_var($bloqueValores[":cores" . $i], FILTER_VALIDATE_INT) !== false){
                $bloqueValores[":cores" . $i] = $cimResults[$i][5];
            }  

            $bloqueValores[":avgCPU" . $i] = 0;
            if(filter_var($bloqueValores[":avgCPU" . $i], FILTER_VALIDATE_INT) !== false){
                $bloqueValores[":avgCPU" . $i] = $cimResults[$i][6];
            }  

            $bloqueValores[":memorySize" . $i] = 0;
            if(filter_var($bloqueValores[":memorySize" . $i], FILTER_VALIDATE_FLOAT) !== false){
                $bloqueValores[":memorySize" . $i] = $cimResults[$i][7];
            }  

            $bloqueValores[":memoryAvaliable" . $i] = 0;
            if(filter_var($bloqueValores[":memoryAvaliable" . $i], FILTER_VALIDATE_FLOAT) !== false){
                $bloqueValores[":memoryAvaliable" . $i] = $cimResults[$i][8];
            }  

            $bloqueValores[":diskTotalSize" . $i] = 0;
            if(filter_var($bloqueValores[":diskTotalSize" . $i], FILTER_VALIDATE_FLOAT) !== false){
                $bloqueValores[":diskTotalSize" . $i] = $cimResults[$i][9];
            }  

            $bloqueValores[":diskAvailable" . $i] = 0;
            if(filter_var($bloqueValores[":diskAvailable" . $i], FILTER_VALIDATE_FLOAT) !== false){
                $bloqueValores[":diskAvailable" . $i] = $cimResults[$i][10];
            }  
            
            $bloqueValores[":fecha" . $i] = $fecha;

            if ($i == 1000){
                if(!$this->insertarCIMResults($bloque, $bloqueValores)){ 
                    //echo "Results: " . $this->error;
                    $error = 1;
                }

                $bloque = "";
                $bloqueValores = array();
                $i = -1;
                $insertarBLoque = false; 
            }
            $i++;
        }

        if($insertarBloque === true){
            if(!$this->insertarCIMResults($bloque, $bloqueValores)){ 
                //echo "Results: " . $this->error;
                $error = 1;
            }
        }
        
        return $error;
    }
    
    function procesarCIMErrors($cimErrors){
        $error = 0;
        $i = 0;
        $bloque = "";
        $bloqueValores = array();
        $insertarBloque = false;

        foreach($cimErrors as $row){
            if($i == 0){
                $insertarBloque = true;
                $bloque .= "(:clienteCIM" . $i . ", :equipo" . $i . ", :source" . $i . ", :errors" . $i . ", :description" . $i . ")";
            } else {
                $bloque .= ", (:clienteCIM" . $i . ", :equipo" . $i . ", :source" . $i . ", :errors" . $i . ", :description" . $i . ")";
            } 

            $bloqueValores[":clienteCIM" . $i] = $cimErrors[$i]["cliente"];
            $bloqueValores[":equipo" . $i] = $cimErrors[$i][0];
            $bloqueValores[":errors" . $i] = $cimErrors[$i][1];
            $bloqueValores[":source" . $i] = $cimErrors[$i][2];
            $bloqueValores[":description" . $i] = $cimErrors[$i][3];

            if ($i == 1000){
                if(!$this->insertarCIMErrors($bloque, $bloqueValores)){ 
                    //echo "Errors: " . $this->error;
                    $error = 2;
                }

                $bloque = "";
                $bloqueValores = array();
                $i = -1;
                $insertarBLoque = false; 
            }
            $i++;
        }

        if($insertarBloque === true){
            if(!$this->insertarCIMErrors($bloque, $bloqueValores)){ 
                //echo "Errors: " . $this->error;
                $error = 2;
            }
        }
        
        return $error;
    }
    
    function procesarCIMSQL($cimSQL){
        $error = 0;
        $i = 0;
        $bloque = "";
        $bloqueValores = array();
        $insertarBloque = false;

        foreach($cimSQL as $row){
            if($i == 0){
                $insertarBloque = true;
                $bloque .= "(:clienteCIM" . $i . ", :DatoControl" . $i . ", :HostName" . $i . ", :LlaveRegistro" . $i . ",
                :Edicion" . $i . ", :Version" . $i . ", :RutaInstalacion" . $i . ")";
            } else {
                $bloque .= ", (:clienteCIM" . $i . ", :DatoControl" . $i . ", :HostName" . $i . ", :LlaveRegistro" . $i . ",
                :Edicion" . $i . ", :Version" . $i . ", :RutaInstalacion" . $i . ")";
            } 

            $bloqueValores[":clienteCIM" . $i] = $cimSQL[$i]["cliente"];
            $bloqueValores[":DatoControl" . $i] = $cimSQL[$i][0];
            $bloqueValores[":HostName" . $i] = $cimSQL[$i][1];
            $bloqueValores[":LlaveRegistro" . $i] = $cimSQL[$i][2];
            $bloqueValores[":Edicion" . $i] = $cimSQL[$i][3];
            $bloqueValores[":Version" . $i] = $cimSQL[$i][4];
            $bloqueValores[":RutaInstalacion" . $i] = $cimSQL[$i][5];

            if ($i == 1000){
                if(!$this->insertarCIMSQL($bloque, $bloqueValores)){ 
                    //echo "SQL: " . $this->error;
                    $error = 3;
                }

                $bloque = "";
                $bloqueValores = array();
                $i = -1;
                $insertarBLoque = false; 
            }
            $i++;
        }

        if($insertarBloque === true){
            if(!$this->insertarCIMSQL($bloque, $bloqueValores)){ 
                //echo "SQL: " . $this->error;
                $error = 3;
            }
        }
        
        return $error;
    }
    
    function procesarCIMLog($cimLog){
        $error = 0;
        $i = 0;
        $bloque = "";
        $bloqueValores = array();
        $insertarBloque = false;

        foreach($cimLog as $row){
            if($i == 0){
                $insertarBloque = true;
                $bloque .= "(:clienteCIM" . $i . ", :MachineName" . $i . ", :AvgCPUUsage" . $i . ", :fecha" . $i . ", :hora" . $i . ")";
            } else {
                $bloque .= ", (:clienteCIM" . $i . ", :MachineName" . $i . ", :AvgCPUUsage" . $i . ", :fecha" . $i . ", :hora" . $i . ")";
            } 

            $bloqueValores[":clienteCIM" . $i] = $cimLog[$i]["cliente"];
            $bloqueValores[":MachineName" . $i] = $cimLog[$i][0];
            
            $bloqueValores[":AvgCPUUsage" . $i] = 0;
            if(filter_var($bloqueValores[":AvgCPUUsage" . $i], FILTER_VALIDATE_FLOAT) !== false){
                $bloqueValores[":AvgCPUUsage" . $i] = $cimLog[$i][1];
            }
            
            $bloqueValores[":fecha" . $i] = $cimLog[$i][2];
            $bloqueValores[":hora" . $i] = $cimLog[$i][3];

            if ($i == 1000){
                if(!$this->insertarCIMLog($bloque, $bloqueValores)){ 
                    //echo "Log: " . $this->error;
                    $error = 4;
                }

                $bloque = "";
                $bloqueValores = array();
                $i = -1;
                $insertarBLoque = false; 
            }
            $i++;
        }

        if($insertarBloque === true){
            if(!$this->insertarCIMLog($bloque, $bloqueValores)){ 
                //echo "Log: " . $this->error;
                $error = 4;
            }
        }
        
        return $error;
    }
    
    function migrationPlan($reseller, $cliente) {
        $this->conexion();
        $query = "SELECT id, equipo, role, 1 AS servidores, numbersCPU, cores, IF(mod(ROUND(memorySize, 0),2) != 0, ROUND(memorySize, 0) + 1, ROUND(memorySize, 0)) AS memory,
                ROUND(diskTotalSize, 0) AS disk, IF(ROUND(diskTotalSize - diskAvailable, 0) < 80, 80, ROUND(diskTotalSize - diskAvailable, 0)) AS diskUsed,
                ((1 * IFNULL(precioServidor.precio, 0))
                + (numbersCPU * IFNULL(precioCPU.precio, 0))
                + (cores * IFNULL(precioCores.precio, 0))
                + (IF(mod(ROUND(memorySize, 0),2) != 0, ROUND(memorySize, 0) + 1, ROUND(memorySize, 0)) * IFNULL(precioMemory.precio, 0))
                + (ROUND(diskTotalSize, 0) * IFNULL(precioDisk.precio, 0))
                + (IF(ROUND(diskTotalSize - diskAvailable) < 80, 80, ROUND(diskTotalSize - diskAvailable, 0)) * IFNULL(precioDiskUsed.precio, 0))) AS total
            FROM cim_results
                 LEFT JOIN cim_components_reseller AS precioServidor ON cim_results.clienteCIM = precioServidor.cliente
                 AND precioServidor.idComponent = 1 AND precioServidor.idReseller = :reseller AND precioServidor.status = 1 
                 LEFT JOIN cim_components_reseller AS precioCPU ON cim_results.clienteCIM = precioCPU.cliente
                 AND precioCPU.idComponent = 2 AND precioCPU.idReseller = :reseller AND precioCPU.status = 1
                 LEFT JOIN cim_components_reseller AS precioCores ON cim_results.clienteCIM = precioCores.cliente
                 AND precioCores.idComponent = 3 AND precioCores.idReseller = :reseller AND precioCores.status = 1
                 LEFT JOIN cim_components_reseller AS precioMemory ON cim_results.clienteCIM = precioMemory.cliente
                 AND precioMemory.idComponent = 4 AND precioMemory.idReseller = :reseller AND precioMemory.status = 1
                 LEFT JOIN cim_components_reseller AS precioDisk ON cim_results.clienteCIM = precioDisk.cliente
                 AND precioDisk.idComponent = 5 AND precioDisk.idReseller = :reseller AND precioDisk.status = 1
                 LEFT JOIN cim_components_reseller AS precioDiskUsed ON cim_results.clienteCIM = precioDiskUsed.cliente
                 AND precioDiskUsed.idComponent = 6 AND precioDiskUsed.idReseller = :reseller AND precioDiskUsed.status = 1
            WHERE cim_results.clienteCIM = :cliente";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":cliente"=>$cliente, ':reseller'=>$reseller));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function compActivos($reseller, $cliente){
        $this->conexion();
        $query = "SELECT idComponent, componente, columnName, status
            FROM cim_components_reseller
                INNER JOIN cim_components ON cim_components_reseller.idComponent = cim_components.id
            WHERE idReseller = :reseller AND cliente = :cliente";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":cliente"=>$cliente, ':reseller'=>$reseller));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function totalRoles($cliente) {
        $this->conexion();
        $query = "SELECT COUNT(role) as cantidad
            FROM cim_results
            WHERE cim_results.clienteCIM = :cliente";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":cliente"=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function updateRolMigrationPlan($id, $role) {
        $this->conexion();
        $query = "UPDATE cim_results SET role = :role WHERE id = :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":id"=>$id, ":role"=>$role));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function hardwareDeployment($cliente) {
        $this->conexion();
        $query = "SELECT equipo, operatingSystem, cpuModel, numbersCPU, cpuSpeed, cores, ROUND((avgCPU + IFNULL(SUM(cim_log.AvgCPUUsage), 0)) / (COUNT(cim_log.AvgCPUUsage) + 1), 2) AS avgCPU,
                memorySize, memoryAvaliable, diskTotalSize, diskAvailable
            FROM cim_results
                LEFT JOIN cim_log ON cim_results.clienteCIM = cim_log.clienteCIM AND cim_results.equipo = cim_log.MachineName
            WHERE cim_results.clienteCIM = :cliente
            GROUP BY cim_results.equipo";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":cliente"=>$cliente));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function softwareInstalled($cliente){
        $this->conexion();
        $query = "SELECT equipo, IFNULL(MAX(cim_log.fecha), cim_results.fecha) AS fecha, operatingSystem, CONCAT(IFNULL(DatoControl, ''), ' ', IFNULL(Edicion, '')) AS nombSQL
            FROM cim_results
                 LEFT JOIN cim_sql ON cim_results.clienteCIM = cim_sql.clienteCIM AND cim_results.equipo = cim_sql.HostName
                 LEFT JOIN cim_log ON cim_results.clienteCIM = cim_log.clienteCIM AND cim_results.equipo = cim_log.MachineName
            WHERE cim_results.clienteCIM = :cliente
            GROUP BY equipo";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":cliente"=>$cliente));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function cantidadSoftwareInstalledWindows($cliente){
        $this->conexion();
        $query = "SELECT COUNT(operatingSystem) operatingSystem
            FROM cim_results
            WHERE cim_results.clienteCIM = :cliente";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":cliente"=>$cliente));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array("operatingSystem"=>0, "nombSQL"=>0);
        }
    }
    
    function cantidadSoftwareInstalledSQL($cliente){
        $this->conexion();
        $query = "SELECT COUNT(DatoControl) AS nombSQL
            FROM cim_results
                LEFT JOIN cim_sql ON cim_results.clienteCIM = cim_sql.clienteCIM AND cim_results.equipo = cim_sql.HostName
            WHERE cim_results.clienteCIM = :cliente";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":cliente"=>$cliente));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array("operatingSystem"=>0, "nombSQL"=>0);
        }
    }
    
    function listadoComponents($reseller, $cliente){
        $this->conexion();
        $query = "SELECT cim_components.id,
                cim_components.componente,
                IFNULL(cim_components_reseller.precio, 0) AS precio,
                cim_components_reseller.status
            FROM cim_components
                LEFT JOIN cim_components_reseller ON cim_components.id = cim_components_reseller.idComponent
                AND cim_components_reseller.idReseller = :reseller AND cim_components_reseller.cliente = :cliente";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":reseller"=>$reseller, ':cliente'=>$cliente));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function existePrecioComponente($reseller, $cliente, $componente){
        $this->conexion();
        $query = "SELECT COUNT(precio) AS cantidad
            FROM cim_components_reseller
            WHERE idReseller = :reseller AND cliente = :cliente AND idComponent = :componente";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':cliente'=>$cliente, ':componente'=>$componente));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function insertarPrecioComponente($reseller, $cliente, $componente, $precio){
        $this->conexion();
        $query = "INSERT INTO cim_components_reseller (idReseller, cliente, idComponent, precio) VALUES (:reseller, :cliente, :componente, :precio)";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':cliente'=>$cliente, ':componente'=>$componente, ':precio'=>$precio));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarPrecioComponente($reseller, $cliente, $componente, $precio){
        $this->conexion();
        $query = "UPDATE cim_components_reseller SET precio = :precio WHERE idReseller = :reseller AND cliente = :cliente AND idComponent = :componente";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':cliente'=>$cliente, ':componente'=>$componente, ':precio'=>$precio));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function anularPrecioComponentes($reseller, $cliente){
        $this->conexion();
        $query = "UPDATE cim_components_reseller SET status = 0 WHERE idReseller = :reseller AND cliente = :cliente AND idComponent NOT IN (1)";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':cliente'=>$cliente));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function activarPrecioComponentes($reseller, $cliente, $componente){
        $this->conexion();
        $query = "UPDATE cim_components_reseller SET status = 1 WHERE idReseller = :reseller AND cliente = :cliente AND idComponent = :componente";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':cliente'=>$cliente, ':componente'=>$componente));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarCorreos($reseller, $email){
        $result = true;
        $row = $this->obtenerIdEmail($email);
        if($row["id"] == ""){
            $this->conexion();
            $query = "INSERT INTO cim_correos (reseller, correo) VALUES (:reseller, :email)";
            try{
                $sql = $this->conn->prepare($query);
                $sql->execute(array(":reseller"=>$reseller, ":email"=>$email));
            }catch(PDOException $e){
                $this->error = $e->getMessage();
                $result = false;
            }
        }
        
        return $result;
    }
    
    function obtenerIdEmail($email){
        $query = "SELECT id, reseller FROM cim_correos WHERE correo = :email";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(":email"=>$email));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array("id"=>0, "reseller"=>0);
        } 
    }
    
    function existeCorreoOtroCliente($cliente, $email){
        $query = "SELECT id, reseller FROM cim_correos WHERE reseller != :reseller AND correo = :email";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(":reseller"=>$reseller, ":email"=>$email));
            $row = $sql->fetch();
            return $row;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array("id"=>0, "reseller"=>0);
        } 
    }
}
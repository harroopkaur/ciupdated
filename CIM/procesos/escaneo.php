<?php
//error_reporting(0);
$_SESSION["idioma"] = 1;
// Clases 
require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_tabla_maestra.php");
require_once($GLOBALS["app_root"] . "/clases/clase_validar_proceso_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_procesar_consolidado_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_procesar_resumen_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_procesar_SQL_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_pass.php");
require_once($GLOBALS["app_root1"] . "/NADD/clases/clase_resumen.php");
require_once($GLOBALS["app_root"] . "/clases/clase_procesar_LAE.php");
require_once($GLOBALS["app_root"] . "/clases/clase_procesar_actualizar_LAE.php");

// Objetos
$general = new General();
$tablaMaestra = new tablaMaestra();
$procConsolidado = new clase_procesar_consolidado_general();
$procResumen = new clase_procesar_resumen_general();
$procSQL = new clase_procesar_SQL_general();
$validadorGeneral = new clase_validar_proceso_general();
$passLAD = new clase_pass();
$resumen = new ResumenAppEscaneo();
$procLAE = new clase_procesar_LAE();
$procesoActualizarLAE = new clase_procesar_actualizar_LAE();

//variables
$fabricante = 3;
$nombFabricante = "appEscaneo";
$exito = 0;
$error = 0;
$exito2 = 0;
$error2 = 0;
$idDiagnosticResult = 0;
$lista = array();
$opcionDespliegue = "completo";
        
if(isset($_POST['insertar']) && $_POST["insertar"] == 1) {
    if(isset($_FILES["archivo"]["tmp_name"]) && is_uploaded_file($_FILES["archivo"]["tmp_name"])){
        $correo = $_POST["email"];
        $serialHDD = $general->get_escape($_POST["var1"]);
        
        $nombArchivo = $_FILES['archivo']['name'];
        $temp = $_FILES['archivo']['tmp_name'];

        /*$nombArchivo = "LAD_Output20_02_2018_03_48_29.rar";
        $temp = "archivosLAD/LAD_Output20_02_2018_03_48_29.rar";
        $correo = "m_acero_n@hotmail.com";
        $serialHDD = "gdfgdsgsdg";*/
        
        $error = $validadorGeneral->validar_archivo($nombArchivo, "rar");

        if($error == 0){
            $listPassLAD = $passLAD->listar_todo();
            $pass = "";

            foreach($listPassLAD as $row){  
                $psw = $passLAD->desencriptar($row["descripcion"]);
                $rar_file = rar_open($temp, $psw);

                $entries = rar_list($rar_file);   

                $direcEntry = $GLOBALS["app_root1"] . '/tmp';
                if(!file_exists($direcEntry)){
                    mkdir($direcEntry, 0755);
                }
                
                foreach ($entries as $entry) {
                    if($entry->extract($direcEntry)){
                        $pass = $psw;
                        $files = array_diff(scandir($direcEntry), array('.','..')); 
                        foreach ($files as $file) { 
                            unlink($direcEntry.'/'.$file); 
                        } 
                        rmdir($direcEntry);
                    } 
                    
                    break;
                }

                if($pass != ""){
                    break;
                }
            }
         
            rar_close($rar_file);

            if($pass != ""){
                $error = $validadorGeneral->validarArchivosRar($temp, $nombFabricante, $pass);
            } else{
                $error = -2;
            }
        }
                
        if($error == 0) {  
            $validadorGeneral->copiarArchivosLAD($nombFabricante);
           
            $nombreArchivo = $validadorGeneral->nombreArchivo;
            $archivoConsolidado = $validadorGeneral->archivoConsolidado;
            $archivoSQL = $validadorGeneral->archivoSQL;
            $archivoSO = $validadorGeneral->archivoConsolidadoSO;

            $error = $validadorGeneral->validarConsolidadoAddRemove($archivoConsolidado);
        }
        
        if($error == 0){                
            $error = $validadorGeneral->validarSQL($archivoSQL);
        }
        
        if($error == 0){                
            $error = $validadorGeneral->validarSO($archivoSO);
        }
    }
    else{
        $error = -1;
    }

    if($error == 0){            
        $iDatoControl = $validadorGeneral->iDatoControl;
        $iHostName = $validadorGeneral->iHostName;
        $iRegistro = $validadorGeneral->iRegistro;
        $iEditor = $validadorGeneral->iEditor;
        $iVersion = $validadorGeneral->iVersion;
        $iDiaInstalacion = $validadorGeneral->iDiaInstalacion;
        $iSoftware = $validadorGeneral->iSoftware;
        $procesarAddRemove = $validadorGeneral->procesarAddRemove;

        $nDatoControl = $validadorGeneral->nDatoControl;
        $nHostName = $validadorGeneral->nHostName;
        $nEdicionSQL = $validadorGeneral->nEdicionSQL;
        $nVersionSQL = $validadorGeneral->nVersionSQL;
        $procesarSQL = $validadorGeneral->procesarSQL;
        
        $pDatoControl = $validadorGeneral->pDatoControl;
        $pHostName = $validadorGeneral->pHostName;
        $pSistemaOperativo = $validadorGeneral->pSistemaOperativo;
        $pFechaCreacion = $validadorGeneral->pFechaCreacion;
        $procesarSO = $validadorGeneral->procesarSO;
        
        $idDiagnostic = 0;
        /*if(isset($_POST["idDiagnostic"]) && filter_var($_POST["idDiagnostic"], FILTER_VALIDATE_INT) !== false){
            $idDiagnostic = $_POST["idDiagnostic"];
        }

        /*if($error == 0 && $idDiagnostic == 0){
            $procConsolidado->setTabDiagnosticos($nombFabricante);
            if(!$procConsolidado->insertarDiagnoticos($nombreArchivo)){
                $error = -3;
            }
            else{
                $idDiagnostic = $procConsolidado->ultId();
            }
        }*/
        
        //$correo = "m_acero_n@hotmail.com";
        $row = $resumen->obtenerIdEmail($correo);
        $row1 = $procConsolidado->existeAppEscaneo($row["cliente"], $row["id"], $serialHDD);
        
        if($row1["cantidad"] == 0){
            $procConsolidado->insertarAppEscaneo($row["cliente"], $row["id"], $serialHDD, $nombreArchivo);
        } else{
            $procConsolidado->actualizarAppEscaneo($row1["id"], $nombreArchivo);
        }
        

        $procConsolidado->eliminarAppEscaneo($row["cliente"], $row["id"], $serialHDD);
        
        $procConsolidado->procesarConsolidado($row["cliente"], 0, $archivoConsolidado, $procesarAddRemove, $iDatoControl,
        $iHostName, $iRegistro, $iEditor, $iVersion, $iDiaInstalacion, $iSoftware, $opcionDespliegue, $nombFabricante, 
        0, $row["id"], $serialHDD);

        //inicio resumen office
        $procResumen->eliminarAppEscaneo($row["cliente"], $row["id"], $serialHDD);
        
        $tablaProductosMaestra = $tablaMaestra->listadoProductosMaestraActivo($fabricante);
        $procResumen->procesarResumenGeneral($row["cliente"], 0, $tablaProductosMaestra, 
        $opcionDespliegue, $nombFabricante, 0, $row["id"], $serialHDD);
        //fin resumen office

        //inicio agregar consolidado SQL
        $procSQL->procesarSQL($row["cliente"], 0, $archivoSQL, $procesarSQL, $nDatoControl, 
        $nHostName, $nEdicionSQL, $nVersionSQL, $opcionDespliegue, 0, $row["id"], 0, $serialHDD);
        //fin agregar consolidado SQL
        
        //inicio agregar LAE
        $procLAE->eliminarAppEscaneo($row["cliente"], $row["id"], $serialHDD);
        $procLAE->procesarLAEAppEscaneo($archivoSO, $procesarSO, $row["cliente"], $row["id"], $serialHDD);
        //fin agregar LAE
        
        //inicio agregar Detalle
        $fechaDespliegue = date("d/m/Y");
        $procesoActualizarLAE->eliminarAppEscaneo($row["cliente"], $row["id"], $serialHDD);
        $procesoActualizarLAE->procesarActualizarLAE($row["cliente"], 0, $fechaDespliegue, 
        $opcionDespliegue, 0, $nombFabricante, 1, 0, $row["id"], $serialHDD);
        //fin agregar Detalle
        $exito=1;  
    }
}

$array = array('var1'=>$exito, 'var2'=>$error, 'var3'=>$row["cliente"]);

echo json_encode($array);
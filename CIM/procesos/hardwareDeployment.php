<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/CIM");
require_once($GLOBALS["app_root1"] . "/CIM/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientesCIM.php");
require_once($GLOBALS["app_root"] . "/clases/clase_CIM.php");

$clientes = new clase_clientesCIM();
$cim = new clase_CIM();

$mostrarBack = "";
$mostrarNext = "";
$infoCabe = "";
$imgCabecera = "hardwareDeployment1.png"; 
$back = $GLOBALS["domain_root"] . "/migrationPlan.php";
$next = $GLOBALS["domain_root"] . "/softwareDeployment.php";
$selectCliente = "";
$btnSave = "hide";
$mostrarOptFoot = ""; 

$listado = $clientes->clientesReseller($_SESSION["reseller"]);
$list = $cim->hardwareDeployment($_SESSION["client_id"]);

$i = 1;
$cpu = 0;
$cores = 0;
$disk = 0;

$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#243C67';
$color4 = '#00AFF0';
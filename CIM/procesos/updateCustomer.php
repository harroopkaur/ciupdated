<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/CIM");
require_once($GLOBALS["app_root1"] . "/CIM/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientesCIM.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");

$general = new General();
$clientes = new clase_clientesCIM();
$validator = new validator("form1");

$mostrarBack = "";
$mostrarNext = "";
$infoCabe = "";
$imgCabecera = "manageCustomers1.png";
$back = "manageCustomers.php";
$next = "manageCustomers.php";
$selectCliente = "hide";
$btnSave = "hide";
$listado = array();
$mostrarOptFoot = ""; 

$agregar = 0;
$error = 0;
$exito = 0;

$id = 0;
if(isset($_REQUEST["id"]) && filter_var($_REQUEST["id"], FILTER_VALIDATE_INT) !== false){
    $id  = $_REQUEST["id"];
}

if (isset($_POST['actualizar']) && $_POST['actualizar'] == 1 && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false) {
    $agregar = 1;
    // Validaciones
    
    $empresa = ""; 
    if(isset($_POST["empresa"])){
        $empresa = $general->get_escape($_POST['empresa']);
    }
     
    $email = $general->get_escape($_POST['email']);
    $error =  $clientes->existeEmpresa($empresa, $id);

    if ($error == 0) {
        if ($clientes->actualizar($id, $empresa, $email)) {
            $exito = 1;
        } else {
            $error = 5;
        }
    } 
}

$datos = $clientes->datosCliente($id);

$validator->create_message("msj_empresa", "empresa", "Required", 0);
$validator->create_message("msj_email", "email", "Not a valid Email", 3);
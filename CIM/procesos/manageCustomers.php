<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/CIM");
require_once($GLOBALS["app_root1"] . "/CIM/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientesCIM.php");

$clientes = new clase_clientesCIM();

$mostrarBack = "";
$mostrarNext = "";
$infoCabe = "";
$imgCabecera = "manageCustomers1.png";
$back = "softwareDeployment.php";
$next = "proposals.php";
$selectCliente = "hide";
$btnSave = "hide";
$mostrarOptFoot = ""; 
$id = 0;

$listado = $clientes->clientesReseller($_SESSION["reseller"]);
$count = count($listado);
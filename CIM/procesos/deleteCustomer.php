<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/CIM");
require_once($GLOBALS["app_root1"] . "/CIM/plantillas/middleware.php");
//fin middleware

//clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientesCIM.php");

// Objetos
$clientes = new clase_clientesCIM();
$general = new General();

//procesos
$exito = false;
if (isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false) {
    $exito = $clientes->eliminarClienteReseller($_SESSION["reseller"], $_POST['id']);
}
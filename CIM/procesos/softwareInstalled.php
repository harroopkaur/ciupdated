<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/CIM");
require_once($GLOBALS["app_root1"] . "/CIM/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientesCIM.php");
require_once($GLOBALS["app_root"] . "/clases/clase_CIM.php");

$clientes = new clase_clientesCIM();
$cim = new clase_CIM();

$mostrarBack = "";
$mostrarNext = "";
$infoCabe = "";
$imgCabecera = "softwareDeployment1.png";
$back = "hardwareDeployment.php";
$next = "manageCustomers.php";
$selectCliente = "";
$btnSave = "hide";
$mostrarOptFoot = ""; 

$color1 = '#DCDCDC';
$color2 = '#99BFDC';

$listado = $clientes->clientesReseller($_SESSION["reseller"]);
$list = $cim->softwareInstalled($_SESSION["client_id"]);
$cantWindows = $cim->cantidadSoftwareInstalledWindows($_SESSION["client_id"]);
$cantSQL = $cim->cantidadSoftwareInstalledSQL($_SESSION["client_id"]);
<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/CIM");
require_once($GLOBALS["app_root1"] . "/CIM/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientesCIM.php");
require_once($GLOBALS["app_root"] . "/clases/clase_CIM.php");
//require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");


$general = new General();
$clientes = new clase_clientesCIM();
$cim = new clase_CIM();

$listado = $clientes->clientesReseller($_SESSION["reseller"]);

$listadoComponet = array();
if($_SESSION["client_id"] > 0){
    $listadoComponet = $cim->listadoComponents($_SESSION["reseller"], $_SESSION["client_id"]);
}
$mostrarBack = ""; 
$mostrarNext = "";
$back = "NADD.php";
$next = "showResults.php";
$tituloResultados = "hide";
$btnSave = "hide";
$selectCliente = "";
$infoCabe = "";
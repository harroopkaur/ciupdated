<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/CIM");
require_once($GLOBALS["app_root1"] . "/CIM/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientesCIM.php");
require_once($GLOBALS["app_root"] . "/clases/clase_CIM.php");
//require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");

$general = new General();
$clientes = new clase_clientesCIM();
$cim = new clase_CIM();
//$validator = new validator("form1");

$mostrarBack = "";
$mostrarNext = "hide";
$infoCabe = "";
$imgCabecera = "proposals.png";
$back = "manageCustomers.php";
$next = "";
$selectCliente = "";
$btnSave = "hide";
$mostrarOptFoot = ""; 

$actualizar = 0;
$error = 0;
$exito = 0;

/*$id = 0;
if(isset($_REQUEST["id"]) && filter_var($_REQUEST["id"], FILTER_VALIDATE_INT) !== false){
    $id  = $_REQUEST["id"];
}*/

if (isset($_POST['actualizar']) && $_POST['actualizar'] == 1) {
    $actualizar = 1;
    // Validaciones
    
    $comp = array(); 
    if(isset($_POST["component"])){
        $comp = $_POST['component'];
    }
    
    $ver = array(); 
    if(isset($_POST["check"])){
        $listCheck = $_POST['check'];
    }
    
    $agr = 0;
    
    for($index = 0; $index < count($comp); $index++){
        $compon = 0;
        if(filter_var($comp[$index], FILTER_VALIDATE_FLOAT) !== false){
            $compon = $comp[$index];
        }
        
        if($cim->existePrecioComponente($_SESSION["reseller"], $_SESSION["client_id"], $index + 1) == 0){
            if($cim->insertarPrecioComponente($_SESSION["reseller"], $_SESSION["client_id"], $index + 1, $compon)){
                $agr++;
            }
        }else{
            if($cim->actualizarPrecioComponente($_SESSION["reseller"], $_SESSION["client_id"], $index + 1, $compon)){
                $agr++;
            }
        }
    }
    
    if ($agr == $index && $agr > 0) {
        $exito = 1;
    }else if($agr < $index && $agr > 0){
       $exito = 2;
    } 
    
    $cim->anularPrecioComponentes($_SESSION["reseller"], $_SESSION["client_id"]);
    for($index = 0; $index < count($listCheck); $index++){
        $cim->activarPrecioComponentes($_SESSION["reseller"], $_SESSION["client_id"], $listCheck[$index]);
    }
}

$listado = $clientes->clientesReseller($_SESSION["reseller"]);

$listadoComponet = array();
if($_SESSION["client_id"] > 0){
    $listadoComponet = $cim->listadoComponents($_SESSION["reseller"], $_SESSION["client_id"]);
}
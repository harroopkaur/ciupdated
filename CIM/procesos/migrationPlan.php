<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/CIM");
require_once($GLOBALS["app_root1"] . "/CIM/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_clientesCIM.php");
require_once($GLOBALS["app_root"] . "/clases/clase_CIM.php");

$clientes = new clase_clientesCIM();
$cim = new clase_CIM();

$mostrarBack = "";
$mostrarNext = "";
$infoCabe = "";
$imgCabecera = "migrationPlan1.png"; 
$back = $GLOBALS["domain_root"] . "/home.php";
$next = $GLOBALS["domain_root"] . "/hardwareDeployment.php";
$selectCliente = "";
$btnSave = "";
$mostrarOptFoot = ""; 

$listado = $clientes->clientesReseller($_SESSION["reseller"]);
$list = $cim->migrationPlan($_SESSION["reseller"], $_SESSION["client_id"]);
$compActivos = $cim->compActivos($_SESSION["reseller"], $_SESSION["client_id"]);
$roles = $cim->totalRoles($_SESSION["client_id"]);

$i = 1;
$servidores = 0;
$cpus = 0;
$cores = 0;
$memory = 0;
$disk = 0;
$diskUsed = 0;
$total = 0;

$columnCPU = 0;
$columnCORE = 0;
$columnRAM = 0;
$columnDisk = 0;
$columnDiskUsed = 0;

$color1 = '#DCDCDC';
$color2 = '#99BFDC';
$color3 = '#00AFF0';
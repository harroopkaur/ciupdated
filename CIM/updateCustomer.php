<?php
require_once("configuracion/inicio.php");
require_once("procesos/updateCustomer.php");
require_once("plantillas/head.php");
require_once("plantillas/cabecera1.php");
?>

<section class="contenedor1">
    <div class="divContenedor">
        <div class="divMenuContenido">
            <?php
            $menuCIM = 3;
            include_once("plantillas/menu.php");
            ?>
        </div>

        <div class="bordeContenedor">
            <div class="contenido">
                <?php 
                    require_once("plantillas/updateCustomer.php");  
                ?>  
            </div>
        </div>
    </div>
</section>

<?php 
require_once("plantillas/foot1.php");
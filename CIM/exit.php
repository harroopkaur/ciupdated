<?php
session_start();                 // Comienza la sesion
$urlInicio = "../CIM";

session_unset();                 // Vacia las variables de sesion
session_destroy();               // Destruye la sesion

header("location: " . $urlInicio);
?>
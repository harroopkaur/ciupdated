
		<div class="hbox hbox-auto-xs hbox-auto-sm"  >

			<div class="col">
				<!-- main header -->
				<div class="bg-light lter b-b wrapper-md">
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h1 class="m-n font-thin h3 text-black">   <a href="home.php"  > <i class="fa  fa-arrow-circle-o-left"></i> </a>  Migration Plan</h1>
						<!--     <small class="text-muted"> </small> -->
						</div>
						<div class="col-md-6 col-sm-12 text-right hidden-xs">

							<div class="inline text-left">
								<?php echo $selectClienteHtml?>
							</div>
						</div>

					</div>
				</div>
				<!-- / main header -->
				<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">

				   
					<!-- stats -->
				<!--     <div class="row row-sm text-center">
						<div class="col-md-4 col-md-offset-4 ">
							<img src="<?= $GLOBALS["domain_root"] ?>/img/imgLogo.png" class="img img-responsive imgTituloHome">
						</div>
					</div> -->
					<div class="row">


						<div class="col-md-9" >
							 <div class="row">
						<div class="col-xs-12 col-md-12">
							<button  class="export-butsty" type="button" onclick="window.open('<?= $GLOBALS["domain_root"] ?>/reportes/excelMigrationPlan.php?id=' + $('#cliente').val());">Export</button>
							  <button  class="export-butsty" type="button" onclick="location.href='<?= $GLOBALS["domain_root"] ?>/proposals.php';"> Settings</button>
						</div>
					   
					</div>
							<form id="form1" name="form1" method="post">
								<input type="hidden" id="token" name="token">
									<h6 class="main-tablehadsty">Migration Plan Details </h6>
								<div class="contTabla">

									<table class="table table-bordered table-condensed tablaHardware migration-tablesty"  border="0" width="100%" cellspacing="0">
										<thead>
									  
										<tr style="background:#00AFF0; color:#fff;">
											<th valign="middle" style="width:50px;" class="text-center"><span>#</span></th>
											<th valign="middle" style="width:200px;" class="text-center"><span>Hostname</span></th>
											<th valign="middle" style="width:100px;" class="text-center"><span>Role</span></th>
											<?php
											if(count($compActivos) == 0){
												?>
												<th valign="middle" class="text-center"><span>Base Server Plan</span></th>
												<?php
											}

											foreach($compActivos as $row){
												if($row["status"] == 1){
													?>
													<th valign="middle" class="text-center"><span><?= $row["columnName"] ?></span></th>
													<?php
												}

												if($row["idComponent"] == 2 && $row["status"] == 1){
													$columnCPU = 1;
												} elseif ($row["idComponent"] == 3 && $row["status"] == 1){
													$columnCORE = 1;
												} elseif ($row["idComponent"] == 4 && $row["status"] == 1){
													$columnRAM = 1;
												} elseif ($row["idComponent"] == 5 && $row["status"] == 1){
													$columnDisk = 1;
												} elseif ($row["idComponent"] == 6 && $row["status"] == 1){
													$columnDiskUsed = 1;
												}
											}
											?>
											<th valign="middle" class="text-center"><span>Total Cost ($)</span></th>
										</tr>
										</thead>
										<tbody id="registros">
										<?php
										foreach($list as $row){
											?>
											<tr>
												<td class="text-center"><?= $i ?></td>
												<td><input type="hidden" name="valores[]" value="<?= $row["id"] ?>"><?= $row["equipo"] ?></td>
												<td><input type="text" class="form-control" name="role[]" value="<?= $row["role"] ?>" maxlength="70"></td>
												<td class="text-center"><?= $row["servidores"] ?></td>
												<td class="text-center <?php if($columnCPU == 0){ echo 'hide'; } ?>"><?= $row["numbersCPU"] ?></td>
												<td class="text-center <?php if($columnCORE == 0){ echo 'hide'; } ?>"><?= $row["cores"] ?></td>
												<td class="text-center <?php if($columnRAM == 0){ echo 'hide'; } ?>"><?= $row["memory"] ?></td>
												<td class="text-center <?php if($columnDisk == 0){ echo 'hide'; } ?>"><?= $row["disk"] ?></td>
												<td class="text-center <?php if($columnDiskUsed == 0){ echo 'hide'; } ?>"><?= $row["diskUsed"] ?></td>
												<td class="text-right"><?= $row["total"] ?></td>
											</tr>
											<?php
											$servidores += $row["servidores"];
											$cpus += $row["numbersCPU"];
											$cores += $row["cores"];
											$memory += $row["memory"];
											$disk += $row["disk"];
											$diskUsed += $row["diskUsed"];
											$total += $row["total"];
											$i++;
										}
										?>
										<tr>
											<th class="text-center">Totals</th>
											<th class="text-center"><?= count($list) ?></th>
											<th class="text-center"><?= $roles ?></th>
											<th class="text-center"><?= $servidores ?></th>
											<th class="text-center <?php if($columnCPU == 0){ echo 'hide'; } ?>"><?= $cpus ?></th>
											<th class="text-center <?php if($columnCORE == 0){ echo 'hide'; } ?>"><?= $cores ?></th>
											<th class="text-center <?php if($columnRAM == 0){ echo 'hide'; } ?>"><?= $memory ?></th>
											<th class="text-center <?php if($columnDisk == 0){ echo 'hide'; } ?>"><?= $disk ?></th>
											<th class="text-center <?php if($columnDiskUsed == 0){ echo 'hide'; } ?>"><?= $diskUsed ?></th>
											<th class="text-right"><?= $total ?></th>
										</tr>
										</tbody>
									</table>
								</div>
							</form>
						</div>

						<div class="col-md-3 tamGrafico graff-styright" id="container1">
							
							<?php include_once($GLOBALS["app_root"] . "/graficos/migrationPlan.php"); ?>
						 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
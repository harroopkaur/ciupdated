


		<div class="hbox hbox-auto-xs hbox-auto-sm"  >
			<!-- main -->
			<div class="col">
				<!-- main header -->
				<div class="bg-light lter b-b wrapper-md">
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<h1 class="m-n font-thin h3 text-black">Dashboard CIM - Tool</h1>
						<!--     <small class="text-muted"> </small> -->
						</div>

					</div>
				</div>
				<!-- / main header -->
				<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
					<!-- stats -->
				<!--     <div class="row row-sm text-center">
						<div class="col-md-12 dash-imgainsty">
							<img src="<?= $GLOBALS["domain_root"] ?>/img/imgLogo.png" class="img img-responsive imgTituloHome">
						</div>
					</div> -->
					<div class="row" style="margin-top: 30px;">

						<div class="col-md-12 col-xs-12">
							<div class="row row-sm text-center">
								<div class="col-sm-12 col-md-3 main-animatedsty rsp_0242171019">
									<a href="<?= $GLOBALS["domain_root"] ?>/migrationPlan.php" class="block panel block-colorsty621 item">
										<p> Migration <br>Plan </p>
										<span class="das-imgsty621">
										   <!--  <i class="fa fa-cloud-upload"></i> -->
										   <img src="img/mappe.png">
										</span>

									</a>
								</div>
								<div class="col-sm-12 col-md-3 main-animatedsty rsp_0242171019">
									<a href="<?= $GLOBALS["domain_root"] ?>/hardwareDeployment.php" class="block panel  block-colorsty621 item">
										<p>Hardware <br>Deployment</p>
										<span class="das-imgsty621">
											<!-- <i class="fa fa-desktop"></i> -->
											<img src="img/development.png">
										</span>

									</a>
								</div>

								<div class=" col-sm-12 col-md-3 main-animatedsty rsp_0242171019">
									<a href="<?= $GLOBALS["domain_root"] ?>/softwareDeployment.php" class="block panel  block-colorsty621 item">
										<p>Software<br> Deployment</p>
										<span class="das-imgsty621">
											<!-- <i class="fa fa-code"></i> -->
											<img src="img/code.png">
										</span>
									</a>
								</div>
								<div class="  col-sm-12 col-md-3 main-animatedsty rsp_0242171019">
									<a href="<?= $GLOBALS["domain_root"] ?>/manageCustomers.php" class="block panel  block-colorsty621 item">
										<p>Manage <br>Customers</p>
										<span class="das-imgsty621">
											<!-- <i class="fa fa-gears"></i> -->
											<img src="img/project-management.png">
										</span>
									</a>
								</div>
							</div>

						</div>

						<div class="col-md-6 main-animatedsty rsp_0242171019">
							<div class="row row-sm text-center">
								<div class="col-xs-12 m-b-md">
									<a href="<?= $GLOBALS["domain_root"] ?>/proposals.php" class="block panel block-colorsty621 item">
										<p>Proposals</p>
										<span class="das-imgsty621">
											<!-- <i class="fa fa-dollar"></i> -->
											<img src="img/return-on-investment.png">
										</span>
									</a>
								</div>



							</div>
						</div>
						<div class="col-md-6 main-animatedsty rsp_0242171019">
							<div class="row row-sm text-center">
								<div class="col-xs-12 m-b-md">
									<a href="<?= $GLOBALS["domain_root"] ?>/sendEmail.php" class="block panel block-colorsty621 item">
										<p>Send Emails</p>
										<span class="das-imgsty621">
										   <!--  <i class="fa fa-envelope-o"></i> -->
										   <img src="img/paper-plane.png">
										</span>
									</a>
								</div>



							</div>
						</div>

					</div>
					<!-- tasks -->

					<!-- tasks -->

					<!-- / tasks -->
				</div>
			</div>
			<!-- / main -->
			<!-- right col -->
			<div class="col w-md bg-white-only b-l bg-auto no-border-xs right-barsty62">
				<div class="nav-tabs-alt">
					<ul class="nav nav-tabs tabs-colorchg" role="tablist">
						<li class="active"><a href="#follow" data-toggle="tab"><i class="glyphicon glyphicon-user text-md text-muted wrapper-sm"></i></a></li>
						<li><a href="#chat" data-toggle="tab"><i class="glyphicon glyphicon-comment text-md text-muted wrapper-sm"></i></a></li>
						<li><a href="#trans" data-toggle="tab"><i class="glyphicon glyphicon-transfer text-md text-muted wrapper-sm"></i></a></li>
					</ul>
				</div>
				<div class="tab-content">

					   <div class="tab-pane active" id="follow">
						<div class="wrapper-md">
							<div class="m-b-sm text-md">Recent Activity</div>
						  <ul class="activity-stydata321">
							<li>
								<div class="text-muted">Sun, 11 Feb</div>
								<p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
							</li>
							<li>
								<div class="text-muted">Sun, 11 Feb</div>
								<p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
							</li>
							<li>
								<div class="text-muted">Sun, 11 Feb</div>
								<p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
							</li>
							<li>
								<div class="text-muted">Sun, 11 Feb</div>
								<p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
							</li>
						  </ul>
					 
						</div>
					</div>

					<div class="tab-pane" id="chat">
						<div class="wrapper-md">
							<div class="m-b-sm text-md ">Chat</div>
							<ul class="list-group no-borders pull-in auto">
								<li class="list-group-item">
									<span class="pull-left thumb-sm m-r"><img src="img/user.png" alt="..." class="chat-usersty23"></span>
									<a href class="text-muted" ui-toggle="show" target=".app-aside-right"><i class="fa fa-comment-o pull-right m-t-sm text-sm"></i></a>
									<div class="clear">
										<div><a href="">Chris Fox</a></div>
										<small class="text-muted">about 2 minutes ago</small>
									</div>
								</li>
								<li class="list-group-item">
									<span class="pull-left thumb-sm m-r"><img src="img/user.png" alt="..." class="chat-usersty23"></span>
									<a href class="text-muted" ui-toggle="show" target=".app-aside-right"><i class="fa fa-comment-o pull-right m-t-sm text-sm"></i></a>
									<div class="clear">
										<div><a href="">Amanda Conlan</a></div>
										<small class="text-muted">about 2 hours ago</small>
									</div>
								</li>
								<li class="list-group-item">
									<span class="pull-left thumb-sm m-r"><img src="img/user.png" alt="..." class="chat-usersty23"></span>
									<a href class="text-muted" ui-toggle="show" target=".app-aside-right"><i class="fa fa-comment-o pull-right m-t-sm text-sm"></i></a>
									<div class="clear">
										<div><a href="">Dan Doorack</a></div>
										<small class="text-muted">3 days ago</small>
									</div>
								</li>
								<li class="list-group-item">
									<span class="pull-left thumb-sm m-r"><img src="img/user.png" alt="..." class="chat-usersty23"></span>
									<a href class="text-muted" ui-toggle="show" target=".app-aside-right"><i class="fa fa-comment-o pull-right m-t-sm text-sm"></i></a>
									<div class="clear">
										<div><a href="">Lauren Taylor</a></div>
										<small class="text-muted">about 2 minutes ago</small>
									</div>
								</li>
							</ul>
						</div>
					</div>

				</div>
			 <!--    <div class="padder-md">
					
					<div class="m-b text-md">Recent Activity</div>
					<div class="streamline b-l m-b">
						<div class="sl-item">
							<div class="m-l">
								<div class="text-muted">5 minutes ago</div>
								<p><a href class="text-info">Jessi</a> commented your post.</p>
							</div>
						</div>
						<div class="sl-item">
							<div class="m-l">
								<div class="text-muted">11:30</div>
								<p>Join comference</p>
							</div>
						</div>
						<div class="sl-item b-success b-l">
							<div class="m-l">
								<div class="text-muted">10:30</div>
								<p>Call to customer <a href class="text-info">Jacob</a> and discuss the detail.</p>
							</div>
						</div>

						<div class="sl-item b-primary b-l">
							<div class="m-l">
								<div class="text-muted">Wed, 25 Mar</div>
								<p>Finished task <a href class="text-info">Testing</a>.</p>
							</div>
						</div>
						<div class="sl-item b-warning b-l">
							<div class="m-l">
								<div class="text-muted">Thu, 10 Mar</div>
								<p>Trip to the moon</p>
							</div>
						</div>
						<div class="sl-item b-info b-l">
							<div class="m-l">
								<div class="text-muted">Sat, 5 Mar</div>
								<p>Prepare for presentation</p>
							</div>
						</div>
						<div class="sl-item b-l">
							<div class="m-l">
								<div class="text-muted">Sun, 11 Feb</div>
								<p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
							</div>
						</div>
						<div class="sl-item b-l">
							<div class="m-l">
								<div class="text-muted">Thu, 17 Jan</div>
								<p>Follow up to close deal</p>
							</div>
						</div>
					</div>
				 
				</div> -->

				<!-- aside right -->
				<div class="app-aside-right pos-fix no-padder w-md w-auto-xs bg-white b-l animated fadeInRight hide">
					<div class="vbox">
						<div class="wrapper b-b b-t b-light m-b chat-boxmainsty">
							<a href class="pull-right text-muted text-md" ui-toggle-class="show" target=".app-aside-right"><i class="icon-close"></i></a>
							Chat
						</div>
						<div class="row-row">
							<div class="cell">
								<div class="cell-inner padder">
									<!-- chat list -->
									<div class="m-b">
										<a href class="pull-left thumb-xs avatar"><img class="chat-usersty23" src="img/user.png" alt="..."></a>
										<div class="clear">
											<div class="pos-rlt wrapper-sm b b-light r m-l-sm">
												<span class="arrow left pull-up"></span>
												<p class="m-b-none">Hi John, What's up...</p>
											</div>
											<small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>
										</div>
									</div>
									<div class="m-b">
										<a href class="pull-right thumb-xs avatar"><img src="img/user.png" class="chat-usersty23" alt="..."></a>
										<div class="clear">
											<div class="pos-rlt wrapper-sm bg-light r m-r-sm">
												<span class="arrow right pull-up arrow-light"></span>
												<p class="m-b-none">Lorem ipsum dolor :)</p>
											</div>
											<small class="text-muted">1 minutes ago</small>
										</div>
									</div>
									<div class="m-b">
										<a href class="pull-left thumb-xs avatar"><img class="chat-usersty23" src="img/user.png" alt="..."></a>
										<div class="clear">
											<div class="pos-rlt wrapper-sm b b-light r m-l-sm">
												<span class="arrow left pull-up"></span>
												<p class="m-b-none">Great!</p>
											</div>
											<small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i>Just Now</small>
										</div>
									</div>
									<!-- / chat list -->
								</div>
							</div>
						</div>
						<div class="wrapper m-t b-t b-light">
							<form class="m-b-none">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Say something">
									<span class="input-group-btn">
			  <button class="btn btn-default" type="button">SEND</button>
			</span>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- / aside right -->

			</div>
			<!-- / right col -->
		</div>


	</div>
</div>
<!-- / content -->

<!-- footer -->
<!-- <footer id="footer" class="app-footer" role="footer">
	<div class="wrapper b-t bg-light">
		<span class="pull-right">2.0.1 <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
		&copy; 2015 Copyright.
	</div>
</footer> -->


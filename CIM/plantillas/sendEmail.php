<div class="hbox hbox-auto-xs hbox-auto-sm"  >
    <!-- main -->
    <div class="col">
        <!-- main header -->
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black"> <a href="home.php"  >
                            <i class="fa  fa-arrow-circle-o-left"></i> </a> Send Email
                    </h1>
                    <small class="text-muted"> </small>
                </div>
                <div class="col-sm-6 text-right hidden-xs">

                </div>
            </div>
        </div>
        <!-- / main header -->
        <div class="wrapper-md"  >
            <!-- stats -->
            <div class="email-boxsty">

                <div>
                    <h3 class="main-tablehadsty">Enter the email(s) that you would like to send the CIM application</h3>
                    <input type='text' id='example_emailBS' name='example_emailBS' class='form-control email-appldata' value='' placeholder="Type email">
                    <pre class="hide" id='emails'></pre>
                </div>
                <div>
                    <div class="contTextOption hideCont" align="center" id="contSend">
                            <p class="colorAzulOscuro text-center">Send an email to the devices you wish to inventory</p>
                    </div>
                    <button  class="export-butsty insertar" >Send</button>

                </div>
            </div>

            <!-- / stats -->

        </div>
    </div>

    <!-- / main -->
    <!-- right col -->
<!-- =======================chat code start======================= -->
 <div class="col w-md bg-white-only b-l bg-auto no-border-xs right-barsty62">
                <div class="nav-tabs-alt">
                    <ul class="nav nav-tabs tabs-colorchg" role="tablist">
                        <li class="active"><a href="#follow" data-toggle="tab"><i class="glyphicon glyphicon-user text-md text-muted wrapper-sm"></i></a></li>
                        <li><a href="#chat" data-toggle="tab"><i class="glyphicon glyphicon-comment text-md text-muted wrapper-sm"></i></a></li>
                        <li><a href="#trans" data-toggle="tab"><i class="glyphicon glyphicon-transfer text-md text-muted wrapper-sm"></i></a></li>
                    </ul>
                </div>
                <div class="tab-content">

                       <div class="tab-pane active" id="follow">
                        <div class="wrapper-md">
                            <div class="m-b-sm text-md">Recent Activity</div>
                          <ul class="activity-stydata321">
                            <li>
                                <div class="text-muted">Sun, 11 Feb</div>
                                <p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
                            </li>
                            <li>
                                <div class="text-muted">Sun, 11 Feb</div>
                                <p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
                            </li>
                            <li>
                                <div class="text-muted">Sun, 11 Feb</div>
                                <p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
                            </li>
                            <li>
                                <div class="text-muted">Sun, 11 Feb</div>
                                <p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
                            </li>
                          </ul>
                     
                        </div>
                    </div>

                    <div class="tab-pane" id="chat">
                        <div class="wrapper-md">
                            <div class="m-b-sm text-md ">Chat</div>
                            <ul class="list-group no-borders pull-in auto">
                                <li class="list-group-item">
                                    <span class="pull-left thumb-sm m-r"><img src="img/user.png" alt="..." class="chat-usersty23"></span>
                                    <a href class="text-muted" ui-toggle="show" target=".app-aside-right"><i class="fa fa-comment-o pull-right m-t-sm text-sm"></i></a>
                                    <div class="clear">
                                        <div><a href="">Chris Fox</a></div>
                                        <small class="text-muted">about 2 minutes ago</small>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <span class="pull-left thumb-sm m-r"><img src="img/user.png" alt="..." class="chat-usersty23"></span>
                                    <a href class="text-muted" ui-toggle="show" target=".app-aside-right"><i class="fa fa-comment-o pull-right m-t-sm text-sm"></i></a>
                                    <div class="clear">
                                        <div><a href="">Amanda Conlan</a></div>
                                        <small class="text-muted">about 2 hours ago</small>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <span class="pull-left thumb-sm m-r"><img src="img/user.png" alt="..." class="chat-usersty23"></span>
                                    <a href class="text-muted" ui-toggle="show" target=".app-aside-right"><i class="fa fa-comment-o pull-right m-t-sm text-sm"></i></a>
                                    <div class="clear">
                                        <div><a href="">Dan Doorack</a></div>
                                        <small class="text-muted">3 days ago</small>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <span class="pull-left thumb-sm m-r"><img src="img/user.png" alt="..." class="chat-usersty23"></span>
                                    <a href class="text-muted" ui-toggle="show" target=".app-aside-right"><i class="fa fa-comment-o pull-right m-t-sm text-sm"></i></a>
                                    <div class="clear">
                                        <div><a href="">Lauren Taylor</a></div>
                                        <small class="text-muted">about 2 minutes ago</small>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
             <!--    <div class="padder-md">
                    
                    <div class="m-b text-md">Recent Activity</div>
                    <div class="streamline b-l m-b">
                        <div class="sl-item">
                            <div class="m-l">
                                <div class="text-muted">5 minutes ago</div>
                                <p><a href class="text-info">Jessi</a> commented your post.</p>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="m-l">
                                <div class="text-muted">11:30</div>
                                <p>Join comference</p>
                            </div>
                        </div>
                        <div class="sl-item b-success b-l">
                            <div class="m-l">
                                <div class="text-muted">10:30</div>
                                <p>Call to customer <a href class="text-info">Jacob</a> and discuss the detail.</p>
                            </div>
                        </div>

                        <div class="sl-item b-primary b-l">
                            <div class="m-l">
                                <div class="text-muted">Wed, 25 Mar</div>
                                <p>Finished task <a href class="text-info">Testing</a>.</p>
                            </div>
                        </div>
                        <div class="sl-item b-warning b-l">
                            <div class="m-l">
                                <div class="text-muted">Thu, 10 Mar</div>
                                <p>Trip to the moon</p>
                            </div>
                        </div>
                        <div class="sl-item b-info b-l">
                            <div class="m-l">
                                <div class="text-muted">Sat, 5 Mar</div>
                                <p>Prepare for presentation</p>
                            </div>
                        </div>
                        <div class="sl-item b-l">
                            <div class="m-l">
                                <div class="text-muted">Sun, 11 Feb</div>
                                <p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
                            </div>
                        </div>
                        <div class="sl-item b-l">
                            <div class="m-l">
                                <div class="text-muted">Thu, 17 Jan</div>
                                <p>Follow up to close deal</p>
                            </div>
                        </div>
                    </div>
                 
                </div> -->

                <!-- aside right -->
                <div class="app-aside-right pos-fix no-padder w-md w-auto-xs bg-white b-l animated fadeInRight hide">
                    <div class="vbox">
                        <div class="wrapper b-b b-t b-light m-b chat-boxmainsty">
                            <a href class="pull-right text-muted text-md" ui-toggle-class="show" target=".app-aside-right"><i class="icon-close"></i></a>
                            Chat
                        </div>
                        <div class="row-row">
                            <div class="cell">
                                <div class="cell-inner padder">
                                    <!-- chat list -->
                                    <div class="m-b">
                                        <a href class="pull-left thumb-xs avatar"><img class="chat-usersty23" src="img/user.png" alt="..."></a>
                                        <div class="clear">
                                            <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                                                <span class="arrow left pull-up"></span>
                                                <p class="m-b-none">Hi John, What's up...</p>
                                            </div>
                                            <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>
                                        </div>
                                    </div>
                                    <div class="m-b">
                                        <a href class="pull-right thumb-xs avatar"><img src="img/user.png" class="chat-usersty23" alt="..."></a>
                                        <div class="clear">
                                            <div class="pos-rlt wrapper-sm bg-light r m-r-sm">
                                                <span class="arrow right pull-up arrow-light"></span>
                                                <p class="m-b-none">Lorem ipsum dolor :)</p>
                                            </div>
                                            <small class="text-muted">1 minutes ago</small>
                                        </div>
                                    </div>
                                    <div class="m-b">
                                        <a href class="pull-left thumb-xs avatar"><img class="chat-usersty23" src="img/user.png" alt="..."></a>
                                        <div class="clear">
                                            <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                                                <span class="arrow left pull-up"></span>
                                                <p class="m-b-none">Great!</p>
                                            </div>
                                            <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i>Just Now</small>
                                        </div>
                                    </div>
                                    <!-- / chat list -->
                                </div>
                            </div>
                        </div>
                        <div class="wrapper m-t b-t b-light">
                            <form class="m-b-none">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Say something">
                                    <span class="input-group-btn">
              <button class="btn btn-default" type="button">SEND</button>
            </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- / aside right -->

            </div>
<!-- =========================chat code end==================== -->
</div>


</div>
</div>
<footer id="footer" class="app-footer" role="footer">
    <div class="wrapper b-t bg-light">
        <span class="pull-right">2.0.1 <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
        &copy; 2015 Copyright.
    </div>
</footer>

<script>
    $(document).ready(function(){
        $(".insertar").click(function(){
            if($("#emails").val() == ""){
                $.alert.open("warning", "Warning", "You must place the emails", {"Ok" : "Ok"});
                return false;
            }
            
            $("#fondo").show();
            
            $.post("ajax/guardarSendEmail.php", { emails : $("#emails").val(), token : localStorage.smartControlCIMToken   }, function(data){
                $("#fondo").hide();
                <?php require_once($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>

                result = data[0].result;
                if(result === -2){
                    $.alert.open("warning", "Warning", "Emails not sent", {"Ok" : "Ok"});
                } else if(result === 1){
                    $.alert.open("info", "Info", "Emails sent successfully", {"Ok" : "Ok"}, function(){
                        location.href = "home.php";
                    });
                } else if(result === 3){
                    $.alert.open("warning", "Warning", "The following emails belong to another account: " + data[0].correoOtroCliente);
                } else if(result === 5){
                    $.alert.open("warning", "Warning", "The following emails are duplicated: " + data[0].correosDuplicados);
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Aceptar' : 'Aceptar'}, function() {
                });
            });
        });
        
        $(".insertar").mouseover(function(){
            $("#contSend").css("visibility", "visible");
        });
        
        $(".insertar").mouseout(function(){
            $("#contSend").css("visibility", "hidden");
        });
    });
</script>

<style>
.multiple_emails-container {border: 0px #ccc solid !important;     margin-top: 20px;}
.multiple_emails-container input {
    border: solid 1px #c1bfbf !important;
    height: 41px;
}
</style>
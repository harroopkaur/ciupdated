
<div class="hbox hbox-auto-xs hbox-auto-sm"  >
    <!-- main -->
    <div class="col">
        <!-- main header -->
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black"> <a href="home.php"  > <i class="fa  fa-arrow-circle-o-left"></i> </a> Software Deployment </h1>
                 
                </div>
                <div class="col-sm-6 text-right hidden-xs">

                    <div class="inline text-left">
                        <?php echo $selectClienteHtml?>
                    </div>
                </div>

            </div>
        </div>
        <!-- / main header -->
        <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">

           
            <!-- stats -->
         <!--    <div class="row row-sm text-center">
                <div class="col-md-4 col-md-offset-4 ">
                    <img src="<?= $GLOBALS["domain_root"] ?>/img/imgLogo.png" class="img img-responsive imgTituloHome">
                </div>
            </div> -->
            <div class="row">

                <div class="col-sm-9 rsp_mrg_b20_991">
                     <div class="row">
                <div class="col-xs-12 col-md-12">
                    <button  class="export-butsty" type="button"   onclick="window.open('<?= $GLOBALS["domain_root"] ?>/reportes/excelSoftwareDeployment.php?id=' + $('#cliente').val());">Export</button>
                </div>
            </div>

            <h6 class="main-tablehadsty">Software Deployment Details</h6>
                    <div class="contTabla">
                        <table class="tablap table table-bordered table-condensed">
                            <thead>
                         
                            <tr style="background:#00AFF0; color:#fff;">
                                <th class="text-center">#</th>
                                <th class="text-center"><span>Device Name</span></th>
                                <th class="text-center"><span>Upload Date</span></th>
                                <th class="text-center"><span>Windows Server</span></th>
                                <th class="text-center"><span>SQL Server</span></th>
                            </tr>
                            </thead>
                            <tbody id="registros">
                            <?php
                            $i = 1;
                            foreach($list as $row){
                                ?>
                                <tr>
                                    <td class="text-center"><?= $i ?></td>
                                    <td><?= $row["equipo"] ?></td>
                                    <td><?php
                                        $dateArray = explode("-", $row["fecha"]);
                                        if (count($dateArray) > 1){
                                            echo $dateArray[1] . "/" . $dateArray[2] . "/" . $dateArray[0];
                                        }
                                        ?></td>
                                    <td><?= $row["operatingSystem"] ?></td>
                                    <td><?= $row["nombSQL"] ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>

                            <tr>
                                <th class="text-center">Totals</th>
                                <th class="text-center"><?= count($list) ?></th>
                                <th class="text-center">N/A</th>
                                <th class="text-center"><?= $cantWindows["operatingSystem"] ?></th>
                                <th class="text-center"><?= $cantSQL["nombSQL"] ?></th>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div id="container1">
                        <?php include_once($GLOBALS["app_root"] . "/graficos/softwareDeployment.php"); ?>

                    </div>


                </div>

            </div>
        </div>
    </div>
    <!-- / main -->
</div>
</div>
</div>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>.:Licensing Assurance:.</title>
    <link rel="shortcut icon" href="<?= $GLOBALS["domain_root"] ?>/img/Logo.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="../bower_components/animate.css/animate.css" type="text/css" />
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="../bower_components/simple-line-icons/css/simple-line-icons.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" />
    <link rel="stylesheet" href="css/app.css" type="text/css" />
    <link rel="stylesheet" href="css/app2.css" type="text/css" />
    <link href="<?= $GLOBALS["domain_root1"] ?>/plugin-alert/css/alert.min.css" rel="stylesheet" />
    <link href="<?= $GLOBALS["domain_root1"] ?>/plugin-alert/themes/default/theme.min.css" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="css/multiple-emails.css" />
<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="<?= $GLOBALS["domain_root1"] ?>/plugin-alert/js/alert.js"></script>

    <script type="text/javascript" src="<?=$GLOBALS['domain_root1']?>/js/jquery.tablesorter.js"></script>
    <script type="text/javascript" src="<?=$GLOBALS['domain_root1']?>/js/tableHeadFixer.js"></script>
    <script type="text/javascript" src="<?=$GLOBALS['domain_root1']?>/js/jquery.numeric.js"></script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <script type="text/javascript" src="js/multiple-emails.js"></script>
    <script type="text/javascript">

        //Plug-in function for the bootstrap version of the multiple email
        $(function() {
            //To render the input device to multiple email input using BootStrap icon
            $('#example_emailBS').multiple_emails({position: "bottom"});
            //OR $('#example_emailBS').multiple_emails("Bootstrap");

            //Shows the value of the input device, which is in JSON format
            $('#emails').val($('#example_emailBS').val());
            $('#example_emailBS').change( function(){
                $('#emails').val($(this).val());
            });
        });
    </script>
</head>
<body>
<div class="app app-header-fixed  ">


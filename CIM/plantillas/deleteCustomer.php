<?php if($exito == true){  ?>
    <script type="text/javascript">
        $.alert.open('info', 'Successfully eliminated record', function() {
            location.href = '<?= $GLOBALS['domain_root'] ?>/manageCustomers.php';
        });
    </script>
<?php 
}
else{
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Record has not been removed', function() {
            location.href = '<?= $GLOBALS['domain_root'] ?>/manageCustomers.php';
        });
    </script>
<?php
}
?>
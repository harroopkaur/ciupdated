<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Record successfully updated', function(){
            location.href = '<?= $GLOBALS['domain_root'] ?>/manageCustomers.php';
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Failed to update record', function() {
            location.href = '<?= $GLOBALS['domain_root'] ?>/manageCustomers.php';
        });
    </script>
    <?php
}
?>                                
                               
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Customer Data</span></legend>
    
    <div class="col-sm-6 col-sm-offset-3">
        <form id="form1" name="form1" class="form-horizontal" role="form" method="post">
            <input type="hidden" name="actualizar" id="actualizar" value="1" />
            <?php $validator->print_script(); ?>

            <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                 echo $clientes->error;
            } ?></font>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Customer</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="empresa" name="empresa" value="<?= $datos["nombreEmpresa"] ?>">
                </div>
                <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_empresa") ?></font></div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="email" name="email" value="<?= $datos["correo"] ?>">
                </div>
                <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div>
            </div>        

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-4 text-center">
                    <button type="submit" class="btn btn-default" onclick="validate();">Save</button>
                </div>
            </div>
        </form>
    </div>
</fieldset>
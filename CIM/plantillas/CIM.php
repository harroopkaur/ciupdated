<?php include("plantillas/head1.php"); ?>

<br><br><br>
<div class="col-sm-8 col-sm-offset-2" style="height:400px;">
    <h1 class="text-center"><strong>Choose an option</strong></h1>

    <div class="row">
        <br>
        <div class="col-sm-4" >
            <div class="col-sm-1"><p class="numeros">1</p></div><div class="col-sm-8 col-sm-offset-1 btn" id="send"><span class="glyphicon glyphicon-envelope"></span>&nbsp;<strong>Send Emails</strong></div> 
        </div>
        
        <div class="col-sm-4 col-sm-offset-4">
            <div class="col-sm-1"><p class="numeros">2</p></div><div class="col-sm-8 col-sm-offset-1 btn" id="show"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<strong>Show Results</strong></div> 
        </div>
    </div>
    
    <div class="row">
        <br><br><br>
        
        <div class="col-sm-4 well sombreado" style="padding:30px;">
            
            <div class="row">
                <p class="colorAzulOscuro text-justify">Send an email to the devices you wish to inventory</p>
            </div>
        </div>
        
        <div class="col-sm-4 col-sm-offset-4 well sombreado" style="padding:30px;">
            
            <div class="row">
                <p class="colorAzulOscuro text-justify">View the status and results of the discovery</p>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#salir").click(function(){
            location.href = "exit.php";
        });
        
        $("#send").click(function(){
            location.href = "sendEmail.php";
        });
        
        $("#show").click(function(){
            location.href = "showResults.php";
        });
    });
</script>
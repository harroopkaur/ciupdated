<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Record successfully inserted', function(){
            location.href = '<?= $GLOBALS['domain_root'] ?>/manageCustomers.php';
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Failed to add record', function() {
            location.href = '<?= $GLOBALS['domain_root'] ?>/manageCustomers.php';
        });
    </script>
    <?php
}
?>                                
                               
<fieldset>
   
    
    <div class="customer-dataall651">
        <h6 class="cus-data32main">Customer Data</h6>
        <form id="form1" name="form1" class="form-horizontal" role="form" method="post">
            <input type="hidden" name="insertar" id="insertar" value="1" />
            <?php $validator->print_script(); ?>

            <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                 echo $clientes->error;
            } ?></font>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-12 control-label lable-stycusright">Customer</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="empresa" name="empresa">
                </div>
                <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_empresa") ?></font></div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-12 control-label lable-stycusright">Email</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="email" name="email">
                </div>
                <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div>
            </div>        

            <div class="form-group" style="margin-top: 45px;">
                <div class="col-sm-12 text-center">
                    <button type="button" class="export-butsty" onclick="validate();">Save</button>
                    <button type="button" class="export-butsty" onclick="$('#form1').trigger('reset')">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</fieldset>

<style>
    .app-footer {margin-left: 0px !important; }
    </style>
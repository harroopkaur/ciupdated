
<header id="header" class="app-header navbar" role="menu">
	<!-- navbar header -->
	<div class="navbar-header bg-dark log-sty62">
		<button class="pull-right visible-xs dk" ui-toggle="show" target=".navbar-collapse">
			<i class="glyphicon glyphicon-cog"></i>
		</button>
		<button class="pull-right visible-xs sidebar_toggle_mbtn" ui-toggle="off-screen" target="" ui-scroll="app">
			<i class="glyphicon glyphicon-align-justify"></i>
		</button>
		<!-- brand -->
		<a href="<?= $GLOBALS["domain_root"] ?>/home.php" class="navbar-brand text-lt">

			<img src="img/logo.png" alt="." class="hide">
			<span class="hidden-folded m-l-xs">CIM</span>
		</a>
		<!-- / brand -->
	</div>
	<!-- / navbar header -->

	<!-- navbar collapse -->
	<div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
		<!-- buttons -->
		<div class="nav navbar-nav hidden-xs">
			<a href="#" class="btn no-shadow navbar-btn sidebar_toggle_dbtn" >
				<i class="fa fa-dedent fa-fw text"></i>
				<i class="fa fa-indent fa-fw text-active"></i>
			</a>
			<a href="#" class="btn no-shadow navbar-btn" ui-toggle="show" target="#aside-user">
				<i class="icon-user fa-fw"></i>
			</a>
		</div>
		<!-- / buttons -->

		<!-- link and dropdown -->
		<ul class="nav navbar-nav hidden-sm">
			<li class="dropdown pos-stc">


			</li>
			<li class="dropdown">

			</li>
		</ul>

		<!-- nabar right -->
		<ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
				<a href="#" data-toggle="dropdown" class="dropdown-toggle">
					<i class="icon-bell fa-fw"></i>
					<span class="visible-xs-inline">Notifications</span>
					<span class="badge badge-sm up bg-danger pull-right-xs">2</span>
				</a>
				<!-- dropdown -->
				<div class="dropdown-menu w-xl animated fadeInUp" style="    border: solid 1px #fff;">
					<div class="panel bg-white">
						<div class="panel-heading b-light bg-light not-bgsty321">
							<strong>You have <span>2</span> notifications</strong>
						</div>
						<div class="list-group">
							<a href class="media list-group-item">
					<span class="pull-left thumb-sm">
					  <img src="<?=$GLOBALS['domain_root1']?>/img/a0.jpg" alt="..." class="img-circle">
					</span>
								<span class=" block m-b-none">
					  Use awesome animate.css<br>
					  <small class="text-muted">10 minutes ago</small>
					</span>
							</a>
							<a href class="media list-group-item">
					<span class="block m-b-none">
					  1.0 initial released<br>
					  <small class="text-muted">1 hour ago</small>
					</span>
							</a>
						</div>
						<div class="panel-footer text-sm">
							<a href class="pull-right"><i class="fa fa-cog"></i></a>
							<a href="#notes" data-toggle="class:show animated fadeInRight">See all the notifications</a>
						</div>
					</div>
				</div>
				<!-- / dropdown -->
			</li>
			<li class="dropdown">
				<a href="#" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
			  <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
				<img src="<?=$GLOBALS['domain_root1']?>/img/a0.jpg" alt="...">
				<i class="on md b-white bottom"></i>
			  </span>
					<span class="hidden-sm hidden-md"><?= $_SESSION["nombReseller"] ?></span> <b class="caret"></b>
				</a>
				<!-- dropdown -->
				<ul class="dropdown-menu animated zoomIn w setting-dragsty">

					<li>
						<a href="<?= $GLOBALS["domain_root"] ?>/proposals.php">

							<span>Settings</span>
						</a>
					</li>

					<li class="divider"></li>
					<li>
						<a href="#" class="exit">Logout</a>
					</li>
				</ul>
				<!-- / dropdown -->
			</li>
		</ul>
		<!-- / navbar right -->
	</div>
	<!-- / navbar collapse -->
</header>

<aside id="aside" class="app-aside bg-dark">
	<div class="aside-wrap">
		<div class="navi-wrap">
			<!-- user -->
			<div class="clearfix hidden-xs text-center hide" id="aside-user">
				<div class="dropdown wrapper">
					<a href="">
				<span class="thumb-lg w-auto-folded avatar m-t-sm avtar-sty32">
				  <img src="<?=$GLOBALS['domain_root1']?>/img/a0.jpg" class="img-full" alt="...">
				</span>
					</a>
					<a href="#" data-toggle="dropdown" class="dropdown-toggle hidden-folded">
				<span class="clear text-decnone32">
				  <span class="block m-t-sm text-decnone32">
					<strong class="font-bold text-lt"><?= $_SESSION["nombReseller"] ?></strong>
					<b class="caret"></b>
				  </span>
				  <span class="text-muted text-xs block text-decnone32">Art Director</span>
				</span>
					</a>
					<!-- dropdown -->
					<ul class="dropdown-menu animated zoomIn w hidden-folded setting-dragsty" style="transition: 0.5s;">

						<li>
							<a href="<?= $GLOBALS["domain_root"] ?>/proposals.php">Settings</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="#" class="exit">Logout</a>
						</li>
					</ul>
					<!-- / dropdown -->
				</div>
				<div class="line dk hidden-folded"></div>
			</div>
			<!-- / user -->

			<!-- nav -->
			<nav ui-nav class="navi clearfix left-navsty621">
				<ul class="nav">
					<li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
						<span>Navigation</span>
					</li>
					<li>
						<a href="home.php"  >
							<!-- <i class="glyphicon glyphicon-stats icon"></i> -->
							<span class="das-sidemenu">
								<img src="img/statistics.png">
							</span>
							<span>Dashboard</span>
						</a>
					</li>

					<li>
						<a href="info.php">
							<!-- <i class="fa fa-info "></i> -->
							<span class="das-sidemenu">
								<img src="img/ask.png">
							</span>
							<span>Info</span>
						</a>
					</li>


					<li>
						<a href="<?= $GLOBALS["domain_root"] ?>/sendEmail.php"  >
						 <!--    <i class="fa fa-envelope-o"></i> -->
						  <span class="das-sidemenu">
								<img src="img/paper-plane.png">
							</span>
							<span>Send Emails</span>
						</a>
					</li>
					<li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
						<span>Components</span>
					</li>
					<li>
						<a href="#" class="auto">
					  <span class="pull-right text-muted mar-topsty65">
						<i class="fa fa-fw fa-angle-right text"></i>
						<i class="fa fa-fw fa-angle-down text-active"></i>
					  </span>
							<!-- <i class="glyphicon glyphicon-th"></i> -->
							 <span class="das-sidemenu">
								<img src="img/development.png">
							</span>
							<span>CIM Tool</span>
						</a>
						<ul class="nav nav-sub dk">
							<li>
								<a href="migrationPlan.php">
									<span>Migration Plan</span>
								</a>
							</li>
							<li>
								<a href="hardwareDeployment.php">
									<span>Hadware  Deployment</span>
								</a>
							</li>
							<li>
								<a href="softwareDeployment.php">
									<span>Software  Deployment</span>
								</a>
							</li>
							<li>
								<a href="manageCustomers.php">
									<span>Manage Customers </span>
								</a>
							</li>
							<li>
								<a href="proposals.php">
									<span>Proposals  </span>
								</a>
							</li>
						</ul>
					</li>

			 <!--        <li class="line dk"></li> -->

				</ul>
			</nav>
			<!-- nav -->
		</div>
	</div>
</aside>
<?php
$selectClienteHtml = " 
<form class='form-inline <?= $infoCabe ?> <?= $selectCliente ?>'>
  <div class='form-group mb-2'>
	Select Client
  </div>
  <div class='form-group mx-sm-3 mb-2'>
	<label for='inputPassword2' class='sr-only'>Select your client</label>
	
		<select id='cliente' name='cliente' class='form-control selectCliente select-clientsty321'  >
			<option value=''>--Select--</option>";
			foreach($listado as $row){
				$selectClienteHtml .=" <option value='".$row['id']."'";
				if($_SESSION['client_id'] == $row['id']){    $selectClienteHtml .="selected='selected'"; }
					$selectClienteHtml .= ">".$row['nombreEmpresa']."</option>";
				}
	$selectClienteHtml .=" 
		</select>
  </div> 
  <button type='button' name='save' class='export-butsty mb-2 <?= $btnSave ?>'>Save Role</button>
</form>
";
?>
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">



<script>
	$(document).ready(function(){
		$("#back").click(function(){
			location.href = "<?= $back ?>";
		});

		$("#home").click(function(){
			location.href = "<?= $GLOBALS["domain_root"] ?>/home.php";
		});

		$("#next").click(function(){
			location.href = "<?= $next ?>";
		});

		$("#cliente").change(function(){
			if($("#cliente").val() === ""){
				$("#registros").empty();

				if($("#os").length > 0){
					$("#os").empty();
				}

				if($("#sql").length > 0){
					$("#sql").empty();
				}

				return false;
			}

			$.post("ajax/cambiarCliente.php", { cliente : $("#cliente").val(), token : localStorage.smartControlCIMToken }, function(data){
				<?php require($GLOBALS["app_root"] . "/js/validarSesion.js"); ?>

				if(data[0].result === true){
					location.reload();
				}

				$('#fondo').hide();
			}, "json")
				.fail(function( jqXHR ){
					$('#fondo').hide();
					$.alert.open('error', "Error: " + jqXHR.status);
				});
		});

		$(".exit").click(function(){
			location.href = "exit.php";
		});

		$("#info").click(function(){
			location.href = "info.php";
		});
	});
</script>
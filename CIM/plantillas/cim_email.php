<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!-- If you delete this tag, the sky will fall on your head -->
        <meta name="viewport" content="width=device-width" />

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>CIM-Tool</title>

        <style>
            * {
                margin:0;
                padding:0;
            }
            * { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

            img {
                max-width: 100%;
            }
            body {
                -webkit-font-smoothing:antialiased;
                -webkit-text-size-adjust:none;
                width: 100%!important;
                height: 100%;
            }

            table.head-wrap { width: 100%;}

            table.body-wrap { width: 100%;}

            table.footer-wrap { width: 100%;	clear:both!important;
            }
            .footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
            .footer-wrap .container td.content p {
                font-size:10px;
                font-weight: bold;

            }

            h1,h2,h3,h4,h5,h6 {
                font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
            }
            h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

            h4 { font-weight:500; font-size: 23px;}

            .container {
                display:block!important;
                max-width:450px!important;
                margin:0 auto!important;
                clear:both!important;
            }

            .content {
                padding:15px 0px 15px 0px;
                max-width:450px;
                margin:0 auto;
                display:block;
            }

            .content table { width: 100%; }

            .clear { display: block; clear: both; }

            table.linkdownload{
                background:#d8d8d8;
                border-radius:5px;
                width:200px;
                height:30px;
                margin-top:100px;
                margin-left:20px;
            }
            table.linkdownloadfoot{
                background:#d8d8d8;
                border-radius:5px;
                width:200px;
                height:30px;
            }

            .linkdownload a {
                font-weight:bold;
                color: #00CC00;
            }
            .linkdownload a:hover {
                font-weight:bold;
                color: #00CC00;
            }
            .linkdownload a:visited {
                font-weight:bold;
                color: #00CC00;
            }
            .footermail{
                height:100px;
                width:100%;
                background:url(http://smartcontrolportal.com/CIM/img/footer.jpg) no-repeat top center;
                padding-top:34px;
            }

            .footermail a {
                font-weight:bold;
                color: #00CC00;
            }
            .footermail a:hover {
                font-weight:bold;
                color: #00CC00;
            }
            .footermail a:visited {
                font-weight:bold;
                color: #00CC00;
            }
        </style>
    </head>

    <body bgcolor="#FFFFFF">
    <table class="head-wrap" bgcolor="#00174D">
        <tr>
            <td width="88%" headers="207" valign="bottom" class="header container" style=" background:url(http://smartcontrolportal.com/CIM/img/header.jpg) no-repeat top center; padding:50px 0px 20px 0px">
                <table width="200px" class="linkdownload">
                    <tr>
                        <td valign="middle">
                            Download App <a href="http://smartcontrolportal.com/download/SetupCIM.msi">Here</a>
                        </td>
                        <td align="left" valign="middle"> <img src="http://smartcontrolportal.com/CIM/img/cloud.jpg" /> </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="body-wrap">
        <tr>
            <td class="container" bgcolor="#FFFFFF">
                <div class="content">
                    <table>
                        <tr>
                            <td>
                                <p align="center"><img src="http://smartcontrolportal.com/CIM/img/logoEmail.jpg" /></p>
                                <br /><br />
                                <h4>Dear User, <small>CIM - Tool.</small></h4>
                                <p style="text-align:justify">
                                    Licensing Assurance is pleased to present you with a new Tool called CIM Tool, which is focused on the analysis of the Servers of your Active Directory, with the purpose of evaluating your Infrastructure, in addition to identifying the Microsoft applications that you have installed. All this in order to support you in the best Cloud Migration plan.<br /><br />

                                    In the following link you will find a tutorial of execution of the Tool
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td></td>
        </tr>
    </table>
    <table class="footer-wrap">
        <tr>
            <td class="container">
                <div class="content">
                    <table>
                        <tr>
                            <td align="center">
                                <div class="footermail"  >
                                    <table width="200px" class="linkdownloadfoot" >
                                        <tr>
                                            <td valign="middle"> Download App <a href="http://smartcontrolportal.com/download/SetupCIM.msi">Here</a>
                                            </td>
                                            <td align="left" valign="middle"> <img src="http://smartcontrolportal.com/CIM/img/cloud.jpg" /> </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    </body>
</html>
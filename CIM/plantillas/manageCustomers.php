<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="deleteCustomer.php">
    <input type="hidden" id="id" name="id">
</form>
<div class="contTabla">
<table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="table table-bordered table-condensed tablaHardware migration-tablesty">
    <thead>

        <tr style="background:#00AFF0; color:#fff;">
            <th  align="center" valign="midle" class="text-center"><strong class="til">#</strong></th>
            <th  align="center" valign="midle" class="text-center"><strong class="til">Empresa</strong></th>
            <th  align="center" valign="middle" class="text-center"><strong>Email</strong></th>
            <th  align="center" valign="middle" class="text-center"><strong>Registration Date</strong></th>
            <th  align="center" valign="middle" class="text-center"><strong>Update</strong></th>
            <th  align="center" valign="middle" class="text-center"><strong>Delete</strong></th>
        </tr> 
    </thead>

    <tbody id="bodyTable">
        <?php 
        $i = 1;
        foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td class="text-center"><?= $i ?></td>
                <td align="left"><!--<a href="ver.php?id=<?= $registro['id'] ?>">-->
                     <?= $registro['nombreEmpresa'] ?>
                    <!--</a>-->
                </td>
                <td  align="left"><?= $registro['correo'] ?></td>
                <td  align="center"><?= $registro['fechaRegistro'] ?></td>
                <td  align="center">
                    <a href="updateCustomer.php?id=<?= $registro['id'] ?>"><img src="<?= $GLOBALS["domain_root1"] ?>/img/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Update" title="Update" /></a>

                </td>
                <td align="center">
                    <a href="#" onclick="eliminar(<?= $registro['id'] ?>)"><img src="<?= $GLOBALS["domain_root1"] ?>/img/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Delete" title="Delete" /></a>
                </td>
            </tr>
        <?php 
            $i++;
        } ?>
            
        <th class="text-center">Totals</th>
        <th class="text-center"><?= count($listado) ?></th>
        <th class="text-center"><?= count($listado) ?></th>
        <th class="text-center">N/A</th>
        <th class="text-center"><?= count($listado) ?></th>
        <th class="text-center"><?= count($listado) ?></th>
    </tbody>
</table>
<!--<div id="paginador" style="text-align:center; width:90%; margin:0 auto"><?//= $pag->print_paginator("") ?></div>-->
</div>
<?php 
if($count == 0) { ?>
    <div class="no-customerssty321">No customers</div>
<?php 
}
?>

<script>    
    function eliminar(id){
        $.alert.open('confirm', 'You want to delete the record', {Yes: 'Yes', No: 'No'}, function(button) {
            if (button === 'Yes'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>
</div>


<footer id="footer" class="app-footer new_footer" role="footer">
    <div class="wrapper b-t footer-bgsty">
        <span class="pull-right">Optimized Cloud Migration <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
        &copy; Copyright Licensing Assurance LLC. All Rights Reserved <?= date("Y") ?>
    </div>

</footer>

<script src="<?= $GLOBALS["domain_root1"] ?>/js/ui-load.js"></script>
<script src="<?= $GLOBALS["domain_root1"] ?>/js/ui-jp.config.js"></script>
<script src="<?= $GLOBALS["domain_root1"] ?>/js/ui-jp.js"></script>
<script src="<?= $GLOBALS["domain_root1"] ?>/js/ui-nav.js"></script>
<script src="<?= $GLOBALS["domain_root1"] ?>/js/ui-toggle.js"></script>






<script>
	$(document).ready(function(){
		$('.sidebar_toggle_mbtn').click(function(){
			$('#aside').slideToggle();
		});

		$('.sidebar_toggle_dbtn').click(function(){
			$('body').toggleClass('sidebar_toggle_dactive');
		});
	});
</script>





<script>
	$(document).ready(function(){
		$('.btn_181019').click(function(){
			if($(this).hasClass('active_181019')){
				$(this).next('.tab_181019').slideUp();
				$(this).removeClass('active_181019');
			}
			else{
				$('.tab_181019').slideUp();
				$('.btn_181019').removeClass('active_181019');
				$(this).next('.tab_181019').slideDown();
				$(this).addClass('active_181019');
			}
		});
	});
</script>





<script>
	$(document).ready(function(){
		$('.lag_btn1_1215181019').click(function(){
			$('.lag_dv1_1215181019').hide();
			$('.lag_dv2_1215181019').show();
		});
		$('.lag_btn2_1215181019').click(function(){
			$('.lag_dv1_1215181019').show();
			$('.lag_dv2_1215181019').hide();
		});
	});
</script>
</body>
</html>
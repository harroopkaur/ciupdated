<input type="hidden" id="domain_root" value="<?= $GLOBALS["domain_root"] ?>">
<input type="hidden" id="mensaje" value="<p style='color: #ffffff;'>Thanks for contacting us! We will get back to you very soon.</p>">
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <img src="<?= $GLOBALS["domain_root"] ?>/img/imgCIM.png" class="img img-responsive imgCIMInfo">
        </div>
    </div>

    <br>

    <div class="row-personal">
        <p class="tituloCIMInfo">CIM Tool</p>
    </div>

    <div class="row-personal">
        <p class="tituloFootHome">Optimized Cloud Migration</p>
    </div>
</div>

<div class="contVideo hide">
    <p class="tituloVideo">Take a Few seconds to see how CIM works</p>

</div>

<div class="contPreguntas">
    <div class="container-propio">
        <p class="tituloPreguntas">Frequently Asked Questions</p>
        
        <div class="col-sm-5 col-sm-offset-1">
                <br><br>
                <img src="<?= $GLOBALS["domain_root"] ?>/img/checkCIM.png" class="imgCheck"><span class="txInterrogation">What is CIM?</span>
                <br><br>
                <img src="<?= $GLOBALS["domain_root"] ?>/img/checkCIM.png" class="imgCheck"><span class="txInterrogation">How CIM works?</span>
                <br><br>
                <img src="<?= $GLOBALS["domain_root"] ?>/img/checkCIM.png" class="imgCheck"><span class="txInterrogation">What are the most common mistakes of CIM?</span>
        </div>
        
        <div class="col-sm-6 contImgInterrogacion">
            <img src="<?= $GLOBALS["domain_root"] ?>/img/imgInterrogacion.png" class="imgInterro">
        </div>   
    </div>
</div>
            
<div class="container-propio">
    <div class="col-xs-12" style="padding-bottom:30px;">
        <h1 class="contactUs">Contact Us</h1>
        <br>
        <div class="row">
            <div class='col-xs-12 col-sm-6'>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
                        <p class="subtituloLicensing2">Phone: (305) 851-3545<br>
                            E-mail: info@licensingassurance.com<br><br>

                            Licensing Assurance LLC<br>
                            16192 Coastal Highway<br>
                            Lewes, DE 19958
                        </p>
                    </div>
                </div>
                <div class="visible-xs row">&nbsp;</div>
            </div>
            
            <div class='col-xs-12 col-sm-5'>
                <div class="form-bottom contact-form">
                    <form id="form1" name="form1" role="form" action="<?= $GLOBALS["domain_root"] ?>/assets/contact.php" method="post">
                        <input type="hidden" id="antispam" name="antispam" value="12">
                        <div class="form-group">
                            <input type="text" name="name" placeholder="Name" class="contact-name form-control" id="contact-name">
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" placeholder="Email" class="contact-email form-control" id="contact-email">
                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" placeholder="Phone" class="contact-phone form-control" id="contact-phone">
                        </div>
                        <div class="form-group">
                            <input type="text" name="country" placeholder="Country" class="contact-country form-control" id="contact-country">
                        </div>
                        <div class="form-group">
                            <input type="text" name="company" placeholder="Company" class="contact-company form-control" id="contact-company">
                        </div>
                        <div class="form-group">
                            <input type="text" name="subject" placeholder="Subject" class="contact-subject form-control" id="contact-subject">
                        </div>
                        <div class="form-group">
                            <textarea name="message" placeholder="Message" class="contact-message form-control" id="contact-message"></textarea>
                        </div>
                        <div class="col-sm-2 pull-right">
                            <div class="row">
                                <img src="<?= $GLOBALS["domain_root"] ?>/img/btnSend.png" class="imgAjusteHorizontal pointer" id="enviar">
                            </div>
                        </div>
                        <center><!--<button type="submit" class="btn btn-licensing pull-right" id="btn">Send</button>--></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
<script src="<?= $GLOBALS["domain_root"] ?>/assets/js/scripts.js"></script>
<script>
    $(document).ready(function(){
        $("#enviar").click(function(){
            $("#form1").submit();
        });
    });
</script>

		<div class="hbox hbox-auto-xs hbox-auto-sm"  >
			<!-- main -->
			<div class="col">
				<!-- main header -->
				<div class="bg-light lter b-b wrapper-md">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="m-n font-thin h3 text-black"> <a href="home.php"  > <i class="fa  fa-arrow-circle-o-left"></i> </a> Hardware Deployment </h1>
						   <!--  <small class="text-muted"> </small> -->
						</div>
						<div class="col-sm-6 text-right hidden-xs">

							<div class="inline text-left">
								<?php echo $selectClienteHtml?>
							</div>
						</div>

					</div>
				</div>
				<!-- / main header -->
				<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
				  

					<!-- stats -->
				   <!--  <div class="row row-sm text-center">
						<div class="col-md-4 col-md-offset-4 ">
							<img src="<?= $GLOBALS["domain_root"] ?>/img/imgLogo.png" class="img img-responsive imgTituloHome">
						</div>
					</div> -->
					<div class="row">


							<div class="col-md-9 rsp_mrg_b20_991">
								  <div class="row">
						<div class="col-xs-12 col-md-12">
							<button  class="export-butsty" type="button" onclick="window.open('<?= $GLOBALS["domain_root"] ?>/reportes/excelHardwareDeployment.php?id=' + $('#cliente').val());">Export</button>
						</div>
					</div>
								<h6 class="main-tablehadsty">Hardware Deployment Details</h6>
								<div class="contTabla">
									<table class="table table-bordered table-condensed tablaHardware migration-tablesty" id="tabla" style="width:1800px;">
										<thead>
									  
										<tr style="background:#00AFF0; color:#fff;">
											<th class="text-center" valign="middle" style="width:50px;"><span>#</span></th>
											<th class="text-center" valign="middle" style="width:300px;"><span>Device Name</span></th>
											<th class="text-center" valign="middle" style="width:100px;"><span>Operating System</span></th>
											<th class="text-center" valign="middle"><span>CPU</span></th>
											<th class="text-center" valign="middle"><span>Numbers CPU</span></th>
											<th class="text-center" valign="middle"><span>CPU Speed (GHz)</span></th>
											<th class="text-center" valign="middle"><span>Cores</span></th>
											<th class="text-center" valign="middle"><span>Ave CPU Utilization</span></th>
											<th class="text-center" valign="middle"><span>Memory Size (MB)</span></th>
											<th class="text-center" valign="middle"><span>Memory Available (MB)</span></th>
											<th class="text-center" valign="middle"><span>Disk Drive Size (GB)</span></th>
											<th class="text-center" valign="middle"><span>Disk Drive Available (GB)</span></th>
										</tr>
										</thead>
										<tbody id="registros">
										<?php
										foreach($list as $row){
											?>
											<tr>
												<td class="text-center"><?= $i ?></td>
												<td><?= $row["equipo"] ?></td>
												<td><?= $row["operatingSystem"] ?></td>
												<td><?= $row["cpuModel"] ?></td>
												<td class="text-center"><?= $row["numbersCPU"] ?></td>
												<td class="text-center"><?= $row["cpuSpeed"] ?></td>
												<td class="text-center"><?= $row["cores"] ?></td>
												<td class="text-right"><?= $row["avgCPU"] ?></td>
												<td class="text-right"><?= $row["memorySize"] ?></td>
												<td class="text-right"><?= $row["memoryAvaliable"] ?></td>
												<td class="text-right"><?= $row["diskTotalSize"] ?></td>
												<td class="text-right"><?= $row["diskAvailable"] ?></td>
											</tr>
											<?php
											$cpu += $row["numbersCPU"];
											$cores += $row["cores"];
											$disk += $row["diskTotalSize"];
											$i++;
										}
										?>
										<tr>
											<th class="text-center">Totals</th>
											<th class="text-center"><?= count($list) ?></th>
											<th class="text-center"><?= count($list) ?></th>
											<th class="text-center"><?= count($list) ?></th>
											<th class="text-center"><?= $cpu ?></th>
											<th class="text-center">N/A</th>
											<th class="text-center"><?= $cores ?></th>
											<th class="text-center">N/A</th>
											<th class="text-center">N/A</th>
											<th class="text-center">N/A</th>
											<th class="text-right"><?= $disk ?></th>
											<th class="text-center">N/A</th>
										</tr>
										</tbody>
									</table>
								</div>
							</div>

							<div class="col-md-3 tamGrafico" id="container1">
								<?php include_once($GLOBALS["app_root"] . "/graficos/hardwareDeployment.php"); ?>

							</div>

					</div>
				</div>
			</div>
			<!-- / main -->
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$("#tabla").tableHeadFixer({"left" : 2});
	});
</script>


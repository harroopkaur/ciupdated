        <div class="hbox hbox-auto-xs hbox-auto-sm"  >
            <!-- main -->
            <div class="col">
                <!-- main header -->
                <div class="bg-light lter b-b wrapper-md">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <h1 class="m-n font-thin h3 text-black"> <a href="home.php"  > <i class="fa  fa-arrow-circle-o-left"></i> </a> VMs Component Pricing </h1>
                            <!-- <small class="text-muted"> Pricing</small> -->
                        </div>
                        <div class="col-sm-6 text-right hidden-xs">

                            <div class="inline text-left">
                                <?php echo $selectClienteHtml?>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- / main header -->
                <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">


                    <!-- stats -->
                  <!--   <div class="row row-sm text-center">
                        <div class="col-md-4 col-md-offset-4 ">
                            <img src="<?= $GLOBALS["domain_root"] ?>/img/imgLogo.png" class="img img-responsive imgTituloHome">
                        </div>
                    </div> -->
                    <div class="row" style="margin-top: 30px;">



                        <?php
                        if ($actualizar == 1 && $exito == 1) {
                            ?>
                            <script type="text/javascript">
                                $.alert.open('info', 'Records saved successfully', function(){
                                    location.href = '<?= $GLOBALS['domain_root'] ?>/proposals.php';
                                });
                            </script>
                        <?php
                        } else if ($actualizar == 1 && $exito == 2) {
                        ?>
                            <script type="text/javascript">
                                $.alert.open('warning', 'Missing records to save', function() {
                                    location.href = '<?= $GLOBALS['domain_root'] ?>/proposals.php';
                                });
                            </script>
                        <?php
                        }else if ($actualizar == 1 && $exito == 0) {
                        ?>
                            <script type="text/javascript">
                                $.alert.open('warning', 'Records not saved', function() {
                                    location.href = '<?= $GLOBALS['domain_root'] ?>/proposals.php';
                                });
                            </script>
                            <?php
                        }
                        ?>
                            <div class="col-md-12 ">
                                <form id="form1" name="form1" class="form-horizontal" role="form" method="post">
                                    <input type="hidden" name="actualizar" id="actualizar" value="1" />
                                    <div class="contTabla">
                                        <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="table table-bordered table-condensed tablaHardware migration-tablesty" >
                                            <thead>
                                            <tr style="background:#00AFF0; color:#fff;">
                                                <th  align="center" valign="midle" class="text-center"><strong class="til">View</strong></th>
                                                <th  align="center" valign="midle" class="text-center"><strong class="til">#</strong></th>
                                                <th  align="center" valign="midle" class="text-center"><strong class="til">Component</strong></th>
                                                <th  align="center" valign="middle" class="text-center"><strong>Unit Price ($)</strong></th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyTable">
                                            <?php
                                            $i = 1;
                                            foreach ($listadoComponet as $registro) { ?>
                                                <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                                                    <td class="text-center"><input type="checkbox" class="<?php if($registro["id"] == 1){ echo 'hide'; } ?>" id="check <?= $i ?>" name="check[]" value="<?= $registro["id"] ?>" <?php if($registro["status"] == 1){ echo "checked"; }?>></td>
                                                    <td class="text-center"><?= $i ?></td>
                                                    <td align="left"><?= $registro["componente"] ?></td>
                                                    <td align="right">
                                                        <input type="text" class="text-right"  id="component<?= $i ?>" name="component[]" value="<?= $registro['precio'] ?>" maxlength="10">
                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            } ?>
                                            </tbody>
                                        </table>

                                        <div class="form-group">
                                            <div class="col-sm-offset-4 col-sm-4 text-center">
                                                <button type="submit" class="btn btn-default componet-save621" onclick="validate();"><img src="<?= $GLOBALS["domain_root"] ?>/img/btnSave.png" class="btnSave"></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>

                    <script>
                        $(document).ready(function(){
                            $("#component1").numeric();
                            $("#component2").numeric();
                            $("#component3").numeric();
                            $("#component4").numeric();
                            $("#component5").numeric();

                            $("#component1").click(function(){
                                $("#component1").val("");
                            });

                            $("#component2").click(function(){
                                $("#component2").val("");
                            });

                            $("#component3").click(function(){
                                $("#component3").val("");
                            });

                            $("#component4").click(function(){
                                $("#component4").val("");
                            });

                            $("#component5").click(function(){
                                $("#component5").val("");
                            });
                        });
                    </script>

                </div>
            </div>
            <!-- / main -->
        </div>
    </div>
</div>
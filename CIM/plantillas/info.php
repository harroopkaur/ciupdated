
<style>
	.imgCheck{
		width: 3vw;
		height: auto;
	}

	.das-sidemenu{
		padding-top: 5px;
	}
	.app-content{height: auto !important;}
</style>

<div class="hbox hbox-auto-xs hbox-auto-sm"  >
	<!-- main -->
	<div class="col">
		<!-- main header -->
		<div class="bg-light lter b-b wrapper-md">
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<h1 class="m-n font-thin h3 text-black"> <a href="CIM.php"  > <i class="fa  fa-arrow-circle-o-left"></i> </a> Info CIM</h1>
				   <!--  <small class="text-muted"></small> -->
				</div>
				<div class="col-sm-6 text-right hidden-xs">

				</div>
			</div>
		</div>
		<!-- / main header -->
		<div class="wrapper-md"  >
			<!-- stats -->
			<div class="row">
			   <!--  <div class="col-md-12">
					<div class="text-center panel  wrapper" >
						<h1 class="tituloCIMInfo">Non Active Dorectory Discovery Tool</h1>
						<input type="hidden" id="domain_root" value="<?= $GLOBALS["domain_root1"] ?>/CIM">
						<input type="hidden" id="mensaje" value="<p style='color: #ffffff;'>Thanks for contacting us! We will get back to you very soon.</p>">
						<img src="<?= $GLOBALS["domain_root1"] ?>/CIM/img/Logo.png" class="img img-responsive" style="width:20%; height:auto; margin:auto;">
					</div>
				</div> -->
				<div class="col-md-12">
						<div class="row">






							<div class="col-sm-7 lag_dv1_1215181019">
								<h4 class="faq-hadsty651">Frequently Asked Questions <span class="btn_1215181019 lag_btn1_1215181019"> <i class="fa fa-globe"></i> Spanish</span></h4>
								<ul class="infodata-list652 accordian_181019">
									<li>
										<span class="btn_181019 active_181019">What is CIM (Cloud Infrastructure Migration)?</span>
										<p class="tab_181019" style="display: block">It is a tool designed to effectively support you in the process of analyzing servers in physical and / or virtual environments, connected to the network through the Active Directory and suggesting you the best option for a possible cloud migration proposal. CIM Evaluate the infrastructure and facilities of OS / SQL in a single automatic execution.</p>
									</li>
									<li>
										<span class="btn_181019">How CIM works?</span>
										<p class="tab_181019">It is an application that must be installed locally, once the application is installed, it is executed with a domain administrator user, to connect and select the list of servers, then configure the tool scheduling to establish the time metric that best suits you (from a single execution to 10 continuous days of execution). At the end of the analysis the results will be sent to the administrator portal through the internet.</p>
									</li>
									<li>
										<span class="btn_181019">What are the most common errors of CIM?</span>
										<ul class="tab_181019">
											<li>
												<span><b>I did not receive the email with the application:</b></span>
												<span><b>To solve this:</b> It is possible that the mail server will mark it as SPAM, for this it is important to check the SPAM folder in order to verify if there is the mail.</span>
											</li>
											<li>
												<span><b>The username or password is not correct:</b></span>
												<span><b>To solve this:</b> Please ensure that the tool is executed using the credentials of a user with domain administrator privileges (Admin Controller).</span>
											</li>
											<li>
												<span><b>The server is not functional.</b></span>
												<span><b>To solve this:</b> Verify the connection to the AD or Active Directory from the computer where you plan to run the tool.</span>
											</li>
										</ul>
									</li>
								</ul>
							</div>






							<div class="col-sm-7 lag_dv2_1215181019">
								<h4 class="faq-hadsty651">Frequently Asked Questions <span class="btn_1215181019 lag_btn2_1215181019"> <i class="fa fa-globe"></i> English</span></h4>
								<ul class="infodata-list652 accordian_181019">
									<li>
										<span class="btn_181019 active_181019">Que es CIM (Migración de infraestructura a la nube)?</span>
										<p class="tab_181019" style="display: block">Es una herramienta diseñada para apoyarle eficazmente en el proceso de análisis de los servidores en ambientes físicos y/o virtuales, conectados a la red a través del Directorio Activo y sugerirle la mejor opción de propuesta de migración a la nube. Evalúa la infraestructura y las instalaciones de OS/SQL en una sola ejecución automática.</p>
									</li>
									<li>
										<span class="btn_181019">Como funciona CIM?</span>
										<p class="tab_181019">Es una aplicación que hay que instalarla localmente, una vez instalada la aplicación, se ejecuta con un usuario administrador de dominio, para conectarse y selecciona la lista de servidores, configurando luego la calendarización de la herramienta para establecer la métrica de tiempo que mejor le convenga (desde una sola ejecución hasta 10 días continuos de ejecución). Al finalizar el análisis los resultados serán enviados al portal de administrador a través de internet.</p>
									</li>
									<li>
										<span class="btn_181019">Errores más communes del CIM:</span>
										<ul class="tab_181019">
											<li>
												<span><b>No recibí el correo electrónico con la aplicación:</b></span>
												<span><b>Solución:</b> Es posible que el servidor de correos lo marco como SPAM, para ello es importante revisar la carpeta de SPAM con el fin de verificar si allí se encuentra el correo.</span>
											</li>
											<li>
												<span><b>El nombre de usuario o la contraseña no son correctos:</b></span>
												<span><b>Solución:</b> Solución:Por favor asegúrese que a herramienta sea ejecutada empleando las credenciales de un usuario con privilegios de administrador de dominio (Admin Controller).</span>
											</li>
											<li>
												<span><b>El servidor no es funcional.</b></span>
												<span><b>Solución:</b> Verifique la conexión al AD o Directorio Activo desde el equipo donde se planea ejecutar la herramienta.</span>
											</li>
										</ul>
									</li>
								</ul>
							</div>











							<div class="col-sm-5 text-center">
								<div class="img-inforight">
								<img src="<?= $GLOBALS["domain_root1"] ?>/CIM/img/49421_questions-png.png">
								</div>
							</div>
						</div>
				</div>

				<div class="col-md-12" >

					<div class="main-contactsty654" >
						<h1 class="contactUs contact-us985">Contact Us</h1>
						<div class="row">
							<div class='col-xs-12 col-sm-12 col-md-4'>
									<p class="subtituloLicensing2">Phone: (305) 851-3545<br>
										E-mail: info@licensingassurance.com<br><br>

										Licensing Assurance LLC<br>
										16192 Coastal Highway<br>
										Lewes, DE 19958
									</p>
							</div>
							<div class='col-xs-12 col-sm-12 col-md-8'>
							<div class="form-bottom contact-form">
								<form id="form1" name="form1"  action="<?= $GLOBALS["domain_root1"] ?>/assets/contact.php" method="post">
									<input type="hidden" id="antispam" name="antispam" value="12">
									<div class="form-group col-md-6 col-xs-12">
										<input type="text" required name="name" placeholder="Name" class="contact-name form-control" id="contact-name">
									</div>
									<div class="form-group col-md-6 col-xs-12">
										<input type="email" required name="email" placeholder="Email" class="contact-email form-control" id="contact-email">
									</div>
									<div class="form-group col-md-6 col-xs-12">
										<input type="text" required name="phone" placeholder="Phone" class="contact-phone form-control" id="contact-phone">
									</div>
									<div class="form-group col-md-6 col-xs-12">
										<input type="text" name="country" placeholder="Country" class="contact-country form-control" id="contact-country">
									</div>
									<div class="form-group col-md-6 col-xs-12">
										<input type="text" name="company" placeholder="Company" class="contact-company form-control" id="contact-company">
									</div>
									<div class="form-group col-md-6 col-xs-12">
										<input type="text" required name="subject" placeholder="Subject" class="contact-subject form-control" id="contact-subject">
									</div>
									<div class="form-group col-md-12 col-xs-12">
										<textarea name="message" required placeholder="Message" class="contact-message form-control" id="contact-message"></textarea>
									</div>
									 <div class="form-group col-md-12 col-xs-12" style="text-align: center;">
										<input type='submit' value="Send"  class='export-butsty '/>
									</div>

								</form>
							</div>
						</div>
						</div>
					</div>
				</div>
				<script src="<?= $GLOBALS["domain_root1"] ?>/assets/js/scripts.js"></script>
			</div>

			<!-- / stats -->

		</div>
	</div>

	<!-- / main -->
	<!-- right col -->

	<!-- ================= chat side bar================= -->
<div class="col w-md bg-white-only b-l bg-auto no-border-xs right-barsty62">
				<div class="nav-tabs-alt">
					<ul class="nav nav-tabs tabs-colorchg" role="tablist">
						<li class="active"><a href="#follow" data-toggle="tab"><i class="glyphicon glyphicon-user text-md text-muted wrapper-sm"></i></a></li>
						<li><a href="#chat" data-toggle="tab"><i class="glyphicon glyphicon-comment text-md text-muted wrapper-sm"></i></a></li>
						<li><a href="#trans" data-toggle="tab"><i class="glyphicon glyphicon-transfer text-md text-muted wrapper-sm"></i></a></li>
					</ul>
				</div>
				<div class="tab-content">

					   <div class="tab-pane active" id="follow">
						<div class="wrapper-md">
							<div class="m-b-sm text-md">Recent Activity</div>
						  <ul class="activity-stydata321">
							<li>
								<div class="text-muted">Sun, 11 Feb</div>
								<p><a href="" class="text-info">Jessi</a> assign you a task <a href="" class="text-info">Mockup Design</a>.</p>
							</li>
							<li>
								<div class="text-muted">Sun, 11 Feb</div>
								<p><a href="" class="text-info">Jessi</a> assign you a task <a href="" class="text-info">Mockup Design</a>.</p>
							</li>
							<li>
								<div class="text-muted">Sun, 11 Feb</div>
								<p><a href="" class="text-info">Jessi</a> assign you a task <a href="" class="text-info">Mockup Design</a>.</p>
							</li>
							<li>
								<div class="text-muted">Sun, 11 Feb</div>
								<p><a href="" class="text-info">Jessi</a> assign you a task <a href="" class="text-info">Mockup Design</a>.</p>
							</li>
						  </ul>
					 
						</div>
					</div>

					<div class="tab-pane" id="chat">
						<div class="wrapper-md">
							<div class="m-b-sm text-md ">Chat</div>
							<ul class="list-group no-borders pull-in auto">
								<li class="list-group-item">
									<span class="pull-left thumb-sm m-r"><img src="img/user.png" alt="..." class="chat-usersty23"></span>
									<a href="" class="text-muted" ui-toggle="show" target=".app-aside-right"><i class="fa fa-comment-o pull-right m-t-sm text-sm"></i></a>
									<div class="clear">
										<div><a href="">Chris Fox</a></div>
										<small class="text-muted">about 2 minutes ago</small>
									</div>
								</li>
								<li class="list-group-item">
									<span class="pull-left thumb-sm m-r"><img src="img/user.png" alt="..." class="chat-usersty23"></span>
									<a href="" class="text-muted" ui-toggle="show" target=".app-aside-right"><i class="fa fa-comment-o pull-right m-t-sm text-sm"></i></a>
									<div class="clear">
										<div><a href="">Amanda Conlan</a></div>
										<small class="text-muted">about 2 hours ago</small>
									</div>
								</li>
								<li class="list-group-item">
									<span class="pull-left thumb-sm m-r"><img src="img/user.png" alt="..." class="chat-usersty23"></span>
									<a href="" class="text-muted" ui-toggle="show" target=".app-aside-right"><i class="fa fa-comment-o pull-right m-t-sm text-sm"></i></a>
									<div class="clear">
										<div><a href="">Dan Doorack</a></div>
										<small class="text-muted">3 days ago</small>
									</div>
								</li>
								<li class="list-group-item">
									<span class="pull-left thumb-sm m-r"><img src="img/user.png" alt="..." class="chat-usersty23"></span>
									<a href="" class="text-muted" ui-toggle="show" target=".app-aside-right"><i class="fa fa-comment-o pull-right m-t-sm text-sm"></i></a>
									<div class="clear">
										<div><a href="">Lauren Taylor</a></div>
										<small class="text-muted">about 2 minutes ago</small>
									</div>
								</li>
							</ul>
						</div>
					</div>

				</div>
			 <!--    <div class="padder-md">
					
					<div class="m-b text-md">Recent Activity</div>
					<div class="streamline b-l m-b">
						<div class="sl-item">
							<div class="m-l">
								<div class="text-muted">5 minutes ago</div>
								<p><a href class="text-info">Jessi</a> commented your post.</p>
							</div>
						</div>
						<div class="sl-item">
							<div class="m-l">
								<div class="text-muted">11:30</div>
								<p>Join comference</p>
							</div>
						</div>
						<div class="sl-item b-success b-l">
							<div class="m-l">
								<div class="text-muted">10:30</div>
								<p>Call to customer <a href class="text-info">Jacob</a> and discuss the detail.</p>
							</div>
						</div>

						<div class="sl-item b-primary b-l">
							<div class="m-l">
								<div class="text-muted">Wed, 25 Mar</div>
								<p>Finished task <a href class="text-info">Testing</a>.</p>
							</div>
						</div>
						<div class="sl-item b-warning b-l">
							<div class="m-l">
								<div class="text-muted">Thu, 10 Mar</div>
								<p>Trip to the moon</p>
							</div>
						</div>
						<div class="sl-item b-info b-l">
							<div class="m-l">
								<div class="text-muted">Sat, 5 Mar</div>
								<p>Prepare for presentation</p>
							</div>
						</div>
						<div class="sl-item b-l">
							<div class="m-l">
								<div class="text-muted">Sun, 11 Feb</div>
								<p><a href class="text-info">Jessi</a> assign you a task <a href class="text-info">Mockup Design</a>.</p>
							</div>
						</div>
						<div class="sl-item b-l">
							<div class="m-l">
								<div class="text-muted">Thu, 17 Jan</div>
								<p>Follow up to close deal</p>
							</div>
						</div>
					</div>
				 
				</div> -->

				<!-- aside right -->
				<div class="app-aside-right pos-fix no-padder w-md w-auto-xs bg-white b-l animated fadeInRight hide">
					<div class="vbox">
						<div class="wrapper b-b b-t b-light m-b chat-boxmainsty">
							<a href="" class="pull-right text-muted text-md" ui-toggle-class="show" target=".app-aside-right"><i class="icon-close"></i></a>
							Chat
						</div>
						<div class="row-row">
							<div class="cell">
								<div class="cell-inner padder">
									<!-- chat list -->
									<div class="m-b">
										<a href="" class="pull-left thumb-xs avatar"><img class="chat-usersty23" src="img/user.png" alt="..."></a>
										<div class="clear">
											<div class="pos-rlt wrapper-sm b b-light r m-l-sm">
												<span class="arrow left pull-up"></span>
												<p class="m-b-none">Hi John, What's up...</p>
											</div>
											<small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>
										</div>
									</div>
									<div class="m-b">
										<a href="" class="pull-right thumb-xs avatar"><img src="img/user.png" class="chat-usersty23" alt="..."></a>
										<div class="clear">
											<div class="pos-rlt wrapper-sm bg-light r m-r-sm">
												<span class="arrow right pull-up arrow-light"></span>
												<p class="m-b-none">Lorem ipsum dolor :)</p>
											</div>
											<small class="text-muted">1 minutes ago</small>
										</div>
									</div>
									<div class="m-b">
										<a href="" class="pull-left thumb-xs avatar"><img class="chat-usersty23" src="img/user.png" alt="..."></a>
										<div class="clear">
											<div class="pos-rlt wrapper-sm b b-light r m-l-sm">
												<span class="arrow left pull-up"></span>
												<p class="m-b-none">Great!</p>
											</div>
											<small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i>Just Now</small>
										</div>
									</div>
									<!-- / chat list -->
								</div>
							</div>
						</div>
						<div class="wrapper m-t b-t b-light">
							<form class="m-b-none">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Say something">
									<span class="input-group-btn">
			  <button class="btn btn-default" type="button">SEND</button>
			</span>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- / aside right -->

			</div>
	<!-- ================chat side bar=================== -->
</div>


</div>
</div>
<footer id="footer" class="app-footer" role="footer">
	<div class="wrapper b-t bg-light">
		<span class="pull-right">2.0.1 <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
		&copy; 2015 Copyright.
	</div>
</footer>


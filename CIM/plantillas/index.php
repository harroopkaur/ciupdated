<style>
    .app-header-fixed { padding-top: 35px;}
</style>

 <img class="index-logosty" src="<?= $GLOBALS["domain_root"] ?>/img/logoCIM.png">
<div class="m-b-lg">

    <form name="form" class="form-validation" action="authenticate.php" method="post">
        <div class="text-danger wrapper text-center" >

        </div>
        <div class="list-group list-group-sm">
            <div class="list-group-item input-newsty">
                <input type="text" placeholder="Usuario" class="form-control no-border"  id="login" name="login"  required>
                <input type="hidden" name="entrar" value="1">
            </div>
            <div class="list-group-item input-newsty">
                <input type="password" placeholder="Password" name="password" class="form-control no-border"   required>
            </div>
        </div>
        <button type="submit" class="login-butsty6251" >Log in</button>
        <div class="text-center m-t m-b"><a class="forgot-sty621" ui-sref="access.forgotpwd">Forgot password?</a></div>
        <div class="line line-dashed"></div>

    </form>
    <?php if (isset($_GET['error']) == 1) { ?>
        <div align="center" class="error_prog">Invalid Login or password</div>
    <?php } ?>
</div>
<div class="text-center"  >
    <p>
        <small class="text-muted">CIM - CLOUD INFRAESTRUCTURE MIGRATION<br>&copy; 2018</small>
    </p>
</div>


if(data[0].resultado === false){
    $.alert.open('error', "Error", "Tokens do not match", {'Ok' : 'Ok'}, function() {
        location.href = "<?= $GLOBALS['domain_root1'] . '/CIM'?>";
    }); 
    return false;
}
if(data[0].sesion === false){
    $.alert.open('error', "Error", "Error: " + data[0].mensaje, {'Ok' : 'Ok'}, function() {
        location.href = "<?= $GLOBALS['domain_root1'] . '/CIM' ?>";
    });                    
    return false;
}
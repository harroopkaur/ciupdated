<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresas.php");

$array = array(0=>array('resultado'=>false));

if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    $general = new General();
    $empresas = new empresas();

    $login = "";
    if(isset($_POST["login"])){
        $login = $general->get_escape($_POST["login"], 0);
    }

    $pass = "";
    if(isset($_POST["contrasena"])){
        $pass = $_POST["contrasena"];
    }

    $listado = $empresas->listadoEmp($login, $pass);

    $combo = "<option value=''>--Select--</option>";

    foreach($listado as $row){
        $combo .= "<option value='" . $row["id"] . "'>" . $row["empresa"] . "</option>";
    }

    $array = array(0=>array('combo'=>$combo, 'resultado'=>true));
}
    
echo json_encode($array);
?>
<?php
require_once("../configuracion/inicio.php");

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/NADD/clases/clase_resumen.php");

$general     = new General();
$result = 0;

$correo = "";
if(isset($_POST["correo"])){
    $correo = $general->get_escape($_POST['correo']); 
}

$indice = 0;
if(isset($_POST["indice"]) && filter_var($_POST["indice"], FILTER_VALIDATE_INT) !== false){
    $indice = $_POST['indice']; 
}

$resumen = new ResumenAppEscaneo();

$row = $resumen->obtenerIdEmail($correo);

if ($row["id"] > 0){
    $result = 1;
}

$array = array('var1'=>$result, 'var2'=>$indice, 'var3'=>0);

echo json_encode($array);
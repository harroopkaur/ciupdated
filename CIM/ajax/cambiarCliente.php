<?php
require_once("../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_clientes.php");

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

$array = array(0=>array('resultado'=>false));
if($nuevo_middleware->compararCIMAjax($token, true)){
    $general     = new General();
    if(!isset($_SESSION['client_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $usuario = new Clientes();
            
            $cliente = 0;
            if(isset($_POST["cliente"]) && filter_var($_POST["cliente"], FILTER_VALIDATE_INT) !== false){
                $cliente = $_POST["cliente"];
            }

            $result = $usuario->autenticarResellerCIM1($_SESSION["reseller"], $cliente);
            
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>$mensaje, 'result'=>$result, 'resultado'=>true));
        }
    } else{
        $general->eliminarSesion();
    }
}

echo json_encode($array);
<?php
error_reporting(0);
require_once("../configuracion/inicio.php");

//inicio middleware
require_once($GLOBALS["app_root"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"]);
//fin middleware

require_once($GLOBALS["app_root"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/NADD/clases/clase_resumen.php");
require_once($GLOBALS["app_root"] . "/clases/clase_email.php");

$array = array(0=>array('resultado'=>false));
$general     = new General();

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}
if($nuevo_middleware->compararNADDAjax($token)){
    $general     = new General();
    if(!isset($_SESSION['client_NADD_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['client_NADD_autorizado']), $_SESSION['client_NADD_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    if($verifSesion[0]){
        $sesion                    = true;
        $_SESSION['client_tiempo'] = $verifSesion[1];

        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $resumen = new ResumenAppEscaneo();
            $correoDuplicado = "";
            $correoEnviados = "";               
            $result = 0;
            
            $emails = $_POST["emails"];
            $emails = str_replace('"', "", $emails);
            $emails = str_replace("[", "", $emails);
            $emails = str_replace("]", "", $emails);
            
            $tabla =  explode(",", $emails);

            //inicio validando existencia de correos
            if(count($tabla) == 0){
                $result = 4;
            }
            //fin validando existencia de correos

            //inicio validando correos duplicados
            if($result == 0){
                $valores = array_count_values($tabla);

                for($i = 0; $i < count($tabla); $i++){
                    if($valores[$tabla[$i]] > 1){
                        if(strpos($correoDuplicado, $tabla[$i]) === false){
                            if($correoDuplicado != ""){
                                $correoDuplicado .= ", ";
                            }

                            $correoDuplicado .= trim($tabla[$i]);
                        }

                        $result = 5;
                    }
                }
            }
            //fin validando correos duplicados

            //inicio validando correos enviados
            /*if($result == 0){                        
                for($i = 0; $i < count($tabla); $i++){
                    $row = $resumen->obtenerIdEmail($tabla[$i]);

                    if ($row["id"] > 0){
                        if(strpos($correoEnviados, $tabla[$i]) === false){
                            if($correoEnviados != ""){
                                $correoEnviados .= ", ";
                            }

                            $correoEnviados .= $tabla[$i];
                        }
                        $result = 6;
                    }
                }
            }*/
            //fin validando correos enviados
            
            if($result == 0){
                $envioCorreo = new email();
                //$result = -2;

                $i = 0;
                $stringCorreo = "";
                foreach($tabla as $row){
                    if(trim($row) != "" && $resumen->insertarCorreos($_SESSION["client_NADD_id"], trim($row))){
                        if($stringCorreo != ""){
                            $stringCorreo .= ", ";
                        }

                        $stringCorreo .= "<" . trim($row) . ">";
                        $i++;
                    }
                }

                $envioCorreo->enviar_app_correo($stringCorreo, $_SESSION["client_NADD_nombre"] . " " . $_SESSION["client_NADD_apellido"]);

                $result = 1;
            }

            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'correosDuplicados'=>$correoDuplicado, 
            'correosEnviados'=>$correoEnviados, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}

echo json_encode($array);
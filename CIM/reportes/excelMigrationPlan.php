<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_CIM.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = $general->obtenerMensaje();
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}

if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../lib/Excel/PHPExcel.php';

    $cim = new clase_CIM();

    $id = 0;
    if(isset($_GET["id"]) && is_numeric($_GET["id"])){
        $id = $_GET["id"];
    }
        
    $list = $cim->migrationPlan($_SESSION["reseller"], $id);
    $roles = $cim->totalRoles($id);
    $compActivos = $cim->compActivos($_SESSION["reseller"], $_SESSION["client_id"]);
    
    $servidores = 0;
    $cpus = 0;
    $cores = 0;
    $memory = 0;
    $disk = 0;
    $diskUsed = 0;
    $total = 0;
    
    $columnCPU = 0;
    $columnCORE = 0;
    $columnRAM = 0;
    $columnDisk = 0;
    $columnDiskUsed = 0;
    
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("CIM")
                ->setTitle("Migration Plan");


    // Add some data
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', '#')
                ->setCellValue('B1', 'Hostname')
                ->setCellValue('C1', 'Role')
                ->setCellValue('D1', 'Base Server Plan')
                ->setCellValue('E1', 'CPUs')
                ->setCellValue('F1', 'Cores')
                ->setCellValue('G1', 'GB of RAM')
                ->setCellValue('H1', 'GB of Disk')
                ->setCellValue('I1', 'Used Disk Storage')
                ->setCellValue('J1', 'Total Cost ($)');
    
    $nombre = "Excel Migration Plan.xlsx";
    
    $index = 2;
    foreach($list as $row){
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $index, $index - 1)
                    ->setCellValue('B' . $index, $row["equipo"])
                    ->setCellValue('C' . $index, $row["role"])
                    ->setCellValue('D' . $index, $row["servidores"])
                    ->setCellValue('E' . $index, $row["numbersCPU"])
                    ->setCellValue('F' . $index, $row["cores"])
                    ->setCellValue('G' . $index, $row["memory"])
                    ->setCellValue('H' . $index, $row["disk"])
                    ->setCellValue('I' . $index, $row["diskUsed"])
                    ->setCellValue('J' . $index, $row["total"]);
        
        $servidores += $row["servidores"];
        $cpus += $row["numbersCPU"];
        $cores += $row["cores"];
        $memory += $row["memory"];
        $disk += $row["disk"];
        $diskUsed += $row["diskUsed"];
        $total += $row["total"];
        $index++;
    }
    
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $index, "Totals")
                ->setCellValue('B' . $index, count($list))
                ->setCellValue('C' . $index, $roles)
                ->setCellValue('D' . $index, $servidores)
                ->setCellValue('E' . $index, $cpus)
                ->setCellValue('F' . $index, $cores)
                ->setCellValue('G' . $index, $memory)
                ->setCellValue('H' . $index, $disk)
                ->setCellValue('I' . $index, $diskUsed)
                ->setCellValue('J' . $index, $total);
    
    foreach($compActivos as $row){
        if($row["idComponent"] == 2 && $row["status"] == 1){
            $columnCPU = 1;
        } elseif ($row["idComponent"] == 3 && $row["status"] == 1){
            $columnCORE = 1;
        } elseif ($row["idComponent"] == 4 && $row["status"] == 1){
            $columnRAM = 1;
        } elseif ($row["idComponent"] == 5 && $row["status"] == 1){
            $columnDisk = 1;
        } elseif ($row["idComponent"] == 6 && $row["status"] == 1){
            $columnDiskUsed = 1;
        }
    }

    if($columnCPU == 0){
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setVisible(false);
    } 
    
    if($columnCORE == 0){
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setVisible(false);
    } 
    
    if($columnRAM == 0){
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setVisible(false);
    } 
    
    if($columnDisk == 0){
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setVisible(false);
    } 
    
    if($columnDiskUsed == 0){
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setVisible(false);
    }
    
    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombre . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}
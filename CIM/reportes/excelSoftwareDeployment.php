<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
/** Error reporting */
//error_reporting(E_ALL);
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root"] . "/clases/clase_CIM.php");

$general     = new General();
if(!isset($_SESSION['client_tiempo'])){
    $verifSesion[0] = false;
    $mensaje        = $general->obtenerMensaje();
}
else{
    $verifSesion = $general->verifSesion(isset($_SESSION['client_autorizado']), $_SESSION['client_tiempo'], $TIEMPO_MAXIMO_SESION);
    $mensaje     = $verifSesion[2];
}

if($verifSesion[0]){
    $_SESSION['client_tiempo'] = $verifSesion[1];
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);

    if (PHP_SAPI == 'cli')
        die('only be run from a Web Browser');

    /** Include PHPExcel */
    require_once dirname(__FILE__) . '/../../lib/Excel/PHPExcel.php';

    $cim = new clase_CIM();

    $id = 0;
    if(isset($_GET["id"]) && is_numeric($_GET["id"])){
        $id = $_GET["id"];
    }
        
    $list = $cim->softwareInstalled($id);
    $cantWindows = $cim->cantidadSoftwareInstalledWindows($id);
    $cantSQL = $cim->cantidadSoftwareInstalledSQL($id);
    
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("CIM")
                ->setTitle("Migration Plan");


    // Add some data
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', '#')
                ->setCellValue('B1', 'Device Name')
                ->setCellValue('C1', 'Upload Date')
                ->setCellValue('D1', 'Windows Server')
                ->setCellValue('E1', 'SQL Server');

    $nombre = "Excel Software Deployment.xlsx";
    
    $index = 2;
    foreach($list as $row){
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $index, $index - 1)
                    ->setCellValue('B' . $index, $row["equipo"])
                    ->setCellValue('C' . $index, "")
                    ->setCellValue('D' . $index, $row["operatingSystem"])
                    ->setCellValue('E' . $index, $row["nombSQL"]);
        $index++;
    }

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $index, "Totals")
                ->setCellValue('B' . $index, count($list))
                ->setCellValue('C' . $index, "N/A")
                ->setCellValue('D' . $index, $cantWindows["operatingSystem"])
                ->setCellValue('E' . $index, $cantSQL["nombSQL"]);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombre . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}
else{
    $general->eliminarSesion();
}
<script>
    $('#container1').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        credits: {
            enabled: false
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.0f}'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>'
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [{
                name: 'OS',
                /*y: <?= count($list) ?>,*/
                y: 30,
                /*color: '<?= $color1 ?>'*/
                color: 'blue'
            },
            {
                name: 'CPU',
                /*y: <?= $cpu ?>,*/
                y: 50,
                /*color: '<?= $color2 ?>'*/
                color: 'red'
            },
            {
                name: 'Cores',
                /*y: <?= $cores ?>,*/
                y: 5,
                /*color: '<?= $color3 ?>'*/
                color: 'green'
            },
            {
                name: 'Disk',
                /*y: <?= $disk ?>,*/
                y: 15,
                /*color: '<?= $color4 ?>'*/
                color: 'yellow'
            }]
        }],
    });
</script>

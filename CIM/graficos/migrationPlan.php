<script>
    $('#container1').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: ''
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
                showInLegend: true
            }
        },

        series: [
        {
            name: '',
            colorByPoint: true,
            data: [{
                name:'Cores',
                /*y:<?=$cores?>,*/
                y:15,
                /*color:'<?=$color1?>'*/
                color:'red'
            },
            {
                name:'RAM',
                /*y:<?=$memory?>,*/
                y:10,
                /*color:'<?=$color2?>'*/
                color:'green'
            },
            {
                name:'Disk',
                /*y:<?=$disk?>,*/
                y:25,
                /*color:'<?=$color3?>'*/
                color:'yellow'
            }]
        }]
    });
</script>

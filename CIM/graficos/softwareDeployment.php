<script>
    $('#container1').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: ''
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
                showInLegend: true
            }
        },

        series: [
        {
            name: '',
            colorByPoint: true,
            data: [{
                name:'Windows Server',
                /*y:<?= $cantWindows["operatingSystem"] ?>,*/
                y:75,
                /*color:'<?=$color1?>'*/
                color:'red'
            },
            {
                name:'SQL Server',
                /*y:<?= $cantSQL["nombSQL"] ?>,*/
                y:25,
                /*color:'<?=$color2?>'*/
                color:'green'
            }]
        }]
    });
</script>

<?php
require_once("configuracion/inicio.php");
require_once("plantillas/head.php");
require_once("procesos/manageCustomers.php");
require_once("plantillas/cabecera1.php");
?>


    <div class="hbox hbox-auto-xs hbox-auto-sm"  >
        <!-- main -->
        <div class="col">
            <!-- main header -->
            <div class="bg-light lter b-b wrapper-md">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <h1 class="m-n font-thin h3 text-black"> <a href="home.php"  > <i class="fa  fa-arrow-circle-o-left"></i> </a> Manage Customers</h1>
                    </div>
                    <div class="col-sm-6 text-right hidden-xs">

                        <div class="inline text-left">
                            <?php echo $selectClienteHtml?>
                        </div>
                    </div>

                </div>
            </div>
            <!-- / main header -->
            <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">


                <!-- stats -->
              <!--   <div class="row row-sm text-center">
                    <div class="col-md-4 col-md-offset-4 ">
                        <img src="<?= $GLOBALS["domain_root"] ?>/img/imgLogo.png" class="img img-responsive imgTituloHome">
                    </div>
                </div> -->
                <div class="row">

                    <div class="col-md-12">
                        <h6 class="main-tablehadsty" style="margin-top: 0px;">Migration Customers Details </h6>

                        <div class=" ">
                            <?php
                            $menuCIM = 1;
                            include_once("plantillas/menu.php");
                            ?>
                        </div>

                        <div class="bordeContenedor">
                            <div class="contenido">
                                <?php
                                require_once("plantillas/manageCustomers.php");
                                ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- / main -->
    </div>
    </div>
    </div>

<?php 
require_once("plantillas/foot.php");
<style>
    ul, ol {
            list-style:none;
    }

    .nav {
        width:140px; /*Le establecemos un ancho*/
        margin:0 auto; /*Centramos automaticamente*/
        margin:5px;
        margin-left:10px;
        float:left;	
        width:120px;
        padding:10px;
        border-radius:10px;
        text-align:center;
        cursor:pointer;
        background:#000;
        color:#fff;
    }
    
    .nav:hover {
        border-radius:10px 10px 0px 0px;
    }

    .nav li a {
        background-color:#000;
        color:#fff;
        text-decoration:none;
        display:block;
    }
    
    .nav li a:hover {
        background-color:#434343;
    }

    .nav li ul {
        margin-top: 0px;
        margin-left: -10px;
        display:none;
        position:absolute;
        min-width:160px;
    }

    .nav li:hover > ul {
        display:block;
    }
    
    .navSelected{
        background-color:#ffffff;
        color:#000000;
    }
    
    /*inicio submenu*/
    .nav li ul li{
        position: relative;
    }
    
    .nav li ul li ul{
        right: -140px;
        top: 0;
    }
    /*fin submenu*/
</style>


<div class="botones_m <?php if($opcionm1==1){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root1"] ?>/adminControl/home.php';" >
Home
</div>
<div class="botones_m <?php if($opcionm1==2){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root1"] ?>/adminControl/users/';" >
Users
</div>
<div class="botones_m <?php if($opcionm1==3){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root1"] ?>/adminControl/customers/';">
Customers
</div>
<div class="botones_m <?php if($opcionm1==4){ echo 'activado2';   } ?> " id="boton1" onclick="location.href='<?= $GLOBALS["domain_root1"] ?>/adminControl/resellers/';">
Resellers
</div>
<?php
// Verificar inicio sesion
if(!isset($_SESSION['usuario_autorizado'])) {
	echo '<script language="javascript" type="text/javascript">';
	echo '	alert("¡You must log in!");';
	echo '	location.href="'.$GLOBALS['domain_root1'].'/adminControl/index.php";';
	echo '</script>';
}

// Verificar tiempo de sesion
$time = time();
$tiempo_sesion = $time - $_SESSION['usuario_tiempo'];
if($tiempo_sesion > $TIEMPO_MAXIMO_SESION) {
	echo '<script language="javascript" type="text/javascript">';
	echo '	alert("¡You spent a lot of time idle!");';
	echo '	location.href="'.$GLOBALS['domain_root1'].'/adminControl/plantillas/salida.php";';
	echo '</script>';
} else {
	$_SESSION['usuario_tiempo'] = time();
}
?>
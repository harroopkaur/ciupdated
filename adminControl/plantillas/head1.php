<!DOCTYPE HTML>
<html>
    <head>
        <title>.: Licensing Assurance :.</title>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


        <!--Google Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!--Google Fonts-->
        
        <link href="<?=$GLOBALS['domain_root']?>/adminweb/css/style_login.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?=$GLOBALS['domain_root']?>/adminweb/css/style3.css" rel="stylesheet" type="text/css" media="all"/>        
        <link rel="stylesheet" href="<?= $GLOBALS["domain_root"] ?>/css/example.css" type="text/css" />
        <link rel="stylesheet" href="<?= $GLOBALS["domain_root"] ?>/css/jquery.countdown.timer.css" type="text/css" />
        <link href="<?=$GLOBALS['domain_root']?>/plugin-alert/css/alert.min.css" rel="stylesheet" />
        <link href="<?=$GLOBALS['domain_root']?>/plugin-alert/themes/default/theme.min.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?= $GLOBALS['domain_root'] ?>/css/iu-ligereza/jquery-ui-1.7.3.custom.css" />
        
        <!--inicio anteriormente esta en js_cabecera.php-->
        <style>
            body {
                background: #00B0F0 url(<?=$GLOBALS['domain_root']?>/imagenes/inicio/bg_body.jpg) top left no-repeat !important;
                padding:0px !important;		
            }

            div.box {font-size:108.3%;margin:2px 0 15px;padding:20px 15px 20px 65px;-moz-border-radius:6px;-webkit-border-radius:6px;border-radius:6px;/*behavior:url(http://www.yourinspirationweb.com/tf/bolder/wp-content/themes/bolder/PIE.htc);*/}
            div.success-box {background:url("<?=$GLOBALS['domain_root']?>/img/check.png") no-repeat 15px center #ebfab6;border:1px solid #bbcc5b;color:#599847;}

            div.error-box {
                background:#00B0F0 url("<?=$GLOBALS['domain_root']?>/img/error.png") no-repeat 15px center #fdd2d1;
                border: 1px solid #f6988f;
                color: #883333;
            }
        </style>
        <!--fin anteriormente esta en js_cabecera.php-->
        

        <script language="javascript" type="text/javascript" src="<?=$GLOBALS['domain_root']?>/js/funciones_generales.js"></script>
        <script type="text/javascript" src="<?=$GLOBALS['domain_root']?>/js/jquery-1.8.2.min.js"></script>
        <script src="<?=$GLOBALS['domain_root']?>/plugin-alert/js/alert.js"></script>   
        <script type="text/javascript" src="<?= $GLOBALS['domain_root'] ?>/js/calendario/calendario.js"></script>
         <script type="text/javascript" src="<?= $GLOBALS['domain_root'] ?>/js/calendario/calendario-spanish.js"></script>
    </head>
    <body>
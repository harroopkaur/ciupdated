<!DOCTYPE HTML>
<html>
    <head>
        <title>.:Smart Control Admin:.</title>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />-->
        <link rel="shortcut icon" href="<?= $GLOBALS["domain_root1"] ?>/img/Logo.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


        <!--Google Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!--Google Fonts-->
       
        <link href="<?=$GLOBALS['domain_root1']?>/adminControl/css/style_login.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="<?=$GLOBALS['domain_root1']?>/adminControl/css/style3.css" rel="stylesheet" type="text/css" media="all"/>        
        <!--<link rel="stylesheet" href="<?//= $GLOBALS["domain_root"] ?>/css/example.css" type="text/css" />-->
        <link rel="stylesheet" href="<?= $GLOBALS["domain_root1"] ?>/css/jquery.countdown.timer.css" type="text/css" />
        <link href="<?=$GLOBALS['domain_root1']?>/plugin-alert/css/alert.min.css" rel="stylesheet" />
        <link href="<?=$GLOBALS['domain_root1']?>/plugin-alert/themes/default/theme.min.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?= $GLOBALS['domain_root1'] ?>/css/iu-ligereza/jquery-ui-1.7.3.custom.css" />
        
        <!--inicio anteriormente esta en js_cabecera.php-->
        <style>
            body {
                background: #00B0F0 url(<?=$GLOBALS['domain_root1']?>/img/inicio/bg_body.jpg) top left no-repeat !important;
                padding:0px !important;		
            }

            div.box {font-size:108.3%;margin:2px 0 15px;padding:20px 15px 20px 65px;-moz-border-radius:6px;-webkit-border-radius:6px;border-radius:6px;/*behavior:url(http://www.yourinspirationweb.com/tf/bolder/wp-content/themes/bolder/PIE.htc);*/}
            div.success-box {background:url("<?=$GLOBALS['domain_root']?>/img/check.png") no-repeat 15px center #ebfab6;border:1px solid #bbcc5b;color:#599847;}

            div.error-box {
                background:#00B0F0 url("<?=$GLOBALS['domain_root']?>/img/error.png") no-repeat 15px center #fdd2d1;
                border: 1px solid #f6988f;
                color: #883333;
            }
        </style>
        <!--fin anteriormente esta en js_cabecera.php-->
        <link rel="stylesheet" href="<?= $GLOBALS['domain_root1'] ?>/css/jquery.countdown.timer.css" type="text/css" />
        

        <script language="javascript" type="text/javascript" src="<?=$GLOBALS['domain_root1']?>/js/funciones_generales.js"></script>
        <script type="text/javascript" src="<?=$GLOBALS['domain_root1']?>/js/jquery-1.8.2.min.js"></script>
        
        <script src="<?=$GLOBALS['domain_root1']?>/plugin-alert/js/alert.js"></script>   
        <script type="text/javascript" src="<?= $GLOBALS['domain_root1'] ?>/js/calendario/calendario.js"></script>
        <!--<script type="text/javascript" src="<?//= $GLOBALS['domain_root1'] ?>/js/calendario/calendario-spanish.js"></script>-->
        
        <!--<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <script src="<?//=$GLOBALS['domain_root']?>/js/datepicker-es.js"></script>-->
        
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/5/js/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script type="text/javascript" src="<?= $GLOBALS['domain_root1'] ?>/js/jquery.numeric.js"></script>
        <script src="<?=$GLOBALS['domain_root1']?>/js/jquery.pluginCountdown.min.js"></script>
        <script src="<?=$GLOBALS['domain_root1']?>/js/jquery.countdown.min.js"></script>
    </head>
    <body>
        <div class="fondo" id="fondo" style="display:none;">
            <img src="<?= $GLOBALS["domain_root1"] ?>/img/loading.gif" style="width:50px; height:50px; top:50%; margin-top:-25px; left:50%; margin-left:-25px; position:absolute;">
        </div>
		
        <div class="fondo1" id="fondo1"></div>

        <audio id="player" src="<?= $GLOBALS["domain_root1"] ?>/sonidos/ping.mp3"></audio>
        <div id="clock" class="hide"></div>
        
        <script>
            $(function () {
                var shortly = new Date(); 
                shortly.setSeconds(shortly.getSeconds() + <?= $general->tiempoLimite ?>); 
                $('#clock').countdown({until: shortly, onExpiry: liftOff}); 
            });
            
            function liftOff() { 
                $.alert.open('confirm', 'Confirm', '<?= $lib->lg0004 ?>', {'<?= $lib->lg0002 ?>' : '<?= $lib->lg0002 ?>', '<?= $lib->lg0003 ?>' : '<?= $lib->lg0003 ?>'}, function(button){
                    if(button === 'No'){
                        $("#fondo").show();
                        $.post("<?= $GLOBALS["domain_root1"] ?>/adminControl/ajax/finalizarSesion.php", "", function(){
                            location.href="<?= $GLOBALS["domain_root1"] ?>/adminControl/";
                        }, 'json')
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'}, function(){
                                location.href="<?= $GLOBALS["domain_root1"] ?>/adminControl/";
                            });
                        });
                    } else{
                        $("#fondo").show();
                        $.post("<?= $GLOBALS["domain_root1"] ?>/adminControl/ajax/renovarSesion.php", "", function(){
                            $("#fondo").hide();
                            $.alert.open('info', "Renewed session", {'Ok' : 'Ok'});
                            iniciarClock();
                        }, 'json')
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'});
                        });
                    }
                });
                document.getElementById('player').play();
            }
            
            function iniciarClock(){
                shortly = new Date(); 
                shortly.setSeconds(shortly.getSeconds() + <?= $general->tiempoLimite ?>); 
                $('#clock').countdown('option', {until: shortly}); 
            }
        </script>
if(data[0].resultado === false){
    $.alert.open('warning', 'Warning', "Tokens do not match", {'Ok' : 'Ok'}, function() {
        location.href = "<?= $GLOBALS['domain_root'] ?>/adminControl/";
        return false;
    });
}
if(data[0].sesion === "false"){
    $.alert.open('warning', 'Warning', data[0].mensaje, {'Ok' : 'Ok'}, function() {
        location.href = "<?= $GLOBALS['domain_root'] ?>/adminControl/";
        return false;
    });
}
<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_usuario.php");
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");

// Objetos
$usuario = new Usuario();

// Inicializar error
$error = 0;

if (isset($_POST['entrar']) && $_POST['entrar'] == 1) {
    if ($usuario->autenticar($_POST['login'], $_POST['contrasena'])) {
        $nuevo_middleware = new Middleware($GLOBALS["domain_root1"] . "/adminControl");
        ?>
        <script>
            localStorage.smartControlToken =  '<?= $nuevo_middleware->obtener_token() ?>';
            location.href = "home.php";
        </script>
        <?php
    } else {
        header('location: index.php?error=1');
    }
}
?>

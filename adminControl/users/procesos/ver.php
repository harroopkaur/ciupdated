<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware
//
// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_usuario.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");

// Objetos
$usuario = new Usuario();
$usuario2 = new Usuario();
$general = new General();

$id_usuario = 0;
if(isset($_GET["id"]) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_usuario = $_GET["id"];
}

$usuario->datos($id_usuario);
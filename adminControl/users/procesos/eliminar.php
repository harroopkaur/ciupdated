<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_usuario.php");

// Objetos
$usuario = new Usuario();
$general = new General();
$exito = 0;

if (isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false) {
    $exito = $usuario->desactivar($_POST['id']);
}
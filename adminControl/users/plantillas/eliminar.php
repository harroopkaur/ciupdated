<?php if($exito == true){  ?>
    <script type="text/javascript">
        $.alert.open('info', 'Successfully eliminated record', function(button) {
            if (button === 'ok'){
                location.href = '<?= $GLOBALS['domain_root1'] ?>/adminControl/users/';
            }
        });
    </script>
<?php 
}
else{
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Record has not been removed', function(button) {
            if (button === 'ok'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminControl/users/';
            }
        });
    </script>
<?php
}
?>
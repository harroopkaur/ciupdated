<?php if ($exito == true) { ?>
    <script type="text/javascript">
        $.alert.open('info', 'Successfully modified record', {'Ok': 'Ok'}, function(button) {
            if (button === 'Ok'){
                location.href = '<?= $GLOBALS['domain_root1'] ?>/adminControl/users/';
            }
        });
    </script>
<?php
}
?>
    
<form id="form1" name="form1" method="post" enctype="multipart/form-data">
    <input type="hidden" name="modificar" id="modificar" value="1" />
    <?php $validator->print_script(); ?>
    
    <input type="hidden" name="id" id="id" value="<?= $id_usuario ?>">
    <div class="error_prog"><font color="#FF0000"><?php if ($error == 3) {
            echo $usuario->error;
        } ?></font>
    </div>
    
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left: 15px;"><span class="bold"><span class="bold">Update User</span></legend>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="80" align="left" valign="top">Login:</th>
                <td align="left"><input name="login" id="login" type="text" value="<?= $usuario->login ?>" size="14" maxlength="250" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_login") ?><?php if ($error == 1) {
                            echo "Login already exists";
                        } ?></font>
                    </div>
                </td>
            </tr>
            
            <tr>
                <th width="80" align="left" valign="top">Name:</th>
                <td align="left"><input name="nombre" id="nombre" type="text" value="<?= $usuario->nombre ?>" size="30" maxlength="250" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_nombre") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="80" align="left" valign="top">Last Name:</th>
                <td align="left"><input name="apellido" id="apellido" type="text" value="<?= $usuario->apellido ?>" size="30" maxlength="250" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_apellido") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="80" align="left" valign="top">Email:</th>
                <td align="left"><input name="email" id="email" type="text" value="<?= $usuario->email ?>" size="30" maxlength="250" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?><?php if ($error == 2) {
                            echo "Email already exists";
                        } ?></font>
                    </div>
                </td>
            </tr>
            
            <tr>
                <td colspan="2" align="center"><input type="button" value="UPDATE" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </fieldset>
</form>
    
<script>
    $(document).ready(function(){
        $("#login").blur(function(){
            $.post("<?= $GLOBALS['domain_root1'] ?>/adminControl/users/ajax/verificarLogin.php", { login : $("#login").val(), id : $("#id").val(), token : localStorage.smartControlToken }, function(data){
                <?php require_once($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('warning', 'Login already exists', {'Ok': 'Ok'}, function(button) {
                        if (button === 'Ok'){
                            $("#login").val("");
                            $("#login").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'}, function() {
                });
            }); 
        });
        
        $("#email").blur(function(){
            $.post("<?= $GLOBALS['domain_root1'] ?>/adminControl/users/ajax/verificarCorreo.php", { email : $("#email").val(), id : $("#id").val(), token : localStorage.smartControlToken }, function(data){
                <?php require_once($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('alert', 'Email already exists', {'Ok': 'Ok'}, function(button) {
                        if (button === 'Ok'){
                            $("#email").val("");
                            $("#email").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'}, function() {
                });
            }); 
        });
    });
</script>
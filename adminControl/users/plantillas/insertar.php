 <?php if ($exito == true) { ?>
    <script type="text/javascript">
        $.alert.open('info', 'Record successfully inserted', {'Ok': 'Ok'}, function(button) {
            if (button === 'Ok'){
                location.href = '<?= $GLOBALS['domain_root1'] ?>/adminControl/users/';
            }
        });
    </script>
<?php
}
?>
    
<form id="form1" name="form1" method="post"  enctype="multipart/form-data">
    <input type="hidden" name="insertar" id="insertar" value="1" />
    
    <?php $validator->print_script(); ?>
    <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
        echo $usuario->error;
        } ?></font>
    </div>
    
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left: 15px;"><span class="bold">User Add</span></legend>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Login:</th>
                <td align="left"><input name="login" id="login" type="text" value="" size="14" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_login") ?><?php if ($error == 1) {
                            echo "Login already exists";
                        } ?></font>
                    </div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Password:</th>
                <td align="left"><input name="contrasena" id="contrasena" type="password" value="" size="14" maxlength="20"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_contrasena") ?><?php if ($error == 3) {
                            echo "6-Character Minimum password";
                        } ?></font>
                    </div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Name:</th>
                <td align="left"><input name="nombre" id="nombre" type="text" value="" size="30" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_nombre") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Last Name:</th>
                <td align="left"><input name="apellido" id="apellido" type="text" value="" size="30" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_apellido") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Email:</th>
                <td align="left"><input name="email" id="email" type="text" value="" size="30" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?><?php if ($error == 2) {
                            echo "Email already exists";
                        } ?></font>
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input type="button" value="INSERT" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </fieldset>
</form>
    
<script>
    $(document).ready(function(){
        $("#login").blur(function(){
            $.post("<?= $GLOBALS['domain_root1'] ?>/adminControl/users/ajax/verificarLogin.php", { login : $("#login").val(), token : localStorage.smartControlToken }, function(data){
                <?php require_once($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('warning', 'Login already exists', {'Ok': 'Ok'}, function(button) {
                        if (button === 'Ok'){
                            $("#login").val("");
                            $("#login").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'}, function() {
                });
            }); 
        });
        
        $("#email").blur(function(){
            $.post("<?= $GLOBALS['domain_root1'] ?>/adminControl/users/ajax/verificarCorreo.php", { email : $("#email").val(), token : localStorage.smartControlToken }, function(data){
                <?php require_once($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('warning', 'Email already exists', {'Ok': 'Ok'}, function(button) {
                        if (button === 'Ok'){
                            $("#email").val("");
                            $("#email").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'}, function() {
                });
            }); 
        });
    });
</script>
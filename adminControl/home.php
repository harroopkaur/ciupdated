<?php
require_once("../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/adminControl/librerias/idioma_en.php");

$lib = new idioma_web_adminControl_en();

require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/sesion.php");

//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
$general = new General();

include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/head.php");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/cabecera.php");
?>
<section class="contenedor">
    <div class="contenedorMenuAdmin">
        <?php include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/titulo.php"); ?>

        <br><br>

        <?php
        $opcionm1 = 1;
        require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/menu1.php");
        ?>
    </div>

    <div class="contenedorCuerpoAdmin">
        <div class="contenedorCentralInterno">
            <div class="contenedorMenuInterno"></div>

            <div class="bordeContenedor">
                <div class="contenido">
                    <?php include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/inicio.php"); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/foot.php");
?>
<?php
class empresas extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    //function insertar($correo, $telefono, $empresa, $pais, $nivelServicio) {
    function insertar($empresa, $correo, $codigo, $userName, $clave, $fechaInicio, $fechaFin) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO admin004 (nombre, correo, codigo, userName, clave, fechaInicio, fechaFin) '
            . 'VALUES (:empresa, :correo, :codigo, :userName, SHA2(:clave, 224), :fechaInicio, :fechaFin)');
            $sql->execute(array(':empresa'=>$empresa, ':correo'=>$correo, ':codigo'=>$codigo, ':userName'=>$userName, 
            ':clave'=>$clave, ':fechaInicio'=>$fechaInicio, ':fechaFin'=>$fechaFin));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar    
    function actualizar($id, $empresa, $correo, $userName, $fechaInicio, $fechaFin, $estado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE admin004 SET '
            . 'nombre = :nombre, correo = :correo, userName = :userName, fechaInicio = :fechaInicio, '
            . 'fechaFin = :fechaFin, status = :estado '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':nombre'=>$empresa, ':correo'=>$correo, ':userName'=>$userName, 
            ':fechaInicio'=>$fechaInicio, ':fechaFin'=>$fechaFin, ':estado'=>$estado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar    
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE admin004 SET status = 0 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar_clave($id, $clave) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE admin004 SET '
            . 'clave = SHA2(:clave, 224) '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':clave'=>$clave));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar_centralizador($id, $login, $clave, $fechaInicio, $fechaFin, $opcClave = true) {
        try{
            $this->conexion();
            $array = array(':id'=>$id, ':login'=>$login, ':fechaInicio'=>$fechaInicio, ':fechaFin'=>$fechaFin);
            $campo = "";
            
            if ($opcClave == true){
                $array[":clave"] = $clave;
                $campo = ", passCentralizador = SHA2(:clave, 512)";
            }
            
            $sql = $this->conn->prepare('UPDATE admin004 SET '
            . 'loginCentralizador = :login ' . $campo . ', fechaIniCentralizador = :fechaInicio, '
            . ' fechaFinCentralizador = :fechaFin '
            . 'WHERE id = :id');
            $sql->execute($array);
         
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function statusSmartControlDC($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT statusSmartControlDC 
                FROM admin004 
                WHERE id = :id');
            $sql->execute(array("id"=>":id"));
            $row = $sql->fetch();
            return $row["statusSmartControlDC"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("statusSmartControlDC"=>-1);
        }
    }
    
    function actualizarStatusSmartControlDC($id) {
        try{
            $this->conexion();
            $sql2 = $this->conn->prepare('UPDATE admin004 SET statusSmartControlDC = 1 WHERE id = :id');
            $sql2->execute(array(":id"=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }       
    
    // Obtener listado de todos los Usuarios   
    function datos($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT nombre, correo, codigo, userName, clave, 
            DATE_FORMAT(fechaInicio, "%m/%d/%Y") AS fechaInicio, DATE_FORMAT(fechaFin, "%m/%d/%Y") fechaFin, status 
            FROM admin004 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            return array();
        }
    }
    
    function datosCentralizador($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT nombre, codigo, loginCentralizador, passCentralizador, 
            DATE_FORMAT(fechaIniCentralizador, "%m/%d/%Y") AS fechaInicio, DATE_FORMAT(fechaFinCentralizador, "%m/%d/%Y") fechaFin 
            FROM admin004 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            return array();
        }
    }
    
    function ultimo_id(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(id) AS id FROM admin004');
            $sql->execute();
            $row = $sql->fetch();
            return $row['id'];
        }catch(PDOException $e){
            return -1;
        }
    }
    
    function listar_todo() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM admin004 WHERE status = 1 ORDER BY nombre');
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }

    // Obtener listado de todos los Usuarios paginados    
    function listar_todo_paginado($nombre, $estado, $inicio) {
        try{
            $array = array(":nombre"=>"%" . $nombre . "%");
            $where = "";
                        
            if ($estado >= 0){
                $array[":status"] = $estado;
                $where = " AND status = :status ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM admin004 '
                . 'WHERE nombre LIKE :nombre ' . $where . ' '
                . 'ORDER BY  nombre '
                . 'LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            //echo $e->getMessage();
            return array();
        }
    }

    // Contar el total de Usuarios
    function total($nombre, $estado) {
        try{
            $array = array(":nombre"=>"%" . $nombre . "%");
            $where = "";
                       
            if ($estado >= 0){
                $array[":status"] = $estado;
                $where = " AND status = :status ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad '
                . 'FROM admin004 '
                . 'WHERE nombre LIKE :nombre ' . $where);
            $sql->execute($array);
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }

    // Obtener listado de todos los Usuarios desactivados
    function listar_desactivados() {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM admin004 WHERE estado = 0 ORDER BY nombre');
            $sql->execute();
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Obtener listado de todos los Usuarios desactivados paginado
    function listar_desactivados_paginado($inicio, $fin) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM admin004 WHERE estado = 0 ORDER BY nombre LIMIT ' . $inicio . ', ' . $fin);
            $sql->execute();
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Contar el total de Usuarios desactivados
    function total_desactivados() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM admin004 WHERE estado = 0');
            $sql->execute();
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function empresa_existe($nombre, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM admin004 WHERE nombre = :nombre AND id != :id');
            $sql->execute(array(':nombre'=>$nombre, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row['nombre']) > 0){
                return true;
            }
            else{
                return false;
            }
            
        }catch(PDOException $e){
            return false;
        }
    }
    
    function correo_existe($correo, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM admin004 WHERE correo = :correo AND id != :id');
            $sql->execute(array(':correo'=>$correo, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row['correo']) > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }
    
    function listadoEmp($login, $pass){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.id,
                    clientes.empresa
                FROM clientes
                    INNER JOIN admin003 ON clientes.id = admin003.clientes
                    INNER JOIN admin004 ON admin003.resellers = admin004.id AND admin004.status = 1 
                    AND admin004.userName = :login AND admin004.clave = SHA2(:pass, 224)
                WHERE clientes.estado = 1
                GROUP BY clientes.id');
            $sql->execute(array(':login'=>$login, ':pass'=>$pass));
            return $sql->fetchAll();            
        }catch(PDOException $e){
            return array();
        }
    }
    
    function listadoEmp1($reseller){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.id,
                    clientes.empresa
                FROM clientes
                    INNER JOIN admin003 ON clientes.id = admin003.clientes
                    INNER JOIN admin004 ON admin003.resellers = admin004.id AND admin004.status = 1 
                    AND admin004.id = :reseller
                WHERE clientes.estado = 1
                GROUP BY clientes.id');
            $sql->execute(array(':reseller'=>$reseller));
            return $sql->fetchAll();            
        }catch(PDOException $e){
            return array();
        }
    }
    
    function login_existe($login, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM admin004 WHERE userName = :login AND id != :id');
            $sql->execute(array(':login'=>$login, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row['userName']) > 0){
                return true;
            }
            else{
                return false;
            }
            
        }catch(PDOException $e){
            return false;
        }
    }
    
    function codigo_existe($codigo, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM admin004 WHERE codigo = :codigo AND id != :id');
            $sql->execute(array(':codigo'=>$codigo, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row['codigo']) > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }
    
    public function generadorCodigo(){
        $codigo = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"), 0, 8);
        $codigoReal = substr($codigo, 0, 8);
        return $codigoReal;
    } 
    
    function idReseller($codigo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id FROM admin004 WHERE codigo = :codigo');
            $sql->execute(array(':codigo'=>$codigo));
            $row = $sql->fetch();
            $id = 0;
            if(count($row['id']) > 0){
                $id = $row['id'];
            }
            
            return $id;
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function eliminarRelacionReseller($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM admin003 WHERE clientes = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function agregarRelacionReseller($cliente, $reseller) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO admin003 (clientes, resellers) VALUES (:cliente, :reseller)');
            $sql->execute(array(':cliente'=>$cliente, ':reseller'=>$reseller));
            
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
}
?>
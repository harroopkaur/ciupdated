<?php
class Clientes extends General{
    ########################################  Atributos  ########################################
    private $listado = array();
    public  $error = NULL;
    public  $id;
    public  $nombre;
    public  $apellido;
    public  $correo;
    public  $telefono;
    public  $empresa;
    public  $pais;
    public  $login;
    public  $clave;
    public  $estado;
    public  $fecha_registro;
    public  $fecha1;
    public  $fecha2;
    public  $nivelServicio;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    //function insertar($correo, $telefono, $empresa, $pais, $nivelServicio) {
    function insertar($correo, $telefono, $empresa, $pais) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO clientes(correo, telefono, empresa, pais, estado, fecha_registro) '
            . 'VALUES (:correo, :telefono, :empresa, :pais, 1, CURDATE())');
            $sql->execute(array(':correo'=>$correo, ':telefono'=>$telefono, 
            ':empresa'=>$empresa, ':pais'=>$pais));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar    
    function actualizar($id, $correo, $telefono, $empresa, $pais) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'correo = :correo, telefono = :telefono, empresa = :empresa, pais = :pais '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':correo'=>$correo, ':telefono'=>$telefono, 
            ':empresa'=>$empresa, ':pais'=>$pais));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar
    function actualizar2($id, $estado, $fecha1, $fecha2) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'estado = :estado, fecha1 = :fecha1, fecha2 = :fecha2 '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':estado'=>$estado, ':fecha1'=>$fecha1, ':fecha2'=>$fecha2));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar clave    
    function actualizar_clave($id, $clave) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'clave = SHA2(:clave, 224) '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':clave'=>$clave));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarDashboard($id, $dashboard) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'dashboard = :dashboard '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':dashboard'=>$dashboard));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Autorizar    
    function autorizar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'estado = 1 '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar    
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET estado = 0 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarDashboard($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET dashboard = NULL WHERE id = :id');
            $sql->execute(array(':id'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Desactivar     
    function desactivar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE clientes SET '
            . 'estado = 0 '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Autenticar Usuario
    function autenticar($login, $clave, $company) {
        if (($login != "") && ($clave != "")) {
            try{
                $this->conexion();
                $sql = $this->conn->prepare('SELECT clientes.id, clientes.correo, clientes.empresa, clientes.estado,
                        admin004.id AS reseller, admin004.nombre AS nombReseller
                    FROM clientes
                        INNER JOIN admin003 ON clientes.id = admin003.clientes AND admin003.clientes = :company
                        INNER JOIN admin004 ON admin003.resellers = admin004.id AND
                        admin004.userName = :login AND admin004.clave = SHA2(:clave, 224) AND
                        admin004.status = 1
                    WHERE clientes.estado = 1 AND admin004.fechaFin >= CURDATE()
                    GROUP BY admin004.id');
                $sql->execute(array(':login'=>$login, ':clave'=>$clave, ':company'=>$company));
                $rusuario = $sql->fetch();
                
                if($rusuario['id'] > 0){
                    $_SESSION['client_autorizado'] = true;
                    $_SESSION['client_id']         = $rusuario['id'];
                    $_SESSION['client_email']      = $rusuario['correo'];
                    $_SESSION['client_estado']     = $rusuario['estado'];
                    $_SESSION['client_empresa']    = $rusuario['empresa'];
                    $_SESSION['client_tiempo']     = time();
                    $_SESSION['client_fecha']      = date('d/m/Y H:i:s');
                    $_SESSION['reseller']          = $rusuario['reseller'];
                    $_SESSION['nombReseller']      = $rusuario['nombReseller'];

                    return true;
                }
                else{
                    $this->error = 'Invalid user or password';
                    return false;
                }
            }catch(PDOException $e){
                $this->error = 'Invalid user or password'; //$e->getMessage();
                return false;
            }
        } else {
            $this->error = 'Empty user or password';
            return false;
        }
    }
    
    function autenticarReseller($reseller, $company) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.id, clientes.correo, clientes.empresa, clientes.estado,
                    admin004.id AS reseller, admin004.nombre AS nombReseller
                FROM clientes
                    INNER JOIN admin003 ON clientes.id = admin003.clientes AND admin003.clientes = :company
                    INNER JOIN admin004 ON admin003.resellers = admin004.id AND
                    admin004.id = :reseller AND
                    admin004.status = 1
                WHERE clientes.estado = 1 AND admin004.fechaFin >= CURDATE()
                GROUP BY admin004.id');
            $sql->execute(array(':reseller'=>$reseller, ':company'=>$company));
            $rusuario = $sql->fetch();

            if($rusuario['id'] > 0){
                $_SESSION['client_autorizado'] = true;
                $_SESSION['client_id']         = $rusuario['id'];
                $_SESSION['client_email']      = $rusuario['correo'];
                $_SESSION['client_estado']     = $rusuario['estado'];
                $_SESSION['client_empresa']    = $rusuario['empresa'];

                return true;
            }
            else{
                $this->error = 'Invalid user or password';
                return false;
            }
        }catch(PDOException $e){
            $this->error = 'Invalid user or password'; //$e->getMessage();
            return false;
        }
    }
    
    function autenticarResellerCIM($login, $clave) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT admin004.id AS reseller, admin004.nombre AS nombReseller
                FROM admin004                     
                WHERE admin004.userName = :login AND clave = SHA2(:clave, 224) AND admin004.status = 1
                AND admin004.fechaFin >= CURDATE()
                GROUP BY admin004.id');
            $sql->execute(array(':login'=>$login, ':clave'=>$clave));
            $rusuario = $sql->fetch();

            if($rusuario['reseller'] > 0){
                $_SESSION['client_autorizado'] = true;
                $_SESSION['client_id']         = 0;
                $_SESSION['client_email']      = "";
                $_SESSION['client_estado']     = "";
                $_SESSION['client_empresa']    = "";
                $_SESSION['client_tiempo']     = time();
                $_SESSION['client_fecha']      = date('d/m/Y H:i:s');
                $_SESSION['reseller']          = $rusuario['reseller'];
                $_SESSION['nombReseller']      = $rusuario['nombReseller'];

                return true;
            }
            else{
                $this->error = 'Invalid user or password';
                return false;
            }
        }catch(PDOException $e){
            $this->error = 'Invalid user or password'; //$e->getMessage();
            return false;
        }
    }
    
    function autenticarResellerCIM1($reseller, $cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientesCIM.id, clientesCIM.correo, clientesCIM.nombreEmpresa AS empresa, clientesCIM.status AS estado,
                    admin004.id AS reseller, admin004.nombre AS nombReseller
                FROM clientesCIM
                    INNER JOIN clientesResellerCIM ON clientesCIM.id = clientesResellerCIM.clienteCIM AND clientesResellerCIM.clienteCIM = :cliente
                    INNER JOIN admin004 ON clientesResellerCIM.reseller = admin004.id AND
                    admin004.id = :reseller AND
                    admin004.status = 1
                WHERE clientesCIM.status = 1
                GROUP BY admin004.id');
            $sql->execute(array(':reseller'=>$reseller, ':cliente'=>$cliente));
            $rusuario = $sql->fetch();

            if($rusuario['id'] > 0){
                $_SESSION['client_autorizado'] = true;
                $_SESSION['client_id']         = $rusuario['id'];
                $_SESSION['client_email']      = $rusuario['correo'];
                $_SESSION['client_estado']     = $rusuario['estado'];
                $_SESSION['client_empresa']    = $rusuario['empresa'];
                $_SESSION['reseller']          = $rusuario['reseller'];
                $_SESSION['nombReseller']      = $rusuario['nombReseller'];

                return true;
            }
            else{
                $this->error = 'Invalid user or password';
                return false;
            }
        }catch(PDOException $e){
            $this->error = 'Invalid user or password'; //$e->getMessage();
            return false;
        }
    }
    
    function autenticarResellerCentralizer($login, $clave) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT admin004.id AS reseller, admin004.nombre AS nombReseller
                FROM admin004                     
                WHERE admin004.loginCentralizador = :login AND passCentralizador = SHA2(:clave, 512) AND admin004.status = 1
                AND admin004.fechaFinCentralizador >= CURDATE()
                GROUP BY admin004.id');
            $sql->execute(array(':login'=>$login, ':clave'=>$clave));
            $rusuario = $sql->fetch();

            if($rusuario['reseller'] > 0){
                $_SESSION['client_autorizado'] = true;
                $_SESSION['client_id']         = 0;
                $_SESSION['client_email']      = "";
                $_SESSION['client_estado']     = "";
                $_SESSION['client_empresa']    = "";
                $_SESSION['client_tiempo']     = time();
                $_SESSION['client_fecha']      = date('d/m/Y H:i:s');
                $_SESSION['reseller']          = $rusuario['reseller'];
                $_SESSION['nombReseller']      = $rusuario['nombReseller'];

                return true;
            }
            else{
                $this->error = 'Invalid user or password';
                return false;
            }
        }catch(PDOException $e){
            $this->error = 'Invalid user or password'; //$e->getMessage();
            return false;
        }
    }
    
    function autenticarClientCentralizer($login, $clave) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, nombreEmpresa, correo, status
                FROM clientesReseller                     
                WHERE usuario = :login AND password = SHA2(:clave, 224) AND status = 1');
            $sql->execute(array(':login'=>$login, ':clave'=>$clave));
            $rusuario = $sql->fetch();

            if($rusuario['id'] > 0){
                $_SESSION['client_autorizado'] = true;
                $_SESSION['client_id']         = $rusuario['id'];
                $_SESSION['client_email']      = $rusuario['correo'];
                $_SESSION['client_estado']     = $rusuario['status'];
                $_SESSION['client_empresa']    = $rusuario['nombreEmpresa'];
                $_SESSION['client_tiempo']     = time();
                $_SESSION['client_fecha']      = date('d/m/Y H:i:s');

                return true;
            }
            else{
                $this->error = 'Invalid user or password';
                return false;
            }
        }catch(PDOException $e){
            $this->error = 'Invalid user or password'; //$e->getMessage();
            return false;
        }
    }
    
    function autenticarEmpleado($correo, $clave) {
        if (($correo != "") && ($clave != "")) {
            try{
                $this->conexion();
                $sql = $this->conn->prepare('SELECT empleados.id, empleados.cliente, empleados.nombre, empleados.apellido, empleados.correo, clientes.empresa, empleados.estado, nivelServicio.descripcion
                    FROM empleados
                        INNER JOIN clientes ON empleados.cliente = clientes.id AND clientes.estado = 1
                        INNER JOIN nivelServicio ON clientes.nivelServicio = nivelServicio.id AND nivelServicio.estado = 1
                    WHERE empleados.userName = :correo AND empleados.clave = :clave AND empleados.estado = 1');
                $sql->execute(array(':correo'=>$correo, ':clave'=>$clave));
                $rusuario = $sql->fetch();
                
                if($rusuario['nombre'] > 0){
                    $_SESSION['client_autorizado'] = true;
                    $_SESSION['client_id']         = $rusuario['cliente'];
                    $_SESSION['client_empleado']   = $rusuario['id'];
                    $_SESSION['client_nombre']     = $rusuario['nombre'];
                    $_SESSION['client_apellido']   = $rusuario['apellido'];
                    $_SESSION['client_email']      = $rusuario['correo'];
                    $_SESSION['client_estado']     = $rusuario['estado'];
                    $_SESSION['client_empresa']    = $rusuario['empresa'];
                    $_SESSION['client_tiempo']     = time();
                    $_SESSION['client_fecha']      = date('d/m/Y H:i:s');
                    $_SESSION['nivelServicio']     = $rusuario['descripcion'];

                    return true;
                }
                else{
                    $this->error = 'Usuario o Contrase&ntilde;a inv&aacute;lidos';
                    return false;
                }
            }catch(PDOException $e){
                $this->error = 'Usuario o Contrase&ntilde;a inv&aacute;lidos'; //$e->getMessage();
                return false;
            }
        } else {
            $this->error = 'Usuario o Contrase&ntilde;a vac&iacute;os';
            return false;
        }
    }

    // Imprimir datos de Session
    function session() {
        $cadena = "";
        $cadena .= $_SESSION['client_nombre'] . " " . $_SESSION['client_apellido'] . " - Bienvenido";

        return $cadena;
    }

    // Obtener listado de todos los Usuarios   
    function datos($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            if($usuario['id'] > 0){
                $this->id             = $usuario['id'];
                $this->nombre         = $usuario['nombre'];
                $this->apellido       = $usuario['apellido'];
                $this->correo         = $usuario['correo'];
                $this->telefono       = $usuario['telefono'];
                $this->pais           = $usuario['pais'];
                $this->empresa        = $usuario['empresa'];
                $this->login          = $usuario['userName'];
                $this->clave          = $usuario['clave'];
                $this->estado         = $usuario['estado'];
                $this->fecha_registro = $usuario['fecha_registro'];
                $this->fecha1         = $usuario['fecha1'];
                $this->fecha2         = $usuario['fecha2'];
                $this->nivelServicio  = $usuario['nivelServicio'];
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }
    
    function ultimo_id(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(id) AS id FROM clientes');
            $sql->execute();
            $row = $sql->fetch();
            return $row['id'];
        }catch(PDOException $e){
            return -1;
        }
    }
    
    function listar_todo() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE estado = 1 ORDER BY empresa');
            $sql->execute();
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }

    // Obtener listado de todos los Usuarios paginados    
    function listar_todo_paginado($empresa, $email, $fecha, $estado, $inicio) {
        try{
            $array = array(":empresa"=>"%" . $empresa . "%", ":email"=>"%" . $email . "%");
            $where = "";
            
            if ($fecha != ""){
                $array[":fecha"] = $this->reordenarFecha($fecha, "/", "-");
                $where = " AND fecha_registro = :fecha ";
            }
            
            if ($estado >= 0){
                $array[":estado"] = $estado;
                $where = " AND estado = :estado ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM clientes '
                . 'WHERE empresa LIKE :empresa AND correo LIKE :email ' . $where . ''
                . 'ORDER BY  empresa '
                . 'LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute($array);
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Contar el total de Usuarios
    function total($empresa, $email, $fecha, $estado) {
        try{
            $array = array(":empresa"=>"%" . $empresa . "%", ":email"=>"%" . $email . "%");
            $where = "";
            
            if ($fecha != ""){
                $array[":fecha"] = $this->reordenarFecha($fecha, "/", "-");
                $where = " AND fecha_registro = :fecha ";
            }
           
            if ($estado >= 0){
                $array[":estado"] = $estado;
                $where = " AND estado = :estado ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad '
                . 'FROM clientes '
                . 'WHERE empresa LIKE :empresa AND correo LIKE :email ' . $where);
            $sql->execute($array);
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }

    // Obtener listado de todos los Usuarios desactivados
    function listar_desactivados() {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE estado = 0 ORDER BY empresa');
            $sql->execute();
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Obtener listado de todos los Usuarios desactivados paginado
    function listar_desactivados_paginado($inicio, $fin) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE estado = 0 ORDER BY empresa LIMIT ' . $inicio . ', ' . $fin);
            $sql->execute();
            $this->listado = $sql->fetchAll();
            return $this->listado;
        }catch(PDOException $e){
            return $this->listado;
        }
    }

    // Contar el total de Usuarios desactivados
    function total_desactivados() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM clientes WHERE estado = 0');
            $sql->execute();
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            return 0;
        }
    }

    // Verificar si e-mail ya existe
    function email_existe($email, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE correo = :email AND id != :id');
            $sql->execute(array(':email'=>$email, ':id'=>$id));
            $row = $sql->fetch();
            if($row['id'] > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    function login_existe($login, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE userName = :login AND id != :id');
            $sql->execute(array(':login'=>$login, ':id'=>$id));
            $row = $sql->fetch();
            if($row['id'] > 0){
                return true;
            }
            else{
                return false;
            }
            
        }catch(PDOException $e){
            return false;
        }
    }
    
    function empresa_existe($nombre, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM clientes WHERE empresa = :nombre AND id != :id');
            $sql->execute(array(':nombre'=>$nombre, ':id'=>$id));
            $row = $sql->fetch();
            if($row['id'] > 0){
                return true;
            }
            else{
                return false;
            }
            
        }catch(PDOException $e){
            return false;
        }
    }

    // Recuperar Contraseña de un Usuario
    function recuperar_contrasena($login) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, clave, nombre, apellido, correo FROM clientes WHERE userName = :login');
            $sql->execute(array(':login'=>$login));
            $row = $sql->fetch();
            if($row['id'] > 0){
                $this->nombre   = $usuario['nombre'];
                $this->apellido = $usuario['apellido'];
                $this->correo   = $usuario['correo'];
                $this->clave    = $usuario['clave'];
                return true;
            }
            else{
                return false;
            }
            
        }catch(PDOException $e){
            return false;
        }
    }
    
    public function generadorCodigo(){
        $codigo = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"), 0, 16);
        $codigoReal = substr($codigo, 0, 4) . "-" . substr($codigo, 4, 4) . "-" . substr($codigo, 8, 4) . "-" . substr($codigo, 12, 4);
        return $codigoReal;
    } 
    
    function idCliente($serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.id
                FROM clientes
                     INNER JOIN admin005 ON clientes.id = admin005.empresa AND admin005.serialHHD = :serial 
                AND clientes.estado = 1');
            $sql->execute(array(':serial'=>$serial));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            return 0;
        }
    }
}
?>
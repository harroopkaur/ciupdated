<?php
class nivelServicio extends General {
    public  $error = null;
        
    function listar() {
        try{
            $this->conexion();
            $query = "SELECT *
            FROM nivelServicio
            ORDER BY descripcion";
            $sql = $this->conn->prepare($query);
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function nivelServicioEspecifico($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM nivelServicio
            WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            $row = $sql->fetch();
            return $row;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('id'=>"");
        }
    }
}
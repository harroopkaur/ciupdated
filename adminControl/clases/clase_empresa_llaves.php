<?php
class clase_empresa_llaves extends General{ 
    public $error = "";
    
    function insertar($empresa, $fechaIni, $fechaFin, $serial, $cantidad, $nivelServicio) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO admin005 (empresa, fechaIni, fechaFin, fechaCreacion, '
            . 'serial, cantidad, nivelServicio) '
            . 'VALUES (:empresa, :fechaIni, :fechaFin, NOW(), :serial, :cantidad, :nivelServicio)');
            $sql->execute(array(':empresa'=>$empresa, ':fechaIni'=>$fechaIni, 
            ':fechaFin'=>$fechaFin, ':serial'=>$serial, ':cantidad'=>$cantidad, ':nivelServicio'=>$nivelServicio));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $fechaIni, $fechaFin, $cantidad, $nivelServicio, $status) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE admin005 SET '
            . 'fechaIni = :fechaIni, fechaFin = :fechaFin, cantidad = :cantidad, nivelServicio = :nivelServicio, status = :status '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':fechaIni'=>$fechaIni, ':fechaFin'=>$fechaFin, ':cantidad'=>$cantidad, 
            ':nivelServicio'=>$nivelServicio, ':status'=>$status));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE admin005 SET '
            . 'status = 0 '
            . 'WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function datos($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT admin005.empresa, '
                    . 'clientes.empresa AS nombreEmpresa, '
                    . 'admin005.fechaIni, '
                    . 'admin005.fechaFin, '
                    . 'admin005.serial, '
                    . 'admin005.cantidad, '
                    . 'admin005.nivelServicio, '
                    . 'admin005.status '
                . 'FROM admin005 '
                    . 'INNER JOIN clientes ON admin005.empresa = clientes.id '
                . 'WHERE admin005.id = :id');
            $sql->execute(array(':id'=>$id));
            $row = $sql->fetch();
            return $row;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('empresa'=>"", 'fechaIni'=>"0000-00-00", 'fechaFin'=>"0000-00-00", 'fechaCreacion'=>"0000-00-00", 
            'serial'=>"", 'status'=>0);
        }
    }
    
    function ultimo_id(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(id) AS id FROM admin005');
            $sql->execute();
            $row = $sql->fetch();
            return $row['id'];
        }catch(PDOException $e){
            return -1;
        }
    }
    
    function listar_todo_paginado($empresa, $pagina) {
        try{
            $inicio = $pagina - 1; 
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM admin005
            WHERE empresa = :empresa
            ORDER BY fechaIni ASC 
            LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute(array(':empresa'=>$empresa));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function total($empresa) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM admin005 WHERE empresa = :empresa');
            $sql->execute(array(':empresa'=>$empresa));
            $row = $sql->fetch();
            return $row['cantidad'];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function serial_existe($serial, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM admin005 WHERE serial = :serial AND id != :id');
            $sql->execute(array(':serial'=>$serial, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row['serial']) > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            return false;
        }
    }
    
    function idCliente($serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT empresa FROM admin005 WHERE serialHHD = :serial AND status = 2 AND
                fechaIni <= NOW() AND fechaFin >= NOW()');
            $sql->execute(array(':serial'=>$serial));
            $row = $sql->fetch();
            $id = 0;
            if(count($row['empresa']) > 0){
                $id = $row['empresa'];
            }
            
            return $id;
        }catch(PDOException $e){
            return 0;
        }
    }
}
<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/adminControl/librerias/idioma_en.php");

$lib = new idioma_web_adminControl_en();

require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/sesion.php");
require_once($GLOBALS["app_root1"] . "/adminControl/customers/procesos/vigencia.php");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/head.php"); 
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/cabecera.php");
?>

<section class="contenedor">
    <div class="contenedorMenuAdmin">
        <?php include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/titulo.php"); ?>

        <br><br>

        <?php
        $opcionm1 = 3;
        include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/menu1.php");
        ?>
    </div>
    
    <div class="contenedorCuerpoAdmin">
        <div class="contenedorCentralInterno">
            <div class="contenedorMenuInterno">
                <?php 
                $menuClientes = 3;
                include_once($GLOBALS["app_root1"] . "/adminControl/customers/plantillas/menu.php");
                ?>
            </div>
            
            <div class="bordeContenedor">
                <div class="contenido">
                    <?php include_once($GLOBALS["app_root1"] . "/adminControl/customers/plantillas/vigencia.php") ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/foot.php");
?>
<?php if($exito == true){  ?>
    <script type="text/javascript">
        $.alert.open('info', 'Successfully cancelled registration', function(button) {
            if (button === 'ok'){
                location.href = '<?= $GLOBALS['domain_root1'] ?>/adminControl/customers/';
            }
        });
    </script>
<?php 
}
else{
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Registro no ha sido anulado', function(button) {
            if (button === 'ok'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminControl/customers/';
            }
        });
    </script>
<?php
}
?>
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Customer Data</span></legend>
    
    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
        <tr>
            <th width="150" align="left" valign="middle">Company:</th>
            <td align="left" valign="middle"><?= $clientes->empresa ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">Email:</th>
            <td align="left" valign="middle"><?= $clientes->correo ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">Phone:</th>
            <td align="left" valign="middle"><?= $clientes->telefono ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">Country:</th>
            <td align="left" valign="middle"><?= $pais->nombre ?></td>
        </tr>
        
        <!--<tr>
            <th width="150" align="left" valign="middle">Service Level:</th>
            <td align="left" valign="middle"><?//= $nivel ?></td>
        </tr>-->
        
        <tr>
            <th width="150" align="left" valign="middle">Status:</th>
            <td align="left" valign="middle"><?php if($clientes->estado=='1'){ echo'Authorized';  } else{ echo'Not Authorized';  }  ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">Fecha desde:</th>
            <td align="left" valign="middle"><?= $general->muestrafecha($clientes->fecha1) ?></td>
        </tr>
        
        <tr>
            <th width="150" align="left" valign="middle">Fecha hasta:</th>
            <td align="left" valign="middle"><?= $general->muestrafecha($clientes->fecha2) ?></td>
        </tr>
       
        <tr>
            <th width="150" align="left" valign="middle">Fecha registro:</th>
            <td align="left" valign="middle"><?= $general->muestrafecha($clientes->fecha_registro) ?></td>
        </tr>
    </table>
</fieldset>
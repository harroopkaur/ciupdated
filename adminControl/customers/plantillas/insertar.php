<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Record successfully inserted', {'Ok': 'Ok'}, function(button) {
            if (button === 'Ok'){
                location.href = '<?= $GLOBALS['domain_root1'] ?>/adminControl/customers/validity.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Failed to add record', {'Ok': 'Ok'}, function(button) {
            if (button === 'Ok'){
                location.href = '<?= $GLOBALS['domain_root1'] ?>/adminControl/customers/';
            }
        });
    </script>
    <?php
}
?>                                
                               
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Customer Data</span></legend>
    <form id="form1" name="form1" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <?php $validator->print_script(); ?>
        
        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
             echo $clientes->error;
        } ?></font>
        </div>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Company:</th>
                <td align="left"><input name="empresa" id="empresa" type="text" value="" size="30" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_empresa") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Email:</th>
                <td align="left"><input name="email" id="email" type="text" value="" size="30" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div></td>
            </tr>
                    
            <tr>
                <th width="90" align="left" valign="top">Phone:</th>
                <td align="left"><input name="telefono" id="telefono" type="text" value="" size="30" maxlength="250"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_telefono") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Country:</th>
                <td align="left">
                    <select name="pais" id="pais">
                        <option value="" selected>--Select--</option>
                        <?php
                        $lista_p=$paises->listar_todo();
                        if($lista_p){
                            foreach($lista_p as $reg_p){
                            ?>
                                <option value="<?= $reg_p["id"] ?>"><?= $reg_p["nombre"] ?></option>
                            <?php
                            }
                        }
                        ?>
                    </select>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_pais") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <td colspan="2" align="center"><input type="button" value="INSERT" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>
    
<script>
    $(document).ready(function(){
       $("#empresa").blur(function(){
            $.post("ajax/verificarEmpresa.php", { empresa : $("#empresa").val(), token : localStorage.smartControlToken }, function(data){
                <?php require_once($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('warning', 'The company already exists', {'Ok': 'Ok'}, function(button) {
                        if (button === 'Ok'){
                            $("#empresa").val("");
                            $("#empresa").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'}, function() {
                });
            });
        });
        
        $("#email").blur(function(){
            $.post("ajax/verificarCorreo.php", { email : $("#email").val(), token : localStorage.smartControlToken }, function(data){
                <?php require_once($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('warning', 'Email already exists', {'Ok': 'Ok'}, function(button) {
                        if (button === 'Ok'){
                            $("#email").val("");
                            $("#email").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'}, function() {
                });
            });
        });
    });
</script>
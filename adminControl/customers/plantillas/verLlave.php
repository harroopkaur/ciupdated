<form id="form1" name="form1" method="post"  enctype="multipart/form-data">
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Datos de la Llave del Producto</span></legend>

            <input type="hidden" name="insertar" id="insertar" value="1" />
            <input type="hidden" name="id" id="id" value="<?= $id_user ?>" />
            <input type="hidden" name="idLlave" id="idLlave" value="<?= $idLlave ?>" />
            <input type="hidden" name="modif" id="modif" value="<?= $autorizado ?>">
            <?php $validator->print_script(); ?>

            <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                 echo $llaves->error;
            } ?></font>
            </div>
            <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
                <tr>
                    <th width="150" align="left" valign="top">Inicio Acceso:</th>
                    <td colspan="2" align="left"><input name="fechaIni" id="fechaIni" type="text" value="<?= $general->reordenarFecha($row["fechaIni"], "-", "/") ?>" size="30" maxlength="20" readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaIni") ?></font></div></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Fin Acceso:</th>
                    <td colspan="2" align="left"><input name="fechaFin" id="fechaFin" type="text" value="<?= $general->reordenarFecha($row["fechaFin"], "-", "/") ?>" size="30" maxlength="20" readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaFin") ?></font></div></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Serial:</th>
                    <td width="100" align="left"><input name="serial" id="serial" type="text" value="<?= $row["serial"] ?>" size="30" maxlength="20"  readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_serial") ?></font></div></td>
                    <td align="left"></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Cantidad Usuarios:</th>
                    <td colspan="2" align="left"><input name="cantidad" id="cantidad" type="text" size="30" maxlength="5"  value="<?= $row["cantidad"] ?>" readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_cantidad") ?></font></div></td>
                </tr>
                
                <tr>
                    <th width="150" align="left" valign="top">Service Nivel:</th>
                    <td colspan="2" align="left">
                        <select id="nivelServicio" name="nivelServicio" disabled>
                            <option value="">--Select--</option>
                            <option value="250" <?php if($row["nivelServicio"] == "250"){ echo "selected='selected'"; } ?>>250</option>
                            <option value="500" <?php if($row["nivelServicio"] == "500"){ echo "selected='selected'"; } ?>>500</option>
                        </select>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_cantidad") ?></font></div></td>
                </tr>
                
                <tr>
                    <th width="80" align="left" valign="top">Autorizado:</th>
                    <td align="left">
                        <input type="checkbox" id="autorizado" name="autorizado" <?php if($row["status"] == 1){ echo 'checked="checked" disabled="disabled"'; } ?>>
                    </td>
                </tr>
            </table>
    </fieldset>
</form>  
    
<script>
    $(document).ready(function(){
        
        if (!$("#cantidad").prop("readonly")){
            $("#cantidad").numeric(false);
            $("#fechaIni").datepicker();
            $("#fechaFin").datepicker();

            $("#cantidad").click(function(){
               $("#cantidad").val(""); 
            });
        }
        
        $("#autorizado").click(function(){
            if($("#autorizado").prop("disabled")){
                return false;
            }
            
            if($("#autorizado").prop("checked")){
                $("#modif").val(1);
            } else{
                $("#modif").val(0);
            }
        });
    });
</script>
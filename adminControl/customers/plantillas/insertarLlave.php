<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('alert', 'Record successfully inserted', {'Ok': 'Ok'}, function(button) {
            if (button === 'Ok'){
                location.href = '<?= $GLOBALS['domain_root1'] ?>/adminControl/customers/keys.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
} else if ($agregar == 1 && $exito == 0) {
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Failed to add record', {'Ok': 'Ok'}, function(button) {
            if (button === 'Ok'){
                location.href = '<?= $GLOBALS['domain_root'] ?>/adminControl/customers/keys.php?id=<?= $id_user ?>';
            }
        });
    </script>
    <?php
} 
?>                                

<form id="form1" name="form1" method="post"  enctype="multipart/form-data">
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left:15px;"><span class="bold">Product Key Data</span></legend>

            <input type="hidden" name="insertar" id="insertar" value="1" />
            <input type="hidden" name="id" id="id" value="<?= $id_user ?>" />
            <?php $validator->print_script(); ?>

            <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                 echo $llaves->error;
            } ?></font>
            </div>
            <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
                <tr>
                    <th width="150" align="left" valign="top">Access Start:</th>
                    <td colspan="2" align="left"><input name="fechaIni" id="fechaIni" type="text" value="" size="30" maxlength="20" readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaIni") ?></font></div></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Access End:</th>
                    <td colspan="2" align="left"><input name="fechaFin" id="fechaFin" type="text" value="" size="30" maxlength="20" readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaFin") ?></font></div></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Serial:</th>
                    <td width="100" align="left"><input name="serial" id="serial" type="text" value="" size="30" maxlength="20"  readonly/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_serial") ?></font></div></td>
                    <td align="left"><input name="generar" type="button" id="generar" value="Generar" class="boton" /></td>
                </tr>

                <tr>
                    <th width="150" align="left" valign="top">Number of users:</th>
                    <td colspan="2" align="left"><input name="cantidad" id="cantidad" type="text" size="30" maxlength="5"  value="0"/>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_cantidad") ?></font></div></td>
                </tr>
                
                <tr>
                    <th width="150" align="left" valign="top">Service Nivel:</th>
                    <td colspan="2" align="left">
                        <select id="nivelServicio" name="nivelServicio">
                            <option value="">--Select--</option>
                            <option value="250">250</option>
                            <option value="500">500</option>
                        </select>
                        <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_nivel") ?></font></div></td>
                </tr>
            </table>
    </fieldset>

    <br style="clear:both;">
    <div style="width:77px; margin:0 auto;">
        <input style="margin:0 auto; margin-top:10px;" type="button" value="INSERT" onclick="validate();" class="boton" />
    </div>
</form>  
    
<script>
    $(document).ready(function(){
        $("#cantidad").numeric(false);
        $("#fechaIni").datepicker();
        $("#fechaFin").datepicker();
        
        $("#cantidad").click(function(){
           $("#cantidad").val(""); 
        });
        
        $("#generar").click(function(){
           $.post("ajax/generarSerial.php", { id : $("#id").val(), token : localStorage.smartControlToken }, function(data){
                if(data[0].resultado === true){
                    $("#serial").val(data[0].llaveGenerada);
                }
                else{
                    $.alert.open('warning', "Could not generate the serial please try again");
                }
            }, "json")
            .fail(function(jqXHR){
                $.alert.open('error', "Error: " + jqXHR.status);
            }); 
        });
    });
</script>
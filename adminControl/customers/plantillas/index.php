<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="delete.php">
    <input type="hidden" id="id" name="id">
</form>

<table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="middle" ><input type="text" id="filtroEmpresa" name="filtroEmpresa" style="width:120px;" value="<?= $empresa ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroEmail" name="filtroEmail" style="width:120px;" value="<?= $email ?>"></th>
            <th  align="center" valign="middle" ><input type="text" id="filtroFecha" name="filtroFecha" style="width:120px;" value="<?= $fecha ?>" readonly></th>
            <th  align="center" valign="middle" >
                <select id="filtroEstado" name="filtroEstado" style="width:120px;">
                    <option value="-1" <?php if($estado == -1){ echo "selected='selected'"; }?>>All</option>
                    <option value="1" <?php if($estado == 1){ echo "selected='selected'"; }?>>Authorized</option>
                    <option value="0" <?php if($estado == 0){ echo "selected='selected'"; }?>>Not Authorized</option>
                </select>
            </th>
            <th  align="center" valign="middle" class="til"><div id="buscar" class="botonBuscar pointer">Search</div></th>
            <th  align="center" valign="middle" class="til" ><div class="botonBuscar pointer" >Export</div></th><!--onclick="location.href='reports/customers.php';"-->
        </tr>
        <tr  bgcolor="#333333" style="color:#FFF;" class="til">
            <th  align="center" valign="midle" ><strong class="til">Company</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Email</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Date of Registration</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Status</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Update</strong></th>
            <th  align="center" valign="middle" class="til" ><strong>Delete</strong></th>
        </tr> 
    </thead>
    
    <tbody id="bodyTable">
        <?php foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td align="left"><a href="view.php?id=<?= $registro['id'] ?>">
                     <?= $registro['empresa'] ?>
                    </a></td>
                <td  align="left"><?= $registro['correo'] ?></td>
                <td  align="center"><?= $general->muestrafecha($registro['fecha_registro']) ?></td>
                <td  align="center">
                    <?php if($registro['estado'] == 1){ echo 'Authorized'; }else{ echo 'Not authorized'; } ?>
                </td>
                <td  align="center">
                    <a href="update.php?id=<?= $registro['id'] ?>"><img src="<?= $GLOBALS["domain_root1"] ?>/img/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Update" title="Update" /></a>
                </td>
                <td align="center">
                    <a href="#" onclick="eliminar(<?= $registro['id'] ?>)"><img src="<?= $GLOBALS["domain_root1"] ?>/img/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Delete" title="Delete" /></a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<div id="paginador" style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>

<?php 
if($count == 0) { ?>
    <div style="text-align:center; width:90%; margin:0 auto">No customers</div>
<?php 
}
?>

<script>
    $(document).ready(function(){
        $("#filtroFecha").datepicker();
        
        $("#filtroEmpresa").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroEmail").keyup(function(e){
            if(e.keyCode === 13){
                buscarData();
            }
        });
        
        $("#filtroFecha").click(function(){
            $("#filtroFecha").val("");
        });
        
        $("#buscar").click(function(){
            buscarData();
        }); 
    });
    
    function buscarData(){
        $.post("ajax/clientes.php", { empresa : $("#filtroEmpresa").val(), email : $("#filtroEmail").val(), 
        fecha : $("#filtroFecha").val(), estado : $("#filtroEstado").val(), pagina : 0, token : localStorage.smartControlToken }, function(data){
            if(data[0].resultado === false){
                location.href = "<?= $GLOBALS['domain_root1'] ?>/adminControl";
                return false;
            }
            $("#bodyTable").empty();
            $("#paginador").empty();
            $("#bodyTable").append(data[0].tabla);
            $("#paginador").append(data[0].paginador);
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'}, function() {
            });
        });
    }
    
    function eliminar(id){
        $.alert.open('confirm', 'You want to unregister', {'Yes': 'Yes', No: 'No'}, function(button) {
            if (button === 'Yes'){
                $("#id").val(id);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>
<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

//clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresa_llaves.php");

// Objetos
$llaves = new clase_empresa_llaves();
$general = new General();

//procesos
$exito = false;
if (isset($_POST['idLlave']) && filter_var($_POST['idLlave'], FILTER_VALIDATE_INT) !== false) {
    $exito = $llaves->eliminar($_POST['idLlave']);
    $id_user = $_POST["id"];
}
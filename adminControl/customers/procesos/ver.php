<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_clientes.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_pais.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_nivel_servicio.php");

// Objetos
$clientes = new Clientes();
$clientes2 = new Clientes();
$general = new General();
$pais = new Pais();
$nivelServicio = new nivelServicio();

//procesos
$id_user = 0;
$nivel = "";

if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id'];
    $clientes->datos($id_user);
    $pais->datos($clientes->pais);
    $row = $nivelServicio->nivelServicioEspecifico($clientes->nivelServicio);
    $nivel = $row["descripcion"];
}
else{
    $clientes->datos($id_user);
    $pais->datos($id_user);
}
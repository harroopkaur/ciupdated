<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_clientes.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_pais.php");

// Objetos
$clientes = new Clientes();
$validator = new validator("form1");
$paises = new Pais();
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;

if (isset($_POST['insertar']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false && filter_var($_POST['pais'], FILTER_VALIDATE_INT) !== false) {
    $agregar = 1;
    // Validaciones
    
    if ($error == 0) {
        if ($clientes->insertar($general->get_escape($_POST['email']), $general->get_escape($_POST['telefono']), $general->get_escape($_POST['empresa']), 
        $_POST['pais'])) {
            $id_user = $clientes->ultimo_id();
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_empresa", "empresa", "Required", 0);
$validator->create_message("msj_email", "email", "Invalid Email", 3);
$validator->create_message("msj_telefono", "telefono", "Required", 0);
$validator->create_message("msj_pais", "pais", "Required", 0);
<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_clientes.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_pais.php");

// Objetos
$clientes = new Clientes();
$clientes2 = new Clientes();
$general = new General();
$validator = new validator("form1");
$pais = new Pais();

$lista_p = $pais->listar_todo();

//procesos
$error = 0;
$exito = 0;

$id_user = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id']; 
}

$clientes->datos($id_user);

if (isset($_POST['insertar']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false && filter_var($_POST['pais'], FILTER_VALIDATE_INT) !== false
   && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false) {    
    if ($error == 0) {
        if ($clientes2->actualizar($_POST['id'], $general->get_escape($_POST['email']), $general->get_escape($_POST['telefono']), 
        $general->get_escape($_POST['empresa']), $_POST['pais'])) {
            $exito = 1;
        } else {
            $error = 3;
        }
    }
}

$validator->create_message("msj_empresa", "empresa", "Required", 0);
$validator->create_message("msj_email", "email", "Invalid Email", 3);
$validator->create_message("msj_telefono", "telefono", "Required", 0);
$validator->create_message("msj_pais", "pais", "Required", 0);
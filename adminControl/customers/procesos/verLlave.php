<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresa_llaves.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");

// Objetos
$llaves = new clase_empresa_llaves();
//$llaveAccesos = new clase_llave_accesos();
$validator = new validator("form1");
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;

$id_user = 0;
if (isset($_GET["id"]) && is_numeric($_GET["id"])){
    $id_user = $_GET["id"];
}

$idLlave = 0;
if (isset($_GET["idLlave"]) && is_numeric($_GET["idLlave"])){
    $idLlave = $_GET["idLlave"];
}

$validator->create_message("msj_fechaIni", "fechaIni", " Obligatorio", 0);
$validator->create_message("msj_fechaFin", "fechaFin", " Obligatorio", 0);
$validator->create_message("msj_serial", "serial", " Obligatorio", 0);
$validator->create_message("msj_cantidad", "cantidad", " Obligatorio", 0);

$row = $llaves->datos($idLlave);
$autorizado = $row["status"];

$adobe = 0;
$ibm = 0;
$microsoft = 0;
$spla = 0;
$oracle = 0;
$usabilidad = 0;

$unixIbm = 0;
$unixOracle = 0;

$sap = 0;
$VMWare = 0;

$gantt = 0;
$sam = 0;
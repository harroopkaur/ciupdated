<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresa_llaves.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresas.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");

// Objetos
$llaves = new clase_empresa_llaves();
$empresas = new empresas();
$validator = new validator("form1");
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;

$id_user = 0;
if (isset($_GET["id"]) && is_numeric($_GET["id"])){
    $id_user = $_GET["id"];
}

$idLlave = 0;
if (isset($_GET["idLlave"]) && is_numeric($_GET["idLlave"])){
    $idLlave = $_GET["idLlave"];
}

$listadoEmpresas = $empresas->listar_todo();

if (isset($_POST['insertar']) && isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false && 
filter_var($_POST['cantidad'], FILTER_VALIDATE_INT) !== false && filter_var($_POST['nivelServicio'], FILTER_VALIDATE_INT) !== false) {
    $agregar = 1;
    // Validaciones
    
    if ($error == 0) {
        $autorizado = 0;
        if(isset($_POST["modif"]) && ($_POST["modif"] == 1 || $_POST["modif"] == 2)){
            $autorizado = $_POST["modif"];
        }
        
        if ($llaves->actualizar($_POST["idLlave"], $general->reordenarFecha($_POST['fechaIni'], "/", "-"), $general->reordenarFecha($_POST['fechaFin'], "/", "-"), 
        $_POST['cantidad'], $_POST['nivelServicio'], $autorizado)) {
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_fechaIni", "fechaIni", " Obligatorio", 0);
$validator->create_message("msj_fechaFin", "fechaFin", " Obligatorio", 0);
$validator->create_message("msj_serial", "serial", " Obligatorio", 0);
$validator->create_message("msj_cantidad", "cantidad", " Obligatorio", 0);
$validator->create_message("msj_nivel", "nivelServicio", "Required", 0);

$row = $llaves->datos($idLlave);
$autorizado = $row["status"];

$adobe = 0;
$ibm = 0;
$microsoft = 0;
$spla = 0;
$oracle = 0;
$usabilidad = 0;

$nmap = 0;
$unixIbm = 0;
$unixOracle = 0;

$sap = 0;
$VMWare = 0;

$gantt = 0;
$sam = 0;
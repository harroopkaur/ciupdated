<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresa_llaves.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresas.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");

// Objetos
$llaves = new clase_empresa_llaves();
$empresas = new empresas();
$validator = new validator("form1");
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;

$id_user = 0;
if (isset($_GET["id"]) && is_numeric($_GET["id"])){
    $id_user = $_GET["id"];
}

$listadoEmpresas = $empresas->listar_todo();

if (isset($_POST['insertar']) && isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false && 
filter_var($_POST['cantidad'], FILTER_VALIDATE_INT) !== false && filter_var($_POST["nivelServicio"], FILTER_VALIDATE_INT) !== false) {
    $agregar = 1;
    // Validaciones
    
    if ($error == 0) {
        if ($llaves->insertar($_POST["id"], $general->reordenarFecha($_POST['fechaIni'], "/", "-"), $general->reordenarFecha($_POST['fechaFin'], "/", "-"), 
        $general->get_escape($_POST['serial']), $_POST['cantidad'], $_POST["nivelServicio"])) {            
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_fechaIni", "fechaIni", "Required", 0);
$validator->create_message("msj_fechaFin", "fechaFin", "Required", 0);
$validator->create_message("msj_serial", "serial", "Required", 0);
$validator->create_message("msj_cantidad", "cantidad", "Required", 0);
$validator->create_message("msj_nivel", "nivelServicio", "Required", 0);
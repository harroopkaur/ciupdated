<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_clientesReseller.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");

$general = new General();
$clientes = new clase_clientesReseller();
$validator = new validator("form1");

$agregar = 0;
$error = 0;
$exito = 0;

$id = 0;
if(isset($_REQUEST["id"]) && filter_var($_REQUEST["id"], FILTER_VALIDATE_INT) !== false){
    $id  = $_REQUEST["id"];
}

$idEmpresa = 0;
if(isset($_REQUEST["idEmpresa"]) && filter_var($_REQUEST["idEmpresa"], FILTER_VALIDATE_INT) !== false){
    $idEmpresa  = $_REQUEST["idEmpresa"];
}

if (isset($_POST['actualizar']) && $_POST['actualizar'] == 1 && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false) {
    $agregar = 1;
    // Validaciones
    
    $empresa = ""; 
    if(isset($_POST["empresa"])){
        $empresa = $general->get_escape($_POST['empresa']);
    }
     
    $email = $general->get_escape($_POST['email']);
    
    $usuario = ""; 
    if(isset($_POST["usuario"])){
        $usuario = $general->get_escape($_POST['usuario']);
    }
    
    $password = ""; 
    if(isset($_POST["password"])){
        $password = $general->get_escape($_POST['password']);
    }
    
    $error =  $clientes->existeEmpresa($id, $empresa, $idEmpresa);

    if ($error == 0) {
        if ($clientes->actualizar($idEmpresa, $empresa, $email, $usuario, $password)) {
            $exito = 1;
        } else {
            $error = 5;
        }
    } 
}

$datos = $clientes->datosCliente($idEmpresa);

$validator->create_message("msj_empresa", "empresa", "Required", 0);
$validator->create_message("msj_email", "email", "Not a valid Email", 3);
/*$validator->create_message("msj_usuario", "usuario", "Required", 0);
$validator->create_message("msj_password", "password", "Required", 0);*/
<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_centralizador_web.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");

// Objetos
$centralizador = new clase_centralizador_web();
$general = new General();
$validator = new validator("form1");

$id = 0;
if(isset($_REQUEST["id"]) && filter_var($_REQUEST["id"], FILTER_VALIDATE_INT) !== false){
    $id = $_REQUEST["id"];
}

$idEmpresa = 0;
if(isset($_REQUEST["idEmpresa"]) && filter_var($_REQUEST["idEmpresa"], FILTER_VALIDATE_INT) !== false){
    $idEmpresa = $_REQUEST["idEmpresa"];
}

$diasAgente = $centralizador->configCliente($idEmpresa);

//procesos
$actualizar = 0;
$error = 0;
$exito = 0;

$dias = 7;
if (isset($_POST['actualizar']) && filter_var($_POST['dias'], FILTER_VALIDATE_INT) !== false) {
    $dias = $_POST["dias"];
    $actualizar = 1;
    if ($error == 0) {
        if ($centralizador->actualizarConfigCliente($idEmpresa, $dias)) {
            $exito = 1;
        } else {
            $error = 3;
        }
    }
}

$validator->create_message("msj_dias", "dias", " Obligatorio", 0);
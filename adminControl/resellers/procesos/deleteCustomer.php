<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

//clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_clientesReseller.php");

// Objetos
$clientes = new clase_clientesReseller();
$general = new General();

//procesos
$exito = false;

$id = 0;
if (isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false) {
    $id = $_POST['id'];
}

if (isset($_POST['idEmpresa']) && filter_var($_POST['idEmpresa'], FILTER_VALIDATE_INT) !== false) {
    $exito = $clientes->eliminar($_POST['idEmpresa']);
}
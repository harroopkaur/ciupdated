<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware
//
// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresas.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");

// Objetos
$empresas   = new empresas();
$validator = new validator("form1");
$general = new General();

$exito = 0;
$error = 0;

$id = 0;
if(isset($_GET["id"]) && is_numeric($_GET["id"])){
    $id = $_GET["id"];
    $infoCentral = $empresas->datosCentralizador($id);
}

if (isset($_POST['modificar']) && isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false) {
    // Validaciones
    $id = 0;
    if(isset($_POST["id"]) && filter_var($_POST["id"], FILTER_VALIDATE_INT) !== false){
        $id = $_POST["id"];
    }
    $infoCentral = $empresas->datosCentralizador($id);
    
    $login = "";
    if(isset($_POST["login"])){
        $login = $general->get_escape($_POST["login"]);
    }
    
    $clave1 = "";
    if(isset($_POST["clave1"])){
        $clave1 = $general->get_escape($_POST["clave1"]);
    }
    
    $fechaInicio = "";
    if(isset($_POST["fechaInicio"]) && $_POST["fechaInicio"] != ""){
        $fechaInicio = $general->reordenarFecha($_POST["fechaInicio"], "/", "-", "mm/dd/YYYY");
    }
    
    $fechaFin = "";
    if(isset($_POST["fechaFin"]) && $_POST["fechaFin"] != ""){
        $fechaFin = $general->reordenarFecha($_POST["fechaFin"], "/", "-", "mm/dd/YYYY");
    }

    $opcClave = false;
    if($infoCentral["passCentralizador"] != $clave1){
        $opcClave = true;
    }
    
    if ($empresas->actualizar_centralizador($id, $login, $clave1, $fechaInicio, $fechaFin, $opcClave)) {
        
        if($empresas->statusSmartControlDC($id) == 0){
            $empresas->actualizarStatusSmartControlDC($id);
        } 
           
        $exito = 1;
    } else {
        $error = 1;
    } 
}

$validator->create_message("msj_login", "login", "Required", 0);
$validator->create_message("msj_clave1", "clave1", "Required", 0);
$validator->create_message("msj_fechaInicio", "fechaInicio", "Required", 0);
$validator->create_message("msj_fechaFin", "fechaFin", "Required", 0);
<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware
//
// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresas.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");

// Objetos
$empresas   = new empresas();
$validator = new validator("form1");
$general = new General();

$exito = 0;
$error = 0;

if (isset($_POST['modificar']) && isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false) {
    // Validaciones
    if(isset($_POST["clave1"]) && isset($_POST["clave2"])){
        if ($_POST['clave1'] != $_POST['clave2']) {
            $error = 1;
        }
        if ($error == 0) {
            if ($empresas->actualizar_clave($_POST['id'], $_POST['clave1'])) {
                $exito = 1;
            } else {
                $error = 3;
            }
        }
    }
}

$validator->create_message("msj_clave1", "clave1", "Required", 0);
$validator->create_message("msj_clave2", "clave2", "Required", 0);
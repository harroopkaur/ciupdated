<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_centralizador_web.php");

// Objetos
$centralizador = new clase_centralizador_web();
$general  = new General();

$id = 0;
if (isset($_GET["id"]) && is_numeric($_GET["id"])){
    $id = $_GET["id"];
}

$idEmpresa = 0;
if (isset($_GET["idEmpresa"]) && is_numeric($_GET["idEmpresa"])){
    $idEmpresa = $_GET["idEmpresa"];
}

$nombreScheduling = "";
if(isset($_GET["Sche"])){
    $nombreScheduling = $general->get_escape($_GET["Sche"]);
}

$fechaCreacion = "";
if(isset($_GET["fec"])){
    $fechaCreacion = $general->get_escape($_GET["fec"]);
}

$fechaInicio = "";
if(isset($_GET["fecS"])){
    $fechaInicio = $general->get_escape($_GET["fecS"]);
}

$estadoSche = "";
if(isset($_GET["est"])){
    $estadoSche = $general->get_escape($_GET["est"]);
}

//procesos
if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = '&Sche=' . $nombreScheduling . '&fec=' . $fechaCreacion . '&fecS=' . $fechaInicio . '&est=' . $estadoSche;
} else {
    $start_record = 1;
    $parametros   = '';
}

$listado = $centralizador->listaSchedulingClient($idEmpresa, $nombreScheduling, $fechaCreacion, $fechaInicio, $estadoSche, $start_record);
$count   = $centralizador->totalSchedulingClient($idEmpresa, $nombreScheduling, $fechaCreacion, $fechaInicio, $estadoSche);

$pag = new paginator($count, $general->limit_paginacion, 'scheduling.php' . $parametros . '&id=' . $id . '&idEmpresa=' . $idEmpresa);
$i   = $pag->get_total_pages();
<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

//clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_centralizador_web.php");

// Objetos
$centralizador = new clase_centralizador_web();
$general = new General();
//procesos
$exito = false;

$id = 0;
if (isset($_POST['id']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false) {
    $id = $_POST["id"];
}

$idEmpresa = 0;
if (isset($_POST['idEmpresa']) && filter_var($_POST['idEmpresa'], FILTER_VALIDATE_INT) !== false) {
    $idEmpresa = $_POST["idEmpresa"];
}

if (isset($_POST['idScheduling']) && filter_var($_POST['idScheduling'], FILTER_VALIDATE_INT) !== false) {
    $exito = $centralizador->eliminarScheduling($_POST['idScheduling']);
}
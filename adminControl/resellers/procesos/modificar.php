<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresas.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");

// Objetos
$empresas = new empresas();
$general = new General();
$validator = new validator("form1");

//procesos
$error = 0;
$exito = 0;

$id_user = 0;
if(isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) !== false){
    $id_user = $_GET['id']; 
}

$row = $empresas->datos($id_user);
$autorizado = $row["status"];

/*if (isset($_POST['insertar']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false && filter_var($_POST['pais'], FILTER_VALIDATE_INT) !== false
   && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false && filter_var($_POST['nivelServicio'], FILTER_VALIDATE_INT) !== false) {*/
if (isset($_POST['insertar']) && filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false && isset($_POST["email"])
&& filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) !== false) {    
    if ($error == 0) {
        $autorizado = 0;
        if(isset($_POST["modif"]) && $_POST["modif"] == 1){
            $autorizado = 1;
        }
        
        $fechaInicio = $general->guardafechaEnglish($_POST['fechaInicio']);
        $fechaFin = $general->guardafechaEnglish($_POST['fechaFin']);
        
        if ($empresas->actualizar($_POST['id'], $general->get_escape($_POST['empresa']), $general->get_escape($_POST["email"]),
        $general->get_escape($_POST['login']), $fechaInicio, $fechaFin , $autorizado)) {
            $exito = 1;
        } else {
            $error = 3;
        }
    }
}

$validator->create_message("msj_empresa", "empresa", "Required", 0);
$validator->create_message("msj_fechaInicio", "fechaInicio", "Required", 0);
$validator->create_message("msj_fechaFin", "fechaFin", "Required", 0);
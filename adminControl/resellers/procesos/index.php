<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresas.php");

// Objetos
$empresas = new empresas();
$general  = new General();

$empresa = "";
if(isset($_GET["emp"])){
    $empresa = $general->get_escape($_GET["emp"]);
}

$estado = "-1";
if(isset($_GET["est"])){
    $estado = $general->get_escape($_GET["est"]);
}

//procesos
if (isset($_GET['pg']) && filter_var($_GET['pg'], FILTER_VALIDATE_INT) !== false) {
    $start_record = ($_GET['pg'] * $general->limit_paginacion) - $general->limit_paginacion;
    $parametros   = '&emp=' . $empresa . '&est=' . $estado;
} else {
    $start_record = 0;
    $parametros   = '';
}

$listado = $empresas->listar_todo_paginado($empresa, $estado, $start_record);
$count   = $empresas->total($empresa, $estado);

$pag = new paginator($count, $general->limit_paginacion, 'index.php?' . $parametros);
$i   = $pag->get_total_pages();
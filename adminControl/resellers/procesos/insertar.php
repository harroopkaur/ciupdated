<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_paginator.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_empresas.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_validator.php");

// Objetos
$empresas = new empresas();
$validator = new validator("form1");
$general = new General();

//procesos
$error = 0;
$exito = 0;
$agregar = 0;

if (isset($_POST['insertar']) && isset($_POST["email"]) && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) !== false) {
    $agregar = 1;
    // Validaciones
    
    if ($error == 0) {
        $fechaInicio = $general->guardafecha($_POST['fechaInicio']);
        $fechaFin = $general->guardafecha($_POST['fechaFin']);
    
        if ($empresas->insertar($general->get_escape($_POST['empresa']), $general->get_escape($_POST['email']), $general->get_escape($_POST['codigo']), 
        $general->get_escape($_POST['login']), $general->get_escape($_POST['clave']), $fechaInicio, $fechaFin)) {
            $id_user = $empresas->ultimo_id();
            $exito = 1;
        } else {
            $error = 5;
        }
    }
}

$validator->create_message("msj_empresa", "empresa", "Required", 0);
$validator->create_message("msj_codigo", "codigo", "Required", 0);
$validator->create_message("msj_email", "email", "Not a valid email", 3);
$validator->create_message("msj_fechaInicio", "fechaInicio", "Required", 0);
$validator->create_message("msj_fechaFin", "fechaFin", "Required", 0);

<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_clientesReseller.php");

$clientes = new clase_clientesReseller();
$general = new General();

$id = 0;
if (isset($_GET["id"]) && is_numeric($_GET["id"])){
    $id = $_GET["id"];
}

$nombreReseller = $clientes->nombreReseller($id);
$listado = $clientes->clientesReseller($id);
$count = count($listado);
<?php
//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/middleware.php");
//fin middleware

// Clases
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/adminControl/clases/clase_licenciamiento_centralizador.php");

// Objetos
$licencias = new clase_licenciamiento_centralizador();
$general = new General();

//procesos
$exito = false;
if (isset($_POST['idLicencia']) && filter_var($_POST['idLicencia'], FILTER_VALIDATE_INT) !== false) {
    $exito = $licencias->eliminar($_POST['idLicencia']);
    $id = $_POST["id"];
}
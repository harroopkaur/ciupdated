<?php
require_once("../../../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_general.php");
require_once($GLOBALS["app_root1"] . "/clases/clase_clientesReseller.php");

//inicio middleware
require_once($GLOBALS["app_root1"] . "/clases/middleware.php");
$nuevo_middleware = new Middleware($GLOBALS["domain_root"] . "/adminControl");
//fin middleware

$array = array(0=>array('resultado'=>false));

$token = 0;
if(isset($_POST["token"])){
    $token = $_POST["token"];
}

if($nuevo_middleware->compararAjax($token)){
    $general = new General();
    
    if(!isset($_SESSION['usuario_tiempo'])){
        $verifSesion[0] = false;
        $mensaje = $general->obtenerMensaje();
    }
    else{
        $verifSesion = $general->verifSesion(isset($_SESSION['usuario_autorizado']), $_SESSION['usuario_tiempo'], $TIEMPO_MAXIMO_SESION);
        $mensaje     = $verifSesion[2];
    }

    $array = array(0=>array('sesion'=>false, 'mensaje'=>$mensaje, 'resultado'=>true));
    
    if($verifSesion[0]){
        $sesion = true;
        $_SESSION['usuario_tiempo'] = $verifSesion[1];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $clientes = new clase_clientesReseller();
            
            $empresa = "";
            if(isset($_POST["empresa"])){
                $empresa = $general->get_escape($_POST["empresa"]);
            }
            
            $reseller = 0;
            if(isset($_POST["reseller"]) && filter_var($_POST["reseller"], FILTER_VALIDATE_INT) !== false){
                $reseller = $_POST["reseller"];
            }
            
            $idEmpresa = 0;
            if(isset($_POST["idEmpresa"]) && filter_var($_POST["idEmpresa"], FILTER_VALIDATE_INT) !== false){
                $idEmpresa = $_POST["idEmpresa"];
            }

            $result = false;
            if($clientes->existeNombreCliente($reseller, $idEmpresa, $empresa) > 0){
                $result = true;
            }
        
            $array = array(0=>array('sesion'=>$sesion, 'mensaje'=>'', 'result'=>$result, 'resultado'=>true));
        }
    }
    else{
        $general->eliminarSesion();
    }
}
echo json_encode($array);
?>
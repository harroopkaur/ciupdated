<?php
require_once("../../configuracion/inicio.php");
require_once($GLOBALS["app_root1"] . "/adminControl/librerias/idioma_en.php");

$lib = new idioma_web_adminControl_en();

require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/sesion.php");
require_once($GLOBALS["app_root1"] . "/adminControl/resellers/procesos/modificarLicencia.php");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/head.php");
require_once($GLOBALS["app_root1"] . "/adminControl/plantillas/cabecera.php");
?>

<section class="contenedor">
    <div class="contenedorMenuAdmin">
        <?php include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/titulo.php"); ?>

        <br><br>

        <?php
        $opcionm1 = 4;
        include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/menu1.php");
        ?>
    </div>

    <div class="contenedorCuerpoAdmin">
        <div class="contenedorCentralInterno">
            <div class="contenedorMenuInterno">
                <?php 
                $menuCompany = 8;
                include_once($GLOBALS["app_root1"] . "/adminControl/resellers/plantillas/menu.php");
                ?>
            </div>

            <div class="bordeContenedor">
                <div class="contenido">
                    <?php include_once($GLOBALS["app_root1"] . "/adminControl/resellers/plantillas/modificarLicencia.php"); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include_once($GLOBALS["app_root1"] . "/adminControl/plantillas/foot.php");
?>
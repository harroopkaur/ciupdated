 <?php if ($exito == 1) { ?>
    <script type="text/javascript">
        $.alert.open('info', 'Successfully modified record', function() {
            location.href = '<?= $GLOBALS['domain_root1'] ?>/adminControl/resellers/';
        });
    </script>
<?php
} if ($error == 1) { ?>
    <script type="text/javascript">
        $.alert.open('warning', 'Record was not modified');
    </script>
<?php
}
?>
        
<form id="form1" name="form1" method="post"  enctype="multipart/form-data">
    <input type="hidden" name="modificar" id="modificar" value="1" />
    <?php $validator->print_script(); ?>
    
    <input type="hidden" name="id" id="id" value="<?= $id ?>">
    <div class="error_prog"><font color="#FF0000"><?php if ($error == 1) {
            echo $empresas->error;
        } ?></font>
    </div>
    
    
    <fieldset class="fieldset">
        <legend class="text-left" style="margin-left: 15px;"><span class="bold">Credentials Centralizer</span></legend>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Reseller:</th>
                <td colspan="2" align="left"><input name="reseller" id="reseller" type="text" value="<?= $infoCentral["nombre"] ?>" size="30" maxlength="250" readonly/>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Code:</th>
                <td colspan="2" align="left"><input name="codigo" id="codigo" type="text" value="<?= $infoCentral["codigo"] ?>" size="30" maxlength="250" readonly/>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Login:</th>
                <td colspan="2" align="left"><input name="login" id="login" type="text" value="<?= $infoCentral["loginCentralizador"] ?>" size="30" maxlength="250"/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_login") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="105" align="left" valign="middle">Password:</th>
                <td width="845" align="left"><input name="clave1" id="clave1" type="password" value="<?= $infoCentral["passCentralizador"] ?>" size="30" maxlength="250" />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_clave1") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Start Date:</th>
                <td colspan="2" align="left"><input name="fechaInicio" id="fechaInicio" type="text" value="<?= $infoCentral["fechaInicio"] ?>" size="30" maxlength="20" readonly/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaInicio") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">End Date:</th>
                <td colspan="2" align="left"><input name="fechaFin" id="fechaFin" type="text" value="<?= $infoCentral["fechaFin"] ?>" size="30" maxlength="20" readonly/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaFin") ?></font></div></td>
            </tr>
            
            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificarUpdate" value="UPDATE" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </fieldset>
</form>
    
<script>
$(document).ready(function(){
    $("#fechaInicio").datepicker();
    $("#fechaFin").datepicker();
    
    $("#fechaInicio").click(function(){
        $("#fechaInicio").val("");
    });
    
    $("#fechaFin").click(function(){
        $("#fechaFin").val("");
    });
});
</script>
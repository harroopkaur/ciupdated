<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="deleteCustomer.php">
    <input type="hidden" id="id" name="id">
    <input type="hidden" id="idEmpresa" name="idEmpresa">
</form>

<table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
    <thead>
        <tr>
            <th colspan="8" valign="middle" class="filaT text-center"><span>Reseller: <?= $nombreReseller ?> - Manage Customers</span></th>
        </tr>  
        <tr>
            <th  align="center" valign="midle" class="text-center"><strong class="til">#</strong></th>
            <th  align="center" valign="midle" class="text-center"><strong class="til">Empresa</strong></th>
            <th  align="center" valign="middle" class="text-center"><strong>Email</strong></th>
            <th  align="center" valign="middle" class="text-center"><strong>Registration Date</strong></th>
            <th  align="center" valign="middle" class="text-center"><strong>Configuration</strong></th>
            <th  align="center" valign="middle" class="text-center"><strong>Scheduling</strong></th>
            <th  align="center" valign="middle" class="text-center"><strong>Update</strong></th>
            <th  align="center" valign="middle" class="text-center"><strong>Delete</strong></th>
        </tr> 
    </thead>

    <tbody id="bodyTable">
        <?php 
        $i = 1;
        foreach ($listado as $registro) { ?>
            <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                <td class="text-center"><?= $i ?></td>
                <td align="left"><a href="ver.php?id=<?= $registro['id'] ?>">
                     <?= $registro['nombreEmpresa'] ?>
                    </a></td>
                <td  align="left"><?= $registro['correo'] ?></td>
                <td  align="center"><?= $registro['fechaRegistro'] ?></td>
                <td  align="center">
                    <a href="configuration.php?id=<?= $id ?>&idEmpresa=<?= $registro['id'] ?>"><img src="<?= $GLOBALS["domain_root1"] ?>/img/png/glyphicons_137_cogwheels.png" width="24" height="21" border="0" alt="Configuration" title="Configuration" /></a>
                </td>
                <td  align="center">
                    <a href="scheduling.php?id=<?= $id ?>&idEmpresa=<?= $registro['id'] ?>"><img src="<?= $GLOBALS["domain_root1"] ?>/img/png/glyphicons_045_calendar.png" width="24" height="21" border="0" alt="Scheduling" title="Scheduling" /></a>
                </td>
                <td  align="center">
                    <a href="updateCustomer.php?id=<?= $id ?>&idEmpresa=<?= $registro['id'] ?>"><img src="<?= $GLOBALS["domain_root1"] ?>/img/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Update" title="Update" /></a>
                </td>
                <td align="center">
                    <a href="#" onclick="eliminar(<?= $id ?>, <?= $registro['id'] ?>)"><img src="<?= $GLOBALS["domain_root1"] ?>/img/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Delete" title="Delete" /></a>
                </td>
            </tr>
        <?php 
            $i++;
        } ?>
            
        <th class="text-center">Totals</th>
        <th class="text-center"><?= count($listado) ?></th>
        <th class="text-center"><?= count($listado) ?></th>
        <th class="text-center">N/A</th>
        <th colspan="2" class="text-center"></th>
        <th class="text-center"><?= count($listado) ?></th>
        <th class="text-center"><?= count($listado) ?></th>
    </tbody>
</table>
<!--<div id="paginador" style="text-align:center; width:90%; margin:0 auto"><?//= $pag->print_paginator("") ?></div>-->

<?php 
if($count == 0) { ?>
    <div style="text-align:center; width:90%; margin:0 auto">No customers</div>
<?php 
}
?>

<script>    
    function eliminar(id, idEmpresa){
        $.alert.open('confirm', 'You want to delete the record', {Yes: 'Yes', No: 'No'}, function(button) {
            if (button === 'Yes'){
                $("#id").val(id);
                $("#idEmpresa").val(idEmpresa);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>
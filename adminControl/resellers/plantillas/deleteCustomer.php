<?php if($exito == true){  ?>
    <script type="text/javascript">
        $.alert.open('info', 'Successfully eliminated record', function() {
            location.href = 'manageCustomers.php?id=<?= $id ?>';
        });
    </script>
<?php 
}
else{
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Record has not been removed', function() {
            location.href = 'manageCustomers.php?id=<?= $id ?>';
        });
    </script>
<?php
}
?>
<?php if($exito == true){  ?>
    <script type="text/javascript">
        $.alert.open('info', 'Successfully cancelled registration', function() {
            location.href = '<?= $GLOBALS['domain_root1'] ?>/adminControl/resellers/licensesList.php?id=<?= $id ?>';
        });
    </script>
<?php 
}
else{
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Registration has not been cancelled', function() {
            location.href = '<?= $GLOBALS['domain_root1'] ?>/adminControl/resellers/licensesList.php?id=<?= $id ?>';
        });
    </script>
<?php
}
?>
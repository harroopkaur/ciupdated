<form id="eliminarRegistro" name="eliminarRegistro" method="post" action="deleteLicense.php">
    <input type="hidden" id="id" name="id">
    <input type="hidden" id="idLicencia" name="idLicencia">
</form>

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Licenses of <?= $datosReseller["nombre"] ?></span></legend>
    <input type="hidden" name="id" id="id" value="<?= $id ?>">
    
    
    <!--<div style="float:right;"><div class="botones_m2Alterno boton1" id="" onclick="location.href='reportes/llaves.php';">Exportar</div></div>
    <br><br><br>-->

    <table width="100%" align="center" border="0" cellspacing="1" cellpadding="2" class="tablap">
        <thead>
            <tr  bgcolor="#333333" style="color:#FFF;" class="til">
                <th  align="center" valign="midle" ><strong class="til">Key</strong></th>
                <th  align="center" valign="middle" class="til" ><strong>Start Date</strong></th>
                <th  align="center" valign="middle" class="til" ><strong>End Date</strong></th>
                <th  align="center" valign="middle" class="til" ><strong>Status</strong></th>
                <th  align="center" valign="middle" class="til" ><strong>Update</strong></th>
                <th  align="center" valign="middle" class="til" ><strong>Delete</strong></th>
            </tr> 
        </thead>

        <tbody>
            <?php foreach ($listado as $registro) { ?>
                <tr onmouseover="this.style.backgroundColor = '#DEDEDE'" onmouseout="this.style.backgroundColor = '#FFFFFF'">
                    <td align="left"><a href="verLlave.php?id=<?= $id; ?>&idLlave=<?= $registro['id'] ?>">
                         <?= $registro['serial'] ?>
                        </a></td>
                    <td  align="left"><?= $registro['fechaIni'] ?></td>
                    <td  align="center"><?= $registro['fechaFin'] ?></td>
                    <td  align="center">
                        <?php if($registro['status'] == 1){ echo 'Authorized'; }else if($registro['status'] == 2){ echo 'In Use'; }else{ echo 'Unauthorized'; } ?>
                    </td>
                    <td  align="center">
                        <a href="updateLicense.php?id=<?= $id ?>&idLicencia=<?= $registro['id'] ?>"><img src="<?= $GLOBALS["domain_root1"] ?>/img/png/glyphicons_150_edit.png" width="24" height="21" border="0" alt="Update" title="Update" /></a>

                    </td>
                    <td align="center">
                        <a href="#" onclick="eliminar(<?= $id ?>, <?= $registro['id'] ?>)"><img src="<?= $GLOBALS["domain_root1"] ?>/img/png/glyphicons_016_bin.png" width="20" height="28" border="0" alt="Delete" title="Delete" /></a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <div style="text-align:center; width:90%; margin:0 auto"><?= $pag->print_paginator("") ?></div>

    <?php 
    if($count == 0) { ?>
        <div style="text-align:center; width:90%; margin:0 auto">No license for <?= $datosReseller["nombre"] ?></div>
    <?php 
    }
    ?>
</fieldset>

<script>
    function eliminar(id, idLicencia){
        $.alert.open('confirm', 'You want to delete the record', {Yes: 'Yes', No: 'No'}, function(button) {
            if (button === 'Yes'){
                $("#id").val(id);
                $("#idLicencia").val(idLicencia);
                $("#eliminarRegistro").submit();
            }
        });
    }
</script>
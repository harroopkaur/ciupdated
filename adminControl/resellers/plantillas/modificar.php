<?php
if ($exito == true) { ?>
    <script type="text/javascript">
        $.alert.open('info', 'Successfully modified record', {'Ok': 'Ok'}, function(button) {
            if (button === 'Ok'){
                location.href = '<?= $GLOBALS['domain_root1'] ?>/adminControl/resellers/';
            }
        });
    </script>
<?php
}        
?>

<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Update Reseller Data</span></legend>
    
    <form id="form1" name="form1" method="post" enctype="multipart/form-data">
        <input type="hidden" name="insertar" id="insertar" value="1" />
        <input type="hidden" name="id" id="id" value="<?= $id_user ?>">
        <input type="hidden" name="modif" id="modif" value="<?= $autorizado ?>">
        <?php $validator->print_script(); ?>
        <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
            echo $clientes2->error;
        } ?></font>
        </div>
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="tablap2">
            <tr>
                <th width="90" align="left" valign="top">Reseller:</th>
                <td align="left"><input name="empresa" id="empresa" type="text" value="<?= $row["nombre"] ?>" size="30" maxlength="70"  />
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_empresa") ?></font></div>
                </td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Email:</th>
                <td width="100" align="left"><input name="email" id="email" type="text" value="<?= $row["correo"] ?>" size="30" maxlength="70"/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Code:</th>
                <td width="100" align="left"><input name="codigo" id="codigo" type="text" value="<?= $row["codigo"] ?>" size="30" maxlength="20"  readonly/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_codigo") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Login:</th>
                <td colspan="2" align="left"><input name="login" id="login" type="text" value="<?= $row["userName"] ?>" size="30" maxlength="250"/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_login") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">Start Date:</th>
                <td colspan="2" align="left"><input name="fechaInicio" id="fechaInicio" type="text" value="<?= $row["fechaInicio"] ?>" size="30" maxlength="20" readonly/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaInicio") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="90" align="left" valign="top">End Date:</th>
                <td colspan="2" align="left"><input name="fechaFin" id="fechaFin" type="text" value="<?= $row["fechaFin"] ?>" size="30" maxlength="20" readonly/>
                    <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_fechaFin") ?></font></div></td>
            </tr>
            
            <tr>
                <th width="150" align="left" valign="middle">Authorized:</th>
                <td align="left" valign="middle"><input type="checkbox" name="autorizado" id="autorizado" <?php if($row["status"] == '1'){ echo 'checked="checked" disabled';  } ?>></td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input name="modificar" type="button" id="modificar" value="UPDATE" onclick="validate();" class="boton" /></td>
            </tr>
        </table>
    </form>
</fieldset>
    
<script>
    $(document).ready(function(){
        $("#fechaInicio").datepicker();
        
        $("#fechaInicio").click(function(){
            $("#fechaInicio").val("");
        });
        
        $("#fechaFin").datepicker();
        
        $("#fechaFin").click(function(){
            $("#fechaFin").val("");
        });
        
        $("#empresa").blur(function(){
            $.post("ajax/verificarReseller.php", { empresa : $("#empresa").val(), id : $("#id").val(), token : localStorage.smartControlToken }, function(data){
                <?php require_once($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('warning', 'The company already exists', {'Ok': 'Ok'}, function(button) {
                        if (button === 'Ok'){
                            $("#empresa").val("");
                            $("#empresa").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'}, function() {
                });
            });
        });
        
        $("#login").blur(function(){
            $.post("ajax/verificarLogin.php", { login : $("#login").val(), id : $("#id").val(), token : localStorage.smartControlToken }, function(data){
                <?php require_once($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('warning', 'Login already exists', {'Ok': 'Ok'}, function(button) {
                        if (button === 'Ok'){
                            $("#login").val("");
                            $("#login").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'}, function() {
                });
            });
        });
        
        $("#email").blur(function(){
            $.post("ajax/verificarCorreo.php", { email : $("#email").val(), id : $("#id").val(), token : localStorage.smartControlToken }, function(data){
                <?php require_once($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('warning', 'Email already exists', {'Ok': 'Ok'}, function(button) {
                        if (button === 'Ok'){
                            $("#email").val("");
                            $("#email").focus();
                        }
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status, {'Ok' : 'Ok'}, function() {
                });
            });
        });
        
        $("#autorizado").click(function(){
            if($("#autorizado").prop("disabled")){
                return false;
            }
            
            if($("#autorizado").prop("checked")){
                $("#modif").val(1);
            } else{
                $("#modif").val(0);
            }
        });
    });
</script>
<?php
if ($agregar == 1 && $exito == 1) {
?>
    <script type="text/javascript">
        $.alert.open('info', 'Record successfully inserted', function(){
            location.href = 'manageCustomers.php?id=<?= $id ?>';
        });
    </script>
    <?php
} else if ($agregar == 1 && $error > 0) {
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Customer already exists', function() {
            location.href = 'manageCustomers.php?id=<?= $id ?>';
        });
    </script>
    <?php
} else if ($agregar == 1 && $error < 0) {
?>
    <script type="text/javascript">
        $.alert.open('warning', 'Failed to add record', function() {
            location.href = 'manageCustomers.php?id=<?= $id ?>';
        });
    </script>
    <?php
}
?>                                
                               
<fieldset class="fieldset">
    <legend class="text-left" style="margin-left:15px;"><span class="bold">Customer Data</span></legend>
    
    <div class="col-sm-6 col-sm-offset-3">
        <form id="form1" name="form1" class="form-horizontal" role="form" method="post">
            <input type="hidden" name="insertar" id="insertar" value="1" />
            <input type="hidden" name="id" id="id" value="<?= $id ?>" />
            <?php $validator->print_script(); ?>

            <div class="error_prog"><font color="#FF0000"><?php if ($error == 5) {
                 echo $clientes->error;
            } ?></font>
            </div>

            <div class="form-group">
                <label for="inputCustomer" class="col-sm-3 control-label">Customer</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="empresa" name="empresa">
                </div>
                <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_empresa") ?></font></div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="email" name="email">
                </div>
                <div class="error_prog"><font color="#FF0000"><?= $validator->show("msj_email") ?></font></div>
            </div> 
            
            <!--<div class="form-group">
                <label for="inputUsuario" class="col-sm-3 control-label">User</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="usuario" name="usuario">
                </div>
                <div class="error_prog"><font color="#FF0000"><?//= $validator->show("msj_usuario") ?></font></div>
            </div>   
            
            <div class="form-group">
                <label for="inputPassword" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="error_prog"><font color="#FF0000"><?//= $validator->show("msj_password") ?></font></div>
            </div>-->   

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-4 text-center">
                    <button type="submit" class="boton" onclick="verificar();">Save</button>
                </div>
            </div>
        </form>
    </div>
</fieldset>
    
<script>
    $(document).ready(function(){
        $("#empresa").blur(function(){
            $.post("<?= $GLOBALS["domain_root1"] ?>/adminControl/resellers/ajax/verificarEmpresaResellerCustomer.php", { reseller : $("#id").val(), empresa : $("#empresa").val(), token : localStorage.smartControlToken }, function(data){
                <?php require($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('warning', 'The company already exists', function() {
                        $("#empresa").val("");
                        $("#empresa").focus();
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status);
            });
        });

        $("#email").blur(function(){
            $.post("<?= $GLOBALS["domain_root1"] ?>/adminControl/resellers/ajax/verificarCorreoResellerCustomer.php", { reseller : $("#id").val(), email : $("#email").val(), token : localStorage.smartControlToken }, function(data){
                <?php require($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('warning', 'Mail already exists', function() {
                        $("#email").val("");
                        $("#email").focus();
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status);
            });
        });
        
        /*$("#usuario").blur(function(){
            $.post("<?//= $GLOBALS["domain_root1"] ?>/adminControl/resellers/ajax/verificarLoginCustomer.php", { id : $("#id").val(), login : $("#usuario").val(), token : localStorage.smartControlToken }, function(data){
                <?php //require($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

                if(data[0].result === true){
                    $.alert.open('warning', 'User already exists', function() {
                        $("#usuario").val("");
                        $("#usuario").focus();
                    });
                }
            }, "json")
            .fail(function( jqXHR ){
                $.alert.open('error', "Error: " + jqXHR.status);
            });
        });*/
    });
    
    function verificar(){
        $.post("<?= $GLOBALS["domain_root1"] ?>/adminControl/resellers/ajax/verificarEmpresaResellerCustomer.php", { reseller : $("#id").val(), empresa : $("#empresa").val(), token : localStorage.smartControlToken }, function(data){
            <?php require($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>

            if(data[0].result === false){
                $.post("<?= $GLOBALS["domain_root1"] ?>/adminControl/resellers/ajax/verificarCorreoResellerCustomer.php", { reseller : $("#id").val(), email : $("#email").val(), token : localStorage.smartControlToken }, function(data){
                    <?php require($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>
                    
                    if(data[0].result === false){
                        validate();
                    }
                    /*if(data[0].result === false){
                        $.post("<?//= $GLOBALS["domain_root1"] ?>/adminControl/resellers/ajax/verificarLoginCustomer.php", { id : $("#id").val(), login : $("#usuario").val(), token : localStorage.smartControlToken }, function(data){
                            <?php //require($GLOBALS["app_root1"] . "/adminControl/js/validarSesion.js"); ?>
                                
                                
                            
                            }, "json")
                        .fail(function( jqXHR ){
                            $.alert.open('error', "Error: " + jqXHR.status);
                        });
                    }*/
                }, "json")
                .fail(function( jqXHR ){
                    $.alert.open('error', "Error: " + jqXHR.status);
                });
            }
        }, "json")
        .fail(function( jqXHR ){
            $.alert.open('error', "Error: " + jqXHR.status);
        });
    }
</script>
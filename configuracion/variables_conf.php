<?php
########################################################################
# VARIABLES DE CONFIGURACION:                                          #
# approot:      Ubicacion fisica de la aplicacion                      #
# Domain_root:  Ubicacion HTTP de la aplicacion                        #
# DBuser:       Nombre de usuario de la base de datos                  #
# DBpass:       Password del usuario de la base de dato                #
# DBname:       Nombre de la base de datos                             #
# DBserver:     Nombre del servidor de bases de datos                  #
########################################################################

$configuracion = 3;

if($configuracion == 1) {
    #CONFIGURACION SERVER#
    $RAIZ = $_SERVER['DOCUMENT_ROOT']."/results";
    $GLOBALS['app_root1'] = $_SERVER['DOCUMENT_ROOT'];
    $GLOBALS['app_root'] = $_SERVER['DOCUMENT_ROOT']."/results";
    $GLOBALS['domain_root'] = "http://".$_SERVER['HTTP_HOST']."/results";
    $GLOBALS['domain_root1'] = "http://".$_SERVER['HTTP_HOST'];
    $DBusuario = "";
    $DBnombre = "";
    $DBservidor = "";
    $DBcontrasena = "";
    $TIEMPO_MAXIMO_SESION = 600; //3600;  // en segundos	
} else if($configuracion == 2) {
    #CONFIGURACION PRUEBA SERVER#
    $RAIZ = $_SERVER['DOCUMENT_ROOT']."/resultsTest";
    $GLOBALS['app_root1'] = $_SERVER['DOCUMENT_ROOT'];
    $GLOBALS['app_root'] = $_SERVER['DOCUMENT_ROOT']."/resultsTest";
    $GLOBALS['domain_root'] = "https://".$_SERVER['HTTP_HOST']."/resultsTest";
    $GLOBALS['domain_root1'] = "https://".$_SERVER['HTTP_HOST'];
    $DBusuario = "";
    $DBnombre = "";
    $DBservidor = "";
    $DBcontrasena = "";
    $TIEMPO_MAXIMO_SESION = 600; //3600;  // en segundos
} else if($configuracion == 3){
    #CONFIGURACION LOCAL#
    $RAIZ = $_SERVER['DOCUMENT_ROOT']."/smartcontrolportal.com/results";
    $GLOBALS['app_root1'] = $_SERVER['DOCUMENT_ROOT']."/smartcontrolportal.com";
    $GLOBALS['app_root'] = $_SERVER['DOCUMENT_ROOT']."/smartcontrolportal.com/results";
    $GLOBALS['domain_root'] = "http://".$_SERVER['HTTP_HOST']."/smartcontrolportal.com/results";
    $GLOBALS['domain_root1'] = "http://".$_SERVER['HTTP_HOST']."/smartcontrolportal.com";
    $DBusuario = "root";
    $DBnombre = "smartcontrolportal";
    $DBservidor = "127.0.0.1";
    $DBcontrasena = "developer@jk123";
    $TIEMPO_MAXIMO_SESION = 600;  // en segundos
}
<?php
class clase_centralizador extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;
    private $idAgente;
    private $nombTabla;
    private $cabecera; 
    private $bloque;
    private $bloqueValores;
    private $insertarBloque;
    private $i;
    
    #######################################  Operaciones  #######################################    
    function obtenerCliente($serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.id AS cliente
                FROM clientes
                    INNER JOIN licenciasCentralizador ON clientes.id = licenciasCentralizador.cliente AND licenciasCentralizador.serial = :serial AND licenciasCentralizador.status = 2
                    AND fechaIni < NOW() AND fechaFin > NOW()');
            $sql->execute(array(":serial"=>$serial));
            return $sql->fetch();
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01");
        }
    }
    
    function datosAgente($tx_host_name, $tx_serial_disco){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente.id
                FROM agente
                WHERE agente.tx_host_name = :tx_host_name AND agente.tx_serial_disco = :tx_serial_disco');
            $sql->execute(array(':tx_host_name'=>$tx_host_name, ':tx_serial_disco'=>$tx_serial_disco));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("id"=>0, "tx_host_name"=>"", "tx_serial_disco"=>"", "tx_ip"=>"");
        }
    }
    
    function datosAgente1($hostName, $serialCentralizador){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente.id
                FROM agente
                     INNER JOIN agenteRelCentralizador ON agente.id = agenteRelCentralizador.id_agente
                     INNER JOIN licenciasCentralizador ON agenteRelCentralizador.cliente = licenciasCentralizador.cliente
                WHERE agente.tx_host_name = :tx_host_name AND licenciasCentralizador.serial = :serialCentralizador');
            $sql->execute(array(':tx_host_name'=>$hostName, ':serialCentralizador'=>$serialCentralizador));
            return $sql->fetch();
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return array("id"=>0);
        }
    }
    
    Public Function insertarConfig($cliente, $description, $val){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO configCentralizador (cliente, description, val) 
            VALUES (:cliente, :description, :val)');
            $sql->execute(array(':cliente'=>$cliente, ':description'=>$description, ':val'=>$val));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    Public Function actualizarConfigAgenteInactivo($id, $val){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE configCentralizador SET val = :val WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':val'=>$val));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
     
    function insertarAgenteWeb($cliente, $tx_host_name, $tx_serial_disco, $tx_ip) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO agente (clienteReseller, tx_host_name, tx_serial_disco, tx_ip) '
            . 'VALUES (:cliente, :tx_host_name, :tx_serial_disco, :tx_ip)');
            $sql->execute(array(':cliente'=>$cliente, ':tx_host_name'=>$tx_host_name, ':tx_serial_disco'=>$tx_serial_disco, 
            ':tx_ip'=>$tx_ip));
            return true;
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return false;
        }
    }
    
    function ultIdAgenteWeb() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT max(id) AS id FROM agente');
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function actualizarAgenteWeb($id_agente, $tx_serial_disco, $tx_ip) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE agente SET tx_serial_disco = :tx_serial_disco, tx_ip = :tx_ip WHERE id = :id_agente');
            $sql->execute(array(':tx_serial_disco'=>$tx_serial_disco, ':tx_ip'=>$tx_ip, ':id_agente'=>$id_agente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarDatos($idAgente){
        $this->eliminarAddRemove($idAgente);
        $this->eliminarDesinstalacion($idAgente);
        $this->eliminarLlave($idAgente);
        $this->eliminarProcesador($idAgente);
        $this->eliminarProceso($idAgente);
        $this->eliminarResultadoEscaneo($idAgente);
        $this->eliminarSQLData($idAgente);
        $this->eliminarSeguridad($idAgente);
        $this->eliminarSerialMaquina($idAgente);
        $this->eliminarServicio($idAgente);
        $this->eliminarSistemaOperativo($idAgente);
        $this->eliminarTipoEquipo($idAgente);
        $this->eliminarUsuarioEquipo($idAgente);
        $this->eliminarUsabilidadSoftware($idAgente);
        return true;
    }
            
    function eliminarAddRemove($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_add_remove WHERE id_agente = :id_agente'
            . '');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarSerialMaquina($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_serial_maquina WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarSQLData($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_sql_data WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarServicio($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_servicio WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarProcesador($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_procesador WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarSistemaOperativo($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_sistema_operativo WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarTipoEquipo($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_tipo_equipo WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarUsuarioEquipo($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_usuario_equipo WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarProceso($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_proceso WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarDesinstalacion($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_desinstalacion WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarSeguridad($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_seguridad WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarLlave($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_llave WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarResultadoEscaneo($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_resultado_escaneo WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarUsabilidadSoftware($id_agente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM agente_usabilidad_software WHERE id_agente = :id_agente');
            $sql->execute(array(":id_agente"=>$id_agente));           
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarResults() {
        $this->conexion();
        $query = "INSERT INTO " . $this->nombTabla . " (" . $this->cabecera . ") ";
        $query .= "VALUES " . $this->bloque;

        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($this->bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarHistorico($idAgente){
        $this->conexion();
        $query = "INSERT INTO agente_hist (id_agente, fe_ejeccn) VALUES (:id_agente, NOW())";

        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id_agente'=>$idAgente));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function procesar($datos, $idAgente, $opc){
        $this->cabeceraInsert($opc);
        
        $error = 0;
        $this->i = 0;
        $this->bloque = "";
        $this->bloqueValores = array();
        $this->insertarBloque = false;
        $this->idAgente = $idAgente;

        foreach($datos as $row){
            $this->montarData($row, $opc); 
             
            if ($this->i == 1000){
                if(!$this->insertarResults()){ 
                    echo "Results: " . $this->error;
                    $error = 1;
                }

                $this->bloque = "";
                $this->bloqueValores = array();
                $this->i = -1;
                $this->insertarBLoque = false; 
            }
            $this->i++;
        }

        if($this->insertarBloque === true){
            if(!$this->insertarResults()){ 
                echo "Results: " . $this->error;
                $error = 1;
            }
        }
        
        return $error;
    }
    
    function cabeceraInsert($opc){
        switch ($opc) {
            case "add_remove":
                $this->nombTabla = "agente_add_remove";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_regist, tx_editor, tx_verson, tx_dia_instlc, tx_softwr";
                break;
            case "serial_maquina":
                $this->nombTabla = "agente_serial_maquina";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_serial_maquna";
                break;
            case "sql_data":
                $this->nombTabla = "agente_sql_data";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_llave_regist, tx_edicon, tx_verson, tx_ruta_instlc";
                break;
            case "servicio":
                $this->nombTabla = "agente_servicio";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_nb_prodct, tx_estado";
                break;
            case "procesador":
                $this->nombTabla = "agente_procesador";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_tipo_cpu, tx_nu_cpu, tx_nu_cores,
                tx_procsd_logico, tx_tipo_escano";
                break;
            case "sistema_operativo":
                $this->nombTabla = "agente_sistema_operativo";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_sistma_opertv, tx_fe_creacn";
                break;
            case "tipo_equipo":
                $this->nombTabla = "agente_tipo_equipo";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_fabrcn, tx_modelo";
                break;
            case "usuario_equipo":
                $this->nombTabla = "agente_usuario_equipo";
                //$this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_domino, tx_usuario, tx_ip";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_domino, tx_usuario";
                break;
            case "proceso":
                $this->nombTabla = "agente_proceso";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_nb_procso, tx_ruta_procso";
                break;
            case "desinstalacion":
                $this->nombTabla = "agente_desinstalacion";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_host_name, tx_descrp, tx_fe_desins, tx_usuaro_desins";
                break;
            case "seguridad":
                $this->nombTabla = "agente_seguridad";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_hot_name, tx_captin, tx_descrp, tx_fix_commnt, tx_hotfix_id, 
                tx_instll_date, tx_install_by, tx_install_on, tx_name, tx_servce_pack, tx_status";
                break;
            case "llave":
                $this->nombTabla = "agente_llave";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_hot_name, tx_tipo_llave, tx_llave";
                break;
            case "resultado_escaneo":
                $this->nombTabla = "agente_resultado_escaneo";
                $this->cabecera = "id_agente, tx_hot_name, tx_status, tx_error";
                break;
            case "usabilidad_software":
                $this->nombTabla = "agente_usabilidad_software";
                $this->cabecera = "id_agente, tx_dato_contrl, tx_hot_name, tx_softwr, tx_last_exec";
                break;
        }
    }
    
    function montarData($row, $opc){
        switch ($opc) {
            case "add_remove":
                $this->datosAddRemove($row);
                break;
            case "serial_maquina":
                $this->datosSerialMaquina($row);
                break;
            case "sql_data":
                $this->datosSQLData($row);
                break;
            case "servicio":
                $this->datosServicio($row);
                break;
            case "procesador":
                $this->datosProcesador($row);
                break;
            case "sistema_operativo":
                $this->datosSistemaOperativo($row);
                break;
            case "tipo_equipo":
                $this->datosTipoEquipo($row);
                break;
            case "usuario_equipo":
                $this->datosUsuarioEquipo($row);
                break;
            case "proceso":
                $this->datosProceso($row);
                break;
            case "desinstalacion":
                $this->datosDesinstalacion($row);
                break;
            case "seguridad":
                $this->datosSeguridad($row);
                break;
            case "llave":
                $this->datosLlave($row);
                break;
            case "resultado_escaneo":
                $this->datosResultadoEscaneo($row);
                break;
            case "usabilidad_software":
                $this->datosResultadoUsabilidadSoftware($row);
                break;
        }       
    }
    
    function datosAddRemove($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", :tx_regist" . $this->i . ",
            :tx_editor" . $this->i . ", :tx_verson" . $this->i . ", :tx_dia_instlc" . $this->i . ", :tx_softwr" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", :tx_regist" . $this->i . ",
            :tx_editor" . $this->i . ", :tx_verson" . $this->i . ", :tx_dia_instlc" . $this->i . ", :tx_softwr" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_regist" . $this->i] = $row["tx_regist"];
        $this->bloqueValores[":tx_editor" . $this->i] = $row["tx_editor"];
        $this->bloqueValores[":tx_verson" . $this->i] = $row["tx_verson"];
        $this->bloqueValores[":tx_dia_instlc" . $this->i] = $row["tx_dia_instlc"];
        $this->bloqueValores[":tx_softwr" . $this->i] = $row["tx_softwr"];    
    }
    
    function datosSerialMaquina($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", :tx_serial_maquna" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", :tx_serial_maquna" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_serial_maquna" . $this->i] = $row["tx_serial_maquna"];
    }
    
    function datosSQLData($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_llave_regist" . $this->i . ", :tx_edicon" . $this->i . ", :tx_verson" . $this->i . ", :tx_ruta_instlc" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_llave_regist" . $this->i . ", :tx_edicon" . $this->i . ", :tx_verson" . $this->i . ", :tx_ruta_instlc" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_llave_regist" . $this->i] = $row["tx_llave_regist"];
        $this->bloqueValores[":tx_edicon" . $this->i] = $row["tx_edicon"];
        $this->bloqueValores[":tx_verson" . $this->i] = $row["tx_verson"];
        $this->bloqueValores[":tx_ruta_instlc" . $this->i] = $row["tx_ruta_instlc"];
    }
    
    function datosServicio($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_nb_prodct" . $this->i . ", :tx_estado" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_nb_prodct" . $this->i . ", :tx_estado" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_nb_prodct" . $this->i] = $row["tx_nb_prodct"];
        $this->bloqueValores[":tx_estado" . $this->i] = $row["tx_estado"];
    }
    
    function datosProcesador($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", 
            :tx_tipo_cpu" . $this->i . ", :tx_nu_cpu" . $this->i . ", :tx_nu_cores" . $this->i . ", :tx_procsd_logico" . $this->i . ", "
            . ":tx_tipo_escano" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", 
            :tx_tipo_cpu" . $this->i . ", :tx_nu_cpu" . $this->i . ", :tx_nu_cores" . $this->i . ", :tx_procsd_logico" . $this->i . ", "
            . ":tx_tipo_escano" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_tipo_cpu" . $this->i] = $row["tx_tipo_cpu"];
        $this->bloqueValores[":tx_nu_cpu" . $this->i] = $row["tx_nu_cpu"];
        $this->bloqueValores[":tx_nu_cores" . $this->i] = $row["tx_nu_cores"];
        $this->bloqueValores[":tx_procsd_logico" . $this->i] = $row["tx_procsd_logico"];
        $this->bloqueValores[":tx_tipo_escano" . $this->i] = $row["tx_tipo_escano"];
    }
    
    function datosSistemaOperativo($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_sistma_opertv" . $this->i . ", :tx_fe_creacn" . $this->i . ")";
        } else {
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_sistma_opertv" . $this->i . ", :tx_fe_creacn" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_sistma_opertv" . $this->i] = $row["tx_sistma_opertv"];
        $this->bloqueValores[":tx_fe_creacn" . $this->i] = $row["tx_fe_creacn"];
    }
    
    function datosTipoEquipo($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_fabrcn" . $this->i . ", :tx_modelo" . $this->i . ")";
        } else {
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_fabrcn" . $this->i . ", :tx_modelo" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_fabrcn" . $this->i] = $row["tx_fabrcn"];
        $this->bloqueValores[":tx_modelo" . $this->i] = $row["tx_modelo"];
    }
    
    function datosUsuarioEquipo($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            /*$this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_domino" . $this->i . ", :tx_usuario" . $this->i . ", :tx_ip" . $this->i . ")";*/
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_domino" . $this->i . ", :tx_usuario" . $this->i . ")";
        } else {
            /*$this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_domino" . $this->i . ", :tx_usuario" . $this->i . ", :tx_ip" . $this->i . ")";*/
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_domino" . $this->i . ", :tx_usuario" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_domino" . $this->i] = $row["tx_domino"];
        $this->bloqueValores[":tx_usuario" . $this->i] = $row["tx_usuario"];
        //$this->bloqueValores[":tx_ip" . $this->i] = $row["tx_ip"];
    }
    
    function datosProceso($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_nb_procso" . $this->i . ", :tx_ruta_procso" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_nb_procso" . $this->i . ", :tx_ruta_procso" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_nb_procso" . $this->i] = $row["tx_nb_procso"];
        $this->bloqueValores[":tx_ruta_procso" . $this->i] = $row["tx_ruta_procso"];
    }
    
    function datosDesinstalacion($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_descrp" . $this->i . ", :tx_fe_desins" . $this->i . ", :tx_usuaro_desins" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_host_name" . $this->i . ", "
            . ":tx_descrp" . $this->i . ", :tx_fe_desins" . $this->i . ", :tx_usuaro_desins" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_host_name" . $this->i] = $row["tx_host_name"];
        $this->bloqueValores[":tx_descrp" . $this->i] = $row["tx_descrp"];
        $this->bloqueValores[":tx_fe_desins" . $this->i] = $row["tx_fe_desins"];
        $this->bloqueValores[":tx_usuaro_desins" . $this->i] = $row["tx_usuaro_desins"];
    }
    
    function datosSeguridad($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_hot_name" . $this->i . ", 
            :tx_captin" . $this->i . ", :tx_descrp" . $this->i . ", :tx_fix_commnt" . $this->i . ", :tx_hotfix_id" . $this->i . ", 
            :tx_instll_date" . $this->i . ", :tx_install_by" . $this->i . ", :tx_install_on" . $this->i . ", :tx_name" . $this->i . ", "
            . ":tx_servce_pack" . $this->i . ", :tx_status" . $this->i . ")";
        } else {
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_hot_name" . $this->i . ", 
            :tx_captin" . $this->i . ", :tx_descrp" . $this->i . ", :tx_fix_commnt" . $this->i . ", :tx_hotfix_id" . $this->i . ", 
            :tx_instll_date" . $this->i . ", :tx_install_by" . $this->i . ", :tx_install_on" . $this->i . ", :tx_name" . $this->i . ", "
            . ":tx_servce_pack" . $this->i . ", :tx_status" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_hot_name" . $this->i] = $row["tx_hot_name"];
        $this->bloqueValores[":tx_captin" . $this->i] = $row["tx_captin"];
        $this->bloqueValores[":tx_descrp" . $this->i] = $row["tx_descrp"];
        $this->bloqueValores[":tx_fix_commnt" . $this->i] = $row["tx_fix_commnt"];
        $this->bloqueValores[":tx_hotfix_id" . $this->i] = $row["tx_hotfix_id"];
        $this->bloqueValores[":tx_instll_date" . $this->i] = $row["tx_instll_date"];
        $this->bloqueValores[":tx_install_by" . $this->i] = $row["tx_install_by"];
        $this->bloqueValores[":tx_install_on" . $this->i] = $row["tx_install_on"];
        $this->bloqueValores[":tx_name" . $this->i] = $row["tx_name"];
        $this->bloqueValores[":tx_servce_pack" . $this->i] = $row["tx_servce_pack"];
        $this->bloqueValores[":tx_status" . $this->i] = $row["tx_status"];
    } 
    
    function datosLlave($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_hot_name" . $this->i . ", "
            . ":tx_tipo_llave" . $this->i . ", :tx_llave" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_hot_name" . $this->i . ", "
            . ":tx_tipo_llave" . $this->i . ", :tx_llave" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_hot_name" . $this->i] = $row["tx_hot_name"];
        $this->bloqueValores[":tx_tipo_llave" . $this->i] = $row["tx_tipo_llave"];
        $this->bloqueValores[":tx_llave" . $this->i] = $row["tx_llave"];
    }
    
    function datosResultadoEscaneo($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_hot_name" . $this->i . ", :tx_status" . $this->i . ", "
            . ":tx_error" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_hot_name" . $this->i . ", :tx_status" . $this->i . ", "
            . ":tx_error" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_hot_name" . $this->i] = $row["tx_hot_name"];
        $this->bloqueValores[":tx_status" . $this->i] = $row["tx_status"];
        $this->bloqueValores[":tx_error" . $this->i] = $row["tx_error"];
    }
    
    function datosResultadoUsabilidadSoftware($row){
        if($this->i == 0){
            $this->insertarBloque = true;
            $this->bloque .= "(:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_hot_name" . $this->i . ", "
            . ":tx_softwr" . $this->i . ", :tx_last_exec" . $this->i . ")";
        } else {
            $this->bloque .= ", (:id_agente" . $this->i . ", :tx_dato_contrl" . $this->i . ", :tx_hot_name" . $this->i . ", "
            . ":tx_softwr" . $this->i . ", :tx_last_exec" . $this->i . ")";
        } 

        $this->bloqueValores[":id_agente" . $this->i] = $this->idAgente;
        $this->bloqueValores[":tx_dato_contrl" . $this->i] = $row["tx_dato_contrl"];
        $this->bloqueValores[":tx_hot_name" . $this->i] = $row["tx_hot_name"];
        $this->bloqueValores[":tx_softwr" . $this->i] = $row["tx_softwr"];
        $this->bloqueValores[":tx_last_exec" . $this->i] = $row["tx_last_exec"];
    }
    
    function listaAgente($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente.id, agente.tx_host_name, agente.tx_ip, IFNULL(MAX(agente_hist.fe_ejeccn), "") AS fe_ejeccn,
                IF(DATEDIFF(NOW(), IFNULL(MAX(agente_hist.fe_ejeccn), "1981-01-01")) > (SELECT val FROM agente_config WHERE id = 1), "Off", "On") AS estado
                FROM agenteRelCentralizador
                     INNER JOIN agente ON agenteRelCentralizador.id_agente = agente.id
                     LEFT JOIN agente_hist ON agente.id = agente_hist.id_agente
                WHERE agenteRelCentralizador.cliente = :cliente
                GROUP BY agente.id, agente.tx_host_name, agente.tx_ip');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function elimAgente($idAgente, $cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM agenteRelCentralizador WHERE id_agente = :idAgente AND cliente = :cliente');
            $sql->execute(array(':idAgente'=>$idAgente, ':cliente'=>$cliente));
            $row = $sql->fetch();
            if ($row["cantidad"] > 0){
                $sql = $this->conn->prepare('DELETE FROM agente WHERE id = :idAgente');
                $sql->execute(array(':idAgente'=>$idAgente));
                return true;
            } 
            return false;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function agenteClienteReseller($reseller){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente.id AS agente, clientesReseller.id AS cliente
                FROM clientesReseller
                     INNER JOIN agente ON agente.clienteReseller = clientesReseller.id
                WHERE clientesReseller.reseller = :reseller');
            $sql->execute(array(':reseller'=>$reseller));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function agenteClienteResellerLicencia($reseller, $licencia){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente.id AS agente, clientesReseller.id AS cliente
                FROM clientesReseller
                    INNER JOIN agente ON agente.clienteReseller = clientesReseller.id AND agente.licencia = :licencia
                WHERE clientesReseller.reseller = :reseller');
            $sql->execute(array(':reseller'=>$reseller, ':licencia'=>$licencia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function obtenerReseller($codigo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id
                FROM admin004
                WHERE codigo = :codigo');
            $sql->execute(array(':codigo'=>$codigo));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerLicenciaReseller($codigo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id
                FROM licenciasCentralizador
                WHERE serial = :codigo');
            $sql->execute(array(':codigo'=>$codigo));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerIdReseller($codigo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT reseller AS id
                FROM licenciasCentralizador
                WHERE serial = :codigo');
            $sql->execute(array(':codigo'=>$codigo));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function dataAddRemove($reseller){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente_add_remove.id_agente, agente_add_remove.tx_dato_contrl, agente_add_remove.tx_host_name,
                    agente_add_remove.tx_regist, agente_add_remove.tx_editor, agente_add_remove.tx_verson,
                    agente_add_remove.tx_dia_instlc, agente_add_remove.tx_softwr
                FROM agente_add_remove
                    INNER JOIN agente ON agente_add_remove.id_agente = agente.id
                    INNER JOIN clientesReseller ON agente.clienteReseller = clientesReseller.id
                WHERE clientesReseller.reseller = :reseller');
            $sql->execute(array(':reseller'=>$reseller));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function dataAddRemoveLicencia($licencia){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente_add_remove.id_agente, agente_add_remove.tx_dato_contrl, agente_add_remove.tx_host_name,
                    agente_add_remove.tx_regist, agente_add_remove.tx_editor, agente_add_remove.tx_verson,
                    agente_add_remove.tx_dia_instlc, agente_add_remove.tx_softwr
                FROM agente_add_remove
                    INNER JOIN agente ON agente_add_remove.id_agente = agente.id
                    INNER JOIN licenciasCentralizador ON agente.licencia = licenciasCentralizador.id
                WHERE agente.licencia = :licencia');
            $sql->execute(array(':licencia'=>$licencia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function dataProcesador($reseller){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente_procesador.id_agente, agente_procesador.tx_dato_contrl, agente_procesador.tx_host_name,
                    agente_procesador.tx_tipo_cpu, agente_procesador.tx_nu_cpu, agente_procesador.tx_nu_cores,
                    agente_procesador.tx_procsd_logico, agente_procesador.tx_tipo_escano
                FROM agente_procesador
                    INNER JOIN agente ON agente_procesador.id_agente = agente.id
                    INNER JOIN clientesReseller ON agente.clienteReseller = clientesReseller.id
                WHERE clientesReseller.reseller = :reseller');
            $sql->execute(array(':reseller'=>$reseller));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function dataProcesadorLicencia($licencia){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente_procesador.id_agente, agente_procesador.tx_dato_contrl, agente_procesador.tx_host_name,
                    agente_procesador.tx_tipo_cpu, agente_procesador.tx_nu_cpu, agente_procesador.tx_nu_cores,
                    agente_procesador.tx_procsd_logico, agente_procesador.tx_tipo_escano
                FROM agente_procesador
                    INNER JOIN agente ON agente_procesador.id_agente = agente.id
                    INNER JOIN licenciasCentralizador ON agente.licencia = licenciasCentralizador.id
                WHERE agente.licencia = :licencia');
            $sql->execute(array(':licencia'=>$licencia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function dataResultadoEscaneo($reseller){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente_resultado_escaneo.id_agente, agente_resultado_escaneo.tx_hot_name,
                    agente_resultado_escaneo.tx_status, agente_resultado_escaneo.tx_error
                FROM agente_resultado_escaneo
                    INNER JOIN agente ON agente_resultado_escaneo.id_agente = agente.id
                    INNER JOIN clientesReseller ON agente.clienteReseller = clientesReseller.id
                WHERE clientesReseller.reseller = :reseller');
            $sql->execute(array(':reseller'=>$reseller));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function dataResultadoEscaneoLicencia($licencia){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente_resultado_escaneo.id_agente, agente_resultado_escaneo.tx_hot_name,
                    agente_resultado_escaneo.tx_status, agente_resultado_escaneo.tx_error
                FROM agente_resultado_escaneo
                    INNER JOIN agente ON agente_resultado_escaneo.id_agente = agente.id
                    INNER JOIN licenciasCentralizador ON agente.licencia = licenciasCentralizador.id
                WHERE agente.licencia = :licencia');
            $sql->execute(array(':licencia'=>$licencia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function dataSQL($reseller){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente_sql_data.id_agente, agente_sql_data.tx_dato_contrl,
                    agente_sql_data.tx_host_name, agente_sql_data.tx_llave_regist,
                    agente_sql_data.tx_edicon, agente_sql_data.tx_verson,
                    agente_sql_data.tx_ruta_instlc
                FROM agente_sql_data
                    INNER JOIN agente ON agente_sql_data.id_agente = agente.id
                    INNER JOIN clientesReseller ON agente.clienteReseller = clientesReseller.id
                WHERE clientesReseller.reseller = :reseller');
            $sql->execute(array(':reseller'=>$reseller));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function dataSQLLicencia($licencia){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente_sql_data.id_agente, agente_sql_data.tx_dato_contrl,
                    agente_sql_data.tx_host_name, agente_sql_data.tx_llave_regist,
                    agente_sql_data.tx_edicon, agente_sql_data.tx_verson,
                    agente_sql_data.tx_ruta_instlc
                FROM agente_sql_data
                    INNER JOIN agente ON agente_sql_data.id_agente = agente.id
                    INNER JOIN licenciasCentralizador ON agente.licencia = licenciasCentralizador.id
                WHERE agente.licencia = :licencia');
            $sql->execute(array(':licencia'=>$licencia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function dataServicios($licencia){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT agente_servicio.id_agente, agente_servicio.tx_dato_contrl, agente_servicio.tx_host_name,
                    agente_servicio.tx_nb_prodct, agente_servicio.tx_estado
                FROM agente_servicio
                    INNER JOIN agente ON agente_servicio.id_agente = agente.id
                    INNER JOIN licenciasCentralizador ON agente.licencia = licenciasCentralizador.id
                WHERE agente.licencia = :licencia');
            $sql->execute(array(':licencia'=>$licencia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function agentesWebActivos($serial, $serialDisco){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(id) AS cantidad, SUM(fecha) AS cantidadActiva
                FROM (SELECT agente.id, IF(DATEDIFF(CURDATE(), MAX(agente_hist.fe_ejeccn)) <= 7, 1, 0) AS fecha
                    FROM agente
                        INNER JOIN licenciasCentralizador ON agente.licencia = licenciasCentralizador.id AND licenciasCentralizador.serial = :serial
                        AND licenciasCentralizador.serialHDD = :serialDisco
                        INNER JOIN agente_hist ON agente.id = agente_hist.id_agente
                    GROUP BY agente.id) tabla');
            $sql->execute(array(':serial'=>$serial, ':serialDisco'=>$serialDisco));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}
?>
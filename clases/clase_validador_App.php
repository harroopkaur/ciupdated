<?php
class validadorApp extends General{ 
    function verificarSerial($email, $serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT admin005.id, admin005.fechaFin, admin005.nivelServicio
                FROM clientes
                    INNER JOIN admin005 ON clientes.id = admin005.empresa AND admin005.serial = :serial AND admin005.status = 1
                WHERE clientes.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email));
            $row = $sql->fetch();
            return $row;
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01");
        }
    }
    
    function verificarSerial1($email, $serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT admin005.id
                FROM clientes
                    INNER JOIN admin005 ON clientes.id = admin005.empresa AND admin005.serial = :serial AND admin005.status = 2
                WHERE clientes.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function verificarSerial2($email, $serial, $serialHHD) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT admin005.id, admin005.fechaFin, admin005.nivelServicio
                FROM clientes
                    INNER JOIN admin005 ON clientes.id = admin005.empresa AND admin005.serial = :serial AND admin005.status = 2
                    AND serialHHD = :serialHHD
                WHERE clientes.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email, ':serialHHD'=>$serialHHD));
            $row = $sql->fetch();
            if($row["id"] == ""){
                $row["id"] = 0;
                $row["fechaFin"] = null;
            }
            return $row;
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01");
        }
    }
    
    function serialActivado($email, $serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM clientes
                    INNER JOIN admin005 ON clientes.id = admin005.empresa AND admin005.serial = :serial AND admin005.status = 2
                WHERE clientes.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function activarSerial($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE admin005 SET status = 2 WHERE id = :id');
            $sql->execute(array(":id"=>$id));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function activarSerial1($id, $serialHHD) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE admin005 SET serialHHD = :serialHHD, status = 2 WHERE id = :id');
            $sql->execute(array(":id"=>$id, ":serialHHD"=>$serialHHD));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function permisosSerial($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT admin003.nombre,
                    IFNULL(admin004.accesos, 0) AS accesos
                FROM admin003
                    LEFT JOIN admin004 ON admin003.id = admin004.accesos AND admin004.serial = :id AND admin004.status = 1
                WHERE admin003.status = 1
                GROUP BY admin003.id
                ORDER BY admin003.nombre');
            $sql->execute(array(":id"=>$id));
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }
    
    //inicio validacion Smart Control DC
    function verificarSerialSmartControlDC($email, $serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT licenciasCentralizador.id, licenciasCentralizador.fechaIni AS fechaIniCentralizador, 
                    licenciasCentralizador.fechaFin AS fechaFinCentralizador
                FROM admin004 
                    INNER JOIN licenciasCentralizador ON admin004.id = licenciasCentralizador.reseller AND licenciasCentralizador.serial = :serial
                    AND licenciasCentralizador.status = 1 AND licenciasCentralizador.fechaIni <= CURDATE() AND 
                    licenciasCentralizador.fechaFin >= CURDATE()
                WHERE admin004.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email));
            $row = $sql->fetch();
            return $row;
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01", "fechaFin"=>"1981-01-01");
        }
    }
    
    function activarSerial1SmartControlDC($id, $serialDisco) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE licenciasCentralizador SET serialHDD = :serialDisco, status = 2 WHERE id = :id');
            $sql->execute(array(":id"=>$id, ":serialDisco"=>$serialDisco));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function verificarSerial2SmartControlDC($email, $serial, $serialDisco) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT licenciasCentralizador.id, fechaIniCentralizador, fechaFinCentralizador
                FROM admin004 
                    INNER JOIN licenciasCentralizador ON admin004.id = licenciasCentralizador.reseller AND licenciasCentralizador.serial = :serial AND 
                    licenciasCentralizador.status = 2 AND licenciasCentralizador.serialHDD = :serialDisco AND 
                    licenciasCentralizador.fechaIni <= CURDATE() AND licenciasCentralizador.fechaFin >= CURDATE()
                WHERE admin004.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email, ':serialDisco'=>$serialDisco));
            $row = $sql->fetch();
            if($row["id"] == ""){
                $row["id"] = 0;
                $row["fechaFin"] = null;
            }
            return $row;
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01");
        }
    }
    
    function verificarSerialActivadoCentralizadorDC($email, $serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT licenciasCentralizador.id, fechaIniCentralizador, fechaFinCentralizador
                FROM admin004 
                    INNER JOIN licenciasCentralizador ON admin004.id = licenciasCentralizador.reseller AND licenciasCentralizador.serial = :serial AND 
                    licenciasCentralizador.status = 2 AND 
                    licenciasCentralizador.fechaIni <= CURDATE() AND licenciasCentralizador.fechaFin >= CURDATE()
                WHERE admin004.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email));
            $row = $sql->fetch();
            if($row["id"] == ""){
                $row["id"] = 0;
                $row["fechaFin"] = null;
            }
            return $row;
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01");
        }
    }
    
    function serialActivadoSmartControlDC($email, $serial) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM admin004
                    INNER JOIN licenciasCentralizador ON admin004.id = licenciasCentralizador.reseller AND licenciasCentralizador.serial = :serial 
                    AND licenciasCentralizador.fechaIni <= CURDATE() AND licenciasCentralizador.fechaFin >= CURDATE() AND licenciasCentralizador.status = 2 
                WHERE admin004.correo = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    //fin validacion Smart Control DC
    
    //inicio agente scheduling
    function verificarAgenteDC($providerEmail, $companyCustomerName) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT licenciasCentralizador.id, licenciasCentralizador.fechaFin, clientesReseller.id AS cliente
                FROM admin004
                    INNER JOIN clientesReseller ON admin004.id = clientesReseller.reseller AND clientesReseller.nombreEmpresa = :companyCustomerName
                    INNER JOIN licenciasCentralizador ON admin004.id = licenciasCentralizador.reseller AND licenciasCentralizador.fechaIni <= CURDATE() 
                    AND licenciasCentralizador.fechaFin >= CURDATE() AND licenciasCentralizador.status = 2
                WHERE admin004.correo = :providerEmail 
                AND admin004.status = 1');
            $sql->execute(array(":providerEmail"=>$providerEmail, ':companyCustomerName'=>$companyCustomerName));
            return $sql->fetch();
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01", "cliente"=>0);
        }
    }
    
    function verifNombScheduling($nombre, $cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, COUNT(id) AS cantidad
                FROM scheduling
                WHERE tx_descrip = :nombre AND cliente = :cliente');
            $sql->execute(array(':nombre'=>$nombre, ':cliente'=>$cliente));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("id"=>0, "cantidad"=>0);
        }
    }
    
    function dataScheduling($nombre, $cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM scheduling
                WHERE tx_descrip = :nombre AND cliente = :cliente');
            $sql->execute(array(':nombre'=>$nombre, ':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function verificarSchedulingName($providerEmail, $companyCustomerName, $scheduling) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(scheduling.tx_schedule) AS cantidad
                FROM admin004
                    INNER JOIN clientesReseller ON admin004.id = clientesReseller.reseller AND clientesReseller.nombreEmpresa = :companyCustomerName AND clientesReseller.status = 1
                    INNER JOIN scheduling ON clientesReseller.id = scheduling.cliente AND tx_descrip = :scheduling
                    INNER JOIN licenciasCentralizador ON admin004.id = licenciasCentralizador.reseller AND licenciasCentralizador.status = 2
                    AND licenciasCentralizador.fechaIni <= CURDATE() AND licenciasCentralizador.fechaFin >= CURDATE()
                WHERE admin004.correo = :providerEmail AND admin004.status = 1');
            $sql->execute(array(":providerEmail"=>$providerEmail, ":companyCustomerName"=>$companyCustomerName, 
            ":scheduling"=>$scheduling));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    //fin agente scheduling
}
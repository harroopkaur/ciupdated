<?php
//session_start();
class Middleware {

    private $llave_server = "5p9k=E79=brj39H5lN0==f09x0*e911=36";
    private $path;

    function __construct($path) {
        $this->path = $path;
    }
    
    private function comp_token($opcion = false) {
        return '<script>
            function comparar_tokens(token){
                if(localStorage.smartControlToken  !==  token){
                    location.href = "' . $this->path . '";
                }
            }
            comparar_tokens("' . $this->obtener_token($opcion) . '");
        </script>';
    }
    
    private function comp_tokenAjax($token, $opcion) {
        $result = false;
        if($token === $this->obtener_token($opcion)){
            $result =  true;
        }
        return $result;
    }

    public function obtener_token($opcion = false) {
        //$_SESSION["token"] = $this->encriptar_token();
        $header = [
                'typ'=>'JWT', 
                'alg'=>'HS256'
            ];
        $header = json_encode($header);
        $header = base64_encode($header);   
        
        
        
        if($opcion === true){
            $playload = [
                'iss'      =>'http://www.smartcontrolportal.com', 
                'reseller' => $_SESSION['nombReseller'],
                'fecha'    => $_SESSION['client_fecha']
            ];
        }
        else{
            $playload = [
                'iss'      =>'http://www.smartcontrolportal.com', 
                'nombre'   => $_SESSION['usuario_nombre'],
                'apellido' => $_SESSION['usuario_apellido'],
                'fecha'    => $_SESSION['usuario_fecha']
            ];
        }
        
        $playload = json_encode($playload);
        $playload = base64_encode($playload);  
        
        $signature = hash_hmac('sha256', $header.".".$playload, $this->llave_server, true);
        $signature = base64_encode($signature);
        
        return $header.".".$playload.".".$signature; //$this->llave;
    }

    public function comparar($opcion = false) {
        return $this->comp_token($opcion);
    }
    
    public function compararAjax($token, $opcion = false) {
        return $this->comp_tokenAjax($token, $opcion);
    }
    
    //inicio token CIM
    private function comp_CIM_token() {
        return '<script>
            function comparar_tokens(token){
                if(localStorage.smartControlCIMToken  !==  token){
                    location.href = "' . $this->path . '";
                }
            }
            comparar_tokens("' . $this->obtener_tokenCIM() . '");
        </script>';
    }
    
    public function obtener_tokenCIM() {
        $header = [
                'typ'=>'JWT', 
                'alg'=>'HS256'
            ];
        $header = json_encode($header);
        $header = base64_encode($header);   
        
        $nomb = "";
        if(isset($_SESSION['nombReseller'])){ $nomb = $_SESSION["nombReseller"]; }
        
        $fecha = "";
        if(isset($_SESSION['client_fecha'])){ $fecha = $_SESSION["client_fecha"]; }
        
        $playload = [
            'iss'      =>'http://www.smartcontrolportal.com/CIM', 
            'reseller' => $nomb,
            'fecha'    => $fecha
        ];
        
        $playload = json_encode($playload);
        $playload = base64_encode($playload);  
        
        $signature = hash_hmac('sha256', $header.".".$playload, $this->llave_server, true);
        $signature = base64_encode($signature);
        
        return $header.".".$playload.".".$signature; //$this->llave;
    }
    
    private function comp_CIM_tokenAjax($token) {
        $result = false;
        if($token === $this->obtener_tokenCIM()){
            $result =  true;
        }
        return $result;
    }
    
    public function compararCIM() {
        return $this->comp_CIM_token();
    }
    
    public function compararCIMAjax($token) {
        return $this->comp_CIM_tokenAjax($token);
    }
    //fin token CIM
}
?>
<?php
class Pais extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $nombre;
    var $bandera;
    var $estado;
    var $fecha_registro;
    var $usuario_registro;
    var $fecha_actualizacion;
    var $usuario_actualizacion;
    var $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar un Pais a la Base de Datos
    function insertar($nombre, $bandera) {
        $this->conexion();
        $query = "INSERT INTO paises (nombre, bandera, estado, fecha_registro, usuario_registro, fecha_actualizacion, usuario_actualizacion) ";
        $query .= "VALUES (:nombre, :bandera, 1, CURDATE(), :usuario, CURDATE(), :usuario)";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':nombre'=>$nombre, ':bandera'=>$bandera, ':usuario'=>$_SESSION['usuario_id']));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Actualizar un Pais a la Base de Datos identificado por su id
    function actualizar($id, $nombre, $bandera) {
        $this->conexion();
        $query  = "UPDATE paises SET ";
        $query .= "nombre = :nombre, bandera = :bandera, fecha_actualizacion=CURDATE(), usuario_actualizacion = :usuario";
        $query .= "WHERE id = :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':nombre'=>$nombre, ':bandera'=>$bandera, ':usuario'=>$_SESSION['usuario_id'], ':id'=>$id));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar un Pais de la Base de Datos identificado por su id
    function eliminar($id) {
        $this->conexion();
        $query = "DELETE FROM paises WHERE id = :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Desactivar un Pais de la Base de Datos identificado por su id
    function desactivar($id) {
        $this->conexion();
        $query = "UPDATE paises SET estado = 0 WHERE id = :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos de un Pais identifiado por su id
    function datos($id) {
        $this->conexion();
        $query = "SELECT * FROM paises WHERE id = :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id));
            $categoria = $sql->fetch();
            
            $this->id = $categoria['id'];
            $this->nombre = $categoria['nombre'];
            $this->bandera = $categoria['bandera'];
            $this->estado = $categoria['estado'];
            $this->fecha_registro = $categoria['fecha_registro'];
            $this->usuario_registro = $categoria['usuario_registro'];
            $this->fecha_actualizacion = $categoria['fecha_actualizacion'];
            $this->usuario_actualizacion = $categoria['usuario_actualizacion'];
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todas las Paiss
    function listar_todo() {
        $this->conexion();
        $query = "SELECT * FROM paises WHERE estado!=0 ORDER BY nombre";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todas las Paiss paginados
    function listar_todo_paginado($inicio, $fin) {
        $this->conexion();
        $query = "SELECT * FROM paises WHERE estado != 0 ORDER BY nombre LIMIT " . $inicio . ", " . $fin;
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Contar el total de Paiss
    function total() {
        $this->conexion();
        $query = "SELECT COUNT(*) AS cantidad FROM paises WHERE estado != 0";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute();
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }

    // Obtener listado de todas las Paiss desactivados paginado
    function listar_desactivados_paginado($inicio, $fin) {
        $this->conexion();
        $query = "SELECT * FROM paises WHERE estado = 0 ORDER BY nombre LIMIT " . $inicio . ", " . $fin;
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Contar el total de Paiss de un tipo
    function total_desactivados() {
        $this->conexion();
        $query = "SELECT COUNT(*) AS cantidad FROM paises WHERE estado=0";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute();
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
}
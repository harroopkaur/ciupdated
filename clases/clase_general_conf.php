<?php
class General {
    ########################################  Atributos  ########################################

    public  $error = NULL;
    public  $separador;
    public  $limit_paginacion = 50;
    private $configuraciones = 3;
    private $DBusuario;
    private $DBnombre;
    private $DBservidor;
    private $DBcontrasena;
    protected $conn;
    public $registrosBloque = 1000;
    public $tiempoLimite = 600;//el tiempo esta en segundos
    public $pesoMax = 10240; //peso máximo de los archivos
    #######################################  Operaciones  #######################################
    
    protected function conexion(){
        try{
            $this->configuracion();
            $this->conn = null;
            $this->conn = new PDO('mysql:host=' . $this->DBservidor . ';dbname=' . $this->DBnombre, $this->DBusuario, $this->DBcontrasena);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            print "ERROR: No se pudo realizar la conexión a la base de datos";
        }
    }
    
    private function configuracion(){
        if($this->configuraciones == 1){
            $this->DBusuario = "";
            $this->DBnombre = "";
            $this->DBservidor = "";
            $this->DBcontrasena = "";
        }
        else if($this->configuraciones ==  2){
            $this->DBusuario = "";
            $this->DBnombre = "";
            $this->DBservidor = "";
            $this->DBcontrasena = "";
        }
        else if($this->configuraciones == 3){
            $this->DBusuario = "root";
            $this->DBnombre = "smartcontrolportal";
            $this->DBservidor = "127.0.0.1";
            $this->DBcontrasena = "developer@jk123";
        }
    }

    function muestrafecha($fecha) {
        if ((empty($fecha)) || ($fecha == "0000-00-00")) {
            $resultado = '';
        } else {
            $resultado = date("d/m/Y", strtotime($fecha));
        }

        return $resultado;
    }
    
    function muestraFechaHora($fecha) {
        if ((empty($fecha)) || ($fecha == "0000-00-00 00:00:00")) {
            $resultado = '';
        } else {
            $resultado = date("d/m/Y h:i:s A", strtotime($fecha));
        }

        return $resultado;
    }
    
    function muestraFechaHora1($fecha) {
        if ((empty($fecha)) || ($fecha == "0000-00-00 00:00:00")) {
            $resultado = '';
        } else {
            $f = substr($fecha, 0, 10);
            $resultado = $this->reordenarFecha($f, "-", "/");
        }

        return $resultado;
    }

    function muestrafechas($fecha) {
        if ((empty($fecha)) || ($fecha == "0000-00-00")) {
            $this->error = 'Fecha vac&iacute;a';
            $resultado = 'Fecha vac&iacute;a';
        } else {
            $resultado = date("d-m-Y", strtotime($fecha));
        }

        return $resultado;
    }
    
    function cambiarfechasFormato($fecha, $formato = "Y-m-d") {
        if ((empty($fecha)) || ($fecha == "0000-00-00" || $fecha == "00-0000-00" || $fecha == "00-00-0000" ||
        $fecha == "0000/00/00" || $fecha == "00/0000/00" || $fecha == "00/00/0000")) {
            $this->error = 'Fecha vac&iacute;a';
            $resultado = 'Fecha vac&iacute;a';
        } else {
            $resultado = date($formato, strtotime($fecha));
        }

        return $resultado;
    }
    
    function validateDate($date, $format = 'Y-m-d H:i:s'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
       
    function reordenarFecha($fecha, $separador, $sustituirSeparador = null, $formato = "dd/mm/YYYY"){
        $fechaAux = explode($separador, $fecha);
        if($sustituirSeparador == null){
            if($formato == "dd/mm/YYYY"){
                $fecha = $fechaAux[2] . $separador . $fechaAux[1] . $separador . $fechaAux[0];
            } else if("mm/dd/YYYY"){
                $fecha = $fechaAux[2] . $separador . $fechaAux[0] . $separador . $fechaAux[1];
            }
        }
        else{
            if($formato == "dd/mm/YYYY"){
                $fecha = $fechaAux[2] . $sustituirSeparador . $fechaAux[1] . $sustituirSeparador . $fechaAux[0];
            } else if("mm/dd/YYYY"){
                $fecha = $fechaAux[2] . $sustituirSeparador . $fechaAux[0] . $sustituirSeparador . $fechaAux[1];
            }
        }
        return $fecha;
    }
    
    function sustituirSeparadorFecha($fecha, $separador, $sustituirSeparador){
        $fechaAux = explode($separador, $fecha);
        $fecha = $fechaAux[0] . $sustituirSeparador . $fechaAux[1] . $sustituirSeparador . $fechaAux[2];
        return $fecha;
    }
    
    function guardafecha($fecha) {
        $ano = substr($fecha, 6, 4);
        $mes = substr($fecha, 3, 2);
        $dia = substr($fecha, 0, 2);
        $fecha = "$ano-$mes-$dia";

        if ($fecha == '--') {
            $fecha = '';
        }

        return $fecha;
    }
    
    function obtenerMensaje(){
        return "You must log in!";
    }
    
    function verifSesion($clienteAutorizado, $tiempoCliente, $TIEMPO_MAXIMO_SESION){
        $result = array();
        if(!$clienteAutorizado) {
            $result[0] = false;
            $result[2] = "You must sign in!";
        }
        // Verificar tiempo de sesion
        $time = time();
        $tiempo_sesion = $time - $tiempoCliente;
        if($tiempo_sesion > $TIEMPO_MAXIMO_SESION) {
            $result[0] = false;
            $result[2] = "You spent a lot of time idle!";
        } else {
            $result[0] = true;
            $result[1] = time();
            $result[2] = "";
        }
        return $result;
    }
    
    public function get_escape($valor){
        return htmlspecialchars(addslashes(stripslashes(strip_tags(trim($valor)))));
    }
    
    function obtenerCampoResumenProfesional($tabla){
        $valor = " AND (" . $tabla . ".edicion LIKE :edicion OR " . $tabla . ".edicion LIKE '%ProPlus%' OR " . $tabla . ".edicion LIKE '%Profesional%') ";
        return $valor;
    }
    
    function noIncluirResumenProfesional($tabla){
        $valor = " AND NOT " . $tabla . ".edicion LIKE :edicion AND NOT " . $tabla . ".edicion LIKE '%ProPlus%' AND NOT " . $tabla . ".edicion LIKE '%Profesional%' ";
        return $valor;
    }
    
    function eliminarSesion(){
        session_unset();                 // Vacia las variables de sesion
        session_destroy();               // Destruye la sesion
    }
}
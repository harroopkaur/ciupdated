<?php
class email{
    function enviar_CIM_correo($correo, $usuario, $copy_email = "") {
        $arrhtml_mail = file('../plantillas/cim_email.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }

        $contenido = $html_mail;
        $asunto = utf8_decode($usuario . " invites you to download the CIM application");

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info.la@licensingassurance.com>\r\n";
        
        if($copy_email != ""){
            $headers .= "Bcc: {$copy_email}";
        }
        
        return mail($correo, $asunto, $mensaje, $contenido, $headers);
    }
    
    function enviar_notificacion_SCDC($correo, $asunto, $mensaje, $copy_email = "") {
        $arrhtml_mail = file('../plantillas/email_notificacion_SCDC.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        
        $html_mail = str_replace("#ASUNTO#", utf8_decode($asunto), $html_mail);
        $html_mail = str_replace("#MENSAJE#", utf8_decode($mensaje), $html_mail);
       

        $contenido = $html_mail;
        
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info.la@licensingassurance.com>\r\n";
        
        if($copy_email != ""){
            $headers .= "Bcc: {$copy_email}";
        }
        
        return mail($correo, $asunto, $contenido, $headers);
    }
}
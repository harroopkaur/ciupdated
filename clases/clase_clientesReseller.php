<?php
class clase_clientesReseller extends General{
    public $error;
    
    function insertar($reseller, $nombre, $correo, $usuario, $password) {
        $this->conexion();
        $query = "INSERT INTO clientesReseller (reseller, nombreEmpresa, correo, fechaRegistro, usuario, password) ";
        $query .= "VALUES (:reseller, :nombre, :correo, NOW(), :usuario, SHA2(:password, 224))";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':nombre'=>$nombre, ':correo'=>$correo,
            ':usuario'=>$usuario, ':password'=>$password));
            return true;
        } catch (PDOException $e) {
            echo $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $nombre, $correo, $usuario, $password, $status = 1) {
        $this->conexion();
        $array = array(':id'=>$id, ':nombre'=>$nombre, ':correo'=>$correo, ':usuario'=>$usuario, ':status'=>$status);
        $campo = "";
        
        if ($this->verifPassword($id, $password) == 0){
            $campo = ", password = SHA2(:password, 224)";
            $array[":password"] = $password;
        }
        
        $query = "UPDATE clientesReseller SET nombreEmpresa = :nombre, correo = :correo, "
            . "usuario = :usuario " . $campo . ", status = :status WHERE id = :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function verifPassword($id, $password){
        $query = "SELECT id "
            . "FROM clientesReseller "
            . "WHERE id = :id AND password = :password";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id, ':password'=>$password));
            $row = $sql->fetch();
            return $row["id"];
        } catch (PDOException $e) {
            echo $this->error = $e->getMessage();
            return array("id"=>0);
        }
    }
    
    function eliminar($id) {
        $this->conexion();
        $query = "UPDATE clientesReseller SET status = 0 WHERE id = :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id));
            return true;
        } catch (PDOException $e) {
            echo $this->error = $e->getMessage();
            return false;
        }
    }    
    
    function existeEmpresa($reseller, $nombre, $id = 0) {
        $this->conexion();
        $query = "SELECT COUNT(id) AS cantidad "
            . "FROM clientesReseller "
            . "WHERE reseller = :reseller AND nombreEmpresa = :nombre AND id != :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':nombre'=>$nombre, ':id'=>$id));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return -1;
        }
    }
    
    function existeEmpresaDesactivada($reseller, $nombre, $idEmpresa = 0) {
        $this->conexion();
        $query = "SELECT COUNT(id) AS cantidad "
            . "FROM clientesReseller "
            . "WHERE reseller = :reseller AND nombreEmpresa = :nombre AND id != :idEmpresa AND status = 0";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':nombre'=>$nombre, ':idEmpresa'=>$idEmpresa));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return -1;
        }
    }
    
    function datosEmpresa($reseller, $nombre) {
        $this->conexion();
        $query = "SELECT id, correo "
            . "FROM clientesReseller "
            . "WHERE reseller = :reseller AND nombreEmpresa = :nombre AND status = 1";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':nombre'=>$nombre));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array("id"=>"", "correo"=>"");
        }
    }
    
    function existeEmailReseller($email) {
        $this->conexion();
        $query = "SELECT id, COUNT(id) AS cantidad "
            . "FROM admin004 "
            . "WHERE correo = :email AND status = 1";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':email'=>$email));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array("id"=>0, "cantidad"=>"");
        }
    }
    
    function existeResellerCustomer($reseller, $customer) {
        $this->conexion();
        $query = "SELECT COUNT(reseller) AS cantidad "
            . "FROM clientesReseller "
            . "WHERE reseller = :reseller AND id = :customer";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller, ':customer'=>$customer));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function existeCustomerDC($correoReseller, $nombCustomer) {
        $this->conexion();
        $query = "SELECT COUNT(reseller) AS cantidad
            FROM clientesReseller
                INNER JOIN admin004 ON clientesReseller.reseller = admin004.id AND admin004.status = 1
                AND admin004.fechaFin >= CURDATE()
            WHERE admin004.correo = :correo AND clientesReseller.nombreEmpresa = :nombre";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':correo'=>$correoReseller, ':nombre'=>$nombCustomer));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function existeCustomerDCLicencia($correoReseller, $nombCustomer, $serial) {
        $this->conexion();
        $query = "SELECT COUNT(clientesReseller.reseller) AS cantidad
            FROM clientesReseller
                INNER JOIN admin004 ON clientesReseller.reseller = admin004.id AND admin004.status = 1
                INNER JOIN licenciasCentralizador ON admin004.id = licenciasCentralizador.reseller AND
                licenciasCentralizador.status = 2 AND licenciasCentralizador.fechaIni <= CURDATE()
                AND licenciasCentralizador.fechaFin >= CURDATE() AND licenciasCentralizador.serial = :serial
            WHERE admin004.correo = :correo AND clientesReseller.nombreEmpresa = :nombre";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':correo'=>$correoReseller, ':nombre'=>$nombCustomer, ':serial'=>$serial));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function customerDCWeb($correoReseller, $nombCustomer) {
        $this->conexion();
        $query = "SELECT clientesReseller.id, admin004.fechaFin
            FROM clientesReseller
                INNER JOIN admin004 ON clientesReseller.reseller = admin004.id AND admin004.status = 1
                AND admin004.fechaFin >= CURDATE()
            WHERE admin004.correo = :correo AND clientesReseller.nombreEmpresa = :nombre";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':correo'=>$correoReseller, ':nombre'=>$nombCustomer));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array("id"=>0);
        }
    }
    
    function ultIdEmpresa() {
        $this->conexion();
        $query = "SELECT MAX(id) AS id "
            . "FROM clientesReseller";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function idEmpresa($reseller, $empresa) {
        $this->conexion();
        $query = "SELECT id "
            . "FROM clientesReseller "
            . "WHERE reseller = :reseller AND nombreEmpresa = :empresa";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":reseller"=>$reseller, ":empresa"=>$empresa));
            $row = $sql->fetch();
            return $row["id"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function clientesReseller($reseller) {
        $this->conexion();
        $query = "SELECT clientesReseller.id, clientesReseller.nombreEmpresa, clientesReseller.correo, DATE_FORMAT(clientesReseller.fechaRegistro, '%m/%d/%Y') AS fechaRegistro "
            . "FROM clientesReseller "
            . "WHERE clientesReseller.reseller = :reseller AND clientesReseller.status = 1";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':reseller'=>$reseller));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
        
    function datosCliente($id) {
        $this->conexion();
        $query = "SELECT id, nombreEmpresa, correo, "
                . "DATE_FORMAT(fechaRegistro, '%m/%d/%Y') AS fechaRegistro, "
                . "usuario, password, status "
            . "FROM clientesReseller "
            . "WHERE id = :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array("id"=>0, "nombreEmpresa"=>"", "correo"=>"", "fechaRegistro"=>"");
        }
    }
    
    function nombreReseller($id) {
        $this->conexion();
        $query = "SELECT nombre "
            . "FROM admin004 "
            . "WHERE id = :id";
        try {
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id));
            $row = $sql->fetch();
            return $row["nombre"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array("nombre"=>"");
        }
    }
    
    function datosAgente($tx_host_name, $tx_serial_disco){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM agente WHERE tx_host_name = :tx_host_name AND tx_serial_disco = :tx_serial_disco');
            $sql->execute(array(':tx_host_name'=>$tx_host_name, ':tx_serial_disco'=>$tx_serial_disco));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("id"=>0, "tx_host_name"=>"", "tx_serial_disco"=>"", "tx_ip"=>"");
        }
    }
    
    function datosAgenteLicencia($tx_host_name, $tx_serial_disco, $licencia){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM agente WHERE tx_host_name = :tx_host_name AND tx_serial_disco = :tx_serial_disco 
            AND licencia = :licencia');
            $sql->execute(array(':tx_host_name'=>$tx_host_name, ':tx_serial_disco'=>$tx_serial_disco, ':licencia'=>$licencia));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("id"=>0, "tx_host_name"=>"", "tx_serial_disco"=>"", "tx_ip"=>"");
        }
    }
    
    function insertarAgenteWeb($cliente, $tx_host_name, $tx_serial_disco, $tx_ip) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO agente (clienteReseller, tx_host_name, tx_serial_disco, tx_ip) '
            . 'VALUES (:cliente, :tx_host_name, :tx_serial_disco, :tx_ip)');
            $sql->execute(array(':cliente'=>$cliente, ':tx_host_name'=>$tx_host_name, ':tx_serial_disco'=>$tx_serial_disco, 
            ':tx_ip'=>$tx_ip));
            return true;
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarAgenteWebLicencia($cliente, $tx_host_name, $tx_serial_disco, $tx_ip, $licencia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO agente (clienteReseller, licencia, tx_host_name, tx_serial_disco, tx_ip) '
            . 'VALUES (:cliente, :licencia, :tx_host_name, :tx_serial_disco, :tx_ip)');
            $sql->execute(array(':cliente'=>$cliente, ':licencia'=>$licencia, ':tx_host_name'=>$tx_host_name, ':tx_serial_disco'=>$tx_serial_disco, 
            ':tx_ip'=>$tx_ip));
            return true;
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return false;
        }
    }
    
    function ultIdAgenteWeb() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT max(id) AS id FROM agente');
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function existeNombreCliente($reseller, $cliente, $nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT max(id) AS id FROM clientesReseller WHERE nombreEmpresa = :nombre 
            AND reseller = :reseller AND id != :cliente');
            $sql->execute(array(":nombre"=>$nombre, ":reseller"=>$reseller, ":cliente"=>$cliente));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function existeNombreClienteActivo($reseller, $cliente, $nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT max(id) AS id FROM clientesReseller WHERE nombreEmpresa = :nombre 
            AND reseller = :reseller AND id != :cliente AND status = 1');
            $sql->execute(array(":nombre"=>$nombre, ":reseller"=>$reseller, ":cliente"=>$cliente));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function existeEmail($reseller, $cliente, $correo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT max(id) AS id FROM clientesReseller WHERE correo = :correo 
            AND reseller = :reseller AND id != :cliente');
            $sql->execute(array(":correo"=>$correo, ":reseller"=>$reseller, ":cliente"=>$cliente));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function existeLoginCliente($reseller, $cliente, $usuario) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT max(id) AS id FROM clientesReseller WHERE usuario = :usuario 
            AND reseller = :reseller AND id != :cliente');
            $sql->execute(array(":usuario"=>$usuario, ":reseller"=>$reseller, ":cliente"=>$cliente));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
}
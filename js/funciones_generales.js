// JavaScript Document
// Funciones Generales
// Autor: Dayan Betancourt
// Fecha: 19/02/2008

function MM_jumpMenu(targ, selObj, restore) { //v3.0
    eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
    if (restore)
        selObj.selectedIndex = 0;
}

function MM_goToURL() { //v3.0
    var i, args = MM_goToURL.arguments;
    document.MM_returnValue = false;
    for (i = 0; i < (args.length - 1); i += 2)
        eval(args[i] + ".location='" + args[i + 1] + "'");
}

function MM_swapImgRestore() { //v3.0
    var i, x, a = document.MM_sr;
    for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++)
        x.src = x.oSrc;
}

function MM_preloadImages() { //v3.0
    var d = document;
    if (d.images) {
        if (!d.MM_p)
            d.MM_p = new Array();
        var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
        for (i = 0; i < a.length; i++)
            if (a[i].indexOf("#") != 0) {
                d.MM_p[j] = new Image;
                d.MM_p[j++].src = a[i];
            }
    }
}

function MM_findObj(n, d) { //v4.01
    var p, i, x;
    if (!d)
        d = document;
    if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document;
        n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all)
        x = d.all[n];
    for (i = 0; !x && i < d.forms.length; i++)
        x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++)
        x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById)
        x = d.getElementById(n);
    return x;
}

function MM_swapImage() { //v3.0
    var i, j = 0, x, a = MM_swapImage.arguments;
    document.MM_sr = new Array;
    for (i = 0; i < (a.length - 2); i += 3)
        if ((x = MM_findObj(a[i])) != null) {
            document.MM_sr[j++] = x;
            if (!x.oSrc)
                x.oSrc = x.src;
            x.src = a[i + 2];
        }
}

function validarAlfaNumerico(cadena) {
    if (!/^([a-z/A-Z/áéíóúñ/0-9/\s])*$/.test(cadena)) {
        return false;
    } else {
        return true;
    }
}

function validarCaracter(cadena) {
    if (!/^([a-z/A-Z/áéíóúñ/\s])*$/.test(cadena)) {
        return false;
    } else {
        return true;
    }
}

function validarSiNumero(numero) {
    if (!/^([0-9])*$/.test(numero)) {
        return false;
    } else {
        return true;
    }
}
function validarSiNumeroDecimal(numero) {
    if (!/^([0-9/.])*$/.test(numero)) {
        return false;
    } else {
        return true;
    }
}

function validarTelefono(numero) {
    if (!/^([0-9/-])*$/.test(numero)) {
        return false;
    } else {
        return true;
    }
}

function validarEmail(email) {
    apos = eval(email.indexOf('@'));
    dotpos = eval(email.lastIndexOf('.'));
    lastpos = eval(email.length - 1);

    if (apos < 1 || dotpos - apos < 2 || lastpos - dotpos > 3 || lastpos - dotpos < 2) {
        return false;
    } else {
        return true;
    }
}

function PopWindow(url, titulo, caracteristicas) {
    window.open(url, titulo, caracteristicas);
}

function limpiar(etiqueta) {
    etiqueta.value = '';
}

function validar() {
    var doSubmit = true;

    if (isNaN(parseInt(document.form3.codigo.value))) {
        doSubmit = false;
        window.alert("Por favor introduzca un número válido");
    }

    if (doSubmit == true) {
        document.form3.submit();
    }
}
